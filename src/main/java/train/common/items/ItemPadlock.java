package train.common.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import train.common.Traincraft;
import train.common.library.ItemIDs;

import java.util.List;

public class ItemPadlock extends ItemPart {

	public ItemPadlock() {
		super(ItemIDs.padlock.iconName);
		maxStackSize = 1;
		setCreativeTab(Traincraft.tcTab);
	}

	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
		return false;
	}
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add("\u00a77" + "Use it to lock/unlock train cars.");
		par3List.add("\u00a77" + "Use it to trust players to locked train cars.");
		par3List.add("\u00a77" + "Use it to lock & unlock switches.");
	}

	@Override
	public boolean doesSneakBypassUse(World world, int x, int y, int z, EntityPlayer player) {
		return true;
	}
}