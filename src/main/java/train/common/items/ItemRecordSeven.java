package train.common.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemRecord;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import train.common.Traincraft;
import train.common.library.Info;

import java.util.List;

public class ItemRecordSeven extends ItemRecord {
    protected ItemRecordSeven(String p_i45350_1_) {
        super(p_i45350_1_);
        setMaxStackSize(1);
        setCreativeTab(Traincraft.tcTab);
    }

    @Override
    public ResourceLocation getRecordResource(String name)
    {
        return new ResourceLocation(Info.resourceLocation + ":" + "record_seven");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(Info.modID.toLowerCase() + ":record_seven");
    }
}
