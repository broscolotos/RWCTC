package train.common.util;

public class LoggedEntity {
    private final String dateTime;
    private final String stockName;
    private final int locX;
    private final int locY;
    private final int locZ;
    private final String ownerName;
    private final String breakerName;

    /**
     * @author 02skaplan, broscolotos
     * <p>A result LoggedEntity containing information about a logged entity.</p>
     * @param dateTime
     * @param stockName
     * @param locX
     * @param locY
     * @param locZ
     * @param ownerName
     * @param breakerName
     */
    public LoggedEntity(String dateTime, String stockName, int locX, int locY, int locZ, String ownerName, String breakerName) {
        this.dateTime = dateTime;
        this.stockName = stockName;
        this.locX = locX;
        this.locY = locY;
        this.locZ = locZ;
        this.ownerName = ownerName;
        this.breakerName = breakerName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(dateTime);
        sb.append(" ");
        sb.append(breakerName);
        sb.append(" broke a ");
        sb.append(stockName);
        sb.append(" owned by ");
        sb.append(ownerName);
        sb.append( " at location ");
        sb.append(locX);
        sb.append(", ");
        sb.append(locY);
        sb.append(", ");
        sb.append(locZ);
        sb.append(".");
        return sb.toString();
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getStockName() {
        return stockName;
    }

    public int getLocX() {
        return locX;
    }

    public int getLocY() {
        return locY;
    }

    public int getLocZ() {
        return locZ;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getBreakerName() {
        return breakerName;
    }
}
