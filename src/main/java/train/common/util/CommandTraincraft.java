package train.common.util;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.HoverEvent;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import org.apache.commons.cli.*;
import train.common.Traincraft;
import train.common.adminbook.ServerLogger;
import train.common.core.handlers.ConfigHandler;
import train.common.items.ItemRollingStock;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author 02skaplan, broscolotos
 */
public class CommandTraincraft extends CommandBase {
    private int entriesPerPage = 5;
    private static final String[] tabCompleteOptions = new String[]{"lookup", "forcecreator"};
    @Override
    public String getCommandName() {
        return "traincraft";
    }

    @Override
    public List getCommandAliases() {
        List<String> aliases = new ArrayList<>();
        aliases.add("tc");
        aliases.add("tcr");
        return aliases;
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender commandSender, String[] arguments) {
        if (arguments[0].equalsIgnoreCase("lookup") || arguments[0].equalsIgnoreCase("l")) {
            runLookupCommand(commandSender, arguments);
        } else if (arguments[0].equalsIgnoreCase("forcecreator") || arguments[0].equalsIgnoreCase("fc") && arguments.length > 1) {
            runForceCreatorCommand(commandSender, arguments);
        } else {
            commandSender.addChatMessage(new ChatComponentText("[Traincraft] ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GRAY)).appendSibling(new ChatComponentText("Invalid arguments.").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED))));
        }
    }

    private static void runForceCreatorCommand(ICommandSender commandSender, String[] arguments) {
        if (commandSender instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP) commandSender;
            ItemStack heldItemStack = player.inventory.getCurrentItem();
            if (heldItemStack != null && heldItemStack.getItem() instanceof ItemRollingStock) {
                if (heldItemStack.hasTagCompound()) {
                    NBTTagCompound tag = heldItemStack.getTagCompound();
                    tag.setString("trainCreator", arguments[1]);
                    commandSender.addChatMessage(new ChatComponentText("[Traincraft] ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GRAY)).appendSibling(new ChatComponentText("Set creator to " + arguments[1] + ".").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.DARK_GREEN))));
                } else {
                    commandSender.addChatMessage(new ChatComponentText("[Traincraft] ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GRAY)).appendSibling(new ChatComponentText("Error: place down and break the item before setting a new creator!").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED))));
                }
            } else {
                commandSender.addChatMessage(new ChatComponentText("[Traincraft] ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GRAY)).appendSibling(new ChatComponentText("Error: you are not holding a rolling stock item!").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED))));
            }
        } else {
            commandSender.addChatMessage(new ChatComponentText("[Traincraft] Error: this command cannot be run from the command line!"));
        }
    }

    private void runLookupCommand(ICommandSender commandSender, String[] arguments) {
        if (ConfigHandler.ENABLE_ROLLING_STOCK_BREAK_LOGGING) {
            arguments = Arrays.copyOfRange(arguments, 1, arguments.length);
            Options options = getLookupOptions();
            CommandLineParser commandParser = new DefaultParser();
            CommandLine parsedCommand = null;
            try {
                if (arguments[0].equals("p")) {
                    int pageNum = Integer.parseInt(arguments[1]);
                    ArrayList<LoggedEntity> entities = ServerLogger.getCachedResults(commandSender.getCommandSenderName());
                    if (entities != null)
                        sendLookupMessageFormatted(commandSender, entities, (pageNum * entriesPerPage) - entriesPerPage);
                    else
                        commandSender.addChatMessage(new ChatComponentText("[Traincraft] ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GRAY)).appendSibling(new ChatComponentText("Unable to retrieve page. Run a lookup command first.").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED))));
                    return;
                }
                parsedCommand = commandParser.parse(options, arguments);
                if ((parsedCommand.hasOption("breaker") || parsedCommand.hasOption("owner")) && parsedCommand.hasOption("time")) {
                    String userString;
                    if (parsedCommand.hasOption("breaker"))
                        userString = parsedCommand.getOptionValue("breaker");
                    else
                        userString = parsedCommand.getOptionValue("owner");
                    String timeString = parsedCommand.getOptionValue("time");
                    float timeHours = convertTimeStringToHours(timeString);
                    ArrayList<LoggedEntity> entityList = ServerLogger.lookupByTimeAndUser(commandSender.getCommandSenderName(), timeHours, userString, parsedCommand.hasOption("breaker") ? LoggedUserType.BREAKER : LoggedUserType.OWNER);
                    sendLookupMessageFormatted(commandSender, entityList, 0);
                    if (parsedCommand.hasOption("dump")) {
                        if (dumpLookupResultsToCSV(entityList))
                            commandSender.addChatMessage(new ChatComponentText("§2Results saved to CSV file."));
                        else
                            commandSender.addChatMessage(new ChatComponentText("§4Results not saved to CSV file."));
                    }
                } else if (parsedCommand.hasOption("page")) {
                    ArrayList<LoggedEntity> entities = ServerLogger.getCachedResults(commandSender.getCommandSenderName());
                    if (!entities.isEmpty()) {
                        int pageNumber = Integer.parseInt(parsedCommand.getOptionValue("page"));
                        int startingIndex = (pageNumber * entriesPerPage) - entriesPerPage;
                        sendLookupMessageFormatted(commandSender, entities, startingIndex);
                    } else {
                        commandSender.addChatMessage(new ChatComponentText("When does this trigger?"));
                    }

                } else {
                    throw new IllegalArgumentException("Invalid time unit. Use \"H\" for hours or \"D\" for days.");
                }
            } catch (ParseException | IllegalArgumentException exception) {
                commandSender.addChatMessage(new ChatComponentText("[RWCTC] Invalid arguments. " + exception));
                throw new RuntimeException();
            }
        }
    }

    private boolean dumpLookupResultsToCSV(ArrayList<LoggedEntity> entityList) {
        if (!entityList.isEmpty()) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH_mm_ss");
            dateFormat.setTimeZone(Calendar.getInstance().getTimeZone());
            try {
                File file = new File(Traincraft.configDirectory + "/traincraft/exportedlogs-" + dateFormat.format(new Date()) + ".csv");
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write("EventDateTime,StockName,LocX,LocY,LocZ,OwnerName,BreakerName");
                for (LoggedEntity entity : entityList) {
                    fileWriter.write("\n" + entity.getDateTime() + "," + entity.getStockName() + "," + entity.getLocX() + "," + entity.getLocY() + "," + entity.getLocZ() + "," + entity.getOwnerName() + "," + entity.getBreakerName());
                }
                fileWriter.close();
                return true;
            } catch (IOException exception) {
                Traincraft.tcLog.warn("Error exporting log query to CSV file.");
            }
        }
        return false;
    }

    private Options getLookupOptions() {
        Options options = new Options();
        Option breaker = new Option("b", "breaker", true, "Query by person who broke the entity.");
        breaker.setRequired(false);
        options.addOption(breaker);
        Option owner = new Option("o", "owner", true, "Query by person who owned the broken entity.");
        owner.setRequired(false);
        options.addOption(owner);
        Option time = new Option("t", "time", true, "Time range to query.");
        time.setRequired(false);
        options.addOption(time);
        Option page = new Option("p", "page", true, "Page of query results to show.");
        page.setRequired(false);
        options.addOption(page);
        Option dumpToCSV = new Option("d", "dump", false, "Dump query results to a CSV file.");
        dumpToCSV.setRequired(false);
        options.addOption(dumpToCSV);
        return options;
    }

    private void sendLookupMessageFormatted(ICommandSender player, ArrayList<LoggedEntity> entities, int startingIndex) {
        String page;
        if (startingIndex >= entities.size()) {
            startingIndex = (int) Math.floor(entities.size()/entriesPerPage);
        }
        StringBuilder sb = new StringBuilder();
        if(!entities.isEmpty()) {
            sb.append("                                 §3Page §6");
            sb.append((int) Math.ceil(((float) (startingIndex + 1) / entriesPerPage)));
            sb.append("§3 of §6");
            sb.append((int) Math.ceil(entities.size() * .2));
            page = sb.toString();
            sb = new StringBuilder();
        } else {
            page = "                                 §3Page §60§3 of §60";
        }
        sb.append("                      §3Rollingstock Breaking Records");
        String tag = sb.toString();
        player.addChatMessage(new ChatComponentText(""));
        player.addChatMessage(new ChatComponentText(" §3===================================================="));
        player.addChatMessage(new ChatComponentText(tag));
        player.addChatMessage(new ChatComponentText(" §3===================================================="));
        for (int i=startingIndex; i< Math.min(startingIndex+entriesPerPage,entities.size()); i++) {
            sb = new StringBuilder();
            LoggedEntity currentEntryEntity = entities.get(i);
            String dateString = currentEntryEntity.getDateTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            double diff;
            try {
                java.util.Date date = sdf.parse(dateString);
                long datems = date.getTime(); //get the date of the stored data
                java.util.Date nowDate = new Date();
                diff = ((double)(nowDate.getTime()) - (double)(datems)) / 3600000;//gets ms to hours
                diff *=10; //cut to first decimal
                diff = Math.round(diff);
                diff /= 10; //return to hours
            } catch (java.text.ParseException e) {
                throw new RuntimeException(e);
            }
            sb.append("§3 - §7");
            sb.append(diff);
            sb.append("§7h§3 : §6");
            sb.append(currentEntryEntity.getBreakerName());
            sb.append(" §7broke §6");
            String name = currentEntryEntity.getStockName();
            sb.append(name.substring(name.indexOf("minecart")+8));
            sb.append(" §7owned §7by §6"); //literally no reason why it should break, but sometimes it does? include the color for every word at this point (╯°□°)╯︵ ┻━┻
            sb.append(currentEntryEntity.getOwnerName());
            sb.append(" §7at §7the §7location ");
            sb.append(currentEntryEntity.getLocX());
            sb.append("§7, §6");
            sb.append(currentEntryEntity.getLocY());
            sb.append("§7, §6");
            sb.append(currentEntryEntity.getLocX());
            sb.append("§7.");
            player.addChatMessage(new ChatComponentText(sb.toString()).setChatStyle(player.func_145748_c_().getChatStyle()
                    .setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ChatComponentText(dateString + " | Click to Teleport")))
                    .setChatClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
                            ("/tp " + currentEntryEntity.getLocX() + " " + currentEntryEntity.getLocY() + " " + currentEntryEntity.getLocZ())))));
        }
        player.addChatMessage(new ChatComponentText(" §3===================================================="));
        player.addChatMessage(new ChatComponentText(page));
        player.addChatMessage(new ChatComponentText(" §3===================================================="));
        player.addChatMessage(new ChatComponentText(""));
    }
    private float convertTimeStringToHours(String timeString) throws ParseException {
        String timeUnit = timeString.substring(timeString.length() - 1);
        float time = Float.parseFloat(timeString.substring(0, timeString.length() - 1));
        if (timeUnit.equalsIgnoreCase("h"))
            return time;
        else if (timeUnit.equalsIgnoreCase("d"))
            return time * 24;
        else if (timeUnit.equals("m"))
            return time / 60;
        else if (timeUnit.equals("M"))
            return time * 720;
        else if (timeUnit.equalsIgnoreCase("y"))
            return time * 8760;
        //TODO: months, years
        else
            throw new ParseException("Invalid time unit. Use \"H/h\" for hours, \"D/d\" for days, \"m\" for minutes, \"M\" for months, or \"Y/y\" for years.");
    }
    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    /**
     * Adds the strings available in this command to the given list of tab completion options.
     */
    @Override
    public List addTabCompletionOptions(ICommandSender commandSender, String[] arguments) {
        List<String> tabCompleteOptions = new ArrayList<>();
        if (arguments.length == 1)
            for (String option : CommandTraincraft.tabCompleteOptions)
                if (option.startsWith(arguments[0]))
                    tabCompleteOptions.add(option);
        return tabCompleteOptions;
    }

}
