package train.common.core.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import train.common.api.Locomotive;
import train.common.core.util.DefectDetectorSounds;
import train.common.items.ItemTwoWayRadio;
import train.common.tile.TileDefectDetector;

import java.util.LinkedList;
import java.util.Queue;

public class PacketDefectDetectorMessage implements IMessage {

    public PacketDefectDetectorMessage() {} // Do not remove default constructor even if unused! Forge will be very mad!

    private Queue<DefectDetectorSounds> sounds;
    private int defectDetectorTileX;
    private int defectDetectorTileY;
    private int defectDetectorTileZ;

    public PacketDefectDetectorMessage(TileDefectDetector defectDetector, Queue<DefectDetectorSounds> sounds) {
        this.sounds = sounds;
        this.defectDetectorTileY = defectDetector.yCoord;
        this.defectDetectorTileZ = defectDetector.zCoord;
        this.defectDetectorTileX = defectDetector.xCoord;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        defectDetectorTileX = buf.readInt();
        defectDetectorTileY = buf.readInt();
        defectDetectorTileZ = buf.readInt();
        int size = buf.readInt();
        sounds = new LinkedList<>();
        for (int i = 0; i < size; i++)
            sounds.add(DefectDetectorSounds.values()[buf.readInt()]);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(defectDetectorTileX);
        buf.writeInt(defectDetectorTileY);
        buf.writeInt(defectDetectorTileZ);
        buf.writeInt(sounds.size());
        for (DefectDetectorSounds sound : sounds) {
            buf.writeInt(sound.ordinal());
        }
    }

    public static class Handler implements IMessageHandler<PacketDefectDetectorMessage, IMessage> {
        @Override
        public IMessage onMessage(PacketDefectDetectorMessage message, MessageContext context) {
            // Play the message only if the player is in a locomotive or has a two-way radio in their inventory.
            boolean playMessage = Minecraft.getMinecraft().thePlayer.ridingEntity != null && Minecraft.getMinecraft().thePlayer.ridingEntity instanceof Locomotive;
            if (!playMessage) {
                for (ItemStack itemStack : Minecraft.getMinecraft().thePlayer.inventory.mainInventory) {
                    if (itemStack != null && itemStack.getItem() instanceof ItemTwoWayRadio) {
                        playMessage = true;
                        break;
                    }
                }
            }
            if (playMessage) {
                TileDefectDetector defectDetector = (TileDefectDetector) Minecraft.getMinecraft().theWorld.getTileEntity(message.defectDetectorTileX, message.defectDetectorTileY, message.defectDetectorTileZ);
                Queue<DefectDetectorSounds> sounds = message.sounds;
                defectDetector.loadMessageFromPacket(sounds);
            }
            return null;
        }
    }
}
