package train.common.core.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import train.common.api.ArticulatedSteam;
import train.common.api.EntityRollingStock;
import train.common.api.Locomotive;

public class PacketIsComplete implements IMessage {

    boolean	complete;
    int		entityID;

    public PacketIsComplete() {}

    public PacketIsComplete(boolean complete, int trainEntity) {
        this.complete = complete;
        this.entityID = trainEntity;
    }

    @Override
    public void fromBytes(ByteBuf bbuf) {
        this.complete = bbuf.readBoolean();
        this.entityID = bbuf.readInt();
    }

    @Override
    public void toBytes(ByteBuf bbuf) {
        bbuf.writeBoolean(this.complete);
        bbuf.writeInt(this.entityID);
    }

    public static class Handler implements IMessageHandler<PacketIsComplete, IMessage> {

        @Override
        public IMessage onMessage(PacketIsComplete message, MessageContext context) {

            Entity TrainEntity = Minecraft.getMinecraft().theWorld.getEntityByID(message.entityID);

            if (TrainEntity instanceof ArticulatedSteam) {

                ((ArticulatedSteam) TrainEntity).setComplete(message.complete);

            }
            return null;
        }
    }
}
