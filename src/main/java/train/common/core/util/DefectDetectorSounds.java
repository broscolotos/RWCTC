package train.common.core.util;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public enum DefectDetectorSounds {
    ZERO("defectdetector_0", 640),
    ONE("defectdetector_1", 436),
    TWO("defectdetector_2", 397),
    THREE("defectdetector_3", 446),
    FOUR("defectdetector_4", 521),
    FIVE("defectdetector_5", 580),
    SIX("defectdetector_6", 640),
    SEVEN("defectdetector_7", 682),
    EIGHT("defectdetector_8", 369),
    NINE("defectdetector_9", 714),
    POINT("defectdetector_point", 501),
    BLANK("defectdetector_blank", 697),
    RAILROAD("defectdetector_railroad", 997),
    EQUIPMENT("defectdetector_equpment", 784),
    DEFECT("defectdetector_defect", 683),
    DETECTOR("defectdetector_detector", 674),
    NO_DEFECTS("defectdetector_no_defects", 1431),
    REPEAT("defectdetector_repeat", 743),
    MILEPOST("defectdetector_milepost", 914),
    NORTH("defectdetector_north", 669),
    EAST("defectdetector_east", 513),
    SOUTH("defectdetector_south", 615),
    WEST("defectdetector_west", 526),
    BOUND("defectdetector_bound", 700),
    TOTAL("defectdetector_total", 508),
    CARS("defectdetector_cars", 733),
    SPEED("defectdetector_speed", 878),
    TEMPERATURE("defectdetector_temperature", 746),
    DEGREES("defectdetector_degrees", 777),
    END_OF_TRANSMISSION("defectdetector_end_of_transmission", 1656),
    OUT("defectdetector_out", 393);

    public final String soundInternalName;
    public final int soundLengthTicks;
    DefectDetectorSounds(String internalName, int soundLengthMilliseconds) {
        this.soundInternalName = internalName;
        this.soundLengthTicks = (int) (soundLengthMilliseconds * 0.02f); // Convert milliseconds to ticks.
    }

    /**
     * @author 02skaplan
     * @param number Number to be converted into DefectDetectorSounds.
     * @return Queue of DefectDetectorSounds representing the provided number.
     */
    public static Queue<DefectDetectorSounds> getSoundsFromNumber(int number) {
        int size = String.valueOf(number).length();
        Stack<DefectDetectorSounds> soundStack = new Stack<>();
        int activeNumber;
        for (int i = 0; i < size; i++) {
            activeNumber = number % 10;
            switch (activeNumber) {
                case 0:
                    soundStack.add(ZERO);
                    break;
                case 1:
                    soundStack.add(ONE);
                    break;
                case 2:
                    soundStack.add(TWO);
                    break;
                case 3:
                    soundStack.add(THREE);
                    break;
                case 4:
                    soundStack.add(FOUR);
                    break;
                case 5:
                    soundStack.add(FIVE);
                    break;
                case 6:
                    soundStack.add(SIX);
                    break;
                case 7:
                    soundStack.add(SEVEN);
                    break;
                case 8:
                    soundStack.add(EIGHT);
                    break;
                case 9:
                    soundStack.add(NINE);
                    break;
            }
            number /= 10;
        }
        Queue<DefectDetectorSounds> sounds = new LinkedList<>();
        while (!soundStack.empty())
            sounds.add(soundStack.pop());
        return sounds;
    }

}
