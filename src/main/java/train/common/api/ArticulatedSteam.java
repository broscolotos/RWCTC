package train.common.api;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;

public class ArticulatedSteam extends SteamTrain {

    public boolean isComplete = false;

    public Class<? extends EntityRollingStock> coupleOption1;
    public Class<? extends EntityRollingStock> coupleOption2;

    public ArticulatedSteam(World world, int capacity) {
        super(world, capacity);
    }

    public ArticulatedSteam(World world, int capacity, FluidStack filter) {
        super(world, capacity, filter);
    }
    @Override
    public int getSizeInventory() {
        return 0;
    }

    @Override
    public String getInventoryName() {
        return null;
    }

    @Override
    public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_) {
        return false;
    }

    public boolean getComplete() {
        return this.isComplete;
    }

    public void setComplete(boolean loco) {
        this.isComplete = loco;
    }

    @Override
    public void setParkingBrakeFromPacket(boolean t) {
        if (this.isComplete) {
            super.setParkingBrakeFromPacket(t);
        }
    }

    /**
     * <p>This method is called on the client side when an entity is being loaded in. The additionalData buffer is sent from the server
     * and is populated by the server using the writeSpawnData method.</p>
     * <br></br><p>"this is basically NBT for entity spawn, to keep data between client and server in sync because some data is not automatically shared."</p>
     * @param additionalData The packet data stream
     */
    @Override
    public void readSpawnData(ByteBuf additionalData) {
        super.readSpawnData(additionalData);
        this.setComplete(additionalData.readBoolean());
    }

    /**
     * <p>This method is called on the server side when a connected client is loading the entity. Data written
     * to the ByteBuffer will be synced with the client and available to the client through the readSpawnData method.</p>
     * <br></br><p>"this is basically NBT for entity spawn, to keep data between client and server in sync because some data is not automatically shared."</p>
     * @param buffer The packet data stream
     */
    @Override
    public void writeSpawnData(ByteBuf buffer) {
        super.writeSpawnData(buffer);
        this.updateArticulatedStatus();
        buffer.writeBoolean(isComplete);
    }
}
