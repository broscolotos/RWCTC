package train.common.tile;

public class TileFlashers extends TileAbstractCrossing {
    public TileFlashers() {
        super();
        setTextures("Crossings/StandardOff", "Crossings/StandardLeft", "Crossings/StandardRight");
    }
}