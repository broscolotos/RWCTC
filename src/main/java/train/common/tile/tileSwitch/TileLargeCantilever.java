package train.common.tile.tileSwitch;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;
import train.common.tile.TileAbstractCrossing;

public class TileLargeCantilever extends TileAbstractCrossing {
    public TileLargeCantilever() {
        super();
        setTextures("Crossings/LargeCantileverOff", "Crossings/LargeCantileverLeft", "Crossings/LargeCantileverRight");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return AxisAlignedBB.getBoundingBox(xCoord-5, yCoord-1, zCoord-5, xCoord + 7, yCoord + 4, zCoord + 7);
    }

}