package train.common.tile.tileSwitch;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;
import train.common.tile.TileAbstractCrossing;

public class TileMediumCantileverRight extends TileAbstractCrossing {
    public TileMediumCantileverRight() {
        super();
        setTextures("Crossings/MediumCantileverOff", "Crossings/MediumCantileverLeft", "Crossings/MediumCantileverRight");
    }
    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox(xCoord-5, yCoord-1, zCoord-5, xCoord + 5, yCoord + 4, zCoord + 5);    }

}