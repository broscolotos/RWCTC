package train.common.tile.tileSwitch;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;
import train.common.tile.TileAbstractCrossing;

public class TileStandardCantilever3 extends TileAbstractCrossing {
    public TileStandardCantilever3() {
        super();
        setTextures("Crossings/StandardOff", "Crossings/StandardLeft", "Crossings/StandardRight");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox(xCoord-1, yCoord-1, zCoord-1, xCoord + 2, yCoord + 4, zCoord + 3);
    }

}