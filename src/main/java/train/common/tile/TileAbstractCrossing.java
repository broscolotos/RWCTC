package train.common.tile;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.ForgeDirection;
import tmt.Tessellator;
import train.common.library.BlockIDs;
import train.common.library.Info;

import java.util.Random;

public abstract class TileAbstractCrossing extends TileTraincraft {

    protected int updateTicks = 0;
    protected static final Random rand = new Random();
    private ForgeDirection facing;
    public float rotation = 0;
    public boolean flip = true;
    public boolean powered = false;
    protected boolean usesCustomTextures = false;
    protected ResourceLocation offTexture = null;
    protected  ResourceLocation onTexture1 = null;
    protected ResourceLocation onTexture2 = null;
    public void setTextures(String offTextureFilePath, String onTexture1FilePath, String onTexture2FilePath) {
        if (offTextureFilePath != null && onTexture1FilePath != null && onTexture2FilePath != null) {
            usesCustomTextures = true;
            this.offTexture = new ResourceLocation(Info.resourceLocation,Info.modelTexPrefix + offTextureFilePath + ".png");
            this.onTexture1 = new ResourceLocation(Info.resourceLocation,Info.modelTexPrefix + onTexture1FilePath + ".png");
            this.onTexture2 = new ResourceLocation(Info.resourceLocation,Info.modelTexPrefix + onTexture2FilePath + ".png");
        } else {
            usesCustomTextures = false;
            this.offTexture = null;
            this.onTexture1 = null;
            this.onTexture2 = null;
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTag, boolean forSyncing) {
        facing = ForgeDirection.getOrientation(nbtTag.getByte("Orientation"));
    }

    @Override
    public void updateEntity() {
        super.updateEntity();
        updateTicks++;
        if(worldObj.isRemote) {
            powered = getWorldObj().isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
            if (powered) {
                if (this.updateTicks % 10 == 0) {
                    flip = !flip;
                    worldObj.playSound(xCoord, yCoord, zCoord, Info.resourceLocation + ":" + "bell", 1f, 1f, true);
                    if (usesCustomTextures) {
                        if (flip) {
                            Tessellator.bindTexture(onTexture1);
                        } else {
                            Tessellator.bindTexture(onTexture2);
                        }
                    }
                }
            } else {
                if (usesCustomTextures)
                    Tessellator.bindTexture(offTexture);
            }
        } else { // Remove block on top of crossing.
            if (updateTicks % 10 == 0) {
                if (!this.worldObj.isAirBlock(this.xCoord, this.yCoord + 1, this.zCoord)) {
                    Block block = this.worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord);
                    if (block != null) {
                        EntityItem entityitem = new EntityItem(worldObj, this.xCoord, this.yCoord + 1, this.zCoord, new ItemStack(Item.getItemFromBlock(BlockIDs.MFPBWigWag.block), 1));
                        float f3 = 0.05F;
                        entityitem.motionX = (float) rand.nextGaussian() * f3;
                        entityitem.motionY = (float) rand.nextGaussian() * f3 + 0.2F;
                        entityitem.motionZ = (float) rand.nextGaussian() * f3;
                        worldObj.spawnEntityInWorld(entityitem);
                    }
                    this.worldObj.setBlockToAir(this.xCoord, this.yCoord, this.zCoord);
                }
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbtTag, boolean forSyncing) {
        //super.writeToNBT(nbtTag, forSyncing);
        if (facing != null) {

            nbtTag.setByte("Orientation", (byte) facing.ordinal());
        }
        else {

            nbtTag.setByte("Orientation", (byte) ForgeDirection.NORTH.ordinal());
        }
        return nbtTag;
    }

    public ForgeDirection getFacing() {
        if(facing != null){
            return this.facing;
        }
        return ForgeDirection.UNKNOWN;
    }


    public void setFacing(ForgeDirection face) {

        if (facing != face)
            this.facing = face;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox(xCoord-1, yCoord-1, zCoord-1, xCoord + 2, yCoord + 2, zCoord + 2);
    }

}