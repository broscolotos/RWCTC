package train.common.tile;

public class TileCrossingBase extends TileAbstractCrossing {
    public TileCrossingBase() {
        super();
        setTextures("Crossings/StandardCrossingBase/CrossingNoLightsOff", "Crossings/StandardCrossingBase/CrossingNoLightsOn1", "Crossings/StandardCrossingBase/CrossingNoLightsOn2");
    }
}