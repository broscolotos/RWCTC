package train.common.tile;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;

public class TileFlashersWithGate2 extends TileAbstractCrossing {
    public TileFlashersWithGate2() {
        super();
        setTextures("Crossings/StandardOff", "Crossings/StandardRight", "Crossings/StandardLeft");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox(xCoord-1, yCoord-1, zCoord-1, xCoord + 2, yCoord + 4, zCoord + 2);
    }

}