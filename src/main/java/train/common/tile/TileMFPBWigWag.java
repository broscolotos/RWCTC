package train.common.tile;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import tmt.Tessellator;
import train.common.library.BlockIDs;
import train.common.library.Info;

public class TileMFPBWigWag extends TileAbstractCrossing {
    @Override
    public void updateEntity() {
        super.updateEntity();
        updateTicks++;
        if(worldObj.isRemote) {
            if (rotation > 20 || rotation < -20) {
                flip = !flip;
                worldObj.playSound(xCoord,yCoord,zCoord, Info.resourceLocation + ":" + "bell",1f,1f,true);
            }
            powered = getWorldObj().isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
            if (powered) {
                rotation += flip ? 1.75f : -1.75f;
                if (usesCustomTextures) {
                    if (flip) {
                        Tessellator.bindTexture(onTexture1);
                    } else {
                        Tessellator.bindTexture(onTexture2);
                    }
                }
            } else {
                rotation = 0;
                if (usesCustomTextures)
                    Tessellator.bindTexture(offTexture);
            }
        } else { // Remove block on top of crossing.
            if (updateTicks % 20 == 0) {
                if (!this.worldObj.isAirBlock(this.xCoord, this.yCoord + 1, this.zCoord)) {
                    Block block = this.worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord);
                    if (block != null) {
                        EntityItem entityitem = new EntityItem(worldObj, this.xCoord, this.yCoord + 1, this.zCoord, new ItemStack(Item.getItemFromBlock(BlockIDs.MFPBWigWag.block), 1));
                        float f3 = 0.05F;
                        entityitem.motionX = (float) rand.nextGaussian() * f3;
                        entityitem.motionY = (float) rand.nextGaussian() * f3 + 0.2F;
                        entityitem.motionZ = (float) rand.nextGaussian() * f3;
                        worldObj.spawnEntityInWorld(entityitem);
                    }
                    this.worldObj.setBlockToAir(this.xCoord, this.yCoord, this.zCoord);
                }
            }
        }
    }
}