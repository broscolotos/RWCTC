package train.common.tile;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import train.common.Traincraft;
import train.common.api.EntityRollingStock;
import train.common.api.Locomotive;
import train.common.core.network.PacketDefectDetectorMessage;
import train.common.core.util.DefectDetectorSounds;
import train.common.core.util.MP3Player;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author 02skaplan
 * <h1>Defect Detector Tile Class</h1>
 * <p>Contains all code to run defect detector functions on both server and client side.</p><br></br>
 * <h2>Server Side:</h2>
 * <p>The server side handles processing each train as it passes, loading the entire consist after the first car passes.
 * Afterward, a tick counter in onUpdate is used to determine when the last car is past. Then, a PacketDefectDetectorMessage is sent to the client side
 * containing information needed to play the correct sounds and the tile resets for the next train.</p><br></br>
 * <h2>Client Side:</h2>
 * <p>The client side is mostly uninformed and inactive until the server sends it a PacketDefectDetectorMessage containing
 * sounds to play in order. After it recieves the packet, the tile starts playing and uses a tick counter in onUpdate to determine
 * when to play the next sound in the Queue. After all sounds are played, the tile resets for the next train.</p>
 */
public class TileDefectDetector extends TileEntity {

	private final int DEFAULT_TICK_TIMEOUT = 80;
	private final double BROADCAST_RANGE = 256D;
	private final HashSet<EntityRollingStock> watchedEntities = new HashSet<>();
	private Locomotive activeLocomotive = null;
	private Integer motionZClient;
	private Integer motionXClient;
	private int numCars = 0;
	private int delay = 0;
	private final Queue<DefectDetectorSounds> soundQueue = new LinkedList<>();
	private int soundTimeRemaining = 0;
	private boolean playing = false;
	private int facing;
	public MP3Player soundPlayer;

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		facing = nbt.getInteger("Orientation");
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		nbt.setInteger("Orientation", facing);
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, nbt);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
		readFromNBT(pkt.func_148857_g());
	}

	/**
	 * @author 02skaplan
	 * <p>Called every tick on both client and server side.</p>
	 * <p>On server side: determines when a train is over using a delay that resets every time a car runs over but will eventually expire and call endOfTrain().</p>
	 * <p>On client side: dandles playing of messages sent from server using a tick clock to keep track of when to play the next sound.</p>
	 */
	@Override
	public void updateEntity() {
		if (!worldObj.isRemote) { // Server Side
			if (delay > 1)
				delay--;
			else if (delay == 1) {
				delay--;
				endOfTrain();
			}
		} else { // Client Side
			if (playing) {
				if (soundTimeRemaining == 0) {
					playNextSound();
				} else {
					soundTimeRemaining--;
				}
			}
		}
	}

	/**
	 * <p>Called by server side every time an EntityRollingStock passes over a defect detector block/tile.</p>
	 * @param rollingStock The rolling stock entity passing over the defect detector.
	 */
	public void handleTrain(EntityRollingStock rollingStock) {
		if (!worldObj.isRemote) {
			delay = DEFAULT_TICK_TIMEOUT;
			if (!watchedEntities.contains(rollingStock))
				numCars = loadEntireConsist(rollingStock);
		}
	}

	/**
	 * @author 02skaplan
	 * <p>Runs on server side.</p>
	 * <p>Is called after the last car passes or the delay expires.</p>
	 * <p>Collects information about the consist that passed and sends the appropriate sounds to nearby clients through PacketDefectDetectorMessage.</p>
	 * <p>When done, clears information and resets instance variables for next train.</p>
	 */
	private void endOfTrain() {
		// Build defect detector message...
		soundQueue.add(DefectDetectorSounds.RAILROAD);
		soundQueue.add(DefectDetectorSounds.EQUIPMENT);
		soundQueue.add(DefectDetectorSounds.DEFECT);
		soundQueue.add(DefectDetectorSounds.DETECTOR);
		soundQueue.add(DefectDetectorSounds.BLANK);
		if (motionZClient > 0) {
			soundQueue.add(DefectDetectorSounds.SOUTH);
		} else if (motionZClient < 0) {
			soundQueue.add(DefectDetectorSounds.NORTH);
		}
		if (motionXClient > 0) {
			soundQueue.add(DefectDetectorSounds.EAST);
		} else if (motionXClient < 0) {
			soundQueue.add(DefectDetectorSounds.WEST);
		}
		soundQueue.add(DefectDetectorSounds.BOUND);
		soundQueue.add(DefectDetectorSounds.BLANK);
		soundQueue.add(DefectDetectorSounds.NO_DEFECTS);
		soundQueue.add(DefectDetectorSounds.REPEAT);
		soundQueue.add(DefectDetectorSounds.NO_DEFECTS);
		soundQueue.add(DefectDetectorSounds.BLANK);
		soundQueue.add(DefectDetectorSounds.TOTAL);
		soundQueue.add(DefectDetectorSounds.CARS);
		soundQueue.addAll(DefectDetectorSounds.getSoundsFromNumber(numCars));
		if (activeLocomotive != null) {
			soundQueue.add(DefectDetectorSounds.SPEED);
			soundQueue.addAll(DefectDetectorSounds.getSoundsFromNumber((int) activeLocomotive.getSpeed()));
		}
		soundQueue.add(DefectDetectorSounds.TEMPERATURE);
		soundQueue.addAll(DefectDetectorSounds.getSoundsFromNumber(ThreadLocalRandom.current().nextInt(70, 101)));
		soundQueue.add(DefectDetectorSounds.END_OF_TRANSMISSION);
		Traincraft.defectDetectorChannel.sendToAllAround(new PacketDefectDetectorMessage(this, soundQueue), new NetworkRegistry.TargetPoint(worldObj.provider.dimensionId, this.xCoord, this.yCoord, this.zCoord, BROADCAST_RANGE));
		activeLocomotive = null;
		motionXClient = null;
		motionZClient = null;
		numCars = 0;
		watchedEntities.clear();
		soundQueue.clear();
	}

	/**
	 * @author 02skaplan
	 * <p>Called on client side after receiving a PacketDefectDetectorSounds message. Loads the message in and initiates playing.</p>
	 * @param soundQueue Queue containing the sounds to be played in order.
	 */
	@SideOnly(Side.CLIENT)
	public void loadMessageFromPacket(Queue<DefectDetectorSounds> soundQueue) {
		this.soundQueue.clear();
		this.soundQueue.addAll(soundQueue);
		playing = true;
	}

	/**
	 * @author 02skaplan
	 * <p>Called on client side to continue playing the message located in the soundQueue after soundTimeRemaining hits zero.</p>
	 */
	@SuppressWarnings("static-access")
	@SideOnly(Side.CLIENT)
	private void playNextSound() {
		if (soundQueue.isEmpty()) {
			playing = false;
		} else {
			DefectDetectorSounds sound = soundQueue.poll();
			Traincraft.proxy.playerList.remove(this.soundPlayer);
			try {
				this.soundPlayer = new MP3Player(getClass().getClassLoader().getResource("assets/tc/sounds/defectdetector/" + sound.soundInternalName + ".mp3"), this.worldObj, 0);
			} catch (Exception e) { Traincraft.tcLog.warn("Issue playing defect detector sounds."); }
			soundPlayer.setVolume(0);
			Traincraft.proxy.playerList.add(this.soundPlayer);
			soundTimeRemaining = sound.soundLengthTicks;
		}
	}

	/**
	 * @author 02skaplan
	 * <p>Loads consist into memory.</p>
	 * @param rollingStock Any one rolling stock entity in the consist.
	 * @return Number of cars in the consist.
	 */
	private int loadEntireConsist(EntityRollingStock rollingStock) {
		HashSet<EntityRollingStock> completedList = new HashSet<>();
		EntityRollingStock loopRollingStock = rollingStock;
		boolean flipDirection = false;
		// Start with the main piece of rolling stock, then go in one direction until you reach the end of the consist.
		// When the end of the consist is reached, go back to the main piece and go in the other direction.
		while (true) {
			if (completedList.isEmpty()) { // Log the direction of the first car in the consist.
				motionXClient = loopRollingStock.getMotionXClient();
				motionZClient = loopRollingStock.getMotionZClient();
			}
			completedList.add(loopRollingStock);
			watchedEntities.add(loopRollingStock);
			if (loopRollingStock instanceof Locomotive && activeLocomotive == null)
				activeLocomotive = ((Locomotive) loopRollingStock);
			if (loopRollingStock.cartLinked1 != null && !completedList.contains(loopRollingStock.cartLinked1))
				loopRollingStock = loopRollingStock.cartLinked1;
			else if (loopRollingStock.cartLinked2 != null && !completedList.contains(loopRollingStock.cartLinked2))
				loopRollingStock = loopRollingStock.cartLinked2;
			else {
				if (!flipDirection) {
					flipDirection = true;
					loopRollingStock = rollingStock;
				} else
					break;
			}
		}
		return completedList.size();
	}

	/**
	 * <p>Gets the rotational direction of the model</p>
	 * @return Rotational direction of the model.
	 */
	public int getFacing() {
		return facing;
	}

	/**
	 * <p>Sets the rotational direction of the model.</p>
	 * @param facing New rotational direction of the model
	 */
	public void setFacing(int facing) {
		this.facing = facing;
	}

}