package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TileStandardCantilever1;

public class BlockStandardCantilever1 extends BlockAbstractCrossing {
    public BlockStandardCantilever1() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileStandardCantilever1();
    }
}
