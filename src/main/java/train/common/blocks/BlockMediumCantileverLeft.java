package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TileMediumCantileverLeft;

public class BlockMediumCantileverLeft extends BlockAbstractCrossing {
    public BlockMediumCantileverLeft() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileMediumCantileverLeft();
    }
}
