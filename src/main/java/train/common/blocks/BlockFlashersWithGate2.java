package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.TileFlashersWithGate2;

public class BlockFlashersWithGate2 extends BlockAbstractCrossing {
	public BlockFlashersWithGate2() {
		super();
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileFlashersWithGate2();
	}
}
