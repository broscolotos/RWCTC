package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TilePedestrianCrossing;

public class BlockPedestrianCrossing extends BlockAbstractCrossing {
    public BlockPedestrianCrossing() {
        super();
    }
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TilePedestrianCrossing();
    }
}
