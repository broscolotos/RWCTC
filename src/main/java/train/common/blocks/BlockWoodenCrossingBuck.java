package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TileWoodenCrossingBuck;

public class BlockWoodenCrossingBuck extends BlockAbstractCrossing {
    public BlockWoodenCrossingBuck() { super(); }
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileWoodenCrossingBuck();
    }
}
