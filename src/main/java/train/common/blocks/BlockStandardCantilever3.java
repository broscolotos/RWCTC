package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TileStandardCantilever3;

public class BlockStandardCantilever3 extends BlockAbstractCrossing {
    public BlockStandardCantilever3() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileStandardCantilever3();
    }
}
