package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.TileWigWag;

public class BlockWigWag extends BlockAbstractCrossing {
	public BlockWigWag() {
		super();
	}
	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileWigWag();
	}
}
