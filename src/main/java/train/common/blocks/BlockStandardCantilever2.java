package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TileStandardCantilever2;

public class BlockStandardCantilever2 extends BlockAbstractCrossing {
    public BlockStandardCantilever2() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileStandardCantilever2();
    }
}
