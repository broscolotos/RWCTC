package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.TileCrossingBase;

public class BlockCrossingBase extends BlockAbstractCrossing {
	public BlockCrossingBase() {
		super();
	}
	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileCrossingBase();
	}
}
