package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TileMediumCantileverRight;

public class BlockMediumCantileverRight extends BlockAbstractCrossing {
    public BlockMediumCantileverRight() {
        super();
    }
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileMediumCantileverRight();
    }
}
