package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.tileSwitch.TileLargeCantilever;

public class BlockLargeCantilever extends BlockAbstractCrossing {
    public BlockLargeCantilever() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileLargeCantilever();
    }
}
