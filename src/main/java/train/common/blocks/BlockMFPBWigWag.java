package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.TileMFPBWigWag;

public class BlockMFPBWigWag extends BlockAbstractCrossing {
	public BlockMFPBWigWag() {
		super();
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileMFPBWigWag();
	}
}
