package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.TileFlashers;

public class BlockFlashers extends BlockAbstractCrossing {
    public BlockFlashers() {
        super();
    }
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileFlashers();
    }
}
