package train.common.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.tile.TileFlashersWithGate;

public class BlockFlashersWithGate extends BlockAbstractCrossing {
	public BlockFlashersWithGate() {
		super();
	}
	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileFlashersWithGate();
	}
}
