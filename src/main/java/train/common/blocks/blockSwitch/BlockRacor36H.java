package train.common.blocks.blockSwitch;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.blocks.BlockSwitchStand;
import train.common.tile.tileSwitch.TileRacor36H;

public class BlockRacor36H extends BlockSwitchStand {
    public BlockRacor36H() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileRacor36H();
    }
}
