package train.common.blocks.blockSwitch;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.blocks.BlockSwitchStand;
import train.common.tile.tileSwitch.TileRacor36D;

public class BlockRacor36D extends BlockSwitchStand {
    public BlockRacor36D() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileRacor36D();
    }
}
