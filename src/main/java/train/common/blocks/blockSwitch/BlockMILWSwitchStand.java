package train.common.blocks.blockSwitch;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.blocks.BlockSwitchStand;
import train.common.tile.tileSwitch.TileMILWSwitchStand;

public class BlockMILWSwitchStand extends BlockSwitchStand {
	public BlockMILWSwitchStand() {
		super();
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileMILWSwitchStand();
	}
}
