package train.common.blocks.blockSwitch;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import train.common.blocks.BlockSwitchStand;
import train.common.tile.tileSwitch.TileautoSwitchStand;

public class BlockautoSwitchStand extends BlockSwitchStand {
    public BlockautoSwitchStand() {
        super();
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileautoSwitchStand();
    }
}
