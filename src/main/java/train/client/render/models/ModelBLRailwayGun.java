//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: BL 9.2 inch Carrier
// Model Creator: 
// Created on: 05.02.2021 - 16:53:55
// Last changed on: 05.02.2021 - 16:53:55

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.render.RenderRollingStock;
import train.common.api.AbstractTrains;
import train.common.entity.rollingStock.PassengerPL42;
import train.common.library.Info;

public class ModelBLRailwayGun extends ModelConverter //Same as Filename
{
	int textureX = 1024;
	int textureY = 256;

	public ModelBLRailwayGun() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[116];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private ModelRailwayGunBogie trucks = new ModelRailwayGunBogie();

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity,f,f1,f2,f3,f4,f5);
		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/9.2InchRailwayGunBogie_.png"));
		GL11.glPushMatrix();
		GL11.glTranslated(2,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glTranslated(-3.875,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glPopMatrix();
	}
	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Main Frame
		bodyModel[1] = new ModelRendererTurbo(this, 209, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 385, 1, textureX, textureY); // Box 4
		bodyModel[3] = new ModelRendererTurbo(this, 537, 1, textureX, textureY); // Box 5
		bodyModel[4] = new ModelRendererTurbo(this, 609, 1, textureX, textureY); // Box 5
		bodyModel[5] = new ModelRendererTurbo(this, 681, 1, textureX, textureY); // Box 6
		bodyModel[6] = new ModelRendererTurbo(this, 737, 1, textureX, textureY); // Box 8
		bodyModel[7] = new ModelRendererTurbo(this, 585, 1, textureX, textureY); // Box 9
		bodyModel[8] = new ModelRendererTurbo(this, 657, 1, textureX, textureY); // Box 10
		bodyModel[9] = new ModelRendererTurbo(this, 721, 1, textureX, textureY); // Box 11
		bodyModel[10] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 12
		bodyModel[11] = new ModelRendererTurbo(this, 777, 1, textureX, textureY); // Box 13
		bodyModel[12] = new ModelRendererTurbo(this, 801, 1, textureX, textureY); // Box 14
		bodyModel[13] = new ModelRendererTurbo(this, 825, 1, textureX, textureY); // Box 15
		bodyModel[14] = new ModelRendererTurbo(this, 833, 1, textureX, textureY); // Box 16
		bodyModel[15] = new ModelRendererTurbo(this, 897, 1, textureX, textureY); // Box 17
		bodyModel[16] = new ModelRendererTurbo(this, 585, 1, textureX, textureY); // Box 18
		bodyModel[17] = new ModelRendererTurbo(this, 617, 1, textureX, textureY); // Box 19
		bodyModel[18] = new ModelRendererTurbo(this, 921, 1, textureX, textureY); // Box 20
		bodyModel[19] = new ModelRendererTurbo(this, 681, 1, textureX, textureY); // Box 22
		bodyModel[20] = new ModelRendererTurbo(this, 745, 1, textureX, textureY); // Box 24
		bodyModel[21] = new ModelRendererTurbo(this, 945, 1, textureX, textureY); // Box 25
		bodyModel[22] = new ModelRendererTurbo(this, 969, 1, textureX, textureY); // Box 26
		bodyModel[23] = new ModelRendererTurbo(this, 985, 1, textureX, textureY); // Box 29
		bodyModel[24] = new ModelRendererTurbo(this, 1001, 1, textureX, textureY); // Box 30
		bodyModel[25] = new ModelRendererTurbo(this, 209, 9, textureX, textureY); // Box 31
		bodyModel[26] = new ModelRendererTurbo(this, 233, 9, textureX, textureY); // Box 33
		bodyModel[27] = new ModelRendererTurbo(this, 249, 9, textureX, textureY); // Box 34
		bodyModel[28] = new ModelRendererTurbo(this, 265, 9, textureX, textureY); // Box 35
		bodyModel[29] = new ModelRendererTurbo(this, 281, 9, textureX, textureY); // Box 36
		bodyModel[30] = new ModelRendererTurbo(this, 297, 9, textureX, textureY); // Box 37
		bodyModel[31] = new ModelRendererTurbo(this, 313, 9, textureX, textureY); // Box 38
		bodyModel[32] = new ModelRendererTurbo(this, 385, 9, textureX, textureY); // Box 39
		bodyModel[33] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 40
		bodyModel[34] = new ModelRendererTurbo(this, 497, 9, textureX, textureY); // Box 41
		bodyModel[35] = new ModelRendererTurbo(this, 945, 9, textureX, textureY); // Box 42
		bodyModel[36] = new ModelRendererTurbo(this, 985, 9, textureX, textureY); // Box 43
		bodyModel[37] = new ModelRendererTurbo(this, 225, 17, textureX, textureY); // Box 45
		bodyModel[38] = new ModelRendererTurbo(this, 265, 17, textureX, textureY); // Box 46
		bodyModel[39] = new ModelRendererTurbo(this, 321, 17, textureX, textureY); // Box 48
		bodyModel[40] = new ModelRendererTurbo(this, 529, 9, textureX, textureY); // Box 49
		bodyModel[41] = new ModelRendererTurbo(this, 377, 17, textureX, textureY); // Box 50
		bodyModel[42] = new ModelRendererTurbo(this, 409, 17, textureX, textureY); // Box 51
		bodyModel[43] = new ModelRendererTurbo(this, 1017, 1, textureX, textureY); // Box 52
		bodyModel[44] = new ModelRendererTurbo(this, 793, 1, textureX, textureY); // Box 53
		bodyModel[45] = new ModelRendererTurbo(this, 817, 1, textureX, textureY); // Box 54
		bodyModel[46] = new ModelRendererTurbo(this, 433, 17, textureX, textureY); // Box 55
		bodyModel[47] = new ModelRendererTurbo(this, 489, 17, textureX, textureY); // Box 56
		bodyModel[48] = new ModelRendererTurbo(this, 681, 9, textureX, textureY); // Box 57
		bodyModel[49] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Box 59
		bodyModel[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 60
		bodyModel[51] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // Box 61
		bodyModel[52] = new ModelRendererTurbo(this, 657, 1, textureX, textureY); // Box 62
		bodyModel[53] = new ModelRendererTurbo(this, 721, 1, textureX, textureY); // Box 63
		bodyModel[54] = new ModelRendererTurbo(this, 777, 1, textureX, textureY); // Box 64
		bodyModel[55] = new ModelRendererTurbo(this, 841, 1, textureX, textureY); // Box 68
		bodyModel[56] = new ModelRendererTurbo(this, 657, 17, textureX, textureY); // Box 69
		bodyModel[57] = new ModelRendererTurbo(this, 553, 9, textureX, textureY); // Box 70
		bodyModel[58] = new ModelRendererTurbo(this, 689, 9, textureX, textureY); // Box 71
		bodyModel[59] = new ModelRendererTurbo(this, 745, 9, textureX, textureY); // Box 73
		bodyModel[60] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 75
		bodyModel[61] = new ModelRendererTurbo(this, 9, 17, textureX, textureY); // Box 77
		bodyModel[62] = new ModelRendererTurbo(this, 721, 17, textureX, textureY); // Box 78
		bodyModel[63] = new ModelRendererTurbo(this, 17, 17, textureX, textureY); // Box 79
		bodyModel[64] = new ModelRendererTurbo(this, 793, 17, textureX, textureY); // Box 80
		bodyModel[65] = new ModelRendererTurbo(this, 209, 17, textureX, textureY); // Box 81
		bodyModel[66] = new ModelRendererTurbo(this, 217, 17, textureX, textureY); // Box 83
		bodyModel[67] = new ModelRendererTurbo(this, 225, 17, textureX, textureY); // Box 84
		bodyModel[68] = new ModelRendererTurbo(this, 257, 17, textureX, textureY); // Box 86
		bodyModel[69] = new ModelRendererTurbo(this, 913, 17, textureX, textureY); // Box 87
		bodyModel[70] = new ModelRendererTurbo(this, 937, 17, textureX, textureY); // Box 89
		bodyModel[71] = new ModelRendererTurbo(this, 265, 17, textureX, textureY); // Box 90
		bodyModel[72] = new ModelRendererTurbo(this, 961, 17, textureX, textureY); // Box 91
		bodyModel[73] = new ModelRendererTurbo(this, 377, 17, textureX, textureY); // Box 92
		bodyModel[74] = new ModelRendererTurbo(this, 401, 17, textureX, textureY); // Box 94
		bodyModel[75] = new ModelRendererTurbo(this, 409, 17, textureX, textureY); // Box 96
		bodyModel[76] = new ModelRendererTurbo(this, 809, 17, textureX, textureY); // Box 97
		bodyModel[77] = new ModelRendererTurbo(this, 433, 17, textureX, textureY); // Box 102
		bodyModel[78] = new ModelRendererTurbo(this, 489, 17, textureX, textureY); // Box 103
		bodyModel[79] = new ModelRendererTurbo(this, 513, 17, textureX, textureY); // Box 104
		bodyModel[80] = new ModelRendererTurbo(this, 521, 17, textureX, textureY); // Box 105
		bodyModel[81] = new ModelRendererTurbo(this, 825, 17, textureX, textureY); // Box 106
		bodyModel[82] = new ModelRendererTurbo(this, 985, 17, textureX, textureY); // Box 107
		bodyModel[83] = new ModelRendererTurbo(this, 369, 17, textureX, textureY); // Box 109
		bodyModel[84] = new ModelRendererTurbo(this, 689, 17, textureX, textureY); // Box 110
		bodyModel[85] = new ModelRendererTurbo(this, 777, 17, textureX, textureY); // Box 111
		bodyModel[86] = new ModelRendererTurbo(this, 993, 17, textureX, textureY); // Box 112
		bodyModel[87] = new ModelRendererTurbo(this, 1001, 17, textureX, textureY); // Box 113
		bodyModel[88] = new ModelRendererTurbo(this, 785, 17, textureX, textureY); // Box 114
		bodyModel[89] = new ModelRendererTurbo(this, 1009, 17, textureX, textureY); // Box 115
		bodyModel[90] = new ModelRendererTurbo(this, 1017, 17, textureX, textureY); // Box 116
		bodyModel[91] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Box 105
		bodyModel[92] = new ModelRendererTurbo(this, 369, 25, textureX, textureY); // Box 106
		bodyModel[93] = new ModelRendererTurbo(this, 433, 25, textureX, textureY); // Box 115
		bodyModel[94] = new ModelRendererTurbo(this, 473, 25, textureX, textureY); // Box 118
		bodyModel[95] = new ModelRendererTurbo(this, 657, 25, textureX, textureY); // Box 125
		bodyModel[96] = new ModelRendererTurbo(this, 481, 25, textureX, textureY); // Box 126
		bodyModel[97] = new ModelRendererTurbo(this, 521, 25, textureX, textureY); // Box 127
		bodyModel[98] = new ModelRendererTurbo(this, 529, 25, textureX, textureY); // Box 128
		bodyModel[99] = new ModelRendererTurbo(this, 697, 25, textureX, textureY); // Box 129
		bodyModel[100] = new ModelRendererTurbo(this, 705, 25, textureX, textureY); // Box 104
		bodyModel[101] = new ModelRendererTurbo(this, 713, 25, textureX, textureY); // Box 105
		bodyModel[102] = new ModelRendererTurbo(this, 721, 25, textureX, textureY); // Box 396
		bodyModel[103] = new ModelRendererTurbo(this, 729, 25, textureX, textureY); // Box 397
		bodyModel[104] = new ModelRendererTurbo(this, 737, 25, textureX, textureY); // Box 398
		bodyModel[105] = new ModelRendererTurbo(this, 745, 25, textureX, textureY); // Box 174
		bodyModel[106] = new ModelRendererTurbo(this, 761, 25, textureX, textureY); // Box 175
		bodyModel[107] = new ModelRendererTurbo(this, 905, 17, textureX, textureY); // Box 178
		bodyModel[108] = new ModelRendererTurbo(this, 753, 25, textureX, textureY); // Box 179
		bodyModel[109] = new ModelRendererTurbo(this, 777, 25, textureX, textureY); // Box 171
		bodyModel[110] = new ModelRendererTurbo(this, 785, 25, textureX, textureY); // Box 180
		bodyModel[111] = new ModelRendererTurbo(this, 801, 25, textureX, textureY); // Box 181
		bodyModel[112] = new ModelRendererTurbo(this, 833, 25, textureX, textureY); // Box 182
		bodyModel[113] = new ModelRendererTurbo(this, 841, 25, textureX, textureY); // Box 183
		bodyModel[114] = new ModelRendererTurbo(this, 769, 25, textureX, textureY); // Box 184
		bodyModel[115] = new ModelRendererTurbo(this, 793, 25, textureX, textureY); // Box 185

		bodyModel[0].addShapeBox(0F, 0F, 0F, 89, 2, 22, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Main Frame
		bodyModel[0].setRotationPoint(-43.5F, 2F, -11F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 82, 5, 2, 0F,3F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 3F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F); // Box 1
		bodyModel[1].setRotationPoint(-39.5F, 4F, -11F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 82, 5, 2, 0F,3F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 3F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F); // Box 4
		bodyModel[2].setRotationPoint(-39.5F, 4F, 9F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 10, 7, 22, 0F,-2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 5
		bodyModel[3].setRotationPoint(-34.5F, -5F, -11F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 10, 7, 22, 0F,-2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 5
		bodyModel[4].setRotationPoint(27.5F, -5F, -11F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 7, 3, 20, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[5].setRotationPoint(-40.5F, -1F, -10F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 7, 3, 20, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 8
		bodyModel[6].setRotationPoint(35.5F, -1F, -10F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 6, 3, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		bodyModel[7].setRotationPoint(-1.5F, -1F, -8F);

		bodyModel[8].addBox(0F, 0F, 0F, 5, 3, 6, 0F); // Box 10
		bodyModel[8].setRotationPoint(-6.5F, -1F, -3F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 5, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[9].setRotationPoint(4.5F, -1F, -3F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 5, 3, 5, 0F,0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		bodyModel[10].setRotationPoint(-6.5F, -1F, -8F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 5, 3, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F); // Box 13
		bodyModel[11].setRotationPoint(-6.5F, -1F, 3F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 5, 3, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F); // Box 14
		bodyModel[12].setRotationPoint(4.5F, -1F, 3F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 5, 3, 5, 0F,0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		bodyModel[13].setRotationPoint(4.5F, -1F, -8F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 20, 1, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		bodyModel[14].setRotationPoint(-7.5F, -2F, -8F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 8, 9, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		bodyModel[15].setRotationPoint(-4.5F, -11F, 6F);

		bodyModel[16].addBox(0F, 0F, 0F, 4, 9, 2, 0F); // Box 18
		bodyModel[16].setRotationPoint(3.5F, -11F, 6F);

		bodyModel[17].addBox(0F, 0F, 0F, 4, 9, 2, 0F); // Box 19
		bodyModel[17].setRotationPoint(3.5F, -11F, -8F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 8, 9, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		bodyModel[18].setRotationPoint(-4.5F, -11F, -8F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 5, 5, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 22
		bodyModel[19].setRotationPoint(-4.5F, -14F, -8F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 3, 4, 2, 0F,0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 24
		bodyModel[20].setRotationPoint(0.5F, -14F, 6F);

		bodyModel[21].addBox(0F, 0F, 0F, 9, 2, 2, 0F); // Box 25
		bodyModel[21].setRotationPoint(-13.5F, -6F, 6F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 3, 5, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		bodyModel[22].setRotationPoint(-7.5F, -11F, 6F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 3, 4, 2, 0F,0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 29
		bodyModel[23].setRotationPoint(-7.5F, -14F, 6F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 3, 4, 2, 0F,0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 30
		bodyModel[24].setRotationPoint(-10.5F, -9F, 6F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 9, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		bodyModel[25].setRotationPoint(-13.5F, -6F, -8F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 3, 5, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		bodyModel[26].setRotationPoint(-7.5F, -11F, -8F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 3, 4, 2, 0F,0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 34
		bodyModel[27].setRotationPoint(-10.5F, -9F, -8F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 3, 4, 2, 0F,0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 35
		bodyModel[28].setRotationPoint(-7.5F, -14F, -8F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 5, 5, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 36
		bodyModel[29].setRotationPoint(-4.5F, -14F, 6F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 3, 4, 2, 0F,0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 37
		bodyModel[30].setRotationPoint(0.5F, -14F, -8F);

		bodyModel[31].addBox(0F, 0F, 0F, 33, 2, 2, 0F); // Box 38
		bodyModel[31].setRotationPoint(7.5F, -9F, 6F);

		bodyModel[32].addBox(0F, 0F, 0F, 33, 2, 2, 0F); // Box 39
		bodyModel[32].setRotationPoint(7.5F, -9F, -8F);

		bodyModel[33].addBox(0F, 0F, 0F, 16, 2, 1, 0F); // Box 40
		bodyModel[33].setRotationPoint(7.5F, -7F, 7F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 16, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 41
		bodyModel[34].setRotationPoint(7.5F, -5F, 7F);

		bodyModel[35].addBox(0F, 0F, 0F, 16, 2, 1, 0F); // Box 42
		bodyModel[35].setRotationPoint(7.5F, -7F, -8F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 16, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 43
		bodyModel[36].setRotationPoint(7.5F, -5F, -8F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 9, 9, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		bodyModel[37].setRotationPoint(-4.5F, -19F, -4F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 21, 6, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		bodyModel[38].setRotationPoint(-25.5F, -18F, -3F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 20, 4, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		bodyModel[39].setRotationPoint(-45.5F, -17F, -2F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 8, 6, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		bodyModel[40].setRotationPoint(4.5F, -18F, -3F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 5, 8, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		bodyModel[41].setRotationPoint(12.5F, -19F, -4F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 5, 6, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		bodyModel[42].setRotationPoint(17.5F, -18F, -3F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		bodyModel[43].setRotationPoint(22.5F, -17F, -1F);

		bodyModel[44].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 53
		bodyModel[44].setRotationPoint(-2.5F, -13F, 4F);

		bodyModel[45].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 54
		bodyModel[45].setRotationPoint(-2.5F, -13F, -6F);

		bodyModel[46].addBox(0F, 0F, 0F, 13, 2, 12, 0F); // Box 55
		bodyModel[46].setRotationPoint(27.5F, -9F, -6F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 7, 5, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		bodyModel[47].setRotationPoint(34.5F, -14F, -4F);

		bodyModel[48].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 57
		bodyModel[48].setRotationPoint(-29.5F, -8F, -1F);

		bodyModel[49].addBox(0F, 0F, 0F, 1, 1, 6, 0F); // Box 59
		bodyModel[49].setRotationPoint(-29.5F, -9F, -3F);

		bodyModel[50].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 60
		bodyModel[50].setRotationPoint(-29.5F, -11F, -3F);

		bodyModel[51].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 61
		bodyModel[51].setRotationPoint(-29.5F, -11F, 2F);

		bodyModel[52].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 62
		bodyModel[52].setRotationPoint(39.5F, -12F, 7F);

		bodyModel[53].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 63
		bodyModel[53].setRotationPoint(36.5F, -12F, 7F);

		bodyModel[54].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 64
		bodyModel[54].setRotationPoint(33.5F, -12F, 7F);

		bodyModel[55].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 68
		bodyModel[55].setRotationPoint(30.5F, -12F, 7F);

		bodyModel[56].addBox(0F, 0F, 0F, 13, 1, 1, 0F); // Box 69
		bodyModel[56].setRotationPoint(27.5F, -13F, 7F);

		bodyModel[57].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 70
		bodyModel[57].setRotationPoint(27.5F, -12F, 7F);

		bodyModel[58].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 71
		bodyModel[58].setRotationPoint(39.5F, -12F, -8F);

		bodyModel[59].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 73
		bodyModel[59].setRotationPoint(36.5F, -12F, -8F);

		bodyModel[60].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 75
		bodyModel[60].setRotationPoint(33.5F, -12F, -8F);

		bodyModel[61].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 77
		bodyModel[61].setRotationPoint(30.5F, -12F, -8F);

		bodyModel[62].addBox(0F, 0F, 0F, 13, 1, 1, 0F); // Box 78
		bodyModel[62].setRotationPoint(27.5F, -13F, -8F);

		bodyModel[63].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 79
		bodyModel[63].setRotationPoint(27.5F, -12F, -8F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 80
		bodyModel[64].setRotationPoint(0.5F, -7F, 8F);

		bodyModel[65].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 81
		bodyModel[65].setRotationPoint(0.5F, -10F, 10F);

		bodyModel[66].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 83
		bodyModel[66].setRotationPoint(3.5F, -10F, 10F);

		bodyModel[67].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 84
		bodyModel[67].setRotationPoint(6.5F, -10F, 10F);

		bodyModel[68].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 86
		bodyModel[68].setRotationPoint(9.5F, -10F, 10F);

		bodyModel[69].addBox(0F, 0F, 0F, 10, 1, 1, 0F); // Box 87
		bodyModel[69].setRotationPoint(0.5F, -11F, 10F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		bodyModel[70].setRotationPoint(0.5F, -7F, -11F);

		bodyModel[71].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 90
		bodyModel[71].setRotationPoint(0.5F, -10F, -11F);

		bodyModel[72].addBox(0F, 0F, 0F, 10, 1, 1, 0F); // Box 91
		bodyModel[72].setRotationPoint(0.5F, -11F, -11F);

		bodyModel[73].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 92
		bodyModel[73].setRotationPoint(3.5F, -10F, -11F);

		bodyModel[74].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 94
		bodyModel[74].setRotationPoint(6.5F, -10F, -11F);

		bodyModel[75].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 96
		bodyModel[75].setRotationPoint(9.5F, -10F, -11F);

		bodyModel[76].addBox(0F, 0F, 0F, 1, 2, 12, 0F); // Box 97
		bodyModel[76].setRotationPoint(-13.5F, -6F, -6F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		bodyModel[77].setRotationPoint(-12.5F, 1F, -12F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 103
		bodyModel[78].setRotationPoint(14.5F, 1F, -12F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 104
		bodyModel[79].setRotationPoint(-12.5F, 1F, 11F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 105
		bodyModel[80].setRotationPoint(14.5F, 1F, 11F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 106
		bodyModel[81].setRotationPoint(17.5F, -9F, -9F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 107
		bodyModel[82].setRotationPoint(20.5F, -9F, -9F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 109
		bodyModel[83].setRotationPoint(18.5F, -8F, -9F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 110
		bodyModel[84].setRotationPoint(18.5F, -6F, -9F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		bodyModel[85].setRotationPoint(18.5F, -4F, -9F);

		bodyModel[86].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		bodyModel[86].setRotationPoint(17.5F, -9F, 8F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		bodyModel[87].setRotationPoint(20.5F, -9F, 8F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 114
		bodyModel[88].setRotationPoint(18.5F, -8F, 8F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 115
		bodyModel[89].setRotationPoint(18.5F, -6F, 8F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 116
		bodyModel[90].setRotationPoint(18.5F, -4F, 8F);

		bodyModel[91].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 105
		bodyModel[91].setRotationPoint(-2.5F, -13F, 8F);

		bodyModel[92].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 106
		bodyModel[92].setRotationPoint(-2.5F, -13F, 9F);

		bodyModel[93].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 115
		bodyModel[93].setRotationPoint(-2.5F, -13F, -9F);

		bodyModel[94].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 118
		bodyModel[94].setRotationPoint(-2.5F, -13F, -10F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 7, 4, 20, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 125
		bodyModel[95].setRotationPoint(-26.5F, -2F, -10F);

		bodyModel[96].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 126
		bodyModel[96].setRotationPoint(-11.5F, 3F, 11F);

		bodyModel[97].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 127
		bodyModel[97].setRotationPoint(15.5F, 3F, 11F);

		bodyModel[98].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 128
		bodyModel[98].setRotationPoint(15.5F, 3F, -12F);

		bodyModel[99].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 129
		bodyModel[99].setRotationPoint(-11.5F, 3F, -12F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 104
		bodyModel[100].setRotationPoint(22.5F, -16F, -2F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 105
		bodyModel[101].setRotationPoint(22.5F, -16F, 1F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F); // Box 396
		bodyModel[102].setRotationPoint(-44.53F, 2F, -0.5F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 397
		bodyModel[103].setRotationPoint(-45.78F, 3.01F, -0.5F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 398
		bodyModel[104].setRotationPoint(-46.78F, 2.01F, -0.5F);

		bodyModel[105].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 174
		bodyModel[105].setRotationPoint(-46.5F, 3F, -4.5F);

		bodyModel[106].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 175
		bodyModel[106].setRotationPoint(-46.5F, 3F, 3.5F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 178
		bodyModel[107].setRotationPoint(-47.5F, 2F, 2.5F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 179
		bodyModel[108].setRotationPoint(-47.5F, 2F, -5.5F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[109].setRotationPoint(45.5F, 2F, -0.5F);

		bodyModel[110].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 180
		bodyModel[110].setRotationPoint(45.5F, 3F, 3.5F);

		bodyModel[111].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 181
		bodyModel[111].setRotationPoint(45.5F, 3F, -4.5F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 1F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 182
		bodyModel[112].setRotationPoint(46.78F, 3.01F, -0.5F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 183
		bodyModel[113].setRotationPoint(47.78F, 2.01F, -0.5F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 184
		bodyModel[114].setRotationPoint(48.5F, 2F, 2.5F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 185
		bodyModel[115].setRotationPoint(48.5F, 2F, -5.5F);
	}
}