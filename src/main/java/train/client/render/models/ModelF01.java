//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: Class F-01
// Model Creator: Bida
// Created on: 21.03.2022 - 20:58:14
// Last changed on: 21.03.2022 - 20:58:14

package train.client.render.models; //Path where the model is located

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.library.Info;

import java.util.ArrayList;

public class ModelF01 extends ModelConverter //Same as Filename
{
	int textureX = 256;
	int textureY = 256;

	public ModelF01() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[282];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 33
		bodyModel[1] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // Box 34
		bodyModel[2] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Box 35
		bodyModel[3] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Box 36
		bodyModel[4] = new ModelRendererTurbo(this, 65, 1, textureX, textureY); // Box 37
		bodyModel[5] = new ModelRendererTurbo(this, 81, 1, textureX, textureY); // Box 38
		bodyModel[6] = new ModelRendererTurbo(this, 97, 1, textureX, textureY); // Box 39
		bodyModel[7] = new ModelRendererTurbo(this, 113, 1, textureX, textureY); // Box 40
		bodyModel[8] = new ModelRendererTurbo(this, 129, 1, textureX, textureY); // Box 41
		bodyModel[9] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 42
		bodyModel[10] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 143
		bodyModel[11] = new ModelRendererTurbo(this, 169, 1, textureX, textureY); // Box 144
		bodyModel[12] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Box 143
		bodyModel[13] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 144
		bodyModel[14] = new ModelRendererTurbo(this, 201, 1, textureX, textureY); // Box 414
		bodyModel[15] = new ModelRendererTurbo(this, 217, 1, textureX, textureY); // Box 415
		bodyModel[16] = new ModelRendererTurbo(this, 193, 1, textureX, textureY); // Box 128 siderod bit that connects wheels to piston
		bodyModel[17] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 130
		bodyModel[18] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 131 piston back and forth bit
		bodyModel[19] = new ModelRendererTurbo(this, 17, 9, textureX, textureY); // Box 132 piston back and forth bit
		bodyModel[20] = new ModelRendererTurbo(this, 73, 9, textureX, textureY); // Box 133
		bodyModel[21] = new ModelRendererTurbo(this, 97, 9, textureX, textureY); // Box 128 siderod bit that connects the wheels
		bodyModel[22] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Box 129 spinny bar part
		bodyModel[23] = new ModelRendererTurbo(this, 249, 1, textureX, textureY); // Box 130 reverser swaws
		bodyModel[24] = new ModelRendererTurbo(this, 129, 9, textureX, textureY); // Box 131 reverser other swaws
		bodyModel[25] = new ModelRendererTurbo(this, 169, 9, textureX, textureY); // Box 133
		bodyModel[26] = new ModelRendererTurbo(this, 41, 9, textureX, textureY); // Box 134 bit that does the cha cha slide
		bodyModel[27] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 149 reverser swaws
		bodyModel[28] = new ModelRendererTurbo(this, 185, 9, textureX, textureY); // Box 208 WHAT AREE YOU
		bodyModel[29] = new ModelRendererTurbo(this, 193, 9, textureX, textureY); // Box 209 WHAT DO YOU DOO
		bodyModel[30] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 128 siderod bit that connects wheels to piston
		bodyModel[31] = new ModelRendererTurbo(this, 233, 9, textureX, textureY); // Box 130
		bodyModel[32] = new ModelRendererTurbo(this, 33, 17, textureX, textureY); // Box 131 piston back and forth bit
		bodyModel[33] = new ModelRendererTurbo(this, 49, 17, textureX, textureY); // Box 132 piston back and forth bit
		bodyModel[34] = new ModelRendererTurbo(this, 73, 17, textureX, textureY); // Box 133
		bodyModel[35] = new ModelRendererTurbo(this, 97, 17, textureX, textureY); // Box 128 siderod bit that connects the wheels
		bodyModel[36] = new ModelRendererTurbo(this, 129, 17, textureX, textureY); // Box 129 spinny bar part
		bodyModel[37] = new ModelRendererTurbo(this, 137, 17, textureX, textureY); // Box 130 reverser swaws
		bodyModel[38] = new ModelRendererTurbo(this, 145, 17, textureX, textureY); // Box 131 reverser other swaws
		bodyModel[39] = new ModelRendererTurbo(this, 169, 17, textureX, textureY); // Box 133
		bodyModel[40] = new ModelRendererTurbo(this, 185, 17, textureX, textureY); // Box 134 bit that does the cha cha slide
		bodyModel[41] = new ModelRendererTurbo(this, 201, 17, textureX, textureY); // Box 149 reverser swaws
		bodyModel[42] = new ModelRendererTurbo(this, 209, 17, textureX, textureY); // Box 208 WHAT AREE YOU
		bodyModel[43] = new ModelRendererTurbo(this, 217, 17, textureX, textureY); // Box 209 WHAT DO YOU DOO
		bodyModel[44] = new ModelRendererTurbo(this, 225, 17, textureX, textureY); // Box 68 wheelo
		bodyModel[45] = new ModelRendererTurbo(this, 57, 9, textureX, textureY); // Box 71
		bodyModel[46] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Box 72 wheele
		bodyModel[47] = new ModelRendererTurbo(this, 153, 9, textureX, textureY); // Box 63
		bodyModel[48] = new ModelRendererTurbo(this, 25, 25, textureX, textureY); // Box 64 wheela
		bodyModel[49] = new ModelRendererTurbo(this, 49, 25, textureX, textureY); // Box 65 wheely
		bodyModel[50] = new ModelRendererTurbo(this, 73, 25, textureX, textureY); // Box coupler
		bodyModel[51] = new ModelRendererTurbo(this, 81, 25, textureX, textureY); // Box 99
		bodyModel[52] = new ModelRendererTurbo(this, 105, 25, textureX, textureY); // Box 2
		bodyModel[53] = new ModelRendererTurbo(this, 121, 25, textureX, textureY); // Box 113
		bodyModel[54] = new ModelRendererTurbo(this, 25, 17, textureX, textureY); // Box 187
		bodyModel[55] = new ModelRendererTurbo(this, 137, 25, textureX, textureY); // Box 29
		bodyModel[56] = new ModelRendererTurbo(this, 169, 25, textureX, textureY); // Box 54
		bodyModel[57] = new ModelRendererTurbo(this, 65, 33, textureX, textureY); // Box 55
		bodyModel[58] = new ModelRendererTurbo(this, 185, 33, textureX, textureY); // Box 56
		bodyModel[59] = new ModelRendererTurbo(this, 217, 33, textureX, textureY); // Box 57
		bodyModel[60] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 58
		bodyModel[61] = new ModelRendererTurbo(this, 33, 41, textureX, textureY); // Box 56
		bodyModel[62] = new ModelRendererTurbo(this, 89, 33, textureX, textureY); // Box 110
		bodyModel[63] = new ModelRendererTurbo(this, 249, 17, textureX, textureY); // Box 111
		bodyModel[64] = new ModelRendererTurbo(this, 89, 9, textureX, textureY); // Box 112
		bodyModel[65] = new ModelRendererTurbo(this, 97, 25, textureX, textureY); // Box 113
		bodyModel[66] = new ModelRendererTurbo(this, 137, 25, textureX, textureY); // Box 114
		bodyModel[67] = new ModelRendererTurbo(this, 201, 25, textureX, textureY); // Box 116
		bodyModel[68] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // Box 234
		bodyModel[69] = new ModelRendererTurbo(this, 113, 33, textureX, textureY); // Box 370
		bodyModel[70] = new ModelRendererTurbo(this, 137, 41, textureX, textureY); // Box 102
		bodyModel[71] = new ModelRendererTurbo(this, 241, 25, textureX, textureY); // Box 56
		bodyModel[72] = new ModelRendererTurbo(this, 209, 9, textureX, textureY, "lamp"); // Box 146 headlight front
		bodyModel[73] = new ModelRendererTurbo(this, 113, 25, textureX, textureY, "lamp"); // Box 154 headlight glow
		bodyModel[74] = new ModelRendererTurbo(this, 121, 25, textureX, textureY, "lamp"); // Box 155 headlight glow
		bodyModel[75] = new ModelRendererTurbo(this, 225, 9, textureX, textureY); // Box 181
		bodyModel[76] = new ModelRendererTurbo(this, 209, 25, textureX, textureY); // Box 194
		bodyModel[77] = new ModelRendererTurbo(this, 17, 33, textureX, textureY); // Box 195
		bodyModel[78] = new ModelRendererTurbo(this, 121, 33, textureX, textureY); // Box 222
		bodyModel[79] = new ModelRendererTurbo(this, 153, 41, textureX, textureY); // Box 105
		bodyModel[80] = new ModelRendererTurbo(this, 169, 41, textureX, textureY); // Box 106
		bodyModel[81] = new ModelRendererTurbo(this, 169, 1, textureX, textureY); // Box 107
		bodyModel[82] = new ModelRendererTurbo(this, 185, 41, textureX, textureY); // Box 108
		bodyModel[83] = new ModelRendererTurbo(this, 193, 25, textureX, textureY); // Box 109
		bodyModel[84] = new ModelRendererTurbo(this, 89, 33, textureX, textureY); // Box 110
		bodyModel[85] = new ModelRendererTurbo(this, 241, 33, textureX, textureY); // Box 188
		bodyModel[86] = new ModelRendererTurbo(this, 249, 33, textureX, textureY); // Box 189
		bodyModel[87] = new ModelRendererTurbo(this, 201, 41, textureX, textureY); // Box 190
		bodyModel[88] = new ModelRendererTurbo(this, 233, 41, textureX, textureY); // Box 191
		bodyModel[89] = new ModelRendererTurbo(this, 241, 41, textureX, textureY); // Box 193
		bodyModel[90] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 194
		bodyModel[91] = new ModelRendererTurbo(this, 9, 49, textureX, textureY); // Box 195
		bodyModel[92] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 196
		bodyModel[93] = new ModelRendererTurbo(this, 81, 41, textureX, textureY); // Box 197
		bodyModel[94] = new ModelRendererTurbo(this, 17, 49, textureX, textureY); // Box 198
		bodyModel[95] = new ModelRendererTurbo(this, 25, 49, textureX, textureY); // Box 199
		bodyModel[96] = new ModelRendererTurbo(this, 41, 49, textureX, textureY); // Box 390
		bodyModel[97] = new ModelRendererTurbo(this, 49, 49, textureX, textureY); // Box 391
		bodyModel[98] = new ModelRendererTurbo(this, 57, 49, textureX, textureY); // Box 239
		bodyModel[99] = new ModelRendererTurbo(this, 65, 49, textureX, textureY); // Box 240
		bodyModel[100] = new ModelRendererTurbo(this, 81, 49, textureX, textureY); // Box 241
		bodyModel[101] = new ModelRendererTurbo(this, 97, 49, textureX, textureY); // Box 242
		bodyModel[102] = new ModelRendererTurbo(this, 161, 33, textureX, textureY); // Box 93
		bodyModel[103] = new ModelRendererTurbo(this, 193, 49, textureX, textureY); // Box 120
		bodyModel[104] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 145
		bodyModel[105] = new ModelRendererTurbo(this, 217, 41, textureX, textureY); // Box 117
		bodyModel[106] = new ModelRendererTurbo(this, 241, 49, textureX, textureY); // Box 118
		bodyModel[107] = new ModelRendererTurbo(this, 73, 57, textureX, textureY); // Box 119
		bodyModel[108] = new ModelRendererTurbo(this, 81, 57, textureX, textureY); // Box 124
		bodyModel[109] = new ModelRendererTurbo(this, 153, 49, textureX, textureY); // Box 175
		bodyModel[110] = new ModelRendererTurbo(this, 105, 57, textureX, textureY); // Box 469
		bodyModel[111] = new ModelRendererTurbo(this, 121, 57, textureX, textureY); // Box 470
		bodyModel[112] = new ModelRendererTurbo(this, 137, 57, textureX, textureY); // Box 473
		bodyModel[113] = new ModelRendererTurbo(this, 153, 57, textureX, textureY); // Box 475
		bodyModel[114] = new ModelRendererTurbo(this, 169, 57, textureX, textureY); // Box 476
		bodyModel[115] = new ModelRendererTurbo(this, 177, 57, textureX, textureY); // Box 477
		bodyModel[116] = new ModelRendererTurbo(this, 225, 57, textureX, textureY); // Box 478
		bodyModel[117] = new ModelRendererTurbo(this, 241, 57, textureX, textureY); // Box 481
		bodyModel[118] = new ModelRendererTurbo(this, 113, 65, textureX, textureY); // Box 482
		bodyModel[119] = new ModelRendererTurbo(this, 129, 65, textureX, textureY); // Box 137
		bodyModel[120] = new ModelRendererTurbo(this, 137, 65, textureX, textureY); // Box 138
		bodyModel[121] = new ModelRendererTurbo(this, 145, 65, textureX, textureY); // Box 139
		bodyModel[122] = new ModelRendererTurbo(this, 153, 65, textureX, textureY); // Box 140
		bodyModel[123] = new ModelRendererTurbo(this, 161, 65, textureX, textureY); // Box 141
		bodyModel[124] = new ModelRendererTurbo(this, 169, 65, textureX, textureY); // Box 142
		bodyModel[125] = new ModelRendererTurbo(this, 113, 33, textureX, textureY); // Box 5
		bodyModel[126] = new ModelRendererTurbo(this, 169, 65, textureX, textureY); // Box 144
		bodyModel[127] = new ModelRendererTurbo(this, 193, 65, textureX, textureY); // Box 75
		bodyModel[128] = new ModelRendererTurbo(this, 217, 65, textureX, textureY); // Box 16
		bodyModel[129] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Box 17
		bodyModel[130] = new ModelRendererTurbo(this, 49, 73, textureX, textureY); // Box 19
		bodyModel[131] = new ModelRendererTurbo(this, 73, 73, textureX, textureY); // Box 21
		bodyModel[132] = new ModelRendererTurbo(this, 113, 73, textureX, textureY); // Box 22
		bodyModel[133] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 23
		bodyModel[134] = new ModelRendererTurbo(this, 73, 81, textureX, textureY); // Box 24
		bodyModel[135] = new ModelRendererTurbo(this, 121, 81, textureX, textureY); // Box 25
		bodyModel[136] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 26
		bodyModel[137] = new ModelRendererTurbo(this, 241, 65, textureX, textureY); // Box 28
		bodyModel[138] = new ModelRendererTurbo(this, 145, 81, textureX, textureY); // Box 30
		bodyModel[139] = new ModelRendererTurbo(this, 73, 65, textureX, textureY); // Box 31
		bodyModel[140] = new ModelRendererTurbo(this, 89, 17, textureX, textureY); // Box 32
		bodyModel[141] = new ModelRendererTurbo(this, 121, 17, textureX, textureY); // Box 33
		bodyModel[142] = new ModelRendererTurbo(this, 209, 65, textureX, textureY); // Box 34
		bodyModel[143] = new ModelRendererTurbo(this, 193, 81, textureX, textureY); // Box 170
		bodyModel[144] = new ModelRendererTurbo(this, 25, 89, textureX, textureY); // Box 171
		bodyModel[145] = new ModelRendererTurbo(this, 73, 89, textureX, textureY); // Box 329 door swing right
		bodyModel[146] = new ModelRendererTurbo(this, 89, 89, textureX, textureY); // Box 332 door swing left
		bodyModel[147] = new ModelRendererTurbo(this, 97, 89, textureX, textureY); // Box 35
		bodyModel[148] = new ModelRendererTurbo(this, 121, 89, textureX, textureY); // Box 189
		bodyModel[149] = new ModelRendererTurbo(this, 49, 105, textureX, textureY); // Box 168
		bodyModel[150] = new ModelRendererTurbo(this, 169, 97, textureX, textureY); // Box 222
		bodyModel[151] = new ModelRendererTurbo(this, 137, 89, textureX, textureY); // Box 223
		bodyModel[152] = new ModelRendererTurbo(this, 161, 73, textureX, textureY); // Box 225
		bodyModel[153] = new ModelRendererTurbo(this, 169, 73, textureX, textureY); // Box 226
		bodyModel[154] = new ModelRendererTurbo(this, 217, 89, textureX, textureY); // Box 227
		bodyModel[155] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Box 25
		bodyModel[156] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Box 35
		bodyModel[157] = new ModelRendererTurbo(this, 233, 89, textureX, textureY); // Box 41
		bodyModel[158] = new ModelRendererTurbo(this, 217, 97, textureX, textureY); // Box 95
		bodyModel[159] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 96
		bodyModel[160] = new ModelRendererTurbo(this, 153, 73, textureX, textureY); // Box 97
		bodyModel[161] = new ModelRendererTurbo(this, 25, 97, textureX, textureY); // Box 98
		bodyModel[162] = new ModelRendererTurbo(this, 129, 97, textureX, textureY); // Box 199
		bodyModel[163] = new ModelRendererTurbo(this, 158, 190, textureX, textureY); // Box 200
		bodyModel[164] = new ModelRendererTurbo(this, 123, 184, textureX, textureY); // Box 201
		bodyModel[165] = new ModelRendererTurbo(this, 120, 192, textureX, textureY); // Box 202
		bodyModel[166] = new ModelRendererTurbo(this, 123, 188, textureX, textureY); // Box 203
		bodyModel[167] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // Box 130
		bodyModel[168] = new ModelRendererTurbo(this, 233, 113, textureX, textureY); // Box 132
		bodyModel[169] = new ModelRendererTurbo(this, 233, 121, textureX, textureY); // Box 134
		bodyModel[170] = new ModelRendererTurbo(this, 41, 129, textureX, textureY); // Box 135
		bodyModel[171] = new ModelRendererTurbo(this, 65, 129, textureX, textureY); // Box 94
		bodyModel[172] = new ModelRendererTurbo(this, 89, 129, textureX, textureY); // Box 96
		bodyModel[173] = new ModelRendererTurbo(this, 113, 129, textureX, textureY); // Box 244
		bodyModel[174] = new ModelRendererTurbo(this, 137, 129, textureX, textureY); // Box 97
		bodyModel[175] = new ModelRendererTurbo(this, 177, 129, textureX, textureY); // Box 101
		bodyModel[176] = new ModelRendererTurbo(this, 209, 129, textureX, textureY); // Box 102
		bodyModel[177] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 112
		bodyModel[178] = new ModelRendererTurbo(this, 33, 137, textureX, textureY); // Box 116
		bodyModel[179] = new ModelRendererTurbo(this, 137, 113, textureX, textureY); // Box 7
		bodyModel[180] = new ModelRendererTurbo(this, 241, 129, textureX, textureY); // Box 8
		bodyModel[181] = new ModelRendererTurbo(this, 153, 121, textureX, textureY); // Box coupler
		bodyModel[182] = new ModelRendererTurbo(this, 57, 137, textureX, textureY); // Box 10
		bodyModel[183] = new ModelRendererTurbo(this, 65, 137, textureX, textureY); // Box 11
		bodyModel[184] = new ModelRendererTurbo(this, 89, 137, textureX, textureY); // Box 119
		bodyModel[185] = new ModelRendererTurbo(this, 105, 137, textureX, textureY); // Box 186
		bodyModel[186] = new ModelRendererTurbo(this, 169, 137, textureX, textureY); // Box 223
		bodyModel[187] = new ModelRendererTurbo(this, 113, 137, textureX, textureY); // Box 224
		bodyModel[188] = new ModelRendererTurbo(this, 89, 145, textureX, textureY); // Box 225
		bodyModel[189] = new ModelRendererTurbo(this, 113, 145, textureX, textureY); // Box 226
		bodyModel[190] = new ModelRendererTurbo(this, 145, 153, textureX, textureY); // Box 227
		bodyModel[191] = new ModelRendererTurbo(this, 65, 97, textureX, textureY); // Box 201
		bodyModel[192] = new ModelRendererTurbo(this, 105, 89, textureX, textureY); // Box 202
		bodyModel[193] = new ModelRendererTurbo(this, 249, 89, textureX, textureY); // Box 229
		bodyModel[194] = new ModelRendererTurbo(this, 225, 137, textureX, textureY); // Box 283
		bodyModel[195] = new ModelRendererTurbo(this, 217, 97, textureX, textureY, "lamp"); // Box 284 headlight rear
		bodyModel[196] = new ModelRendererTurbo(this, 57, 113, textureX, textureY); // Box 285
		bodyModel[197] = new ModelRendererTurbo(this, 193, 65, textureX, textureY, "lamp"); // Box 154 headlight glow
		bodyModel[198] = new ModelRendererTurbo(this, 249, 65, textureX, textureY, "lamp"); // Box 155 headlight glow
		bodyModel[199] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 315
		bodyModel[200] = new ModelRendererTurbo(this, 33, 153, textureX, textureY); // Box 316
		bodyModel[201] = new ModelRendererTurbo(this, 73, 137, textureX, textureY); // Box 238
		bodyModel[202] = new ModelRendererTurbo(this, 217, 113, textureX, textureY); // Box 239
		bodyModel[203] = new ModelRendererTurbo(this, 41, 121, textureX, textureY); // Box 240
		bodyModel[204] = new ModelRendererTurbo(this, 225, 145, textureX, textureY); // Box 347
		bodyModel[205] = new ModelRendererTurbo(this, 185, 153, textureX, textureY); // Box 242e
		bodyModel[206] = new ModelRendererTurbo(this, 65, 145, textureX, textureY); // Box 243
		bodyModel[207] = new ModelRendererTurbo(this, 121, 97, textureX, textureY); // Box 244
		bodyModel[208] = new ModelRendererTurbo(this, 209, 153, textureX, textureY); // Box 245
		bodyModel[209] = new ModelRendererTurbo(this, 249, 97, textureX, textureY); // Box 246
		bodyModel[210] = new ModelRendererTurbo(this, 33, 129, textureX, textureY); // Box 247
		bodyModel[211] = new ModelRendererTurbo(this, 57, 121, textureX, textureY); // Box 248
		bodyModel[212] = new ModelRendererTurbo(this, 129, 121, textureX, textureY); // Box 249
		bodyModel[213] = new ModelRendererTurbo(this, 169, 129, textureX, textureY); // Box 250
		bodyModel[214] = new ModelRendererTurbo(this, 33, 137, textureX, textureY); // Box 251
		bodyModel[215] = new ModelRendererTurbo(this, 57, 161, textureX, textureY); // Box 400
		bodyModel[216] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 597
		bodyModel[217] = new ModelRendererTurbo(this, 161, 41, textureX, textureY); // Box 254
		bodyModel[218] = new ModelRendererTurbo(this, 1, 161, textureX, textureY); // Box 255
		bodyModel[219] = new ModelRendererTurbo(this, 121, 161, textureX, textureY); // Box 457
		bodyModel[220] = new ModelRendererTurbo(this, 65, 169, textureX, textureY); // Box 458
		bodyModel[221] = new ModelRendererTurbo(this, 169, 161, textureX, textureY); // Box 258
		bodyModel[222] = new ModelRendererTurbo(this, 241, 1, textureX, textureY); // Box 259
		bodyModel[223] = new ModelRendererTurbo(this, 137, 17, textureX, textureY); // Box 260
		bodyModel[224] = new ModelRendererTurbo(this, 73, 81, textureX, textureY); // Box 261
		bodyModel[225] = new ModelRendererTurbo(this, 193, 169, textureX, textureY); // Box 262
		bodyModel[226] = new ModelRendererTurbo(this, 137, 97, textureX, textureY); // Box 263
		bodyModel[227] = new ModelRendererTurbo(this, 249, 129, textureX, textureY); // Box 264
		bodyModel[228] = new ModelRendererTurbo(this, 49, 97, textureX, textureY); // Box 265
		bodyModel[229] = new ModelRendererTurbo(this, 105, 137, textureX, textureY); // Box 266
		bodyModel[230] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 267
		bodyModel[231] = new ModelRendererTurbo(this, 41, 113, textureX, textureY); // Box 268
		bodyModel[232] = new ModelRendererTurbo(this, 1, 161, textureX, textureY); // Box 239
		bodyModel[233] = new ModelRendererTurbo(this, 217, 161, textureX, textureY); // Box 240
		bodyModel[234] = new ModelRendererTurbo(this, 25, 161, textureX, textureY); // Box 273
		bodyModel[235] = new ModelRendererTurbo(this, 121, 169, textureX, textureY); // Box 274
		bodyModel[236] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 275
		bodyModel[237] = new ModelRendererTurbo(this, 25, 177, textureX, textureY); // Box 276
		bodyModel[238] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // Box 114
		bodyModel[239] = new ModelRendererTurbo(this, 137, 129, textureX, textureY); // Box 74
		bodyModel[240] = new ModelRendererTurbo(this, 57, 137, textureX, textureY); // Box 78
		bodyModel[241] = new ModelRendererTurbo(this, 233, 81, textureX, textureY); // Box 113
		bodyModel[242] = new ModelRendererTurbo(this, 89, 137, textureX, textureY); // Box 114
		bodyModel[243] = new ModelRendererTurbo(this, 113, 137, textureX, textureY); // Box 115
		bodyModel[244] = new ModelRendererTurbo(this, 161, 145, textureX, textureY); // Box 116
		bodyModel[245] = new ModelRendererTurbo(this, 249, 145, textureX, textureY); // Box 117
		bodyModel[246] = new ModelRendererTurbo(this, 89, 145, textureX, textureY); // Box 351
		bodyModel[247] = new ModelRendererTurbo(this, 113, 145, textureX, textureY); // Box 352
		bodyModel[248] = new ModelRendererTurbo(this, 41, 153, textureX, textureY); // Box 353
		bodyModel[249] = new ModelRendererTurbo(this, 57, 153, textureX, textureY); // Box 354
		bodyModel[250] = new ModelRendererTurbo(this, 241, 161, textureX, textureY); // Box 355
		bodyModel[251] = new ModelRendererTurbo(this, 105, 169, textureX, textureY); // Box 91
		bodyModel[252] = new ModelRendererTurbo(this, 137, 169, textureX, textureY); // Box 211
		bodyModel[253] = new ModelRendererTurbo(this, 17, 169, textureX, textureY); // Box 214
		bodyModel[254] = new ModelRendererTurbo(this, 153, 169, textureX, textureY); // Box 453
		bodyModel[255] = new ModelRendererTurbo(this, 49, 177, textureX, textureY); // Box 454
		bodyModel[256] = new ModelRendererTurbo(this, 65, 177, textureX, textureY); // Box 89
		bodyModel[257] = new ModelRendererTurbo(this, 81, 177, textureX, textureY); // Box 90
		bodyModel[258] = new ModelRendererTurbo(this, 161, 177, textureX, textureY, "cull"); // Box 262 cull
		bodyModel[259] = new ModelRendererTurbo(this, 177, 177, textureX, textureY, "cull"); // Box 263 cull
		bodyModel[260] = new ModelRendererTurbo(this, 193, 177, textureX, textureY); // Box 336
		bodyModel[261] = new ModelRendererTurbo(this, 217, 177, textureX, textureY); // Box 481
		bodyModel[262] = new ModelRendererTurbo(this, 1, 185, textureX, textureY); // Box 482
		bodyModel[263] = new ModelRendererTurbo(this, 97, 177, textureX, textureY); // Box 350
		bodyModel[264] = new ModelRendererTurbo(this, 145, 153, textureX, textureY); // Box 351
		bodyModel[265] = new ModelRendererTurbo(this, 41, 185, textureX, textureY, "lamp"); // Box 352 firebox
		bodyModel[266] = new ModelRendererTurbo(this, 161, 169, textureX, textureY); // Box 353
		bodyModel[267] = new ModelRendererTurbo(this, 249, 161, textureX, textureY); // Box 354
		bodyModel[268] = new ModelRendererTurbo(this, 57, 185, textureX, textureY); // Box 355
		bodyModel[269] = new ModelRendererTurbo(this, 25, 169, textureX, textureY); // Box 356
		bodyModel[270] = new ModelRendererTurbo(this, 105, 177, textureX, textureY); // Box 357
		bodyModel[271] = new ModelRendererTurbo(this, 81, 185, textureX, textureY); // Box 358
		bodyModel[272] = new ModelRendererTurbo(this, 121, 177, textureX, textureY); // Box 359
		bodyModel[273] = new ModelRendererTurbo(this, 57, 185, textureX, textureY); // Box 360
		bodyModel[274] = new ModelRendererTurbo(this, 73, 185, textureX, textureY); // Box 361
		bodyModel[275] = new ModelRendererTurbo(this, 81, 185, textureX, textureY); // Box 362
		bodyModel[276] = new ModelRendererTurbo(this, 129, 177, textureX, textureY); // Box 363
		bodyModel[277] = new ModelRendererTurbo(this, 105, 185, textureX, textureY); // Box 364
		bodyModel[278] = new ModelRendererTurbo(this, 113, 185, textureX, textureY); // Box 365
		bodyModel[279] = new ModelRendererTurbo(this, 185, 169, textureX, textureY); // Box 366
		bodyModel[280] = new ModelRendererTurbo(this, 177, 65, textureX, textureY); // Box 579
		bodyModel[281] = new ModelRendererTurbo(this, 98, 187, textureX, textureY, "cull"); // Box 259 cull ladder

		bodyModel[0].addBox(0F, 0F, 0F, 5, 5, 2, 0F); // Box 33
		bodyModel[0].setRotationPoint(-16F, 3F, 7F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F); // Box 34
		bodyModel[1].setRotationPoint(-16F, 4.5F, 5.5F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F,0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		bodyModel[2].setRotationPoint(-16F, 4.5F, 8.5F);

		bodyModel[3].addBox(0F, 0F, 0F, 4, 3, 5, 0F); // Box 36
		bodyModel[3].setRotationPoint(-15.5F, 0.5F, 5.5F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		bodyModel[4].setRotationPoint(-15.5F, 3.5F, 6.5F);

		bodyModel[5].addBox(0F, 0F, 0F, 5, 5, 2, 0F); // Box 38
		bodyModel[5].setRotationPoint(-16F, 3F, -9F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F); // Box 39
		bodyModel[6].setRotationPoint(-16F, 4.5F, -10.5F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F,0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, -0.5F, 0F, 1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		bodyModel[7].setRotationPoint(-16F, 4.5F, -7.5F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		bodyModel[8].setRotationPoint(-15.5F, 3.5F, -9.5F);

		bodyModel[9].addBox(0F, 0F, 0F, 4, 3, 5, 0F); // Box 42
		bodyModel[9].setRotationPoint(-15.5F, 0.5F, -10.5F);

		bodyModel[10].addBox(0F, 0F, 0F, 4, 1, 0, 0F); // Box 143
		bodyModel[10].setRotationPoint(-2F, 1.5F, -8.01F);

		bodyModel[11].addBox(0F, 0F, 0F, 0, 1, 5, 0F); // Box 144
		bodyModel[11].setRotationPoint(-2F, 1.5F, -8F);

		bodyModel[12].addBox(0F, 0F, 0F, 4, 1, 0, 0F); // Box 143
		bodyModel[12].setRotationPoint(-2F, 1.5F, 8.01F);

		bodyModel[13].addBox(0F, 0F, 0F, 0, 1, 5, 0F); // Box 144
		bodyModel[13].setRotationPoint(-2F, 1.5F, 3F);

		bodyModel[14].addBox(0F, 0F, 0F, 0, 5, 5, 0F); // Box 414
		bodyModel[14].setRotationPoint(-16.01F, 3F, -10.5F);

		bodyModel[15].addBox(0F, 0F, 0F, 0, 5, 5, 0F); // Box 415
		bodyModel[15].setRotationPoint(-16.01F, 3F, 5.5F);

		bodyModel[16].addBox(0F, -0.5F, 0F, 11, 1, 1, 0F); // Box 128 siderod bit that connects wheels to piston
		bodyModel[16].setRotationPoint(-5F, 5.5F, -7.5F);
		bodyModel[16].rotateAngleZ = -0.27925268F;

		bodyModel[17].addBox(-7F, 0F, 0F, 8, 1, 1, 0F); // Box 130
		bodyModel[17].setRotationPoint(-4F, 6.5F, -8.5F);

		bodyModel[18].addBox(0F, -2F, 0F, 2, 4, 2, 0F); // Box 131 piston back and forth bit
		bodyModel[18].setRotationPoint(-6F, 5.5F, -8.75F);

		bodyModel[19].addBox(-7F, -0.5F, 0F, 7, 1, 1, 0F); // Box 132 piston back and forth bit
		bodyModel[19].setRotationPoint(-4F, 5.5F, -8.5F);

		bodyModel[20].addBox(-7F, 0F, 0F, 8, 1, 1, 0F); // Box 133
		bodyModel[20].setRotationPoint(-4F, 3.5F, -8.5F);

		bodyModel[21].addBox(-6F, 1.5F, 0F, 11, 1, 1, 0F); // Box 128 siderod bit that connects the wheels
		bodyModel[21].setRotationPoint(0.5F, 6.5F, -6.5F);

		bodyModel[22].addShapeBox(1.5F, -0.5F, -3F, 1, 3, 1, 0F,-1F, 0F, -0.5F, 1F, 0F, -0.5F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 129 spinny bar part
		bodyModel[22].setRotationPoint(3F, 6.5F, -5.5F);

		bodyModel[23].addShapeBox(-0.5F, -2F, -3F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 130 reverser swaws
		bodyModel[23].setRotationPoint(0F, 3.5F, -5F);
		bodyModel[23].rotateAngleZ = -0.26179939F;

		bodyModel[24].addShapeBox(-1F, 0F, -4F, 7, 1, 1, 0F,0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0F, 0F, 0F); // Box 131 reverser other swaws
		bodyModel[24].setRotationPoint(0F, 4.5F, -5F);
		bodyModel[24].rotateAngleZ = -0.2268928F;

		bodyModel[25].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 133
		bodyModel[25].setRotationPoint(-11.5F, 0.5F, -8.5F);

		bodyModel[26].addShapeBox(-0.5F, -0.5F, 0F, 1, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 134 bit that does the cha cha slide
		bodyModel[26].setRotationPoint(-7F, 1F, -7.5F);

		bodyModel[27].addShapeBox(-1.5F, -2F, -3F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 149 reverser swaws
		bodyModel[27].setRotationPoint(0F, 3.5F, -5F);
		bodyModel[27].rotateAngleZ = -0.26179939F;

		bodyModel[28].addShapeBox(-0.5F, -0.5F, 0F, 1, 6, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 208 WHAT AREE YOU
		bodyModel[28].setRotationPoint(-8F, 3F, -9.5F);
		bodyModel[28].rotateAngleZ = -0.20943951F;

		bodyModel[29].addShapeBox(-0.5F, -0.5F, 0F, 1, 9, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 209 WHAT DO YOU DOO
		bodyModel[29].setRotationPoint(-8F, 3F, -8.5F);
		bodyModel[29].rotateAngleZ = 1.57079633F;

		bodyModel[30].addBox(0F, -0.5F, 0F, 11, 1, 1, 0F); // Box 128 siderod bit that connects wheels to piston
		bodyModel[30].setRotationPoint(-5F, 5.5F, 6.5F);
		bodyModel[30].rotateAngleZ = -0.27925268F;

		bodyModel[31].addBox(-7F, 0F, 0F, 8, 1, 1, 0F); // Box 130
		bodyModel[31].setRotationPoint(-4F, 6.5F, 7.5F);

		bodyModel[32].addBox(0F, -2F, 0F, 2, 4, 2, 0F); // Box 131 piston back and forth bit
		bodyModel[32].setRotationPoint(-6F, 5.5F, 6.75F);

		bodyModel[33].addBox(-7F, -0.5F, 0F, 7, 1, 1, 0F); // Box 132 piston back and forth bit
		bodyModel[33].setRotationPoint(-4F, 5.5F, 7.5F);

		bodyModel[34].addBox(-7F, 0F, 0F, 8, 1, 1, 0F); // Box 133
		bodyModel[34].setRotationPoint(-4F, 3.5F, 7.5F);

		bodyModel[35].addBox(-6F, 1.5F, 0F, 11, 1, 1, 0F); // Box 128 siderod bit that connects the wheels
		bodyModel[35].setRotationPoint(0.5F, 6.5F, 5.5F);

		bodyModel[36].addShapeBox(1.5F, -0.5F, 2F, 1, 3, 1, 0F,-1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 129 spinny bar part
		bodyModel[36].setRotationPoint(3F, 6.5F, 5.5F);

		bodyModel[37].addShapeBox(-0.5F, -2F, 0F, 1, 4, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 130 reverser swaws
		bodyModel[37].setRotationPoint(0F, 3.5F, 7F);
		bodyModel[37].rotateAngleZ = -0.26179939F;

		bodyModel[38].addShapeBox(-1F, 0F, 2F, 7, 1, 1, 0F,0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 131 reverser other swaws
		bodyModel[38].setRotationPoint(0F, 4.5F, 6F);
		bodyModel[38].rotateAngleZ = -0.2268928F;

		bodyModel[39].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 133
		bodyModel[39].setRotationPoint(-11.5F, 0.5F, 7.5F);

		bodyModel[40].addShapeBox(-0.5F, -0.5F, 0F, 1, 5, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 134 bit that does the cha cha slide
		bodyModel[40].setRotationPoint(-7F, 1F, 6.5F);

		bodyModel[41].addShapeBox(-1.5F, -2F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 149 reverser swaws
		bodyModel[41].setRotationPoint(0F, 3.5F, 7F);
		bodyModel[41].rotateAngleZ = -0.26179939F;

		bodyModel[42].addShapeBox(-0.5F, -0.5F, 0F, 1, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 208 WHAT AREE YOU
		bodyModel[42].setRotationPoint(-8F, 3F, 8.5F);
		bodyModel[42].rotateAngleZ = -0.20943951F;

		bodyModel[43].addShapeBox(-0.5F, -0.5F, 0F, 1, 9, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 209 WHAT DO YOU DOO
		bodyModel[43].setRotationPoint(-8F, 3F, 7.5F);
		bodyModel[43].rotateAngleZ = 1.57079633F;

		bodyModel[44].addBox(-4F, -4F, 0F, 8, 8, 0, 0F); // Box 68 wheelo
		bodyModel[44].setRotationPoint(-6.5F, 6F, -5.5F);

		bodyModel[45].addBox(-1F, -1F, 0F, 2, 2, 11, 0F); // Box 71
		bodyModel[45].setRotationPoint(-6.5F, 6F, -5.5F);

		bodyModel[46].addBox(-4F, -4F, 0F, 8, 8, 0, 0F); // Box 72 wheele
		bodyModel[46].setRotationPoint(-6.5F, 6F, 5.5F);

		bodyModel[47].addBox(-1F, -1F, 0F, 2, 2, 11, 0F); // Box 63
		bodyModel[47].setRotationPoint(3.5F, 6F, -5.5F);

		bodyModel[48].addBox(-4F, -4F, 0F, 8, 8, 0, 0F); // Box 64 wheela
		bodyModel[48].setRotationPoint(3.5F, 6F, -5.5F);

		bodyModel[49].addBox(-4F, -4F, 0F, 8, 8, 0, 0F); // Box 65 wheely
		bodyModel[49].setRotationPoint(3.5F, 6F, 5.5F);

		bodyModel[50].addBox(0F, 0F, 0F, 3, 2, 3, 0F); // Box coupler
		bodyModel[50].setRotationPoint(-27F, 3F, -1.5F);

		bodyModel[51].addBox(0F, 0F, 0F, 3, 1, 6, 0F); // Box 99
		bodyModel[51].setRotationPoint(-26F, 8F, -9F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 1, 5, 4, 0F,0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		bodyModel[52].setRotationPoint(-24F, 1.5F, -2F);

		bodyModel[53].addBox(0F, 0F, 0F, 3, 1, 6, 0F); // Box 113
		bodyModel[53].setRotationPoint(-26F, 8F, 3F);

		bodyModel[54].addBox(0F, 0F, 0F, 0, 5, 18, 0F); // Box 187
		bodyModel[54].setRotationPoint(-23F, 3F, -9F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 10, 4, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		bodyModel[55].setRotationPoint(-19F, -8F, -5F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 10, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		bodyModel[56].setRotationPoint(-19F, -4F, -5F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 10, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 55
		bodyModel[57].setRotationPoint(-19F, -4F, 2F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 10, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		bodyModel[58].setRotationPoint(-19F, -11F, 2F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 10, 3, 3, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		bodyModel[59].setRotationPoint(-19F, -11F, -5F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 10, 3, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		bodyModel[60].setRotationPoint(-19F, -11F, -2F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 20, 3, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		bodyModel[61].setRotationPoint(-19F, -4F, -2F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 110
		bodyModel[62].setRotationPoint(-19.5F, -7.5F, -4F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F); // Box 111
		bodyModel[63].setRotationPoint(-19.5F, -10F, 1.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 112
		bodyModel[64].setRotationPoint(-19.5F, -10F, -1.5F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 113
		bodyModel[65].setRotationPoint(-19.5F, -10F, -3.5F);

		bodyModel[66].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F,0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 114
		bodyModel[66].setRotationPoint(-19.5F, -5F, -3.5F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 116
		bodyModel[67].setRotationPoint(-19.5F, -5F, 1.5F);

		bodyModel[68].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // Box 234
		bodyModel[68].setRotationPoint(-20F, -7.5F, -1.5F);

		bodyModel[69].addBox(0F, 0F, 0F, 0, 10, 10, 0F); // Box 370
		bodyModel[69].setRotationPoint(-19.01F, -11F, -5F);

		bodyModel[70].addBox(0F, 0F, 0F, 3, 9, 3, 0F); // Box 102
		bodyModel[70].setRotationPoint(-17F, -20F, -1.5F);

		bodyModel[71].addBox(0F, 0F, 0F, 3, 3, 3, 0F); // Box 56
		bodyModel[71].setRotationPoint(-21F, -15F, -1.5F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F); // Box 146 headlight front
		bodyModel[72].setRotationPoint(-21.5F, -15F, -1.5F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 2, 2, 0, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 154 headlight glow
		bodyModel[73].setRotationPoint(-20.5F, -14.5F, -1.51F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 2, 2, 0, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 155 headlight glow
		bodyModel[74].setRotationPoint(-20.5F, -14.5F, 1.51F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 181
		bodyModel[75].setRotationPoint(-19.5F, -5F, -1.5F);

		bodyModel[76].addBox(0F, 0F, 0F, 1, 7, 1, 0F); // Box 194
		bodyModel[76].setRotationPoint(-18F, -20F, -1F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F); // Box 195
		bodyModel[77].setRotationPoint(-18F, -13F, -1F);

		bodyModel[78].addBox(0F, 0F, 0F, 3, 1, 2, 0F); // Box 222
		bodyModel[78].setRotationPoint(-20F, -12F, -1F);

		bodyModel[79].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 105
		bodyModel[79].setRotationPoint(-1F, -6.5F, -8F);

		bodyModel[80].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 106
		bodyModel[80].setRotationPoint(1.5F, -6.5F, -8F);

		bodyModel[81].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 107
		bodyModel[81].setRotationPoint(1.5F, -9.5F, -8F);

		bodyModel[82].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 108
		bodyModel[82].setRotationPoint(-1F, -9.5F, -8F);

		bodyModel[83].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 109
		bodyModel[83].setRotationPoint(-0.5F, -7.5F, -7.5F);

		bodyModel[84].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 110
		bodyModel[84].setRotationPoint(2F, -7.5F, -7.5F);

		bodyModel[85].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 188
		bodyModel[85].setRotationPoint(-3F, -9F, -7F);

		bodyModel[86].addShapeBox(0F, -1F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 189
		bodyModel[86].setRotationPoint(-18F, -7F, -6F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 11, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 190
		bodyModel[87].setRotationPoint(-17F, -7F, -6F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 191
		bodyModel[88].setRotationPoint(-6F, -7F, -7F);

		bodyModel[89].addBox(0F, 0F, 0F, 2, 3, 4, 0F); // Box 193
		bodyModel[89].setRotationPoint(-11F, -15F, -2F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 194
		bodyModel[90].setRotationPoint(-9F, -15F, -1F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F); // Box 195
		bodyModel[91].setRotationPoint(-12F, -15F, -1F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 196
		bodyModel[92].setRotationPoint(-9F, -16F, -1F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F); // Box 197
		bodyModel[93].setRotationPoint(-12F, -16F, -1F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 198
		bodyModel[94].setRotationPoint(-11F, -16F, -1F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 199
		bodyModel[95].setRotationPoint(-11F, -12F, -2F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 1F, 0F, 0F); // Box 390
		bodyModel[96].setRotationPoint(-12F, -12F, -1F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 2F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 2F); // Box 391
		bodyModel[97].setRotationPoint(-9F, -12F, -1F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 239
		bodyModel[98].setRotationPoint(-10F, -7F, 5F);

		bodyModel[99].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F); // Box 240
		bodyModel[99].setRotationPoint(-9F, -7F, 5F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 241
		bodyModel[100].setRotationPoint(-3F, -7F, 6F);

		bodyModel[101].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 242
		bodyModel[101].setRotationPoint(2F, -10F, 6F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 1, 3, 18, 0F,0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F); // Box 93
		bodyModel[102].setRotationPoint(-23F, 0F, -9F);

		bodyModel[103].addBox(0F, 0F, 0F, 6, 1, 7, 0F); // Box 120
		bodyModel[103].setRotationPoint(-22F, 0F, -10F);

		bodyModel[104].addBox(0F, 0F, 0F, 30, 3, 6, 0F); // Box 145
		bodyModel[104].setRotationPoint(-22F, 0F, -3F);

		bodyModel[105].addShapeBox(-0.5F, -0.5F, 0F, 1, 1, 10, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 117
		bodyModel[105].setRotationPoint(-18F, -5F, -5F);
		bodyModel[105].rotateAngleZ = 1.06465084F;

		bodyModel[106].addBox(-6.5F, -0.5F, -1F, 6, 1, 1, 0F); // Box 118
		bodyModel[106].setRotationPoint(-18F, -5F, -5F);
		bodyModel[106].rotateAngleZ = 1.06465084F;

		bodyModel[107].addBox(-6.5F, -0.5F, 10F, 6, 1, 1, 0F); // Box 119
		bodyModel[107].setRotationPoint(-18F, -5F, -5F);
		bodyModel[107].rotateAngleZ = 1.06465084F;

		bodyModel[108].addBox(0F, 0F, 0F, 6, 1, 7, 0F); // Box 124
		bodyModel[108].setRotationPoint(-22F, 0F, 3F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F); // Box 175
		bodyModel[109].setRotationPoint(6.5F, 1.5F, 3F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F); // Box 469
		bodyModel[110].setRotationPoint(-6.5F, 2F, 3F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,-0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F); // Box 470
		bodyModel[111].setRotationPoint(-10.5F, 2F, 3F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F); // Box 473
		bodyModel[112].setRotationPoint(3.5F, 2F, 3F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,-0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F); // Box 475
		bodyModel[113].setRotationPoint(-0.5F, 2F, 3F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F); // Box 476
		bodyModel[114].setRotationPoint(6.5F, 1.5F, -4F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F); // Box 477
		bodyModel[115].setRotationPoint(3.5F, 2F, -4F);

		bodyModel[116].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,-0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F); // Box 478
		bodyModel[116].setRotationPoint(-0.5F, 2F, -4F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F); // Box 481
		bodyModel[117].setRotationPoint(-6.5F, 2F, -4F);

		bodyModel[118].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,-0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F); // Box 482
		bodyModel[118].setRotationPoint(-10.5F, 2F, -4F);

		bodyModel[119].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,-0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F); // Box 137
		bodyModel[119].setRotationPoint(-0.5F, 1.5F, 3F);

		bodyModel[120].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,-0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F); // Box 138
		bodyModel[120].setRotationPoint(-0.5F, 1.5F, -4F);

		bodyModel[121].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,-0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F); // Box 139
		bodyModel[121].setRotationPoint(-10.5F, 1.5F, 3F);

		bodyModel[122].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,-0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F); // Box 140
		bodyModel[122].setRotationPoint(-10.5F, 1.5F, -4F);

		bodyModel[123].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F); // Box 141
		bodyModel[123].setRotationPoint(-3.5F, 1.5F, -4F);

		bodyModel[124].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0F, 0F, -0.05F, -0.05F, 0F, -0.05F, -0.05F, 0F, -0.05F, 0F, 0F, -0.05F, 1F, 0F, -0.05F, -1.05F, 0F, -0.05F, -1.05F, 0F, -0.05F, 1F, 0F, -0.05F); // Box 142
		bodyModel[124].setRotationPoint(-3.5F, 1.5F, 3F);

		bodyModel[125].addShapeBox(0F, 0F, 0F, 1, 5, 4, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		bodyModel[125].setRotationPoint(-23F, 1.5F, -2F);

		bodyModel[126].addBox(0F, 0F, 0F, 5, 17, 12, 0F); // Box 144
		bodyModel[126].setRotationPoint(8F, -9F, -6F);

		bodyModel[127].addBox(0F, 0F, 0F, 5, 5, 5, 0F); // Box 75
		bodyModel[127].setRotationPoint(-16F, -4F, -2.5F);

		bodyModel[128].addBox(0F, 0F, 0F, 8, 16, 1, 0F); // Box 16
		bodyModel[128].setRotationPoint(7F, -16F, 9F);

		bodyModel[129].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // Box 17
		bodyModel[129].setRotationPoint(6F, -19F, -2F);

		bodyModel[130].addBox(0F, 0F, 0F, 8, 16, 1, 0F); // Box 19
		bodyModel[130].setRotationPoint(7F, -16F, -10F);

		bodyModel[131].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		bodyModel[131].setRotationPoint(6F, -17F, 9F);

		bodyModel[132].addShapeBox(0F, 0F, 0F, 18, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		bodyModel[132].setRotationPoint(6F, -18F, 7F);

		bodyModel[133].addShapeBox(0F, 0F, 0F, 18, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		bodyModel[133].setRotationPoint(6F, -19F, 2F);

		bodyModel[134].addShapeBox(0F, 0F, 0F, 18, 1, 5, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		bodyModel[134].setRotationPoint(6F, -19F, -7F);

		bodyModel[135].addShapeBox(0F, 0F, 0F, 18, 1, 2, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		bodyModel[135].setRotationPoint(6F, -18F, -9F);

		bodyModel[136].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		bodyModel[136].setRotationPoint(6F, -17F, -10F);

		bodyModel[137].addBox(0F, 0F, 0F, 1, 16, 3, 0F); // Box 28
		bodyModel[137].setRotationPoint(21F, -16F, -10F);

		bodyModel[138].addBox(0F, 0F, 0F, 1, 17, 18, 0F); // Box 30
		bodyModel[138].setRotationPoint(7F, -17F, -9F);

		bodyModel[139].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 31
		bodyModel[139].setRotationPoint(6F, -17F, -9F);

		bodyModel[140].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 32
		bodyModel[140].setRotationPoint(6F, -18F, -7F);

		bodyModel[141].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		bodyModel[141].setRotationPoint(6F, -18F, 2F);

		bodyModel[142].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		bodyModel[142].setRotationPoint(6F, -17F, 7F);

		bodyModel[143].addBox(0F, 0F, 0F, 1, 1, 14, 0F); // Box 170
		bodyModel[143].setRotationPoint(21F, -18F, -7F);

		bodyModel[144].addBox(0F, 0F, 0F, 1, 1, 18, 0F); // Box 171
		bodyModel[144].setRotationPoint(21F, -17F, -9F);

		bodyModel[145].addShapeBox(-5.5F, 0F, -0.5F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 329 door swing right
		bodyModel[145].setRotationPoint(20.5F, -8F, -9.5F);

		bodyModel[146].addShapeBox(-5.5F, 0F, -0.5F, 6, 8, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 332 door swing left
		bodyModel[146].setRotationPoint(20.5F, -8F, 9.5F);

		bodyModel[147].addBox(0F, 0F, 0F, 1, 1, 14, 0F); // Box 35
		bodyModel[147].setRotationPoint(7F, -18F, -7F);

		bodyModel[148].addShapeBox(0F, -0.5F, 0F, 4, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 189
		bodyModel[148].setRotationPoint(10F, -19F, -2F);
		bodyModel[148].rotateAngleZ = 0.27925268F;

		bodyModel[149].addBox(0F, 0F, 0F, 28, 1, 20, 0F); // Box 168
		bodyModel[149].setRotationPoint(7F, 0F, -10F);

		bodyModel[150].addBox(0F, 0F, 0F, 11, 10, 20, 0F); // Box 222
		bodyModel[150].setRotationPoint(22F, -10F, -10F);

		bodyModel[151].addBox(0F, 0F, 0F, 9, 2, 1, 0F); // Box 223
		bodyModel[151].setRotationPoint(22F, -12F, -10F);

		bodyModel[152].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 225
		bodyModel[152].setRotationPoint(31F, -12F, -10F);

		bodyModel[153].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 226
		bodyModel[153].setRotationPoint(31F, -12F, 9F);

		bodyModel[154].addBox(0F, 0F, 0F, 9, 2, 1, 0F); // Box 227
		bodyModel[154].setRotationPoint(22F, -12F, 9F);

		bodyModel[155].addShapeBox(0F, 0F, 0F, 1, 10, 18, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 25
		bodyModel[155].setRotationPoint(33F, -10F, -9F);

		bodyModel[156].addBox(0F, 0F, 0F, 4, 3, 4, 0F); // Box 35
		bodyModel[156].setRotationPoint(25F, -12F, 0F);

		bodyModel[157].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 41
		bodyModel[157].setRotationPoint(30F, -11F, -1F);

		bodyModel[158].addBox(0F, 0F, 0F, 1, 5, 10, 0F); // Box 95
		bodyModel[158].setRotationPoint(21F, -6.5F, -3F);

		bodyModel[159].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Box 96
		bodyModel[159].setRotationPoint(21F, -10.5F, -2F);

		bodyModel[160].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 97
		bodyModel[160].setRotationPoint(21F, -10.5F, -1F);

		bodyModel[161].addBox(0F, 0F, 0F, 6, 1, 1, 0F); // Box 98
		bodyModel[161].setRotationPoint(22F, -10.5F, 1.5F);

		bodyModel[162].addBox(0F, 0F, 0F, 1, 16, 3, 0F); // Box 199
		bodyModel[162].setRotationPoint(21F, -16F, 7F);

		bodyModel[163].addShapeBox(0F, 0F, 0F, 16, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 200
		bodyModel[163].setRotationPoint(8F, -18F, -7F);

		bodyModel[164].addShapeBox(0F, 0F, 0F, 16, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 201
		bodyModel[164].setRotationPoint(8F, -17F, -9F);

		bodyModel[165].addShapeBox(0F, 0F, 0F, 16, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 202
		bodyModel[165].setRotationPoint(8F, -18F, 2F);

		bodyModel[166].addShapeBox(0F, 0F, 0F, 16, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 203
		bodyModel[166].setRotationPoint(8F, -17F, 7F);

		bodyModel[167].addShapeBox(0F, 0F, 0F, 10, 1, 6, 0F,0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 130
		bodyModel[167].setRotationPoint(-9F, -12F, -3F);

		bodyModel[168].addShapeBox(0F, 0F, 0F, 10, 6, 1, 0F,0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 132
		bodyModel[168].setRotationPoint(-9F, -9F, -6F);

		bodyModel[169].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F,0F, -2F, 3F, 0F, -2F, 2F, 0F, -1F, -3F, 0F, -2F, -4F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -1F); // Box 134
		bodyModel[169].setRotationPoint(-9F, -13F, 5F);

		bodyModel[170].addShapeBox(0F, 0F, 0F, 10, 6, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F); // Box 135
		bodyModel[170].setRotationPoint(-9F, -9F, 5F);

		bodyModel[171].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F,0F, -2F, -4F, 0F, -1F, -3F, 0F, -2F, 2F, 0F, -2F, 3F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 94
		bodyModel[171].setRotationPoint(-9F, -13F, -6F);

		bodyModel[172].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -1F, -3F, 0F, -1F, -4F); // Box 96
		bodyModel[172].setRotationPoint(-9F, -4F, 5F);

		bodyModel[173].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1F, -4F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, -1F, 3F); // Box 244
		bodyModel[173].setRotationPoint(-9F, -4F, -6F);

		bodyModel[174].addBox(0F, 0F, 0F, 12, 3, 6, 0F); // Box 97
		bodyModel[174].setRotationPoint(1F, -12F, -3F);

		bodyModel[175].addShapeBox(0F, 0F, 0F, 12, 3, 3, 0F,0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 101
		bodyModel[175].setRotationPoint(1F, -12F, -6F);

		bodyModel[176].addShapeBox(0F, 0F, 0F, 12, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		bodyModel[176].setRotationPoint(1F, -12F, 3F);

		bodyModel[177].addBox(0F, 0F, 0F, 7, 6, 12, 0F); // Box 112
		bodyModel[177].setRotationPoint(1F, -9F, -6F);

		bodyModel[178].addShapeBox(0F, 0F, 0F, 7, 3, 8, 0F,0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 116
		bodyModel[178].setRotationPoint(1F, -3F, -4F);

		bodyModel[179].addShapeBox(0F, 0F, 0F, 1, 5, 4, 0F,0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		bodyModel[179].setRotationPoint(36F, 1.5F, -2F);

		bodyModel[180].addShapeBox(0F, 0F, 0F, 1, 5, 4, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[180].setRotationPoint(35F, 1.5F, -2F);

		bodyModel[181].addBox(0F, 0F, 0F, 3, 2, 3, 0F); // Box coupler
		bodyModel[181].setRotationPoint(37F, 3F, -1.5F);

		bodyModel[182].addBox(0F, 0F, 0F, 3, 1, 6, 0F); // Box 10
		bodyModel[182].setRotationPoint(36F, 8F, -9F);

		bodyModel[183].addShapeBox(0F, 0F, 0F, 1, 3, 18, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 11
		bodyModel[183].setRotationPoint(35F, 0F, -9F);

		bodyModel[184].addBox(0F, 0F, 0F, 3, 1, 6, 0F); // Box 119
		bodyModel[184].setRotationPoint(36F, 8F, 3F);

		bodyModel[185].addBox(0F, 0F, 0F, 0, 5, 18, 0F); // Box 186
		bodyModel[185].setRotationPoint(36F, 3F, -9F);

		bodyModel[186].addBox(0F, 0F, 0F, 22, 2, 10, 0F); // Box 223
		bodyModel[186].setRotationPoint(13F, 1F, -5F);

		bodyModel[187].addBox(0F, 0F, 0F, 5, 1, 6, 0F); // Box 224
		bodyModel[187].setRotationPoint(2F, -2F, -10F);

		bodyModel[188].addBox(0F, 0F, 0F, 5, 1, 6, 0F); // Box 225
		bodyModel[188].setRotationPoint(2F, -2F, 4F);

		bodyModel[189].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // Box 226
		bodyModel[189].setRotationPoint(-16F, -4F, 4F);

		bodyModel[190].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // Box 227
		bodyModel[190].setRotationPoint(-16F, -4F, -10F);

		bodyModel[191].addBox(-1F, -1F, 0F, 2, 2, 2, 0F); // Box 201
		bodyModel[191].setRotationPoint(-4.5F, -13F, -1F);
		bodyModel[191].rotateAngleZ = 0.78539816F;

		bodyModel[192].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 202
		bodyModel[192].setRotationPoint(-4.5F, -18.5F, -0.5F);

		bodyModel[193].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Box 229
		bodyModel[193].setRotationPoint(-5F, -12F, -1F);

		bodyModel[194].addBox(0F, 0F, 0F, 3, 3, 3, 0F); // Box 283
		bodyModel[194].setRotationPoint(30.5F, -14F, -6.5F);

		bodyModel[195].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F); // Box 284 headlight rear
		bodyModel[195].setRotationPoint(33F, -14F, -6.5F);

		bodyModel[196].addBox(0F, 0F, 0F, 3, 1, 2, 0F); // Box 285
		bodyModel[196].setRotationPoint(29.5F, -11F, -6F);

		bodyModel[197].addShapeBox(0F, 0F, 0F, 2, 2, 0, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 154 headlight glow
		bodyModel[197].setRotationPoint(31F, -13.5F, -6.51F);

		bodyModel[198].addShapeBox(0F, 0F, 0F, 2, 2, 0, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 155 headlight glow
		bodyModel[198].setRotationPoint(31F, -13.5F, -3.49F);

		bodyModel[199].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 315
		bodyModel[199].setRotationPoint(-15F, -2.5F, 7F);

		bodyModel[200].addBox(0F, 0F, 0F, 2, 2, 14, 0F); // Box 316
		bodyModel[200].setRotationPoint(-15F, -2.5F, -7F);

		bodyModel[201].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 238
		bodyModel[201].setRotationPoint(-15F, -2.5F, -9F);

		bodyModel[202].addBox(0F, 0F, 0F, 2, 1, 2, 0F); // Box 239
		bodyModel[202].setRotationPoint(-15F, -0.5F, -9F);

		bodyModel[203].addBox(0F, 0F, 0F, 2, 1, 2, 0F); // Box 240
		bodyModel[203].setRotationPoint(-15F, -0.5F, 7F);

		bodyModel[204].addBox(-1.5F, -1.5F, 0F, 3, 3, 10, 0F); // Box 347
		bodyModel[204].setRotationPoint(-1F, -1.5F, -6F);
		bodyModel[204].rotateAngleY = 1.57079633F;
		bodyModel[204].rotateAngleZ = 0.78539816F;

		bodyModel[205].addBox(-1.5F, -1.5F, 0F, 3, 3, 10, 0F); // Box 242e
		bodyModel[205].setRotationPoint(-1F, -1.5F, 6F);
		bodyModel[205].rotateAngleY = 1.57079633F;
		bodyModel[205].rotateAngleZ = 0.78539816F;

		bodyModel[206].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 243
		bodyModel[206].setRotationPoint(2F, -15F, -2F);

		bodyModel[207].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 1F, 0F, 0F); // Box 244
		bodyModel[207].setRotationPoint(1F, -13F, -1F);

		bodyModel[208].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 245
		bodyModel[208].setRotationPoint(2F, -13F, -2F);

		bodyModel[209].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 2F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 2F); // Box 246
		bodyModel[209].setRotationPoint(4F, -13F, -1F);

		bodyModel[210].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 247
		bodyModel[210].setRotationPoint(4F, -15F, -1F);

		bodyModel[211].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 248
		bodyModel[211].setRotationPoint(4F, -16F, -1F);

		bodyModel[212].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 249
		bodyModel[212].setRotationPoint(2F, -16F, -1F);

		bodyModel[213].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F); // Box 250
		bodyModel[213].setRotationPoint(1F, -16F, -1F);

		bodyModel[214].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F); // Box 251
		bodyModel[214].setRotationPoint(1F, -15F, -1F);

		bodyModel[215].addBox(0F, 0F, 0F, 30, 1, 1, 0F); // Box 400
		bodyModel[215].setRotationPoint(1F, 0.5F, -9F);

		bodyModel[216].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 597
		bodyModel[216].setRotationPoint(5F, 0.5F, -8.5F);
		bodyModel[216].rotateAngleX = -0.78539816F;

		bodyModel[217].addBox(0F, 0F, 0F, 0, 3, 6, 0F); // Box 254
		bodyModel[217].setRotationPoint(-16F, -3F, 4F);

		bodyModel[218].addBox(0F, 0F, 0F, 0, 3, 6, 0F); // Box 255
		bodyModel[218].setRotationPoint(-16F, -3F, -10F);

		bodyModel[219].addBox(0F, 0F, 0F, 23, 2, 1, 0F); // Box 457
		bodyModel[219].setRotationPoint(-15F, 5.25F, -5F);

		bodyModel[220].addBox(0F, 0F, 0F, 23, 2, 1, 0F); // Box 458
		bodyModel[220].setRotationPoint(-15F, 5.25F, 4F);

		bodyModel[221].addBox(0F, 0F, 0F, 1, 2, 8, 0F); // Box 258
		bodyModel[221].setRotationPoint(-15F, 5.25F, -4F);

		bodyModel[222].addBox(0F, 0F, 0F, 0, 1, 6, 0F); // Box 259
		bodyModel[222].setRotationPoint(2F, -3F, 4F);

		bodyModel[223].addBox(0F, 0F, 0F, 0, 1, 6, 0F); // Box 260
		bodyModel[223].setRotationPoint(2F, -3F, -10F);

		bodyModel[224].addBox(0F, 0F, -1F, 1, 2, 1, 0F); // Box 261
		bodyModel[224].setRotationPoint(5F, 0.5F, 8.5F);
		bodyModel[224].rotateAngleX = 0.78539816F;

		bodyModel[225].addBox(0F, 0F, 0F, 30, 1, 1, 0F); // Box 262
		bodyModel[225].setRotationPoint(1F, 0.5F, 8F);

		bodyModel[226].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 263
		bodyModel[226].setRotationPoint(-1F, -2F, 5.5F);

		bodyModel[227].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 264
		bodyModel[227].setRotationPoint(0F, -2F, 6.5F);

		bodyModel[228].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F); // Box 265
		bodyModel[228].setRotationPoint(0F, -1F, 8F);

		bodyModel[229].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 266
		bodyModel[229].setRotationPoint(-1F, -2F, -6.5F);

		bodyModel[230].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 267
		bodyModel[230].setRotationPoint(0F, -2F, -8.5F);

		bodyModel[231].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F); // Box 268
		bodyModel[231].setRotationPoint(0F, -1F, -9F);

		bodyModel[232].addBox(0F, 0F, 0F, 9, 0, 2, 0F); // Box 239
		bodyModel[232].setRotationPoint(-18F, -10F, 3F);
		bodyModel[232].rotateAngleX = 0.78539816F;

		bodyModel[233].addBox(0F, 0F, -2F, 9, 0, 2, 0F); // Box 240
		bodyModel[233].setRotationPoint(-18F, -10F, -3F);
		bodyModel[233].rotateAngleX = -0.78539816F;

		bodyModel[234].addBox(0F, 0F, 0F, 7, 0, 2, 0F); // Box 273
		bodyModel[234].setRotationPoint(1F, -11F, 4F);
		bodyModel[234].rotateAngleX = 0.78539816F;

		bodyModel[235].addBox(0F, 0F, -2F, 7, 0, 2, 0F); // Box 274
		bodyModel[235].setRotationPoint(1F, -11F, -4F);
		bodyModel[235].rotateAngleX = -0.78539816F;

		bodyModel[236].addShapeBox(0F, 0F, 0F, 10, 0, 2, 0F,0F, 0F, 0F, 0F, 0F, -1.4F, 0F, 0F, 1.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.4F, 0F, 0F, 1.4F, 0F, 0F, 0F); // Box 275
		bodyModel[236].setRotationPoint(-9F, -10F, 3F);
		bodyModel[236].rotateAngleX = 0.78539816F;

		bodyModel[237].addShapeBox(0F, 0F, -2F, 10, 0, 2, 0F,0F, 0F, 0F, 0F, 0F, 1.4F, 0F, 0F, -1.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.4F, 0F, 0F, -1.4F, 0F, 0F, 0F); // Box 276
		bodyModel[237].setRotationPoint(-9F, -10F, -3F);
		bodyModel[237].rotateAngleX = -0.78539816F;

		bodyModel[238].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0.5F, 0.5F, 0F, 0.5F, 0.5F, 0F, 0.5F, 0.5F, 0F, 0.5F); // Box 114
		bodyModel[238].setRotationPoint(-2F, -15F, -0.5F);

		bodyModel[239].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.2F, 0F, -0.2F, -0.2F, 0F, -0.2F, -0.2F, 0F, -0.2F, -0.2F, 0F, -0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74
		bodyModel[239].setRotationPoint(-2F, -16F, -0.5F);

		bodyModel[240].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 78
		bodyModel[240].setRotationPoint(-1.75F, -14.5F, -0.25F);

		bodyModel[241].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 113
		bodyModel[241].setRotationPoint(-2F, -13F, -2F);

		bodyModel[242].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 114
		bodyModel[242].setRotationPoint(-2F, -16F, -2F);

		bodyModel[243].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 115
		bodyModel[243].setRotationPoint(-2F, -16F, 1F);

		bodyModel[244].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 116
		bodyModel[244].setRotationPoint(-2F, -16.5F, -1F);

		bodyModel[245].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Box 117
		bodyModel[245].setRotationPoint(-2F, -12.5F, -1F);

		bodyModel[246].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 351
		bodyModel[246].setRotationPoint(-8F, -16F, 1F);

		bodyModel[247].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 352
		bodyModel[247].setRotationPoint(-8F, -16F, -1F);

		bodyModel[248].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 353
		bodyModel[248].setRotationPoint(-8F, -13F, 0F);

		bodyModel[249].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 354
		bodyModel[249].setRotationPoint(-8F, -18F, -2.5F);

		bodyModel[250].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 355
		bodyModel[250].setRotationPoint(-10F, -13F, -2.5F);

		bodyModel[251].addBox(0F, 0F, 0F, 1, 1, 11, 0F); // Box 91
		bodyModel[251].setRotationPoint(-19.5F, 7F, -5.5F);

		bodyModel[252].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 211
		bodyModel[252].setRotationPoint(-20F, 6.25F, -4.5F);

		bodyModel[253].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // Box 214
		bodyModel[253].setRotationPoint(-14F, 5.25F, -1.5F);

		bodyModel[254].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F); // Box 453
		bodyModel[254].setRotationPoint(-18F, 7.25F, 0.5F);

		bodyModel[255].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 454
		bodyModel[255].setRotationPoint(-18F, 7.25F, -1.5F);

		bodyModel[256].addBox(-2.5F, -2.5F, 0F, 5, 5, 0, 0F); // Box 89
		bodyModel[256].setRotationPoint(-19F, 7.5F, -5.5F);

		bodyModel[257].addBox(-2.5F, -2.5F, 0F, 5, 5, 0, 0F); // Box 90
		bodyModel[257].setRotationPoint(-19F, 7.5F, 5.5F);

		bodyModel[258].addBox(0F, 0F, 0F, 4, 6, 1, 0F); // Box 262 cull
		bodyModel[258].setRotationPoint(-21F, 1F, -4F);

		bodyModel[259].addBox(0F, 0F, 0F, 4, 6, 1, 0F); // Box 263 cull
		bodyModel[259].setRotationPoint(-21F, 1F, 3F);

		bodyModel[260].addBox(0F, 0F, 0F, 4, 1, 4, 0F); // Box 336
		bodyModel[260].setRotationPoint(21.5F, 2.5F, -2F);

		bodyModel[261].addBox(0F, 0F, 0F, 16, 2, 3, 0F); // Box 481
		bodyModel[261].setRotationPoint(16F, 0.5F, -8F);

		bodyModel[262].addBox(0F, 0F, 0F, 16, 2, 3, 0F); // Box 482
		bodyModel[262].setRotationPoint(16F, 0.5F, 5F);

		bodyModel[263].addShapeBox(-0.5F, -7F, -0.5F, 1, 7, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 350
		bodyModel[263].setRotationPoint(14.5F, -2F, 8F);
		bodyModel[263].rotateAngleZ = -0.2268928F;

		bodyModel[264].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F); // Box 351
		bodyModel[264].setRotationPoint(14F, -2F, 7.5F);

		bodyModel[265].addShapeBox(0F, 0F, 0F, 1, 4, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 352 firebox
		bodyModel[265].setRotationPoint(13F, -4F, -2F);

		bodyModel[266].addShapeBox(-0.5F, -1F, -3.5F, 1, 2, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 353
		bodyModel[266].setRotationPoint(8F, -15F, 1F);
		bodyModel[266].rotateAngleY = 0.54105207F;

		bodyModel[267].addShapeBox(-0.5F, 0F, -0.5F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 354
		bodyModel[267].setRotationPoint(8.5F, -16F, 2.5F);
		bodyModel[267].rotateAngleY = 0.01745329F;

		bodyModel[268].addShapeBox(-0.5F, 0F, -6.5F, 1, 4, 7, 0F,0.5F, 0F, -3F, -0.5F, 0F, -3F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 355
		bodyModel[268].setRotationPoint(8.5F, -14F, 5F);
		bodyModel[268].rotateAngleY = 0.61086524F;

		bodyModel[269].addShapeBox(-0.5F, -2F, -1F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 356
		bodyModel[269].setRotationPoint(8F, -15F, 1F);
		bodyModel[269].rotateAngleY = 0.54105207F;

		bodyModel[270].addShapeBox(-0.5F, -2F, -3F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 357
		bodyModel[270].setRotationPoint(8F, -15F, 1F);
		bodyModel[270].rotateAngleY = 0.54105207F;

		bodyModel[271].addShapeBox(-0.5F, 0F, -0.5F, 1, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 358
		bodyModel[271].setRotationPoint(15F, -8F, 2F);
		bodyModel[271].rotateAngleX = 0.34906585F;
		bodyModel[271].rotateAngleY = 0.29670597F;

		bodyModel[272].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 359
		bodyModel[272].setRotationPoint(13F, -8F, 1F);

		bodyModel[273].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 360
		bodyModel[273].setRotationPoint(13F, -7.5F, -4F);

		bodyModel[274].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 361
		bodyModel[274].setRotationPoint(13F, -7.5F, -2.5F);

		bodyModel[275].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 362
		bodyModel[275].setRotationPoint(14F, -7.5F, -3.5F);

		bodyModel[276].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 363
		bodyModel[276].setRotationPoint(12.5F, -6.5F, -3F);

		bodyModel[277].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 364
		bodyModel[277].setRotationPoint(12.5F, -8F, -1F);

		bodyModel[278].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F); // Box 365
		bodyModel[278].setRotationPoint(14F, -5F, 4F);

		bodyModel[279].addShapeBox(-0.5F, 0F, -3.5F, 1, 1, 4, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 366
		bodyModel[279].setRotationPoint(14.5F, -6F, 4.5F);
		bodyModel[279].rotateAngleY = 0.62831853F;

		bodyModel[280].addBox(-0.5F, -4F, -1F, 0, 6, 1, 0F); // Box 579
		bodyModel[280].setRotationPoint(15F, -14F, 5F);
		bodyModel[280].rotateAngleY = 0.64577182F;

		bodyModel[281].addBox(0F, 0F, 0F, 1, 10, 4, 0F); // Box 259 cull ladder
		bodyModel[281].setRotationPoint(34F, -10.5F, -2F);
	}
	ModelF01_bogie theTrucc = new ModelF01_bogie();
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		for (int i = 0; i < 282; i++) {
			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("lamp")) {
				Minecraft.getMinecraft().entityRenderer.disableLightmap(1D);
				bodyModel[i].render(f5);
				Minecraft.getMinecraft().entityRenderer.enableLightmap(1D);
			}else if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("cull")) {
				GL11.glDisable(GL11.GL_CULL_FACE);
				bodyModel[i].render(f5);
				GL11.glEnable(GL11.GL_CULL_FACE);
			} else {
				bodyModel[i].render(f5);
			}
		}

			Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/F01_bogie_texture.png"));
			GL11.glPushMatrix();
			//GL11.glTranslated(-0.9, -0.00, 0);
			//theTrucc.render(entity, f, f1, f2, f3, f4, f5);

			GL11.glTranslated(1.5, 0, 0);
			theTrucc.render(entity, f, f1, f2, f3, f4, f5);
			GL11.glPopMatrix();
	}

	public float[] getTrans() {
		return new float[]{-1.25F, 0.15F, 0.00F};
	}

	public float[] getScale() {
		return null;
	}

	public ArrayList<double[]> getSmokePosition() {
		return new ArrayList<double[]>() {
			{
				add(new double[]{2.2D, 1.4D, 0.0D});
			}
		};
	}
}