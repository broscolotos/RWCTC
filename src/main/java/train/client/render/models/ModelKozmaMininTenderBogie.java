//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 27.12.2021 - 16:20:26
// Last changed on: 27.12.2021 - 16:20:26

package train.client.render.models;

import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

public class ModelKozmaMininTenderBogie extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 64;

	public ModelKozmaMininTenderBogie() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[19];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 84
		bodyModel[1] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // Box 156
		bodyModel[2] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Box 172
		bodyModel[3] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 181
		bodyModel[4] = new ModelRendererTurbo(this, 97, 1, textureX, textureY); // Box 182
		bodyModel[5] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // Box 183
		bodyModel[6] = new ModelRendererTurbo(this, 129, 1, textureX, textureY); // Box 184
		bodyModel[7] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Box 185
		bodyModel[8] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 186
		bodyModel[9] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Box 187
		bodyModel[10] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 188
		bodyModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 189
		bodyModel[12] = new ModelRendererTurbo(this, 129, 1, textureX, textureY); // Box 190
		bodyModel[13] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // Box 191
		bodyModel[14] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 192
		bodyModel[15] = new ModelRendererTurbo(this, 273, 1, textureX, textureY); // Box 193
		bodyModel[16] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 194
		bodyModel[17] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 195
		bodyModel[18] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Box 196

		bodyModel[0].addShapeBox(-2F, 0F, 0F, 1, 1, 14, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 84
		bodyModel[0].setRotationPoint(-4.5F, 6.5F, -7F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 156
		bodyModel[1].setRotationPoint(-10F, 4F, -7F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 172
		bodyModel[2].setRotationPoint(2F, 4F, -7F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 181
		bodyModel[3].setRotationPoint(-10F, 4F, 6F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 182
		bodyModel[4].setRotationPoint(2F, 4F, 6F);

		bodyModel[5].addShapeBox(-2F, 0F, 0F, 1, 1, 14, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 183
		bodyModel[5].setRotationPoint(7.5F, 6.5F, -7F);

		bodyModel[6].addShapeBox(-2F, 0F, 0F, 3, 1, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 184
		bodyModel[6].setRotationPoint(0.5F, 2.5F, -6F);

		bodyModel[7].addShapeBox(-2F, 0F, 0F, 1, 3, 12, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F); // Box 185
		bodyModel[7].setRotationPoint(12.5F, 4.5F, -6F);

		bodyModel[8].addShapeBox(-2F, 0F, 0F, 1, 3, 12, 0F,-0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F); // Box 186
		bodyModel[8].setRotationPoint(-9.5F, 4.5F, -6F);

		bodyModel[9].addShapeBox(-2F, 0F, 0F, 21, 3, 1, 0F,0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 187
		bodyModel[9].setRotationPoint(-8.5F, 2.5F, -7F);

		bodyModel[10].addShapeBox(-2F, 0F, 0F, 21, 3, 1, 0F,0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F); // Box 188
		bodyModel[10].setRotationPoint(-8.5F, 2.5F, 6F);

		bodyModel[11].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 189
		bodyModel[11].setRotationPoint(-3.5F, 5.5F, -7F);

		bodyModel[12].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 190
		bodyModel[12].setRotationPoint(-5.5F, 5.5F, -7F);

		bodyModel[13].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 191
		bodyModel[13].setRotationPoint(6.5F, 5.5F, -7F);

		bodyModel[14].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 192
		bodyModel[14].setRotationPoint(8.5F, 5.5F, -7F);

		bodyModel[15].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 193
		bodyModel[15].setRotationPoint(6.5F, 5.5F, 5F);

		bodyModel[16].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 194
		bodyModel[16].setRotationPoint(8.5F, 5.5F, 5F);

		bodyModel[17].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 195
		bodyModel[17].setRotationPoint(-3.5F, 5.5F, 5F);

		bodyModel[18].addShapeBox(-2F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 196
		bodyModel[18].setRotationPoint(-5.5F, 5.5F, 5F);
	}
}