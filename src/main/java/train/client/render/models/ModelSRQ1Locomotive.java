//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 19.07.2022 - 23:23:30
// Last changed on: 19.07.2022 - 23:23:30

package train.client.render.models; //Path where the model is located

import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

public class ModelSRQ1Locomotive extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 128;

	public ModelSRQ1Locomotive() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[84];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 3
		bodyModel[1] = new ModelRendererTurbo(this, 217, 1, textureX, textureY); // Box 4
		bodyModel[2] = new ModelRendererTurbo(this, 249, 1, textureX, textureY); // Box 6
		bodyModel[3] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 10
		bodyModel[4] = new ModelRendererTurbo(this, 321, 1, textureX, textureY); // Box 15
		bodyModel[5] = new ModelRendererTurbo(this, 393, 1, textureX, textureY); // Box 26
		bodyModel[6] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 27
		bodyModel[7] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Box 28
		bodyModel[8] = new ModelRendererTurbo(this, 321, 9, textureX, textureY); // Box 29
		bodyModel[9] = new ModelRendererTurbo(this, 361, 9, textureX, textureY); // Box 30
		bodyModel[10] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 31
		bodyModel[11] = new ModelRendererTurbo(this, 265, 1, textureX, textureY); // Box 33
		bodyModel[12] = new ModelRendererTurbo(this, 385, 9, textureX, textureY); // Box 35
		bodyModel[13] = new ModelRendererTurbo(this, 162, 18, textureX, textureY); // Box 36
		bodyModel[14] = new ModelRendererTurbo(this, 193, 17, textureX, textureY); // Box 59
		bodyModel[15] = new ModelRendererTurbo(this, 337, 9, textureX, textureY); // Box 31
		bodyModel[16] = new ModelRendererTurbo(this, 209, 17, textureX, textureY); // Box 138
		bodyModel[17] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // Box 152
		bodyModel[18] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 163
		bodyModel[19] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 167
		bodyModel[20] = new ModelRendererTurbo(this, 233, 17, textureX, textureY); // Box 142
		bodyModel[21] = new ModelRendererTurbo(this, 137, 9, textureX, textureY); // Box 147
		bodyModel[22] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 153
		bodyModel[23] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 164
		bodyModel[24] = new ModelRendererTurbo(this, 321, 1, textureX, textureY); // Box 167
		bodyModel[25] = new ModelRendererTurbo(this, 209, 17, textureX, textureY); // Box 168
		bodyModel[26] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 170
		bodyModel[27] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 171
		bodyModel[28] = new ModelRendererTurbo(this, 9, 17, textureX, textureY); // Box 177
		bodyModel[29] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 182
		bodyModel[30] = new ModelRendererTurbo(this, 385, 1, textureX, textureY); // Box 183
		bodyModel[31] = new ModelRendererTurbo(this, 410, 26, textureX, textureY); // Box 126
		bodyModel[32] = new ModelRendererTurbo(this, 137, 17, textureX, textureY); // Box 126
		bodyModel[33] = new ModelRendererTurbo(this, 153, 17, textureX, textureY); // Box 126
		bodyModel[34] = new ModelRendererTurbo(this, 289, 17, textureX, textureY); // Box 126
		bodyModel[35] = new ModelRendererTurbo(this, 313, 17, textureX, textureY); // Box 126
		bodyModel[36] = new ModelRendererTurbo(this, 17, 17, textureX, textureY); // Box 172
		bodyModel[37] = new ModelRendererTurbo(this, 337, 17, textureX, textureY); // Box 173
		bodyModel[38] = new ModelRendererTurbo(this, 345, 17, textureX, textureY); // Box 177
		bodyModel[39] = new ModelRendererTurbo(this, 225, 25, textureX, textureY); // Box 15
		bodyModel[40] = new ModelRendererTurbo(this, 161, 33, textureX, textureY); // Box 6
		bodyModel[41] = new ModelRendererTurbo(this, 233, 33, textureX, textureY); // Box 1
		bodyModel[42] = new ModelRendererTurbo(this, 177, 41, textureX, textureY); // Box 1
		bodyModel[43] = new ModelRendererTurbo(this, 369, 33, textureX, textureY); // Box 26
		bodyModel[44] = new ModelRendererTurbo(this, 265, 25, textureX, textureY); // Box 29
		bodyModel[45] = new ModelRendererTurbo(this, 369, 49, textureX, textureY); // Box 30
		bodyModel[46] = new ModelRendererTurbo(this, 361, 17, textureX, textureY); // Box 138
		bodyModel[47] = new ModelRendererTurbo(this, 489, 25, textureX, textureY); // Box 138
		bodyModel[48] = new ModelRendererTurbo(this, 225, 41, textureX, textureY); // Box 138
		bodyModel[49] = new ModelRendererTurbo(this, 409, 49, textureX, textureY); // Box 138
		bodyModel[50] = new ModelRendererTurbo(this, 433, 49, textureX, textureY); // Box 138
		bodyModel[51] = new ModelRendererTurbo(this, 145, 49, textureX, textureY); // Box 33
		bodyModel[52] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 142
		bodyModel[53] = new ModelRendererTurbo(this, 2, 66, textureX, textureY); // Box 126
		bodyModel[54] = new ModelRendererTurbo(this, 425, 17, textureX, textureY); // Box 152
		bodyModel[55] = new ModelRendererTurbo(this, 393, 49, textureX, textureY); // Box 33
		bodyModel[56] = new ModelRendererTurbo(this, 457, 49, textureX, textureY); // Box 26
		bodyModel[57] = new ModelRendererTurbo(this, 73, 57, textureX, textureY); // Box 28
		bodyModel[58] = new ModelRendererTurbo(this, 177, 65, textureX, textureY); // Box 28
		bodyModel[59] = new ModelRendererTurbo(this, 394, 70, textureX, textureY); // Box 31
		bodyModel[60] = new ModelRendererTurbo(this, 361, 57, textureX, textureY); // Box 31
		bodyModel[61] = new ModelRendererTurbo(this, 145, 57, textureX, textureY); // Box 126
		bodyModel[62] = new ModelRendererTurbo(this, 465, 17, textureX, textureY); // Box 126
		bodyModel[63] = new ModelRendererTurbo(this, 313, 25, textureX, textureY); // Box 126
		bodyModel[64] = new ModelRendererTurbo(this, 385, 17, textureX, textureY); // Box 153
		bodyModel[65] = new ModelRendererTurbo(this, 145, 17, textureX, textureY); // Box 164
		bodyModel[66] = new ModelRendererTurbo(this, 353, 1, textureX, textureY); // Box 167
		bodyModel[67] = new ModelRendererTurbo(this, 161, 17, textureX, textureY); // Box 164
		bodyModel[68] = new ModelRendererTurbo(this, 273, 9, textureX, textureY); // Box 167
		bodyModel[69] = new ModelRendererTurbo(this, 497, 17, textureX, textureY); // Box 170
		bodyModel[70] = new ModelRendererTurbo(this, 353, 9, textureX, textureY); // Box 167
		bodyModel[71] = new ModelRendererTurbo(this, 193, 33, textureX, textureY); // Box 163
		bodyModel[72] = new ModelRendererTurbo(this, 169, 49, textureX, textureY); // Box 147
		bodyModel[73] = new ModelRendererTurbo(this, 497, 49, textureX, textureY); // Box 147
		bodyModel[74] = new ModelRendererTurbo(this, 105, 57, textureX, textureY); // Box 147
		bodyModel[75] = new ModelRendererTurbo(this, 193, 17, textureX, textureY); // Box 147
		bodyModel[76] = new ModelRendererTurbo(this, 409, 17, textureX, textureY); // Box 147
		bodyModel[77] = new ModelRendererTurbo(this, 417, 17, textureX, textureY); // Box 147
		bodyModel[78] = new ModelRendererTurbo(this, 433, 17, textureX, textureY); // Box 147
		bodyModel[79] = new ModelRendererTurbo(this, 449, 17, textureX, textureY); // Box 147
		bodyModel[80] = new ModelRendererTurbo(this, 457, 17, textureX, textureY); // Box 147
		bodyModel[81] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 171
		bodyModel[82] = new ModelRendererTurbo(this, 17, 17, textureX, textureY); // Box 172
		bodyModel[83] = new ModelRendererTurbo(this, 337, 17, textureX, textureY); // Box 173

		bodyModel[0].addBox(-9F, -5F, -9F, 13, 7, 1, 0F); // Box 3
		bodyModel[0].setRotationPoint(24F, -11F, -0.75F);

		bodyModel[1].addBox(-9F, -5F, -9F, 13, 7, 1, 0F); // Box 4
		bodyModel[1].setRotationPoint(24F, -11F, 18.25F);

		bodyModel[2].addBox(-9F, -12F, -9F, 11, 7, 1, 0F); // Box 6
		bodyModel[2].setRotationPoint(24F, -11F, 18.25F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 14, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F); // Box 10
		bodyModel[3].setRotationPoint(14F, -26F, -3.25F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 11, 1, 5, 0F,0F, 0F, -4F, 1F, 0F, -4F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 1F, 1F, -0.5F, 1F, 1F, -3.5F, 0F, 1F, -3.5F); // Box 15
		bodyModel[4].setRotationPoint(14F, -25F, -10.25F);

		bodyModel[5].addBox(0F, 1F, 0F, 9, 2, 2, 0F); // Box 26
		bodyModel[5].setRotationPoint(-23F, -24F, -0.75F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 9, 2, 5, 0F,0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		bodyModel[6].setRotationPoint(-23F, -23F, -5.75F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		bodyModel[7].setRotationPoint(-23F, -11F, -5.75F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 13, 2, 4, 0F,1F, 0F, -4F, 0F, 0F, -4F, 0F, -1F, 0F, 1F, -1F, 0F, 1F, 1F, -1.5F, 0F, 1F, -1.5F, 0F, 1F, -1.5F, 1F, 1F, -1.5F); // Box 29
		bodyModel[8].setRotationPoint(-13F, -24F, -8.25F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 13, 2, 4, 0F,1F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -4F, 1F, 0F, -4F, 1F, 1F, -1.5F, 0F, 1F, -1.5F, 0F, 1F, -1.5F, 1F, 1F, -1.5F); // Box 30
		bodyModel[9].setRotationPoint(-13F, -24F, 4.75F);

		bodyModel[10].addBox(0F, 0F, 0F, 1, 8, 8, 0F); // Box 31
		bodyModel[10].setRotationPoint(-24F, -20F, -3.75F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 1, 2, 11, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 33
		bodyModel[11].setRotationPoint(-14F, -23F, -5.25F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 5, 6, 12, 0F,0F, 0F, -2.25F, 0F, 0F, -2.25F, 0F, 0F, -2.25F, 0F, 0F, -2.25F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 35
		bodyModel[12].setRotationPoint(-20F, -9F, -5.85F);

		bodyModel[13].addShapeBox(-46F, -18F, -10F, 8, 2, 10, 0F,0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		bodyModel[13].setRotationPoint(31F, 13F, 5.25F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 59
		bodyModel[14].setRotationPoint(-25.05F, -17.5F, -2.75F);

		bodyModel[15].addBox(0F, 0F, 0F, 1, 14, 20, 0F); // Box 31
		bodyModel[15].setRotationPoint(14F, -23F, -9.75F);

		bodyModel[16].addBox(0F, 0F, 0F, 9, 9, 0, 0F); // Box 138
		bodyModel[16].setRotationPoint(-20F, -4.5F, -5.4F);

		bodyModel[17].addBox(0F, 0F, 0F, 3, 7, 0, 0F); // Box 152
		bodyModel[17].setRotationPoint(24F, -8F, 10.25F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 163
		bodyModel[18].setRotationPoint(-23.5F, -3F, -6.25F);

		bodyModel[19].addBox(0F, 0F, 0F, 1, 1, 0, 0F); // Box 167
		bodyModel[19].setRotationPoint(-24.05F, -19F, -4.5F);

		bodyModel[20].addBox(0F, 0F, 0F, 34, 1, 1, 0F); // Box 142
		bodyModel[20].setRotationPoint(-17F, 1F, 5.9F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[21].setRotationPoint(7F, -4F, -6.35F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 153
		bodyModel[22].setRotationPoint(-24F, -5F, -5F);

		bodyModel[23].addBox(0F, 0F, 0F, 1, 0, 1, 0F); // Box 164
		bodyModel[23].setRotationPoint(-25F, -13F, -3.25F);

		bodyModel[24].addBox(0F, 0F, 0F, 0, 1, 1, 0F); // Box 167
		bodyModel[24].setRotationPoint(-25F, -14F, -3.25F);

		bodyModel[25].addBox(-47F, 0F, -9F, 0, 3, 19, 0F); // Box 168
		bodyModel[25].setRotationPoint(23F, -3F, -0.25F);

		bodyModel[26].addBox(-47F, 0F, -9F, 0, 1, 3, 0F); // Box 170
		bodyModel[26].setRotationPoint(22.95F, -19F, 4.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[27].setRotationPoint(-25.05F, -2.5F, 5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 177
		bodyModel[28].setRotationPoint(-25.05F, -2.5F, -1.25F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 182
		bodyModel[29].setRotationPoint(-26.35F, -2.5F, 2.5F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 183
		bodyModel[30].setRotationPoint(-25.05F, -2.5F, 2.5F);

		bodyModel[31].addBox(0F, 0F, 0F, 32, 5, 11, 0F); // Box 126
		bodyModel[31].setRotationPoint(-7F, -8F, -5.25F);

		bodyModel[32].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 126
		bodyModel[32].setRotationPoint(3.5F, -26F, -0.75F);

		bodyModel[33].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 126
		bodyModel[33].setRotationPoint(-22F, -7.5F, -3.5F);

		bodyModel[34].addBox(0F, 0F, 0F, 2, 10, 16, 0F); // Box 126
		bodyModel[34].setRotationPoint(15F, -19.5F, -7.75F);

		bodyModel[35].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 126
		bodyModel[35].setRotationPoint(-22F, -7.5F, 1.5F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 172
		bodyModel[36].setRotationPoint(-26.05F, -2F, 5.5F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 173
		bodyModel[37].setRotationPoint(-27.05F, -2.5F, 5F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 177
		bodyModel[38].setRotationPoint(-28.05F, -2.5F, -1.25F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 11, 1, 5, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 1F, 0F, -4F, 1F, 1F, -3.5F, 0F, 1F, -3.5F, 0F, 1F, -0.5F, 1F, 1F, -0.5F); // Box 15
		bodyModel[39].setRotationPoint(15F, -25F, 5.75F);

		bodyModel[40].addBox(-9F, -12F, -9F, 11, 7, 1, 0F); // Box 6
		bodyModel[40].setRotationPoint(24F, -11F, -0.75F);

		bodyModel[41].addBox(-47F, 0F, -9F, 14, 1, 20, 0F); // Box 1
		bodyModel[41].setRotationPoint(61F, -9F, -0.75F);

		bodyModel[42].addBox(-47F, 0F, -9F, 14, 13, 16, 0F); // Box 1
		bodyModel[42].setRotationPoint(47F, -21F, 1.25F);

		bodyModel[43].addBox(0F, 0F, 0F, 14, 1, 11, 0F); // Box 26
		bodyModel[43].setRotationPoint(0F, -25F, -5.25F);

		bodyModel[44].addShapeBox(0F, 0F, 0F, 13, 3, 4, 0F,1F, 0F, -4F, 0F, 0F, -4F, 0F, -1F, 0F, 1F, -1F, 0F, 1F, 1F, -1.5F, 0F, 1F, -1.5F, 0F, 1F, -1.5F, 1F, 1F, -1.5F); // Box 29
		bodyModel[44].setRotationPoint(1F, -25F, -9.25F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 13, 3, 4, 0F,1F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -4F, 1F, 0F, -4F, 1F, 1F, -1.5F, 0F, 1F, -1.5F, 0F, 1F, -1.5F, 1F, 1F, -1.5F); // Box 30
		bodyModel[45].setRotationPoint(1F, -25F, 5.75F);

		bodyModel[46].addBox(0F, 0F, 0F, 9, 9, 0, 0F); // Box 138
		bodyModel[46].setRotationPoint(13F, -4.5F, -5.4F);

		bodyModel[47].addBox(0F, 0F, 0F, 9, 9, 0, 0F); // Box 138
		bodyModel[47].setRotationPoint(-4F, -4.5F, -5.4F);

		bodyModel[48].addBox(0F, 0F, 0F, 9, 9, 0, 0F); // Box 138
		bodyModel[48].setRotationPoint(-20F, -4.5F, 5.8F);

		bodyModel[49].addBox(0F, 0F, 0F, 9, 9, 0, 0F); // Box 138
		bodyModel[49].setRotationPoint(13F, -4.5F, 5.8F);

		bodyModel[50].addBox(0F, 0F, 0F, 9, 9, 0, 0F); // Box 138
		bodyModel[50].setRotationPoint(-4F, -4.5F, 5.8F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 2, 17, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 33
		bodyModel[51].setRotationPoint(14F, -25F, -8.5F);

		bodyModel[52].addBox(0F, 0F, 0F, 34, 1, 1, 0F); // Box 142
		bodyModel[52].setRotationPoint(-14F, 1F, -6.5F);

		bodyModel[53].addBox(0F, 0F, 0F, 51, 5, 11, 0F); // Box 126
		bodyModel[53].setRotationPoint(-24F, -3F, -5.25F);

		bodyModel[54].addBox(0F, 0F, 0F, 3, 7, 0, 0F); // Box 152
		bodyModel[54].setRotationPoint(24F, -8F, -9.75F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 1, 3, 13, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 33
		bodyModel[55].setRotationPoint(0F, -24F, -6.25F);

		bodyModel[56].addBox(0F, 1F, 0F, 14, 1, 9, 0F); // Box 26
		bodyModel[56].setRotationPoint(-14F, -25F, -4.25F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 9, 2, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		bodyModel[57].setRotationPoint(-23F, -23F, 1.25F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 28
		bodyModel[58].setRotationPoint(-23F, -11F, 0.25F);

		bodyModel[59].addBox(0F, 0F, 0F, 14, 13, 14, 0F); // Box 31
		bodyModel[59].setRotationPoint(-14F, -21F, -6.75F);

		bodyModel[60].addBox(0F, 0F, 0F, 9, 10, 12, 0F); // Box 31
		bodyModel[60].setRotationPoint(-23F, -21F, -5.75F);

		bodyModel[61].addBox(0F, 0F, 0F, 4, 4, 4, 0F); // Box 126
		bodyModel[61].setRotationPoint(-20.5F, -27F, -1.75F);

		bodyModel[62].addBox(0F, 0F, 0F, 4, 2, 4, 0F); // Box 126
		bodyModel[62].setRotationPoint(-5.5F, -26F, -1.75F);

		bodyModel[63].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 126
		bodyModel[63].setRotationPoint(8.5F, -26F, -0.75F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 153
		bodyModel[64].setRotationPoint(-24F, -5F, 4F);

		bodyModel[65].addBox(0F, 0F, 0F, 1, 0, 1, 0F); // Box 164
		bodyModel[65].setRotationPoint(-25F, -13F, 2.75F);

		bodyModel[66].addBox(0F, 0F, 0F, 0, 1, 1, 0F); // Box 167
		bodyModel[66].setRotationPoint(-25F, -14F, 2.75F);

		bodyModel[67].addBox(0F, 0F, 0F, 1, 0, 1, 0F); // Box 164
		bodyModel[67].setRotationPoint(-25F, -19F, -0.25F);

		bodyModel[68].addBox(0F, 0F, 0F, 0, 1, 1, 0F); // Box 167
		bodyModel[68].setRotationPoint(-25F, -20F, -0.25F);

		bodyModel[69].addBox(-47F, 0F, -9F, 0, 1, 3, 0F); // Box 170
		bodyModel[69].setRotationPoint(22.95F, -15F, 4.5F);

		bodyModel[70].addBox(0F, 0F, 0F, 1, 1, 0, 0F); // Box 167
		bodyModel[70].setRotationPoint(-24.05F, -15F, -4.5F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 163
		bodyModel[71].setRotationPoint(-23.5F, -3F, 5.75F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[72].setRotationPoint(-9.5F, -4F, -6.35F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 147
		bodyModel[73].setRotationPoint(7F, -4F, 5.85F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 147
		bodyModel[74].setRotationPoint(-9.5F, -4F, 5.85F);

		bodyModel[75].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 147
		bodyModel[75].setRotationPoint(-23F, -4F, 5.85F);

		bodyModel[76].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 147
		bodyModel[76].setRotationPoint(8.5F, -5F, 5.85F);

		bodyModel[77].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 147
		bodyModel[77].setRotationPoint(-23F, -4F, -6.35F);

		bodyModel[78].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 147
		bodyModel[78].setRotationPoint(8.5F, -5F, -6.35F);

		bodyModel[79].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 147
		bodyModel[79].setRotationPoint(-8F, -5F, 5.85F);

		bodyModel[80].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 147
		bodyModel[80].setRotationPoint(-8F, -5F, -6.35F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[81].setRotationPoint(-25.05F, -2.5F, -6.25F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 172
		bodyModel[82].setRotationPoint(-26.05F, -2F, -5.75F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 173
		bodyModel[83].setRotationPoint(-27.05F, -2.5F, -6.25F);
	}
}