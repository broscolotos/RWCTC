//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2023 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: Siemens SD100 Bogie
// Model Creator: DARTRider
// Created on: 08.07.2023 - 07:34:20
// Last changed on: 08.07.2023 - 07:34:20

package train.client.render.models; //Path where the model is located

import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

public class ModelSiemensSD100Bogie extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelSiemensSD100Bogie() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[22];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 57, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 81, 1, textureX, textureY); // Box 0
		bodyModel[3] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // Box 0
		bodyModel[4] = new ModelRendererTurbo(this, 121, 1, textureX, textureY); // Box 0
		bodyModel[5] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // Box 0
		bodyModel[6] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // Box 6
		bodyModel[7] = new ModelRendererTurbo(this, 169, 1, textureX, textureY); // Box 6
		bodyModel[8] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 0
		bodyModel[9] = new ModelRendererTurbo(this, 241, 1, textureX, textureY); // Box 1
		bodyModel[10] = new ModelRendererTurbo(this, 265, 1, textureX, textureY); // Box 0
		bodyModel[11] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 0
		bodyModel[12] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 0
		bodyModel[13] = new ModelRendererTurbo(this, 321, 1, textureX, textureY); // Box 0
		bodyModel[14] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // Box 6
		bodyModel[15] = new ModelRendererTurbo(this, 353, 1, textureX, textureY); // Box 6
		bodyModel[16] = new ModelRendererTurbo(this, 41, 1, textureX, textureY); // Box 16
		bodyModel[17] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 16
		bodyModel[18] = new ModelRendererTurbo(this, 353, 1, textureX, textureY); // Box 18
		bodyModel[19] = new ModelRendererTurbo(this, 401, 1, textureX, textureY); // Box 19
		bodyModel[20] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Box 20
		bodyModel[21] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 20

		bodyModel[0].addBox(0F, 0F, 0F, 23, 1, 1, 0F); // Box 0
		bodyModel[0].setRotationPoint(1F, -4.5F, 7F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 7, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, 0.2F, 0F, -0.2F, 0.2F, 0F, -0.2F, 0.2F, 0F, -0.2F, 0.2F, 0F); // Box 1
		bodyModel[1].setRotationPoint(8.5F, -3.5F, 6F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 7, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[2].setRotationPoint(17F, -5.5F, 6F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1.5F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 1F, 0F); // Box 0
		bodyModel[3].setRotationPoint(14F, -5.5F, 6F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[4].setRotationPoint(1F, -5.5F, 6F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 1F, 0F, -1.5F, 1F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[5].setRotationPoint(7F, -5.5F, 6F);

		bodyModel[6].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 6
		bodyModel[6].setRotationPoint(1.5F, -6.8F, 6F);

		bodyModel[7].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 6
		bodyModel[7].setRotationPoint(17.5F, -6.8F, 6F);

		bodyModel[8].addBox(0F, 0F, 0F, 23, 1, 1, 0F); // Box 0
		bodyModel[8].setRotationPoint(1F, -4.5F, -8F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 7, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, 0.2F, 0F, -0.2F, 0.2F, 0F, -0.2F, 0.2F, 0F, -0.2F, 0.2F, 0F); // Box 1
		bodyModel[9].setRotationPoint(8.5F, -3.5F, -7F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 7, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[10].setRotationPoint(17F, -5.5F, -7F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1.5F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 1F, 0F); // Box 0
		bodyModel[11].setRotationPoint(14F, -5.5F, -7F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[12].setRotationPoint(1F, -5.5F, -7F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 1F, 0F, -1.5F, 1F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[13].setRotationPoint(7F, -5.5F, -7F);

		bodyModel[14].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 6
		bodyModel[14].setRotationPoint(1.5F, -6.8F, -6F);

		bodyModel[15].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 6
		bodyModel[15].setRotationPoint(17.5F, -6.8F, -6F);

		bodyModel[16].addBox(0F, 0F, 0F, 1, 1, 12, 0F); // Box 16
		bodyModel[16].setRotationPoint(3.5F, -4.75F, -6F);

		bodyModel[17].addBox(0F, 0F, 0F, 1, 1, 12, 0F); // Box 16
		bodyModel[17].setRotationPoint(19.5F, -4.75F, -6F);

		bodyModel[18].addBox(0F, 0F, 0F, 14, 2, 12, 0F); // Box 18
		bodyModel[18].setRotationPoint(5F, -5F, -6F);

		bodyModel[19].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Box 19
		bodyModel[19].setRotationPoint(12F, -9F, 0F);

		bodyModel[20].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 20
		bodyModel[20].setRotationPoint(1F, -7.5F, 7F);

		bodyModel[21].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 20
		bodyModel[21].setRotationPoint(1F, -7.5F, -8F);
	}
	public float[] getTrans() {
		return new float[]{ 0.0f, 0.0f, 0.0f };
	}
}