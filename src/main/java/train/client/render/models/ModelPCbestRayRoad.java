//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2023 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: PCbestRayRoad
// Model Creator: bida
// Created on: 12.08.2023 - 23:38:11
// Last changed on: 12.08.2023 - 23:38:11

package train.client.render.models; //Path where the model is located


import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

public class ModelPCbestRayRoad extends ModelConverter //Same as Filename
{
	int textureX = 128;
	int textureY = 128;

	public ModelPCbestRayRoad() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[3];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // Box 437
		bodyModel[1] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Box 438
		bodyModel[2] = new ModelRendererTurbo(this, -9, 37, textureX, textureY); // Box 439

		bodyModel[0].addShapeBox(0F, 0F, 0F, 50, 16, 0, 0F,0F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, -25F, -8F, 0F, -25F, -8F, 0F, 0F, -8F, 0F); // Box 437
		bodyModel[0].setRotationPoint(-8F, -1F, -10.01F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 50, 16, 0, 0F,0F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, -25F, -8F, 0F, -25F, -8F, 0F, 0F, -8F, 0F); // Box 438
		bodyModel[1].setRotationPoint(-8F, -1F, 10.01F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 30, 0, 30, 0F,0F, 0F, 0F, -15F, 0F, 0F, -15F, 0F, -15F, 0F, 0F, -15F, 0F, 0F, 0F, -15F, 0F, 0F, -15F, 0F, -15F, 0F, 0F, -15F); // Box 439
		bodyModel[2].setRotationPoint(-23F, -15.51F, -7.5F);
	}
}