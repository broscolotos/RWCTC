//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2023 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: Siemens SD100
// Model Creator: DARTRider
// Created on: 23.06.2023 - 20:17:22
// Last changed on: 23.06.2023 - 20:17:22

package train.client.render.models;

import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.api.EntityRollingStock;
import train.common.library.Info;

public class ModelSiemensSD100 extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;
	public ModelSiemensSD100Bogie bogie = new ModelSiemensSD100Bogie();

	public ModelSiemensSD100() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[262];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	@Override
	public void render(Entity entity, float f0, float f1, float f2, float f3, float f4, float scale){
		super.render(entity, f0, f1, f2, f3, f4, scale);
		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, Info.trainsPrefix + "siemenssd100_bogie.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(-1.6f,0.5f,0.0f);
		bogie.render(entity, f0, f1, f2, f3, f4, scale);
		// If color number uneven, render the other bogie.
		// Using modulo for this only works because A: The colors in EnumTrains for this model are in order; and B: every other texture needs the second bogie!
		if (((EntityRollingStock) entity).getColor() % 2 != 0) {
			GL11.glTranslatef(4.1f,0.0f,0.0f);
			bogie.render(entity, f0, f1, f2, f3, f4, scale);
		}
		GL11.glPopMatrix();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 97, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[4] = new ModelRendererTurbo(this, 353, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[5] = new ModelRendererTurbo(this, 369, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[6] = new ModelRendererTurbo(this, 385, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[7] = new ModelRendererTurbo(this, 401, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[8] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[9] = new ModelRendererTurbo(this, 433, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[10] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // SD100 Fold Door Left
		bodyModel[11] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 14
		bodyModel[12] = new ModelRendererTurbo(this, 489, 1, textureX, textureY); // Box 2
		bodyModel[13] = new ModelRendererTurbo(this, 145, 9, textureX, textureY); // Box 23
		bodyModel[14] = new ModelRendererTurbo(this, 161, 9, textureX, textureY); // Box 28
		bodyModel[15] = new ModelRendererTurbo(this, 177, 9, textureX, textureY); // Box 28
		bodyModel[16] = new ModelRendererTurbo(this, 193, 9, textureX, textureY); // Box 31
		bodyModel[17] = new ModelRendererTurbo(this, 209, 9, textureX, textureY); // Box 0
		bodyModel[18] = new ModelRendererTurbo(this, 145, 17, textureX, textureY); // Box 1
		bodyModel[19] = new ModelRendererTurbo(this, 305, 33, textureX, textureY); // Box 2
		bodyModel[20] = new ModelRendererTurbo(this, 489, 9, textureX, textureY); // SD100 Fold Door Right
		bodyModel[21] = new ModelRendererTurbo(this, 193, 17, textureX, textureY); // SD100 Fold Door Right
		bodyModel[22] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // SD100 Fold Door Right
		bodyModel[23] = new ModelRendererTurbo(this, 17, 33, textureX, textureY); // SD100 Fold Door Right
		bodyModel[24] = new ModelRendererTurbo(this, 33, 33, textureX, textureY); // SD100 Fold Door Right
		bodyModel[25] = new ModelRendererTurbo(this, 49, 33, textureX, textureY); // SD100 Fold Door Right
		bodyModel[26] = new ModelRendererTurbo(this, 65, 33, textureX, textureY); // SD100 Fold Door Right
		bodyModel[27] = new ModelRendererTurbo(this, 81, 33, textureX, textureY); // SD100 Fold Door Right
		bodyModel[28] = new ModelRendererTurbo(this, 97, 33, textureX, textureY); // Box 13
		bodyModel[29] = new ModelRendererTurbo(this, 305, 9, textureX, textureY); // Box 14
		bodyModel[30] = new ModelRendererTurbo(this, 465, 25, textureX, textureY); // Box 2
		bodyModel[31] = new ModelRendererTurbo(this, 169, 25, textureX, textureY); // Box 16
		bodyModel[32] = new ModelRendererTurbo(this, 305, 9, textureX, textureY); // Box 18
		bodyModel[33] = new ModelRendererTurbo(this, 193, 41, textureX, textureY); // Box 19
		bodyModel[34] = new ModelRendererTurbo(this, 273, 41, textureX, textureY); // Box 19
		bodyModel[35] = new ModelRendererTurbo(this, 361, 41, textureX, textureY); // Box 21
		bodyModel[36] = new ModelRendererTurbo(this, 409, 41, textureX, textureY); // Box 22
		bodyModel[37] = new ModelRendererTurbo(this, 97, 33, textureX, textureY); // Box 23
		bodyModel[38] = new ModelRendererTurbo(this, 457, 41, textureX, textureY); // Box 25
		bodyModel[39] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 27
		bodyModel[40] = new ModelRendererTurbo(this, 129, 33, textureX, textureY); // Box 28
		bodyModel[41] = new ModelRendererTurbo(this, 97, 41, textureX, textureY); // Box 28
		bodyModel[42] = new ModelRendererTurbo(this, 345, 25, textureX, textureY); // Box 28
		bodyModel[43] = new ModelRendererTurbo(this, 129, 41, textureX, textureY); // Box 31
		bodyModel[44] = new ModelRendererTurbo(this, 129, 65, textureX, textureY); // Box 60
		bodyModel[45] = new ModelRendererTurbo(this, 33, 65, textureX, textureY); // Box 61
		bodyModel[46] = new ModelRendererTurbo(this, 353, 65, textureX, textureY); // Box 62
		bodyModel[47] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 63
		bodyModel[48] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 64
		bodyModel[49] = new ModelRendererTurbo(this, 113, 89, textureX, textureY); // Box 65
		bodyModel[50] = new ModelRendererTurbo(this, 153, 9, textureX, textureY); // Pantograph
		bodyModel[51] = new ModelRendererTurbo(this, 169, 9, textureX, textureY); // Pantograph
		bodyModel[52] = new ModelRendererTurbo(this, 89, 25, textureX, textureY); // Pantograph
		bodyModel[53] = new ModelRendererTurbo(this, 249, 41, textureX, textureY); // Pantograph
		bodyModel[54] = new ModelRendererTurbo(this, 345, 41, textureX, textureY); // Pantograph
		bodyModel[55] = new ModelRendererTurbo(this, 129, 41, textureX, textureY); // Pantograph
		bodyModel[56] = new ModelRendererTurbo(this, 505, 9, textureX, textureY); // Pantograph
		bodyModel[57] = new ModelRendererTurbo(this, 505, 17, textureX, textureY); // Pantograph
		bodyModel[58] = new ModelRendererTurbo(this, 137, 25, textureX, textureY); // Pantograph
		bodyModel[59] = new ModelRendererTurbo(this, 265, 41, textureX, textureY); // Pantograph
		bodyModel[60] = new ModelRendererTurbo(this, 505, 25, textureX, textureY); // Pantograph
		bodyModel[61] = new ModelRendererTurbo(this, 113, 33, textureX, textureY); // Pantograph
		bodyModel[62] = new ModelRendererTurbo(this, 505, 33, textureX, textureY); // Pantograph
		bodyModel[63] = new ModelRendererTurbo(this, 113, 41, textureX, textureY); // Pantograph
		bodyModel[64] = new ModelRendererTurbo(this, 209, 41, textureX, textureY); // Pantograph
		bodyModel[65] = new ModelRendererTurbo(this, 393, 41, textureX, textureY); // Pantograph
		bodyModel[66] = new ModelRendererTurbo(this, 281, 41, textureX, textureY); // Pantograph
		bodyModel[67] = new ModelRendererTurbo(this, 369, 41, textureX, textureY); // Pantograph
		bodyModel[68] = new ModelRendererTurbo(this, 393, 41, textureX, textureY); // Pantograph
		bodyModel[69] = new ModelRendererTurbo(this, 409, 41, textureX, textureY); // Pantograph
		bodyModel[70] = new ModelRendererTurbo(this, 361, 25, textureX, textureY); // Pantograph
		bodyModel[71] = new ModelRendererTurbo(this, 417, 41, textureX, textureY); // Pantograph
		bodyModel[72] = new ModelRendererTurbo(this, 441, 41, textureX, textureY); // Pantograph
		bodyModel[73] = new ModelRendererTurbo(this, 457, 41, textureX, textureY); // Pantograph
		bodyModel[74] = new ModelRendererTurbo(this, 465, 41, textureX, textureY); // Pantograph
		bodyModel[75] = new ModelRendererTurbo(this, 473, 41, textureX, textureY); // Pantograph
		bodyModel[76] = new ModelRendererTurbo(this, 489, 41, textureX, textureY); // Pantograph
		bodyModel[77] = new ModelRendererTurbo(this, 345, 49, textureX, textureY); // Pantograph
		bodyModel[78] = new ModelRendererTurbo(this, 97, 49, textureX, textureY); // Pantograph
		bodyModel[79] = new ModelRendererTurbo(this, 393, 49, textureX, textureY); // Pantograph
		bodyModel[80] = new ModelRendererTurbo(this, 441, 49, textureX, textureY); // Pantograph
		bodyModel[81] = new ModelRendererTurbo(this, 105, 49, textureX, textureY); // Pantograph
		bodyModel[82] = new ModelRendererTurbo(this, 153, 49, textureX, textureY); // Pantograph
		bodyModel[83] = new ModelRendererTurbo(this, 113, 49, textureX, textureY); // Pantograph
		bodyModel[84] = new ModelRendererTurbo(this, 249, 49, textureX, textureY); // Pantograph
		bodyModel[85] = new ModelRendererTurbo(this, 465, 49, textureX, textureY); // Pantograph
		bodyModel[86] = new ModelRendererTurbo(this, 161, 49, textureX, textureY); // Pantograph
		bodyModel[87] = new ModelRendererTurbo(this, 209, 49, textureX, textureY); // Pantograph
		bodyModel[88] = new ModelRendererTurbo(this, 489, 49, textureX, textureY); // Box 1020
		bodyModel[89] = new ModelRendererTurbo(this, 289, 49, textureX, textureY); // Box 104
		bodyModel[90] = new ModelRendererTurbo(this, 377, 9, textureX, textureY); // Box 13
		bodyModel[91] = new ModelRendererTurbo(this, 425, 25, textureX, textureY); // Running Lights Left
		bodyModel[92] = new ModelRendererTurbo(this, 441, 25, textureX, textureY); // Running Lights Right
		bodyModel[93] = new ModelRendererTurbo(this, 377, 49, textureX, textureY); // Medium Marker Light Left
		bodyModel[94] = new ModelRendererTurbo(this, 425, 49, textureX, textureY); // Medium Marker Light Right
		bodyModel[95] = new ModelRendererTurbo(this, 227, 143, textureX, textureY); // Rollsign Front
		bodyModel[96] = new ModelRendererTurbo(this, 465, 65, textureX, textureY); // Box 65
		bodyModel[97] = new ModelRendererTurbo(this, 193, 89, textureX, textureY); // Box 65
		bodyModel[98] = new ModelRendererTurbo(this, 337, 65, textureX, textureY); // Box 65
		bodyModel[99] = new ModelRendererTurbo(this, 73, 73, textureX, textureY); // Box 65
		bodyModel[100] = new ModelRendererTurbo(this, 209, 89, textureX, textureY); // Box 63
		bodyModel[101] = new ModelRendererTurbo(this, 273, 89, textureX, textureY); // Box 63
		bodyModel[102] = new ModelRendererTurbo(this, 249, 57, textureX, textureY); // Box 63
		bodyModel[103] = new ModelRendererTurbo(this, 345, 57, textureX, textureY); // Box 63
		bodyModel[104] = new ModelRendererTurbo(this, 393, 57, textureX, textureY); // Box 63
		bodyModel[105] = new ModelRendererTurbo(this, 441, 57, textureX, textureY); // Box 63
		bodyModel[106] = new ModelRendererTurbo(this, 97, 81, textureX, textureY); // Box 63
		bodyModel[107] = new ModelRendererTurbo(this, 313, 89, textureX, textureY); // Box 63
		bodyModel[108] = new ModelRendererTurbo(this, 345, 89, textureX, textureY); // Box 63
		bodyModel[109] = new ModelRendererTurbo(this, 409, 89, textureX, textureY); // Box 63
		bodyModel[110] = new ModelRendererTurbo(this, 433, 89, textureX, textureY); // Box 63
		bodyModel[111] = new ModelRendererTurbo(this, 497, 33, textureX, textureY); // Box 28
		bodyModel[112] = new ModelRendererTurbo(this, 489, 49, textureX, textureY); // Box 28
		bodyModel[113] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 19
		bodyModel[114] = new ModelRendererTurbo(this, 465, 89, textureX, textureY); // Box 118
		bodyModel[115] = new ModelRendererTurbo(this, 169, 97, textureX, textureY); // Box 118
		bodyModel[116] = new ModelRendererTurbo(this, 305, 97, textureX, textureY); // Box 118
		bodyModel[117] = new ModelRendererTurbo(this, 329, 105, textureX, textureY); // Box 118
		bodyModel[118] = new ModelRendererTurbo(this, 449, 65, textureX, textureY); // Box 124
		bodyModel[119] = new ModelRendererTurbo(this, 369, 89, textureX, textureY); // Box 124
		bodyModel[120] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 126
		bodyModel[121] = new ModelRendererTurbo(this, 385, 65, textureX, textureY); // Box 126
		bodyModel[122] = new ModelRendererTurbo(this, 505, 49, textureX, textureY); // Small Marker Light Right
		bodyModel[123] = new ModelRendererTurbo(this, 145, 57, textureX, textureY); // Small Marker Light Left
		bodyModel[124] = new ModelRendererTurbo(this, 153, 57, textureX, textureY); // Large Marker Light Left
		bodyModel[125] = new ModelRendererTurbo(this, 161, 57, textureX, textureY); // Large Marker Light Left
		bodyModel[126] = new ModelRendererTurbo(this, 353, 105, textureX, textureY); // Box 124
		bodyModel[127] = new ModelRendererTurbo(this, 409, 105, textureX, textureY); // Box 124
		bodyModel[128] = new ModelRendererTurbo(this, 489, 81, textureX, textureY); // Box 126
		bodyModel[129] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 126
		bodyModel[130] = new ModelRendererTurbo(this, 57, 113, textureX, textureY); // Box 124
		bodyModel[131] = new ModelRendererTurbo(this, 89, 113, textureX, textureY); // Box 124
		bodyModel[132] = new ModelRendererTurbo(this, 121, 113, textureX, textureY); // Box 124
		bodyModel[133] = new ModelRendererTurbo(this, 217, 113, textureX, textureY); // Box 124
		bodyModel[134] = new ModelRendererTurbo(this, 329, 97, textureX, textureY); // Box 126
		bodyModel[135] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 126
		bodyModel[136] = new ModelRendererTurbo(this, 249, 113, textureX, textureY); // Box 124
		bodyModel[137] = new ModelRendererTurbo(this, 145, 121, textureX, textureY); // Box 124
		bodyModel[138] = new ModelRendererTurbo(this, 281, 113, textureX, textureY); // Box 126
		bodyModel[139] = new ModelRendererTurbo(this, 449, 121, textureX, textureY); // Box 126
		bodyModel[140] = new ModelRendererTurbo(this, 473, 121, textureX, textureY); // Box 124
		bodyModel[141] = new ModelRendererTurbo(this, 81, 129, textureX, textureY); // Box 124
		bodyModel[142] = new ModelRendererTurbo(this, 113, 129, textureX, textureY); // Box 126
		bodyModel[143] = new ModelRendererTurbo(this, 169, 129, textureX, textureY); // Box 126
		bodyModel[144] = new ModelRendererTurbo(this, 193, 129, textureX, textureY); // Box 124
		bodyModel[145] = new ModelRendererTurbo(this, 225, 129, textureX, textureY); // Box 124
		bodyModel[146] = new ModelRendererTurbo(this, 257, 129, textureX, textureY); // Box 124
		bodyModel[147] = new ModelRendererTurbo(this, 289, 129, textureX, textureY); // Box 124
		bodyModel[148] = new ModelRendererTurbo(this, 369, 129, textureX, textureY); // Box 126
		bodyModel[149] = new ModelRendererTurbo(this, 393, 129, textureX, textureY); // Box 126
		bodyModel[150] = new ModelRendererTurbo(this, 124, 192, textureX, textureY); // Rollsign Side Exterior
		bodyModel[151] = new ModelRendererTurbo(this, 124, 192, textureX, textureY); // Rollsign Side Interior
		bodyModel[152] = new ModelRendererTurbo(this, 33, 65, textureX, textureY); // Box 36
		bodyModel[153] = new ModelRendererTurbo(this, 41, 65, textureX, textureY); // Box 17
		bodyModel[154] = new ModelRendererTurbo(this, 73, 65, textureX, textureY); // Box 36
		bodyModel[155] = new ModelRendererTurbo(this, 81, 65, textureX, textureY); // Box 17
		bodyModel[156] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 36
		bodyModel[157] = new ModelRendererTurbo(this, 417, 65, textureX, textureY); // Box 17
		bodyModel[158] = new ModelRendererTurbo(this, 497, 65, textureX, textureY); // Box 36
		bodyModel[159] = new ModelRendererTurbo(this, 505, 65, textureX, textureY); // Box 17
		bodyModel[160] = new ModelRendererTurbo(this, 393, 97, textureX, textureY); // Box 36
		bodyModel[161] = new ModelRendererTurbo(this, 505, 121, textureX, textureY); // Box 17
		bodyModel[162] = new ModelRendererTurbo(this, 137, 129, textureX, textureY); // Box 36
		bodyModel[163] = new ModelRendererTurbo(this, 321, 129, textureX, textureY); // Box 17
		bodyModel[164] = new ModelRendererTurbo(this, 489, 97, textureX, textureY); // Box 17
		bodyModel[165] = new ModelRendererTurbo(this, 417, 129, textureX, textureY); // Box 17
		bodyModel[166] = new ModelRendererTurbo(this, 329, 137, textureX, textureY); // Box 17
		bodyModel[167] = new ModelRendererTurbo(this, 353, 137, textureX, textureY); // Box 17
		bodyModel[168] = new ModelRendererTurbo(this, 433, 137, textureX, textureY); // Box 17
		bodyModel[169] = new ModelRendererTurbo(this, 457, 137, textureX, textureY); // Box 17
		bodyModel[170] = new ModelRendererTurbo(this, 481, 137, textureX, textureY); // Box 17
		bodyModel[171] = new ModelRendererTurbo(this, 209, 145, textureX, textureY); // Box 17
		bodyModel[172] = new ModelRendererTurbo(this, 233, 145, textureX, textureY); // Box 17
		bodyModel[173] = new ModelRendererTurbo(this, 257, 145, textureX, textureY); // Box 17
		bodyModel[174] = new ModelRendererTurbo(this, 281, 145, textureX, textureY); // Box 17
		bodyModel[175] = new ModelRendererTurbo(this, 305, 145, textureX, textureY); // Box 17
		bodyModel[176] = new ModelRendererTurbo(this, 409, 97, textureX, textureY); // Box 17
		bodyModel[177] = new ModelRendererTurbo(this, 145, 113, textureX, textureY); // Box 17
		bodyModel[178] = new ModelRendererTurbo(this, 193, 113, textureX, textureY); // Box 17
		bodyModel[179] = new ModelRendererTurbo(this, 57, 129, textureX, textureY); // Box 17
		bodyModel[180] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 17
		bodyModel[181] = new ModelRendererTurbo(this, 33, 137, textureX, textureY); // Box 17
		bodyModel[182] = new ModelRendererTurbo(this, 369, 145, textureX, textureY); // Box 17
		bodyModel[183] = new ModelRendererTurbo(this, 401, 145, textureX, textureY); // Box 17
		bodyModel[184] = new ModelRendererTurbo(this, 321, 153, textureX, textureY); // Box 189
		bodyModel[185] = new ModelRendererTurbo(this, 345, 153, textureX, textureY); // Box 189
		bodyModel[186] = new ModelRendererTurbo(this, 369, 153, textureX, textureY); // Box 124
		bodyModel[187] = new ModelRendererTurbo(this, 337, 65, textureX, textureY); // Box 126
		bodyModel[188] = new ModelRendererTurbo(this, 233, 137, textureX, textureY); // Box 124
		bodyModel[189] = new ModelRendererTurbo(this, 177, 89, textureX, textureY); // Box 124
		bodyModel[190] = new ModelRendererTurbo(this, 353, 97, textureX, textureY); // Box 124
		bodyModel[191] = new ModelRendererTurbo(this, 9, 57, textureX, textureY); // Box 196
		bodyModel[192] = new ModelRendererTurbo(this, 209, 57, textureX, textureY); // Box 196
		bodyModel[193] = new ModelRendererTurbo(this, 281, 57, textureX, textureY); // Box 196
		bodyModel[194] = new ModelRendererTurbo(this, 25, 57, textureX, textureY); // Box 196
		bodyModel[195] = new ModelRendererTurbo(this, 289, 57, textureX, textureY); // Box 196
		bodyModel[196] = new ModelRendererTurbo(this, 377, 57, textureX, textureY); // Box 196
		bodyModel[197] = new ModelRendererTurbo(this, 352, 189, textureX, textureY); // Box 189
		bodyModel[198] = new ModelRendererTurbo(this, 375, 189, textureX, textureY); // Box 189
		bodyModel[199] = new ModelRendererTurbo(this, 400, 189, textureX, textureY); // Box 189
		bodyModel[200] = new ModelRendererTurbo(this, 353, 228, textureX, textureY); // Box 189
		bodyModel[201] = new ModelRendererTurbo(this, 21, 181, textureX, textureY); // Box 201
		bodyModel[202] = new ModelRendererTurbo(this, 32, 181, textureX, textureY); // Box 201
		bodyModel[203] = new ModelRendererTurbo(this, 32, 181, textureX, textureY); // Box 201
		bodyModel[204] = new ModelRendererTurbo(this, 14, 214, textureX, textureY); // Box 201
		bodyModel[205] = new ModelRendererTurbo(this, 67, 151, textureX, textureY); // Left Side Plug Door SD160
		bodyModel[206] = new ModelRendererTurbo(this, 188, 151, textureX, textureY); // Right Side Plug Door SD160
		bodyModel[207] = new ModelRendererTurbo(this, 172, 151, textureX, textureY); // Left Side Plug Door SD160
		bodyModel[208] = new ModelRendererTurbo(this, 156, 151, textureX, textureY); // Right Side Plug Door SD160
		bodyModel[209] = new ModelRendererTurbo(this, 138, 151, textureX, textureY); // Right Side Plug Door SD160
		bodyModel[210] = new ModelRendererTurbo(this, 118, 151, textureX, textureY); // Left Side Plug Door SD160
		bodyModel[211] = new ModelRendererTurbo(this, 101, 151, textureX, textureY); // Right Side Plug Door SD160
		bodyModel[212] = new ModelRendererTurbo(this, 84, 151, textureX, textureY); // Left Side Plug Door SD160
		bodyModel[213] = new ModelRendererTurbo(this, 304, 180, textureX, textureY); // Box 294
		bodyModel[214] = new ModelRendererTurbo(this, 209, 33, textureX, textureY); // Box 299
		bodyModel[215] = new ModelRendererTurbo(this, 307, 174, textureX, textureY); // Box 301
		bodyModel[216] = new ModelRendererTurbo(this, 306, 191, textureX, textureY); // Box 302
		bodyModel[217] = new ModelRendererTurbo(this, 338, 173, textureX, textureY); // Box 342
		bodyModel[218] = new ModelRendererTurbo(this, 339, 171, textureX, textureY); // Box 343
		bodyModel[219] = new ModelRendererTurbo(this, 329, 172, textureX, textureY); // Box 344
		bodyModel[220] = new ModelRendererTurbo(this, 329, 178, textureX, textureY); // Box 345
		bodyModel[221] = new ModelRendererTurbo(this, 283, 202, textureX, textureY); // Box 346
		bodyModel[222] = new ModelRendererTurbo(this, 336, 177, textureX, textureY); // Box 347
		bodyModel[223] = new ModelRendererTurbo(this, 323, 178, textureX, textureY); // Box 348
		bodyModel[224] = new ModelRendererTurbo(this, 313, 178, textureX, textureY); // Box 349
		bodyModel[225] = new ModelRendererTurbo(this, 316, 170, textureX, textureY); // Box 350
		bodyModel[226] = new ModelRendererTurbo(this, 328, 194, textureX, textureY); // Box 351
		bodyModel[227] = new ModelRendererTurbo(this, 293, 189, textureX, textureY); // Box 352
		bodyModel[228] = new ModelRendererTurbo(this, 326, 183, textureX, textureY); // Box 353
		bodyModel[229] = new ModelRendererTurbo(this, 332, 189, textureX, textureY); // Box 354
		bodyModel[230] = new ModelRendererTurbo(this, 297, 202, textureX, textureY); // Box 355
		bodyModel[231] = new ModelRendererTurbo(this, 318, 180, textureX, textureY); // Box 356
		bodyModel[232] = new ModelRendererTurbo(this, 306, 194, textureX, textureY); // Box 358
		bodyModel[233] = new ModelRendererTurbo(this, 323, 199, textureX, textureY); // Box 360
		bodyModel[234] = new ModelRendererTurbo(this, 317, 205, textureX, textureY); // Box 361
		bodyModel[235] = new ModelRendererTurbo(this, 299, 208, textureX, textureY); // Box 362
		bodyModel[236] = new ModelRendererTurbo(this, 286, 189, textureX, textureY); // Box 363
		bodyModel[237] = new ModelRendererTurbo(this, 284, 173, textureX, textureY); // Box 365
		bodyModel[238] = new ModelRendererTurbo(this, 289, 167, textureX, textureY); // Box 366
		bodyModel[239] = new ModelRendererTurbo(this, 295, 179, textureX, textureY); // Box 367
		bodyModel[240] = new ModelRendererTurbo(this, 256, 171, textureX, textureY); // Box 368
		bodyModel[241] = new ModelRendererTurbo(this, 286, 179, textureX, textureY); // Box 370
		bodyModel[242] = new ModelRendererTurbo(this, 317, 186, textureX, textureY); // Box 371
		bodyModel[243] = new ModelRendererTurbo(this, 274, 178, textureX, textureY); // Box 373
		bodyModel[244] = new ModelRendererTurbo(this, 263, 179, textureX, textureY); // Box 376
		bodyModel[245] = new ModelRendererTurbo(this, 284, 195, textureX, textureY); // Box 377
		bodyModel[246] = new ModelRendererTurbo(this, 291, 185, textureX, textureY); // Box 378
		bodyModel[247] = new ModelRendererTurbo(this, 305, 204, textureX, textureY); // Box 380
		bodyModel[248] = new ModelRendererTurbo(this, 260, 176, textureX, textureY); // Box 360
		bodyModel[249] = new ModelRendererTurbo(this, 2, 149, textureX, textureY); // Box 250
		bodyModel[250] = new ModelRendererTurbo(this, 0, 144, textureX, textureY); // Box 250
		bodyModel[251] = new ModelRendererTurbo(this, 10, 145, textureX, textureY); // Box 250
		bodyModel[252] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // Box 250
		bodyModel[253] = new ModelRendererTurbo(this, 14, 149, textureX, textureY); // Box 250
		bodyModel[254] = new ModelRendererTurbo(this, 10, 154, textureX, textureY); // Box 250
		bodyModel[255] = new ModelRendererTurbo(this, 2, 166, textureX, textureY); // Medium Marker Light Left
		bodyModel[256] = new ModelRendererTurbo(this, 13, 163, textureX, textureY); // Medium Marker Light Right
		bodyModel[257] = new ModelRendererTurbo(this, 23, 144, textureX, textureY); // High Beam Headlight
		bodyModel[258] = new ModelRendererTurbo(this, 19, 155, textureX, textureY); // Large Marker Light Left
		bodyModel[259] = new ModelRendererTurbo(this, 20, 164, textureX, textureY); // Large Marker Light Left
		bodyModel[260] = new ModelRendererTurbo(this, 72, 141, textureX, textureY); // Large Marker Light Left
		bodyModel[261] = new ModelRendererTurbo(this, 63, 142, textureX, textureY); // Large Marker Light Left

		bodyModel[0].addShapeBox(0F, 0F, 0F, 45, 23, 1, 0F,0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[0].setRotationPoint(-25F, -21F, 10F);

		bodyModel[1].addBox(0F, 0F, 0F, 19, 23, 1, 0F); // Box 1
		bodyModel[1].setRotationPoint(33F, -21F, 10F);

		bodyModel[2].addBox(0F, 0F, 0F, 91, 3, 1, 0F); // Box 2
		bodyModel[2].setRotationPoint(-39F, -24F, 10F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[3].setRotationPoint(-29F, -21F, 10.5F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[4].setRotationPoint(-32.5F, -21F, 10.5F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[5].setRotationPoint(-36F, -21F, 10.5F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-1.1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -1.1F, 0F, -0.8F, -1.1F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -1.1F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[6].setRotationPoint(-39.5F, -21F, 10.5F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[7].setRotationPoint(29F, -21F, 10.5F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[8].setRotationPoint(25.5F, -21F, 10.5F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[9].setRotationPoint(22F, -21F, 10.5F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-1.1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -1.1F, 0F, -0.8F, -1.1F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -1.1F, 0.5F, -0.8F); // SD100 Fold Door Left
		bodyModel[10].setRotationPoint(18.5F, -21F, 10.5F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 8, 19, 1, 0F,0F, 0F, 2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F); // Box 14
		bodyModel[11].setRotationPoint(-47.5F, -21F, 10F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F,0F, 0F, 2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F); // Box 2
		bodyModel[12].setRotationPoint(-47.5F, -24F, 10F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,3.3F, 0F, 1.7F, 0F, 0F, 0F, 0F, 0F, 0F, 3.3F, 0F, -1.7F, 0F, 0.5F, 1.7F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -1.7F); // Box 23
		bodyModel[13].setRotationPoint(-43F, -2F, 10F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F,0F, 0.6F, 0F, -2F, 0.6F, 0F, -2F, 0.6F, 0F, 0F, 0.6F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 28
		bodyModel[14].setRotationPoint(-48F, 0F, 0F);

		bodyModel[15].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 28
		bodyModel[15].setRotationPoint(-53F, -0.5F, 0F);

		bodyModel[16].addBox(0F, 0F, 0F, 2, 4, 2, 0F); // Box 31
		bodyModel[16].setRotationPoint(-55F, -2F, 0F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 45, 23, 1, 0F,0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[17].setRotationPoint(-25F, -21F, -11F);

		bodyModel[18].addBox(0F, 0F, 0F, 19, 23, 1, 0F); // Box 1
		bodyModel[18].setRotationPoint(33F, -21F, -11F);

		bodyModel[19].addBox(0F, 0F, 0F, 91, 3, 1, 0F); // Box 2
		bodyModel[19].setRotationPoint(-39F, -24F, -11F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[20].setRotationPoint(-29F, -21F, -10.5F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[21].setRotationPoint(-32.5F, -21F, -10.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[22].setRotationPoint(-36F, -21F, -10.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-1.1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -1.1F, 0F, -0.8F, -1.1F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -1.1F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[23].setRotationPoint(-39.5F, -21F, -10.5F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[24].setRotationPoint(29F, -21F, -10.5F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[25].setRotationPoint(25.5F, -21F, -10.5F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -0.5F, 0F, -0.8F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -0.5F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[26].setRotationPoint(22F, -21F, -10.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 4, 23, 1, 0F,-1.1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, -1.1F, 0F, -0.8F, -1.1F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.8F, -1.1F, 0.5F, -0.8F); // SD100 Fold Door Right
		bodyModel[27].setRotationPoint(18.5F, -21F, -10.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 1, 24, 22, 0F,0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -0.4F, 0.5F, 0F, -0.4F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 13
		bodyModel[28].setRotationPoint(-39F, -21F, -11F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 8, 19, 1, 0F,0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 2F); // Box 14
		bodyModel[29].setRotationPoint(-47.5F, -21F, -11F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F,0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 2F); // Box 2
		bodyModel[30].setRotationPoint(-47.5F, -24F, -11F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 1, 19, 18, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F); // Box 16
		bodyModel[31].setRotationPoint(-48.5F, -21F, -9F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 1, 1, 22, 0F,0F, 0.5F, 0F, 0.3F, 0.5F, 0F, 0.3F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -0.7F, 0F, 0F, -0.7F, 0F, 0F, 0F, 0F, 0F); // Box 18
		bodyModel[32].setRotationPoint(33F, 2.5F, -11F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 14, 1, 22, 0F,-0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F); // Box 19
		bodyModel[33].setRotationPoint(19F, 2.5F, -11F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 21, 1, 22, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 19
		bodyModel[34].setRotationPoint(-1.4F, 2F, -11F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 1, 1, 22, 0F,0.7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.7F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 21
		bodyModel[35].setRotationPoint(18.6F, 3F, -11F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 1, 1, 22, 0F,-0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, 0F, 0F, -0.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.8F, 0F, 0F); // Box 22
		bodyModel[36].setRotationPoint(-1.9F, 2F, -11F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,3.3F, 0F, -1.7F, 0F, 0F, 0F, 0F, 0F, 0F, 3.3F, 0F, 1.7F, 0F, 0.5F, -1.7F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 1.7F); // Box 23
		bodyModel[37].setRotationPoint(-43F, -2F, -11F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 1, 1, 22, 0F,0.2F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -0.5F, -0.7F, 1F, -0.5F, 0F, 1F, 0F, 0F, 1F, 0F, -0.7F, 1F, -0.5F); // Box 25
		bodyModel[38].setRotationPoint(-40F, 1.5F, -11F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 1, 1, 22, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -0.7F, 0.5F, 0F, -0.7F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 27
		bodyModel[39].setRotationPoint(-25F, 2F, -11F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F,0F, 0.6F, 0F, -2F, 0.6F, 0F, -2F, 0.6F, 0F, 0F, 0.6F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 28
		bodyModel[40].setRotationPoint(-48F, 0F, -1F);

		bodyModel[41].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 28
		bodyModel[41].setRotationPoint(-53F, -0.5F, -1F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,-0.5F, 0.6F, 0F, 0.2F, 0.6F, 0F, 0.2F, 0.6F, 0F, -1F, 0.6F, 0F, -0.5F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, -1F, 0F, 0F); // Box 28
		bodyModel[42].setRotationPoint(-50F, -3F, 5F);

		bodyModel[43].addBox(0F, 0F, 0F, 2, 4, 2, 0F); // Box 31
		bodyModel[43].setRotationPoint(-55F, -2F, -2F);

		bodyModel[44].addShapeBox(0F, 0F, 0F, 91, 1, 20, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		bodyModel[44].setRotationPoint(-39F, -24F, -10F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 8, 3, 16, 0F,0F, 0F, 0F, 0.5F, 0F, 2F, 0.5F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 2F, 0.5F, 0F, 2F, 0F, 0F, 0F); // Box 61
		bodyModel[45].setRotationPoint(-47.5F, -24F, -8F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 7, 4, 17, 0F,0F, -0.5F, 0F, 0.3F, -0.5F, 1.7F, 0.3F, -0.5F, 1.3F, 0F, -0.5F, -0.4F, -3.3F, 0F, 0F, 0.3F, 0F, 1.7F, 0.3F, 0F, 1.3F, -3.3F, 0F, -0.4F); // Box 62
		bodyModel[46].setRotationPoint(-46.3F, -2.5F, -8.3F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 44, 1, 20, 0F,0F, 0F, 0F, 0.6F, 0F, 0F, 0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.6F, 0F, 0F, 0.6F, 0F, 0F, 0F, 0F, 0F); // Box 63
		bodyModel[47].setRotationPoint(-25F, -3F, -10F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 8, 1, 16, 0F,0F, 0F, 0F, 0.5F, 0F, 2F, 0.5F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 2F, 0.5F, 0F, 2F, 0F, 0F, 0F); // Box 64
		bodyModel[48].setRotationPoint(-47.5F, -3F, -8F);

		bodyModel[49].addBox(0F, 0F, 0F, 22, 4, 14, 0F); // Box 65
		bodyModel[49].setRotationPoint(-8.5F, -28F, -7F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Pantograph
		bodyModel[50].setRotationPoint(40F, -25F, -2.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[51].setRotationPoint(49F, -25F, -2.5F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[52].setRotationPoint(44.25F, -25F, -2.5F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Pantograph
		bodyModel[53].setRotationPoint(40F, -24.5F, -3F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Pantograph
		bodyModel[54].setRotationPoint(40F, -24.5F, 2F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[55].setRotationPoint(48.5F, -39F, -5F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.6F, -1.7F, 0F, -0.1F, -1.7F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.6F, 1F, 0F, -0.1F, 1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[56].setRotationPoint(48.5F, -39F, -7F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, -0.1F, -1.7F, 0F, -0.6F, -1.7F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.1F, 1F, 0F, -0.6F, 1F, 0F); // Pantograph
		bodyModel[57].setRotationPoint(48.5F, -39F, 5F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, -0.1F, -1.7F, 0F, -0.6F, -1.7F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.1F, 1F, 0F, -0.6F, 1F, 0F); // Pantograph
		bodyModel[58].setRotationPoint(47F, -39F, 5F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[59].setRotationPoint(47F, -39F, -5F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.6F, -1.7F, 0F, -0.1F, -1.7F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.6F, 1F, 0F, -0.1F, 1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[60].setRotationPoint(47F, -39F, -7F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Pantograph
		bodyModel[61].setRotationPoint(48F, -39F, -5F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Pantograph
		bodyModel[62].setRotationPoint(48F, -39F, -3F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Pantograph
		bodyModel[63].setRotationPoint(48F, -39F, 4F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Pantograph
		bodyModel[64].setRotationPoint(48F, -39F, 2F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[65].setRotationPoint(48.5F, -25.5F, -3F);

		bodyModel[66].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Pantograph
		bodyModel[66].setRotationPoint(46F, -25F, 1.5F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Pantograph
		bodyModel[67].setRotationPoint(46F, -25F, -2.5F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Pantograph
		bodyModel[68].setRotationPoint(46F, -25.5F, -3F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[69].setRotationPoint(46F, -25.5F, 2F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Pantograph
		bodyModel[70].setRotationPoint(45F, -25.5F, -2.5F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Pantograph
		bodyModel[71].setRotationPoint(46F, -25F, -1.5F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Pantograph
		bodyModel[72].setRotationPoint(46F, -25F, 0.5F);

		bodyModel[73].addBox(-0.5F, -0.5F, -1F, 1, 1, 2, 0F); // Pantograph
		bodyModel[73].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[73].rotateAngleZ = -0.6981317F;

		bodyModel[74].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Pantograph
		bodyModel[74].setRotationPoint(48F, -26F, -1.5F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Pantograph
		bodyModel[75].setRotationPoint(48F, -26F, 0.5F);

		bodyModel[76].addBox(-10.5F, -0.5F, -0.5F, 10, 1, 1, 0F); // Pantograph
		bodyModel[76].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[76].rotateAngleZ = -0.61086524F;

		bodyModel[77].addShapeBox(-3.5F, -11F, -0.25F, 11, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -4.75F, 0F, -0.5F, 4.25F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -4.75F, 0F, 0F, 4.25F, 0F, 0F, 0F); // Pantograph
		bodyModel[77].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[77].rotateAngleZ = 0.61086524F;

		bodyModel[78].addBox(-11.5F, -0.5F, -1F, 1, 1, 2, 0F); // Pantograph
		bodyModel[78].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[78].rotateAngleZ = -0.61086524F;

		bodyModel[79].addShapeBox(-3.5F, -11F, -0.75F, 11, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 4.25F, 0F, -0.5F, -4.75F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 4.25F, 0F, 0F, -4.75F, 0F, 0F, -0.5F); // Pantograph
		bodyModel[79].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[79].rotateAngleZ = 0.61086524F;

		bodyModel[80].addShapeBox(-9.5F, -0.5F, -0.5F, 10, 1, 1, 0F,0F, -0.3F, -0.3F, -0.5F, -0.3F, -0.3F, -0.5F, -0.3F, -0.3F, 0F, -0.3F, -0.3F, 0F, -0.3F, -0.3F, -0.5F, -0.3F, -0.3F, -0.5F, -0.3F, -0.3F, 0F, -0.3F, -0.3F); // Pantograph
		bodyModel[80].setRotationPoint(46.5F, -25.5F, 0F);
		bodyModel[80].rotateAngleZ = -0.66322512F;

		bodyModel[81].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Pantograph
		bodyModel[81].setRotationPoint(45.75F, -25.75F, -0.5F);

		bodyModel[82].addShapeBox(0F, -11F, 1.75F, 2, 1, 1, 0F,0F, -0.5F, 0.15F, -0.35F, 0F, 1.25F, 0.15F, 0F, -2.25F, -0.45F, -0.5F, -0.98F, 0F, 0F, 0.15F, -0.35F, -0.5F, 1.25F, 0.15F, -0.5F, -2.25F, -0.45F, 0F, -0.98F); // Pantograph
		bodyModel[82].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[82].rotateAngleZ = 0.61086524F;

		bodyModel[83].addShapeBox(1.65F, -11.5F, -0.5F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Pantograph
		bodyModel[83].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[83].rotateAngleZ = 0.61086524F;

		bodyModel[84].addShapeBox(2F, -11F, 1.75F, 3, 1, 1, 0F,-0.65F, 0F, 1.25F, 0F, -0.5F, -1.78F, -0.45F, -0.5F, 0.6F, -0.15F, 0F, -2.25F, -0.65F, -0.5F, 1.25F, 0F, 0F, -1.78F, -0.45F, 0F, 0.6F, -0.15F, -0.5F, -2.25F); // Pantograph
		bodyModel[84].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[84].rotateAngleZ = 0.61086524F;

		bodyModel[85].addShapeBox(2F, -11F, 2.25F, 3, 1, 1, 0F,-0.15F, 0F, -2.25F, -0.45F, -0.5F, 0.6F, 0F, -0.5F, -1.78F, -0.65F, 0F, 1.25F, -0.15F, -0.5F, -2.25F, -0.45F, 0F, 0.6F, 0F, 0F, -1.78F, -0.65F, -0.5F, 1.25F); // Pantograph
		bodyModel[85].setRotationPoint(48.5F, -25.5F, -5F);
		bodyModel[85].rotateAngleZ = 0.61086524F;

		bodyModel[86].addShapeBox(0F, -11F, 2.25F, 2, 1, 1, 0F,-0.45F, -0.5F, -0.98F, 0.15F, 0F, -2.25F, -0.35F, 0F, 1.25F, 0F, -0.5F, 0.15F, -0.45F, 0F, -0.98F, 0.15F, -0.5F, -2.25F, -0.35F, -0.5F, 1.25F, 0F, 0F, 0.15F); // Pantograph
		bodyModel[86].setRotationPoint(48.5F, -25.5F, -5F);
		bodyModel[86].rotateAngleZ = 0.61086524F;

		bodyModel[87].addShapeBox(-11.5F, 0.5F, -0.5F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Pantograph
		bodyModel[87].setRotationPoint(48.5F, -25.5F, 0F);
		bodyModel[87].rotateAngleZ = -0.61086524F;

		bodyModel[88].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 1020
		bodyModel[88].setRotationPoint(-47F, -26.7F, -1F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.5F, -0.3F, -0.5F, 0F, -0.3F, -0.5F, 0F, -0.3F, 0F, -0.5F, -0.3F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 104
		bodyModel[89].setRotationPoint(-47.25F, -25F, -0.75F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 1, 20, 0F,0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F); // Box 13
		bodyModel[90].setRotationPoint(-39F, 2F, -10F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 1, 2, 5, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Running Lights Left
		bodyModel[91].setRotationPoint(-49F, -6.5F, 3.75F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 2, 5, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Running Lights Right
		bodyModel[92].setRotationPoint(-49F, -6.5F, -8.75F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,-0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.25F, 0F, 0F); // Medium Marker Light Left
		bodyModel[93].setRotationPoint(-48F, -23.75F, 7.75F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,-0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.25F, 0F, 0F); // Medium Marker Light Right
		bodyModel[94].setRotationPoint(-48F, -23.75F, -9.25F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 1, 9, 50, 0F,-0.95F, -7F, -38F, 0F, -7F, -38F, 0F, -7F, 0F, -0.95F, -7F, 0F, -0.95F, 0.25F, -38F, 0F, 0.25F, -38F, 0F, 0.25F, 0F, -0.95F, 0.25F, 0F); // Rollsign Front
		bodyModel[95].setRotationPoint(-48.5F, -30.65F, -44F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 1, 3, 14, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		bodyModel[96].setRotationPoint(-9.5F, -28F, -7F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 1, 3, 14, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		bodyModel[97].setRotationPoint(13.5F, -28F, -7F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 1, 14, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 65
		bodyModel[98].setRotationPoint(-9.5F, -25F, -7F);

		bodyModel[99].addShapeBox(0F, 0F, 0F, 1, 1, 14, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 65
		bodyModel[99].setRotationPoint(13.5F, -25F, -7F);

		bodyModel[100].addBox(0F, 0F, 0F, 19, 1, 20, 0F); // Box 63
		bodyModel[100].setRotationPoint(33F, -3F, -10F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 13, 5, 12, 0F,0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F); // Box 63
		bodyModel[101].setRotationPoint(19.6F, -3F, -6F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 13, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 63
		bodyModel[102].setRotationPoint(19.6F, -1F, -8F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 13, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 63
		bodyModel[103].setRotationPoint(19.6F, 1F, -10F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 13, 3, 2, 0F,0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0.4F, 0.5F, 0F); // Box 63
		bodyModel[104].setRotationPoint(-37.95F, -1F, -8F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 13, 1, 2, 0F,0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0.4F, 0.5F, 0F); // Box 63
		bodyModel[105].setRotationPoint(-37.95F, 1F, -10F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 13, 3, 2, 0F,0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0.4F, 0.5F, 0F); // Box 63
		bodyModel[106].setRotationPoint(-37.95F, -1F, 6F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 13, 1, 2, 0F,0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0.4F, 0.5F, 0F); // Box 63
		bodyModel[107].setRotationPoint(-37.95F, 1F, 8F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 13, 3, 2, 0F,0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0.4F, 0.5F, 0F); // Box 63
		bodyModel[108].setRotationPoint(20F, -1F, 6F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 13, 1, 2, 0F,0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0.4F, 0.5F, 0F); // Box 63
		bodyModel[109].setRotationPoint(20F, 1F, 8F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 12, 5, 12, 0F,0F, 0F, 0F, 1.4F, 0F, 0F, 1.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.4F, 0F, 0F, 1.4F, 0F, 0F, 0F, 0F, 0F); // Box 63
		bodyModel[110].setRotationPoint(-38.4F, -3F, -6F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,-1F, 0.6F, 0F, 0.2F, 0.6F, 0F, 0.2F, 0.6F, 0F, -0.5F, 0.6F, 0F, -1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, -0.5F, 0F, 0F); // Box 28
		bodyModel[111].setRotationPoint(-50F, -3F, -9F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,-0.5F, 0.6F, 0F, 0.2F, 0.6F, 0F, 0.2F, 0.6F, 0F, -0.5F, 0.6F, 0F, -0.5F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, -0.5F, 0F, 0F); // Box 28
		bodyModel[112].setRotationPoint(-50F, -3F, -5F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 14, 1, 22, 0F,-0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F); // Box 19
		bodyModel[113].setRotationPoint(-39F, 2.5F, -11F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 1, 4, 20, 0F,0F, 0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, 0F, 0F, 0F); // Box 118
		bodyModel[114].setRotationPoint(33F, -2F, -10F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 1, 4, 20, 0F,0F, 0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, 0F, 0F, 0F); // Box 118
		bodyModel[115].setRotationPoint(-1.7F, -2F, -10F);

		bodyModel[116].addShapeBox(0F, 0F, 0F, 1, 4, 20, 0F,-1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 118
		bodyModel[116].setRotationPoint(-26F, -2F, -10F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 1, 4, 20, 0F,0F, 0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, 0F, 0F, 0F); // Box 118
		bodyModel[117].setRotationPoint(18.3F, -2F, -10F);

		bodyModel[118].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[118].setRotationPoint(14F, -6F, 3F);

		bodyModel[119].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[119].setRotationPoint(14F, -6F, -10F);

		bodyModel[120].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[120].setRotationPoint(18F, -10F, 3F);

		bodyModel[121].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[121].setRotationPoint(18F, -10F, -10F);

		bodyModel[122].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.25F, 0F, 0F); // Small Marker Light Right
		bodyModel[122].setRotationPoint(-48F, -23.25F, -9.25F);

		bodyModel[123].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.25F, 0F, 0F); // Small Marker Light Left
		bodyModel[123].setRotationPoint(-48F, -23.25F, 7.75F);

		bodyModel[124].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.25F, 0F, 0F); // Large Marker Light Left
		bodyModel[124].setRotationPoint(-47F, -23.75F, 6.75F);

		bodyModel[125].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.25F, 0F, 0F); // Large Marker Light Left
		bodyModel[125].setRotationPoint(-47F, -23.75F, -9.25F);

		bodyModel[126].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[126].setRotationPoint(5F, -6F, 3F);

		bodyModel[127].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[127].setRotationPoint(5F, -6F, -10F);

		bodyModel[128].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[128].setRotationPoint(4F, -10F, 3F);

		bodyModel[129].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[129].setRotationPoint(4F, -10F, -10F);

		bodyModel[130].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[130].setRotationPoint(0F, -6F, 3F);

		bodyModel[131].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[131].setRotationPoint(0F, -6F, -10F);

		bodyModel[132].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[132].setRotationPoint(-9F, -6F, 3F);

		bodyModel[133].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[133].setRotationPoint(-9F, -6F, -10F);

		bodyModel[134].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[134].setRotationPoint(-10F, -10F, 3F);

		bodyModel[135].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[135].setRotationPoint(-10F, -10F, -10F);

		bodyModel[136].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[136].setRotationPoint(-14F, -6F, 3F);

		bodyModel[137].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[137].setRotationPoint(-14F, -6F, -10F);

		bodyModel[138].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[138].setRotationPoint(-24.5F, -10F, 3F);

		bodyModel[139].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[139].setRotationPoint(-24.5F, -10F, -10F);

		bodyModel[140].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[140].setRotationPoint(-23.5F, -6F, 3F);

		bodyModel[141].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[141].setRotationPoint(-23.5F, -6F, -10F);

		bodyModel[142].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[142].setRotationPoint(33.5F, -10F, 3F);

		bodyModel[143].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[143].setRotationPoint(33.5F, -10F, -10F);

		bodyModel[144].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[144].setRotationPoint(34.5F, -6F, 3F);

		bodyModel[145].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[145].setRotationPoint(34.5F, -6F, -10F);

		bodyModel[146].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[146].setRotationPoint(44F, -6F, 3F);

		bodyModel[147].addBox(0F, 0F, 0F, 4, 1, 7, 0F); // Box 124
		bodyModel[147].setRotationPoint(44F, -6F, -10F);

		bodyModel[148].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[148].setRotationPoint(48F, -10F, 3F);

		bodyModel[149].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 126
		bodyModel[149].setRotationPoint(48F, -10F, -10F);

		bodyModel[150].addShapeBox(0F, 0F, 0F, 50, 9, 1, 0F,0F, -7F, -0.95F, -38F, -7F, -0.95F, -38F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, -0.95F, -38F, 0F, -0.95F, -38F, 0F, 0F, 0F, 0F, 0F); // Rollsign Side Exterior
		bodyModel[150].setRotationPoint(35.5F, -26.5F, 10.09F);

		bodyModel[151].addShapeBox(0F, 0F, 0F, 50, 9, 1, 0F,0F, -7F, -0.95F, -38F, -7F, -0.95F, -38F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, -0.95F, -38F, 0F, -0.95F, -38F, 0F, 0F, 0F, 0F, 0F); // Rollsign Side Interior
		bodyModel[151].setRotationPoint(35.5F, -26.5F, 8.99F);

		bodyModel[152].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 36
		bodyModel[152].setRotationPoint(-24.5F, -23.75F, -4.25F);

		bodyModel[153].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[153].setRotationPoint(-24.5F, -23.75F, 2.75F);

		bodyModel[154].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 36
		bodyModel[154].setRotationPoint(18F, -23.75F, -4.25F);

		bodyModel[155].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[155].setRotationPoint(18F, -23.75F, 2.75F);

		bodyModel[156].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 36
		bodyModel[156].setRotationPoint(4F, -23.75F, -4.25F);

		bodyModel[157].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[157].setRotationPoint(4F, -23.75F, 2.75F);

		bodyModel[158].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 36
		bodyModel[158].setRotationPoint(33.5F, -23.75F, -4.25F);

		bodyModel[159].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[159].setRotationPoint(33.5F, -23.75F, 2.75F);

		bodyModel[160].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 36
		bodyModel[160].setRotationPoint(48F, -23.75F, -4.25F);

		bodyModel[161].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[161].setRotationPoint(48F, -23.75F, 2.75F);

		bodyModel[162].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 36
		bodyModel[162].setRotationPoint(-10F, -23.75F, -4.25F);

		bodyModel[163].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[163].setRotationPoint(-10F, -23.75F, 2.75F);

		bodyModel[164].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[164].setRotationPoint(-24.5F, -18.75F, 3.25F);

		bodyModel[165].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[165].setRotationPoint(-24.5F, -18.75F, -10.5F);

		bodyModel[166].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[166].setRotationPoint(-24.5F, -12.75F, 3.25F);

		bodyModel[167].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[167].setRotationPoint(-24.5F, -12.75F, -10.5F);

		bodyModel[168].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[168].setRotationPoint(18F, -18.75F, 3.25F);

		bodyModel[169].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[169].setRotationPoint(18F, -18.75F, -10.5F);

		bodyModel[170].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[170].setRotationPoint(18F, -12.75F, 3.25F);

		bodyModel[171].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[171].setRotationPoint(18F, -12.75F, -10.5F);

		bodyModel[172].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[172].setRotationPoint(33.5F, -18.75F, 3.25F);

		bodyModel[173].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[173].setRotationPoint(33.5F, -18.75F, -10.5F);

		bodyModel[174].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[174].setRotationPoint(33.5F, -12.75F, 3.25F);

		bodyModel[175].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.25F, -0.25F, -0.5F, -0.25F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F); // Box 17
		bodyModel[175].setRotationPoint(33.5F, -12.75F, -10.5F);

		bodyModel[176].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[176].setRotationPoint(-24F, -22.75F, 2.75F);

		bodyModel[177].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[177].setRotationPoint(-24F, -22.75F, -4.25F);

		bodyModel[178].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[178].setRotationPoint(-9.5F, -22.75F, 2.75F);

		bodyModel[179].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[179].setRotationPoint(-9.5F, -22.75F, -4.25F);

		bodyModel[180].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[180].setRotationPoint(4.5F, -22.75F, 2.75F);

		bodyModel[181].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[181].setRotationPoint(4.5F, -22.75F, -4.25F);

		bodyModel[182].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[182].setRotationPoint(34F, -22.75F, 2.75F);

		bodyModel[183].addShapeBox(0F, 0F, 0F, 14, 1, 1, 0F,-0.25F, -0.5F, -0.5F, 0.25F, -0.5F, -0.5F, 0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 17
		bodyModel[183].setRotationPoint(34F, -22.75F, -4.25F);

		bodyModel[184].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F,0.15F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0.15F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 189
		bodyModel[184].setRotationPoint(-46.5F, -8.3F, 0F);

		bodyModel[185].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.1F, 0F, 0F, -0.1F, 0F, 0F, 0F, 0F, 0F); // Box 189
		bodyModel[185].setRotationPoint(-47.5F, -10F, 0F);

		bodyModel[186].addBox(0F, 0F, 0F, 4, 3, 5, 0F); // Box 124
		bodyModel[186].setRotationPoint(-44F, -6F, 1F);

		bodyModel[187].addBox(0F, 0F, 0F, 1, 7, 5, 0F); // Box 126
		bodyModel[187].setRotationPoint(-40F, -10F, 1F);

		bodyModel[188].addShapeBox(0F, 0F, 0F, 2, 5, 17, 0F,0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F); // Box 124
		bodyModel[188].setRotationPoint(-47.5F, -8F, -8F);

		bodyModel[189].addShapeBox(0F, 0F, 0F, 6, 5, 2, 0F,0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 1F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 1F, 0F, 0F, -0.5F); // Box 124
		bodyModel[189].setRotationPoint(-45.5F, -8F, 7F);

		bodyModel[190].addShapeBox(0F, 0F, 0F, 6, 5, 2, 0F,0F, 0F, -0.5F, 0.5F, 0F, 1F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, 1F, 0.5F, 0F, 0F, 0F, 0F, 0F); // Box 124
		bodyModel[190].setRotationPoint(-45.5F, -8F, -9F);

		bodyModel[191].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.1F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.1F, -0.5F, 0F, 0F); // Box 196
		bodyModel[191].setRotationPoint(-45.5F, -22.5F, -12.5F);

		bodyModel[192].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 196
		bodyModel[192].setRotationPoint(-45.5F, -22F, -12.5F);

		bodyModel[193].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, -0.5F, -0.3F, 1F, -0.5F, 0.7F, 1F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, -0.3F, 1F, 0F, 0.7F, 1F, 0F, -1F, 0F, 0F, 0F); // Box 196
		bodyModel[193].setRotationPoint(-45.75F, -19.5F, -11.9F);

		bodyModel[194].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, -0.1F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, -0.1F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 196
		bodyModel[194].setRotationPoint(-45.5F, -22.5F, 9.6F);

		bodyModel[195].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 196
		bodyModel[195].setRotationPoint(-45F, -22F, 11.6F);

		bodyModel[196].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, -0.5F, 0F, 1F, -0.5F, -1F, 1F, -0.5F, 0.7F, 0F, -0.5F, -0.3F, 0F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, 0.7F, 0F, 0F, -0.3F); // Box 196
		bodyModel[196].setRotationPoint(-45.75F, -19.5F, 11F);

		bodyModel[197].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F,-0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -6F, -0.5F, 0F, -6F, -0.9F, -6F, 0F, -0.1F, -6F, 0F, -0.1F, -6F, -6F, -0.9F, -6F, -6F); // Box 189
		bodyModel[197].setRotationPoint(-47.49F, -10F, 2F);

		bodyModel[198].addShapeBox(0F, 0F, 0F, 2, 8, 9, 0F,-1.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -6F, -1.5F, 0F, -6F, -1.9F, -6F, 0F, -0.1F, -6F, 0F, -0.1F, -6F, -6F, -1.9F, -6F, -6F); // Box 189
		bodyModel[198].setRotationPoint(-48.49F, -10F, 4F);

		bodyModel[199].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F,-1.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -6F, -1.5F, 0F, -6F, -1.9F, -6F, 0F, -0.1F, -6F, 0F, -0.1F, -6F, -6F, -1.9F, -6F, -6F); // Box 189
		bodyModel[199].setRotationPoint(-48.49F, -10F, 0F);

		bodyModel[200].addShapeBox(0F, 0F, 0F, 6, 1, 22, 0F,0.15F, 0F, 0F, -4F, -0.5F, 0F, -4F, -0.5F, -15F, 0.15F, 0F, -15F, 0.1F, -1F, 0F, -4F, -0.5F, 0F, -4F, -0.5F, -15F, 0.1F, -1F, -15F); // Box 189
		bodyModel[200].setRotationPoint(-46.5F, -8.31F, 0F);

		bodyModel[201].addBox(0F, 0F, 0F, 3, 23, 1, 0F); // Box 201
		bodyModel[201].setRotationPoint(52F, -24F, 9F);

		bodyModel[202].addBox(0F, 0F, 0F, 3, 23, 1, 0F); // Box 201
		bodyModel[202].setRotationPoint(52F, -24F, -10F);

		bodyModel[203].addBox(0F, 0F, 0F, 3, 1, 18, 0F); // Box 201
		bodyModel[203].setRotationPoint(52F, -24F, -9F);

		bodyModel[204].addBox(0F, 0F, 0F, 3, 1, 18, 0F); // Box 201
		bodyModel[204].setRotationPoint(52F, -3F, -9F);

		bodyModel[205].addShapeBox(0F, 0F, 0F, 6, 23, 1, 0F,0F, 0F, -0.8F, 0.4F, 0F, -0.8F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Left Side Plug Door SD160
		bodyModel[205].setRotationPoint(-38.4F, -21F, 10F);

		bodyModel[206].addShapeBox(0F, 0F, 0F, 6, 23, 1, 0F,0F, 0F, -0.8F, 0.4F, 0F, -0.8F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Right Side Plug Door SD160
		bodyModel[206].setRotationPoint(-38.4F, -21F, -11.8F);

		bodyModel[207].addShapeBox(0F, 0F, 0F, 6, 23, 1, 0F,0F, 0F, -0.8F, 0.4F, 0F, -0.8F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Left Side Plug Door SD160
		bodyModel[207].setRotationPoint(19.6F, -21F, 10F);

		bodyModel[208].addShapeBox(0F, 0F, 0F, 6, 23, 1, 0F,0F, 0F, -0.8F, 0.4F, 0F, -0.8F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Right Side Plug Door SD160
		bodyModel[208].setRotationPoint(19.6F, -21F, -11.8F);

		bodyModel[209].addShapeBox(0F, 0F, 0F, 7, 23, 1, 0F,0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Right Side Plug Door SD160
		bodyModel[209].setRotationPoint(-32F, -21F, -11.8F);

		bodyModel[210].addShapeBox(0F, 0F, 0F, 7, 23, 1, 0F,0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Left Side Plug Door SD160
		bodyModel[210].setRotationPoint(-32F, -21F, 10F);

		bodyModel[211].addShapeBox(0F, 0F, 0F, 7, 23, 1, 0F,0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Right Side Plug Door SD160
		bodyModel[211].setRotationPoint(26F, -21F, -11.8F);

		bodyModel[212].addShapeBox(0F, 0F, 0F, 7, 23, 1, 0F,0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, -0.8F, 0F, 0.4F, -0.8F, 0.4F, 0.4F, 0F, 0F, 0.4F, 0F); // Left Side Plug Door SD160
		bodyModel[212].setRotationPoint(26F, -21F, 10F);

		bodyModel[213].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 294
		bodyModel[213].setRotationPoint(-35F, -25F, -2.5F);

		bodyModel[214].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 299
		bodyModel[214].setRotationPoint(-26F, -25F, -2.5F);

		bodyModel[215].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 301
		bodyModel[215].setRotationPoint(-35F, -24.5F, -3F);

		bodyModel[216].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 302
		bodyModel[216].setRotationPoint(-35F, -24.5F, 2F);

		bodyModel[217].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 342
		bodyModel[217].setRotationPoint(-26.5F, -27.25F, -5F);

		bodyModel[218].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.6F, -1.7F, 0F, -0.1F, -1.7F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.6F, 1F, 0F, -0.1F, 1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 343
		bodyModel[218].setRotationPoint(-26.5F, -27.25F, -7F);

		bodyModel[219].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, -0.1F, -1.7F, 0F, -0.6F, -1.7F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.1F, 1F, 0F, -0.6F, 1F, 0F); // Box 344
		bodyModel[219].setRotationPoint(-26.5F, -27.25F, 5F);

		bodyModel[220].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, -0.1F, -1.7F, 0F, -0.6F, -1.7F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.1F, 1F, 0F, -0.6F, 1F, 0F); // Box 345
		bodyModel[220].setRotationPoint(-28F, -27.25F, 5F);

		bodyModel[221].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 346
		bodyModel[221].setRotationPoint(-28F, -27.25F, -5F);

		bodyModel[222].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.6F, -1.7F, 0F, -0.1F, -1.7F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.6F, 1F, 0F, -0.1F, 1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 347
		bodyModel[222].setRotationPoint(-28F, -27.25F, -7F);

		bodyModel[223].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 348
		bodyModel[223].setRotationPoint(-27F, -27.25F, -5F);

		bodyModel[224].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 349
		bodyModel[224].setRotationPoint(-27F, -27.25F, -3F);

		bodyModel[225].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 350
		bodyModel[225].setRotationPoint(-27F, -27.25F, 4F);

		bodyModel[226].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 351
		bodyModel[226].setRotationPoint(-27F, -27.25F, 2F);

		bodyModel[227].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 352
		bodyModel[227].setRotationPoint(-26.5F, -25.5F, -3F);

		bodyModel[228].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 353
		bodyModel[228].setRotationPoint(-29F, -25F, 1.5F);

		bodyModel[229].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 354
		bodyModel[229].setRotationPoint(-29F, -25F, -2.5F);

		bodyModel[230].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 355
		bodyModel[230].setRotationPoint(-29F, -25.5F, -3F);

		bodyModel[231].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 356
		bodyModel[231].setRotationPoint(-29F, -25.5F, 2F);

		bodyModel[232].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F,-0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 358
		bodyModel[232].setRotationPoint(-30F, -25.5F, -2.5F);

		bodyModel[233].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 360
		bodyModel[233].setRotationPoint(-29F, -25F, 0.5F);

		bodyModel[234].addBox(-0.5F, -0.5F, -1F, 1, 1, 2, 0F); // Box 361
		bodyModel[234].setRotationPoint(-26.5F, -25.5F, 0F);

		bodyModel[235].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 362
		bodyModel[235].setRotationPoint(-27F, -26F, -1.5F);

		bodyModel[236].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 363
		bodyModel[236].setRotationPoint(-27F, -26F, 0.5F);

		bodyModel[237].addBox(-10.5F, -0.5F, -0.5F, 10, 1, 1, 0F); // Box 365
		bodyModel[237].setRotationPoint(-26.5F, -25.5F, 0F);

		bodyModel[238].addShapeBox(-4.25F, -10.4F, -0.25F, 11, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -4.75F, 0F, -0.5F, 4.25F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -4.75F, 0F, 0F, 4.25F, 0F, 0F, 0F); // Box 366
		bodyModel[238].setRotationPoint(-32F, -16.2F, 0F);
		bodyModel[238].rotateAngleZ = 0.08726646F;

		bodyModel[239].addBox(-10.75F, 3F, -1F, 1, 1, 2, 0F); // Box 367
		bodyModel[239].setRotationPoint(-31.5F, -16.2F, 0F);
		bodyModel[239].rotateAngleZ = -1.36135682F;

		bodyModel[240].addShapeBox(-4.25F, -10.4F, -0.75F, 11, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 4.25F, 0F, -0.5F, -4.75F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 4.25F, 0F, 0F, -4.75F, 0F, 0F, -0.5F); // Box 368
		bodyModel[240].setRotationPoint(-32F, -16.2F, 0F);
		bodyModel[240].rotateAngleZ = 0.08726646F;

		bodyModel[241].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 370
		bodyModel[241].setRotationPoint(-29.25F, -25.75F, -0.5F);

		bodyModel[242].addShapeBox(-0.75F, -10.4F, 1.75F, 2, 1, 1, 0F,0F, -0.5F, 0.15F, -0.35F, 0F, 1.25F, 0.15F, 0F, -2.25F, -0.45F, -0.5F, -0.98F, 0F, 0F, 0.15F, -0.35F, -0.5F, 1.25F, 0.15F, -0.5F, -2.25F, -0.45F, 0F, -0.98F); // Box 371
		bodyModel[242].setRotationPoint(-32F, -16.2F, 0F);
		bodyModel[242].rotateAngleZ = 0.08726646F;

		bodyModel[243].addShapeBox(1.46F, -10.85F, -0.5F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 373
		bodyModel[243].setRotationPoint(-32F, -16.2F, 0F);
		bodyModel[243].rotateAngleZ = 0.13962634F;

		bodyModel[244].addShapeBox(1.25F, -10.4F, 1.75F, 3, 1, 1, 0F,-0.65F, 0F, 1.25F, 0F, -0.5F, -1.78F, -0.45F, -0.5F, 0.6F, -0.15F, 0F, -2.25F, -0.65F, -0.5F, 1.25F, 0F, 0F, -1.78F, -0.45F, 0F, 0.6F, -0.15F, -0.5F, -2.25F); // Box 376
		bodyModel[244].setRotationPoint(-32F, -16.2F, 0F);
		bodyModel[244].rotateAngleZ = 0.08726646F;

		bodyModel[245].addShapeBox(1.25F, -10.4F, 2.25F, 3, 1, 1, 0F,-0.15F, 0F, -2.25F, -0.45F, -0.5F, 0.6F, 0F, -0.5F, -1.78F, -0.65F, 0F, 1.25F, -0.15F, -0.5F, -2.25F, -0.45F, 0F, 0.6F, 0F, 0F, -1.78F, -0.65F, -0.5F, 1.25F); // Box 377
		bodyModel[245].setRotationPoint(-32F, -16.2F, -5F);
		bodyModel[245].rotateAngleZ = 0.08726646F;

		bodyModel[246].addShapeBox(-0.75F, -10.4F, 2.25F, 2, 1, 1, 0F,-0.45F, -0.5F, -0.98F, 0.15F, 0F, -2.25F, -0.35F, 0F, 1.25F, 0F, -0.5F, 0.15F, -0.45F, 0F, -0.98F, 0.15F, -0.5F, -2.25F, -0.35F, -0.5F, 1.25F, 0F, 0F, 0.15F); // Box 378
		bodyModel[246].setRotationPoint(-32F, -16.2F, -5F);
		bodyModel[246].rotateAngleZ = 0.08726646F;

		bodyModel[247].addShapeBox(-10.75F, 4F, -0.5F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 380
		bodyModel[247].setRotationPoint(-31.5F, -16.2F, 0F);
		bodyModel[247].rotateAngleZ = -1.36135682F;

		bodyModel[248].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 360
		bodyModel[248].setRotationPoint(-29F, -25F, -2F);

		bodyModel[249].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 250
		bodyModel[249].setRotationPoint(-50.01F, -6F, -8.75F);

		bodyModel[250].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 250
		bodyModel[250].setRotationPoint(-50.01F, -6F, -5.75F);

		bodyModel[251].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 250
		bodyModel[251].setRotationPoint(-50.01F, -6F, -6.75F);

		bodyModel[252].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 250
		bodyModel[252].setRotationPoint(-50.01F, -6F, 3.75F);

		bodyModel[253].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 250
		bodyModel[253].setRotationPoint(-50.01F, -6F, 6.75F);

		bodyModel[254].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 250
		bodyModel[254].setRotationPoint(-50.01F, -6F, 5.75F);

		bodyModel[255].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Medium Marker Light Left
		bodyModel[255].setRotationPoint(-48.26F, -23.75F, 7.75F);

		bodyModel[256].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Medium Marker Light Right
		bodyModel[256].setRotationPoint(-48.26F, -23.75F, -9.25F);

		bodyModel[257].addBox(0F, 0F, 0F, 0, 2, 2, 0F); // High Beam Headlight
		bodyModel[257].setRotationPoint(-47.01F, -26.7F, -1F);

		bodyModel[258].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Large Marker Light Left
		bodyModel[258].setRotationPoint(-48.26F, -23.75F, 6.75F);

		bodyModel[259].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Large Marker Light Left
		bodyModel[259].setRotationPoint(-48.26F, -23.75F, -9.25F);

		bodyModel[260].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.75F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.75F, 0F, 0F); // Large Marker Light Left
		bodyModel[260].setRotationPoint(-48.26F, -23.75F, 6.75F);

		bodyModel[261].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.75F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.75F, 0F, 0F); // Large Marker Light Left
		bodyModel[261].setRotationPoint(-48.26F, -23.75F, -9.25F);
	}
	public float[] getTrans() {
		return new float[]{ -2.34f, -0.06f, 0.0f };
	}
}