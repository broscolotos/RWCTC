//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2023 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: Wood Panel Stock Car
// Model Creator: BlueTheWolf1204
// Created on: 30.12.2022 - 16:18:21
// Last changed on: 30.12.2022 - 16:18:21

package train.client.render.models; //Path where the model is located

import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.api.EntityRollingStock;
import train.common.library.Info;

public class ModelWoodPanelStockCar extends ModelConverter //Same as Filename
{
	int textureX = 1024;
	int textureY = 1024;
	public ModelFreightTruckM bogie = new ModelFreightTruckM();

	public ModelWoodPanelStockCar() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[147];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}
	@Override
	public void render(Entity entity, float f0, float f1, float f2, float f3, float f4, float scale){
		super.render(entity, f0, f1, f2, f3, f4, scale);
		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, Info.trainsPrefix + "freighttruckm.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(1.68f,-0.1f,-0.18f);
		bogie.render(entity, f0, f1, f2, f3, f4, scale);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glTranslatef(-2.825f,-0.1f,-0.18f);
		bogie.render(entity, f0, f1, f2, f3, f4, scale);
		GL11.glPopMatrix();
	}
	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 241, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 321, 1, textureX, textureY); // Box 3
		bodyModel[3] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 4
		bodyModel[4] = new ModelRendererTurbo(this, 241, 9, textureX, textureY); // Box 5
		bodyModel[5] = new ModelRendererTurbo(this, 337, 9, textureX, textureY); // Box 6
		bodyModel[6] = new ModelRendererTurbo(this, 393, 9, textureX, textureY); // Box 8
		bodyModel[7] = new ModelRendererTurbo(this, 313, 9, textureX, textureY); // Box 9
		bodyModel[8] = new ModelRendererTurbo(this, 433, 9, textureX, textureY); // Box 10
		bodyModel[9] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 11
		bodyModel[10] = new ModelRendererTurbo(this, 233, 33, textureX, textureY); // Box 12
		bodyModel[11] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 13
		bodyModel[12] = new ModelRendererTurbo(this, 225, 41, textureX, textureY); // Box 14
		bodyModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 18
		bodyModel[14] = new ModelRendererTurbo(this, 9, 1, textureX, textureY); // Box 20
		bodyModel[15] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // Box 21
		bodyModel[16] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 22
		bodyModel[17] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 23
		bodyModel[18] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 24
		bodyModel[19] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 25
		bodyModel[20] = new ModelRendererTurbo(this, 473, 9, textureX, textureY); // Box 27
		bodyModel[21] = new ModelRendererTurbo(this, 481, 9, textureX, textureY); // Box 29
		bodyModel[22] = new ModelRendererTurbo(this, 489, 9, textureX, textureY); // Box 30
		bodyModel[23] = new ModelRendererTurbo(this, 497, 25, textureX, textureY); // Box 37
		bodyModel[24] = new ModelRendererTurbo(this, 505, 25, textureX, textureY); // Box 38
		bodyModel[25] = new ModelRendererTurbo(this, 457, 33, textureX, textureY); // Box 39
		bodyModel[26] = new ModelRendererTurbo(this, 465, 33, textureX, textureY); // Box 40
		bodyModel[27] = new ModelRendererTurbo(this, 337, 17, textureX, textureY); // Box 49
		bodyModel[28] = new ModelRendererTurbo(this, 337, 25, textureX, textureY); // Box 50
		bodyModel[29] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 51
		bodyModel[30] = new ModelRendererTurbo(this, 81, 49, textureX, textureY); // Box 52
		bodyModel[31] = new ModelRendererTurbo(this, 161, 49, textureX, textureY); // Box 57
		bodyModel[32] = new ModelRendererTurbo(this, 257, 49, textureX, textureY); // Box 58
		bodyModel[33] = new ModelRendererTurbo(this, 353, 49, textureX, textureY); // Box 59
		bodyModel[34] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 60
		bodyModel[35] = new ModelRendererTurbo(this, 81, 57, textureX, textureY); // Box 65
		bodyModel[36] = new ModelRendererTurbo(this, 177, 57, textureX, textureY); // Box 66
		bodyModel[37] = new ModelRendererTurbo(this, 273, 57, textureX, textureY); // Box 67
		bodyModel[38] = new ModelRendererTurbo(this, 353, 57, textureX, textureY); // Box 68
		bodyModel[39] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 73
		bodyModel[40] = new ModelRendererTurbo(this, 97, 65, textureX, textureY); // Box 74
		bodyModel[41] = new ModelRendererTurbo(this, 433, 57, textureX, textureY); // Box 75
		bodyModel[42] = new ModelRendererTurbo(this, 193, 65, textureX, textureY); // Box 76
		bodyModel[43] = new ModelRendererTurbo(this, 273, 65, textureX, textureY); // Box 79
		bodyModel[44] = new ModelRendererTurbo(this, 329, 65, textureX, textureY); // Box 80
		bodyModel[45] = new ModelRendererTurbo(this, 385, 65, textureX, textureY); // Box 83
		bodyModel[46] = new ModelRendererTurbo(this, 441, 65, textureX, textureY); // Box 84
		bodyModel[47] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Box 87
		bodyModel[48] = new ModelRendererTurbo(this, 57, 73, textureX, textureY); // Box 88
		bodyModel[49] = new ModelRendererTurbo(this, 113, 73, textureX, textureY); // Box 91
		bodyModel[50] = new ModelRendererTurbo(this, 169, 73, textureX, textureY); // Box 92
		bodyModel[51] = new ModelRendererTurbo(this, 457, 33, textureX, textureY); // Box 95
		bodyModel[52] = new ModelRendererTurbo(this, 249, 57, textureX, textureY); // Box 96
		bodyModel[53] = new ModelRendererTurbo(this, 209, 73, textureX, textureY); // Box 99
		bodyModel[54] = new ModelRendererTurbo(this, 273, 73, textureX, textureY); // Box 100
		bodyModel[55] = new ModelRendererTurbo(this, 321, 73, textureX, textureY); // Box 103
		bodyModel[56] = new ModelRendererTurbo(this, 369, 73, textureX, textureY); // Box 104
		bodyModel[57] = new ModelRendererTurbo(this, 417, 73, textureX, textureY); // Box 107
		bodyModel[58] = new ModelRendererTurbo(this, 465, 73, textureX, textureY); // Box 108
		bodyModel[59] = new ModelRendererTurbo(this, 481, 33, textureX, textureY); // Box 109
		bodyModel[60] = new ModelRendererTurbo(this, 489, 33, textureX, textureY); // Box 110
		bodyModel[61] = new ModelRendererTurbo(this, 241, 17, textureX, textureY); // Box 111
		bodyModel[62] = new ModelRendererTurbo(this, 433, 17, textureX, textureY); // Box 112
		bodyModel[63] = new ModelRendererTurbo(this, 225, 33, textureX, textureY); // Box 113
		bodyModel[64] = new ModelRendererTurbo(this, 433, 49, textureX, textureY); // Box 114
		bodyModel[65] = new ModelRendererTurbo(this, 497, 65, textureX, textureY); // Box 115
		bodyModel[66] = new ModelRendererTurbo(this, 233, 73, textureX, textureY); // Box 116
		bodyModel[67] = new ModelRendererTurbo(this, 297, 73, textureX, textureY); // Box 117
		bodyModel[68] = new ModelRendererTurbo(this, 313, 73, textureX, textureY); // Box 118
		bodyModel[69] = new ModelRendererTurbo(this, 329, 73, textureX, textureY); // Box 119
		bodyModel[70] = new ModelRendererTurbo(this, 345, 73, textureX, textureY); // Box 120
		bodyModel[71] = new ModelRendererTurbo(this, 353, 73, textureX, textureY); // Box 121
		bodyModel[72] = new ModelRendererTurbo(this, 369, 73, textureX, textureY); // Box 122
		bodyModel[73] = new ModelRendererTurbo(this, 377, 73, textureX, textureY); // Box 123
		bodyModel[74] = new ModelRendererTurbo(this, 393, 73, textureX, textureY); // Box 124
		bodyModel[75] = new ModelRendererTurbo(this, 409, 73, textureX, textureY); // Box 125
		bodyModel[76] = new ModelRendererTurbo(this, 425, 73, textureX, textureY); // Box 126
		bodyModel[77] = new ModelRendererTurbo(this, 441, 73, textureX, textureY); // Box 127
		bodyModel[78] = new ModelRendererTurbo(this, 457, 73, textureX, textureY); // Box 128
		bodyModel[79] = new ModelRendererTurbo(this, 473, 73, textureX, textureY); // Box 129
		bodyModel[80] = new ModelRendererTurbo(this, 489, 73, textureX, textureY); // Box 130
		bodyModel[81] = new ModelRendererTurbo(this, 249, 17, textureX, textureY); // Box 131
		bodyModel[82] = new ModelRendererTurbo(this, 257, 17, textureX, textureY); // Box 132
		bodyModel[83] = new ModelRendererTurbo(this, 441, 17, textureX, textureY); // Box 133
		bodyModel[84] = new ModelRendererTurbo(this, 257, 25, textureX, textureY); // Box 134
		bodyModel[85] = new ModelRendererTurbo(this, 433, 25, textureX, textureY); // Box 135
		bodyModel[86] = new ModelRendererTurbo(this, 441, 25, textureX, textureY); // Box 136
		bodyModel[87] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 137
		bodyModel[88] = new ModelRendererTurbo(this, 233, 33, textureX, textureY); // Box 138
		bodyModel[89] = new ModelRendererTurbo(this, 449, 41, textureX, textureY); // Box 139
		bodyModel[90] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 140
		bodyModel[91] = new ModelRendererTurbo(this, 225, 41, textureX, textureY); // Box 141
		bodyModel[92] = new ModelRendererTurbo(this, 505, 73, textureX, textureY); // Box 142
		bodyModel[93] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 143
		bodyModel[94] = new ModelRendererTurbo(this, 9, 81, textureX, textureY); // Box 144
		bodyModel[95] = new ModelRendererTurbo(this, 17, 81, textureX, textureY); // Box 145
		bodyModel[96] = new ModelRendererTurbo(this, 25, 81, textureX, textureY); // Box 146
		bodyModel[97] = new ModelRendererTurbo(this, 33, 81, textureX, textureY); // Box 147
		bodyModel[98] = new ModelRendererTurbo(this, 41, 81, textureX, textureY); // Box 148
		bodyModel[99] = new ModelRendererTurbo(this, 449, 49, textureX, textureY); // Box 149
		bodyModel[100] = new ModelRendererTurbo(this, 441, 49, textureX, textureY); // Box 150
		bodyModel[101] = new ModelRendererTurbo(this, 497, 49, textureX, textureY); // Box 151
		bodyModel[102] = new ModelRendererTurbo(this, 505, 49, textureX, textureY); // Box 152
		bodyModel[103] = new ModelRendererTurbo(this, 505, 65, textureX, textureY); // Box 153
		bodyModel[104] = new ModelRendererTurbo(this, 241, 73, textureX, textureY); // Box 154
		bodyModel[105] = new ModelRendererTurbo(this, 249, 73, textureX, textureY); // Box 155
		bodyModel[106] = new ModelRendererTurbo(this, 257, 73, textureX, textureY); // Box 156
		bodyModel[107] = new ModelRendererTurbo(this, 273, 73, textureX, textureY); // Box 157
		bodyModel[108] = new ModelRendererTurbo(this, 281, 73, textureX, textureY); // Box 158
		bodyModel[109] = new ModelRendererTurbo(this, 305, 73, textureX, textureY); // Box 159
		bodyModel[110] = new ModelRendererTurbo(this, 49, 81, textureX, textureY); // Box 160
		bodyModel[111] = new ModelRendererTurbo(this, 57, 81, textureX, textureY); // Box 161
		bodyModel[112] = new ModelRendererTurbo(this, 65, 81, textureX, textureY); // Box 162
		bodyModel[113] = new ModelRendererTurbo(this, 73, 81, textureX, textureY); // Box 163
		bodyModel[114] = new ModelRendererTurbo(this, 81, 81, textureX, textureY); // Box 164
		bodyModel[115] = new ModelRendererTurbo(this, 89, 81, textureX, textureY); // Box 0
		bodyModel[116] = new ModelRendererTurbo(this, 105, 81, textureX, textureY); // Box 0
		bodyModel[117] = new ModelRendererTurbo(this, 121, 81, textureX, textureY); // Box 11
		bodyModel[118] = new ModelRendererTurbo(this, 89, 89, textureX, textureY); // Box 14
		bodyModel[119] = new ModelRendererTurbo(this, 89, 97, textureX, textureY); // Box 11
		bodyModel[120] = new ModelRendererTurbo(this, 161, 97, textureX, textureY); // Box 14
		bodyModel[121] = new ModelRendererTurbo(this, 161, 89, textureX, textureY); // Box 11
		bodyModel[122] = new ModelRendererTurbo(this, 233, 97, textureX, textureY); // Box 11
		bodyModel[123] = new ModelRendererTurbo(this, 305, 97, textureX, textureY); // Box 11
		bodyModel[124] = new ModelRendererTurbo(this, 377, 97, textureX, textureY); // Box 11
		bodyModel[125] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // Box 11
		bodyModel[126] = new ModelRendererTurbo(this, 73, 105, textureX, textureY); // Box 14
		bodyModel[127] = new ModelRendererTurbo(this, 145, 105, textureX, textureY); // Box 11
		bodyModel[128] = new ModelRendererTurbo(this, 217, 105, textureX, textureY); // Box 14
		bodyModel[129] = new ModelRendererTurbo(this, 289, 105, textureX, textureY); // Box 11
		bodyModel[130] = new ModelRendererTurbo(this, 361, 105, textureX, textureY); // Box 11
		bodyModel[131] = new ModelRendererTurbo(this, 433, 105, textureX, textureY); // Box 11
		bodyModel[132] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 11
		bodyModel[133] = new ModelRendererTurbo(this, 193, 81, textureX, textureY); // Box 0
		bodyModel[134] = new ModelRendererTurbo(this, 209, 81, textureX, textureY); // Box 0
		bodyModel[135] = new ModelRendererTurbo(this, 73, 113, textureX, textureY); // Box 11
		bodyModel[136] = new ModelRendererTurbo(this, 145, 113, textureX, textureY); // Box 11
		bodyModel[137] = new ModelRendererTurbo(this, 217, 113, textureX, textureY); // Box 11
		bodyModel[138] = new ModelRendererTurbo(this, 289, 113, textureX, textureY); // Box 11
		bodyModel[139] = new ModelRendererTurbo(this, 233, 81, textureX, textureY); // Box 122
		bodyModel[140] = new ModelRendererTurbo(this, 241, 81, textureX, textureY); // Box 122
		bodyModel[141] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 23
		bodyModel[142] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 24
		bodyModel[143] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 25
		bodyModel[144] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 23
		bodyModel[145] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 24
		bodyModel[146] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 25

		bodyModel[0].addBox(0F, 0F, 0F, 105, 1, 23, 0F); // Box 0
		bodyModel[0].setRotationPoint(-50F, 0F, -11.5F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 1
		bodyModel[1].setRotationPoint(19F, -18F, 10F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 3
		bodyModel[2].setRotationPoint(-49F, -18F, 10F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 4
		bodyModel[3].setRotationPoint(19F, -18F, -10.5F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 5
		bodyModel[4].setRotationPoint(-49F, -18F, -10.5F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 6
		bodyModel[5].setRotationPoint(-5.5F, -18F, -11F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 8
		bodyModel[6].setRotationPoint(-5.5F, -18F, 10.5F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 9
		bodyModel[7].setRotationPoint(-50F, -18F, -10F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 10
		bodyModel[8].setRotationPoint(54.5F, -18F, -10F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 106, 1, 6, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[9].setRotationPoint(-50.5F, -19F, -12F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 106, 2, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, 1F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 12
		bodyModel[10].setRotationPoint(-50.5F, -20F, -6F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 106, 2, 5, 0F,0F, 1F, 1F, 0F, 1F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		bodyModel[11].setRotationPoint(-50.5F, -20F, 1F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 106, 1, 6, 0F,0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 14
		bodyModel[12].setRotationPoint(-50.5F, -19F, 6F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		bodyModel[13].setRotationPoint(54F, -18F, -11F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 14F, 0F, 0F, -14F, 0F, 0F, -14F, 0F, -0.5F, 14F, 0F, -0.5F); // Box 20
		bodyModel[14].setRotationPoint(53F, -18F, -11F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -14F, 0F, 0F, 14F, 0F, 0F, 14F, 0F, -0.5F, -14F, 0F, -0.5F); // Box 21
		bodyModel[15].setRotationPoint(20.5F, -18F, -11F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 22
		bodyModel[16].setRotationPoint(36.5F, -18F, -11F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 14F, 0F, 0F, -14F, 0F, 0F, -14F, 0F, -0.5F, 14F, 0F, -0.5F); // Box 23
		bodyModel[17].setRotationPoint(53F, -18F, 10.5F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 24
		bodyModel[18].setRotationPoint(36.5F, -18F, 10.5F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -14F, 0F, 0F, 14F, 0F, 0F, 14F, 0F, -0.5F, -14F, 0F, -0.5F); // Box 25
		bodyModel[19].setRotationPoint(20.5F, -18F, 10.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 27
		bodyModel[20].setRotationPoint(-36.5F, -18F, -11F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 8F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -0.5F, 8F, 0F, -0.5F); // Box 29
		bodyModel[21].setRotationPoint(-24.25F, -18F, 10.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 30
		bodyModel[22].setRotationPoint(-36.5F, -18F, 10.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 37
		bodyModel[23].setRotationPoint(19.5F, -18F, 11F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 38
		bodyModel[24].setRotationPoint(-6.5F, -18F, 11F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 39
		bodyModel[25].setRotationPoint(19.5F, -18F, -11.5F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 40
		bodyModel[26].setRotationPoint(-6.5F, -18F, -11.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 49
		bodyModel[27].setRotationPoint(-49F, -14F, -10.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 50
		bodyModel[28].setRotationPoint(-49F, -14F, 10F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 51
		bodyModel[29].setRotationPoint(19F, -14F, 10F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 52
		bodyModel[30].setRotationPoint(19F, -14F, -10.5F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 57
		bodyModel[31].setRotationPoint(-49F, -10F, -10.5F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 58
		bodyModel[32].setRotationPoint(-49F, -10F, 10F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 59
		bodyModel[33].setRotationPoint(19F, -10F, 10F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 60
		bodyModel[34].setRotationPoint(19F, -10F, -10.5F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 65
		bodyModel[35].setRotationPoint(-49F, -6F, -10.5F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 66
		bodyModel[36].setRotationPoint(-49F, -6F, 10F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 67
		bodyModel[37].setRotationPoint(19F, -6F, 10F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 68
		bodyModel[38].setRotationPoint(19F, -6F, -10.5F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 73
		bodyModel[39].setRotationPoint(-49F, -2F, -10.5F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 44, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 74
		bodyModel[40].setRotationPoint(-49F, -2F, 10F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 75
		bodyModel[41].setRotationPoint(19F, -2F, 10F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 35, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 76
		bodyModel[42].setRotationPoint(19F, -2F, -10.5F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 79
		bodyModel[43].setRotationPoint(-5.5F, -14F, -11F);

		bodyModel[44].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 80
		bodyModel[44].setRotationPoint(-5.5F, -14F, 10.5F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 83
		bodyModel[45].setRotationPoint(-5.5F, -10F, -11F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 84
		bodyModel[46].setRotationPoint(-5.5F, -10F, 10.5F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 87
		bodyModel[47].setRotationPoint(-5.5F, -6F, -11F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 88
		bodyModel[48].setRotationPoint(-5.5F, -6F, 10.5F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 91
		bodyModel[49].setRotationPoint(-5.5F, -2F, -11F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 25, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 92
		bodyModel[50].setRotationPoint(-5.5F, -2F, 10.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 95
		bodyModel[51].setRotationPoint(54.5F, -14F, -10F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 96
		bodyModel[52].setRotationPoint(-50F, -14F, -10F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 99
		bodyModel[53].setRotationPoint(54.5F, -10F, -10F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 100
		bodyModel[54].setRotationPoint(-50F, -10F, -10F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 103
		bodyModel[55].setRotationPoint(54.5F, -6F, -10F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 104
		bodyModel[56].setRotationPoint(-50F, -6F, -10F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 107
		bodyModel[57].setRotationPoint(54.5F, -2F, -10F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 108
		bodyModel[58].setRotationPoint(-50F, -2F, -10F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 109
		bodyModel[59].setRotationPoint(-50.5F, -18F, -9F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 110
		bodyModel[60].setRotationPoint(-50.5F, -18F, -6F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 111
		bodyModel[61].setRotationPoint(-50.5F, -17F, -8.5F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 112
		bodyModel[62].setRotationPoint(-50.5F, -15F, -8.5F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 113
		bodyModel[63].setRotationPoint(-50.5F, -13F, -8.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 114
		bodyModel[64].setRotationPoint(-50.5F, -11F, -8.5F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 115
		bodyModel[65].setRotationPoint(-50.5F, -9F, -8.5F);

		bodyModel[66].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 116
		bodyModel[66].setRotationPoint(-50.5F, -7F, -8.5F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 117
		bodyModel[67].setRotationPoint(-50.5F, -5F, -8.5F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 118
		bodyModel[68].setRotationPoint(-50.5F, -3F, -8.5F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 119
		bodyModel[69].setRotationPoint(-50.5F, -1F, -8.5F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 120
		bodyModel[70].setRotationPoint(55F, -18F, 5.5F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 121
		bodyModel[71].setRotationPoint(55F, -15F, 6F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 122
		bodyModel[72].setRotationPoint(55F, -18F, 8.5F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 123
		bodyModel[73].setRotationPoint(55F, -17F, 6F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 124
		bodyModel[74].setRotationPoint(55F, -13F, 6F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 125
		bodyModel[75].setRotationPoint(55F, -11F, 6F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 126
		bodyModel[76].setRotationPoint(55F, -9F, 6F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 127
		bodyModel[77].setRotationPoint(55F, -1F, 6F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 128
		bodyModel[78].setRotationPoint(55F, -3F, 6F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 129
		bodyModel[79].setRotationPoint(55F, -5F, 6F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 130
		bodyModel[80].setRotationPoint(55F, -7F, 6F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F); // Box 131
		bodyModel[81].setRotationPoint(55F, -14F, 1F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 132
		bodyModel[82].setRotationPoint(55F, -14F, 3F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 133
		bodyModel[83].setRotationPoint(55F, -13F, 1F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 134
		bodyModel[84].setRotationPoint(55F, -13F, 3F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 135
		bodyModel[85].setRotationPoint(55F, -14.5F, 2F);

		bodyModel[86].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 136
		bodyModel[86].setRotationPoint(55F, -12F, 2F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 137
		bodyModel[87].setRotationPoint(55F, -13.5F, 1F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 138
		bodyModel[88].setRotationPoint(55F, -13.5F, 3F);

		bodyModel[89].addShapeBox(0F, -1F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 139
		bodyModel[89].setRotationPoint(55F, -12.25F, 1.5F);

		bodyModel[90].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F); // Box 140
		bodyModel[90].setRotationPoint(55F, -13F, 2.25F);

		bodyModel[91].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F); // Box 141
		bodyModel[91].setRotationPoint(55F, -11.75F, 2.25F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 142
		bodyModel[92].setRotationPoint(54F, -18F, 10F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 143
		bodyModel[93].setRotationPoint(-50F, -18F, 10F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 144
		bodyModel[94].setRotationPoint(-50F, -18F, -11F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 145
		bodyModel[95].setRotationPoint(-4.5F, -18F, 10F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 146
		bodyModel[96].setRotationPoint(18F, -18F, 10F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 147
		bodyModel[97].setRotationPoint(18F, -18F, -10.5F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 148
		bodyModel[98].setRotationPoint(-4.5F, -18F, -10.5F);

		bodyModel[99].addShapeBox(0F, -1F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 149
		bodyModel[99].setRotationPoint(-50.5F, -12.25F, -4F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 150
		bodyModel[100].setRotationPoint(-50.5F, -13F, -4.5F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 151
		bodyModel[101].setRotationPoint(-50.5F, -12F, -3.5F);

		bodyModel[102].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F); // Box 152
		bodyModel[102].setRotationPoint(-50.5F, -11.75F, -3.25F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 153
		bodyModel[103].setRotationPoint(-50.5F, -13F, -2.5F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 154
		bodyModel[104].setRotationPoint(-50.5F, -13.5F, -2.5F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 155
		bodyModel[105].setRotationPoint(-50.5F, -14F, -2.5F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 156
		bodyModel[106].setRotationPoint(-50.5F, -14.5F, -3.5F);

		bodyModel[107].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, -0.5F, 0F, -0.25F, -0.5F); // Box 157
		bodyModel[107].setRotationPoint(-50.5F, -13F, -3.25F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F); // Box 158
		bodyModel[108].setRotationPoint(-50.5F, -14F, -4.5F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 159
		bodyModel[109].setRotationPoint(-50.5F, -13.5F, -4.5F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 8F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -0.5F, 8F, 0F, -0.5F); // Box 160
		bodyModel[110].setRotationPoint(-24.25F, -18F, -11F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -8F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, -0.5F, -8F, 0F, -0.5F); // Box 161
		bodyModel[111].setRotationPoint(-49F, -18F, 10.5F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -8F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, -0.5F, -8F, 0F, -0.5F); // Box 162
		bodyModel[112].setRotationPoint(-49F, -18F, -11F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 163
		bodyModel[113].setRotationPoint(-23.25F, -18F, -11F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 164
		bodyModel[114].setRotationPoint(-23.25F, -18F, 10.5F);

		bodyModel[115].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 0
		bodyModel[115].setRotationPoint(-54F, -1F, -1F);

		bodyModel[116].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 0
		bodyModel[116].setRotationPoint(55F, -1F, -1F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 11
		bodyModel[117].setRotationPoint(-12.5F, 2.5F, -9F);

		bodyModel[118].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[118].setRotationPoint(-12.5F, 2.5F, -7F);

		bodyModel[119].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 11
		bodyModel[119].setRotationPoint(-12.5F, 1.5F, -9F);

		bodyModel[120].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 14
		bodyModel[120].setRotationPoint(-12.5F, 1.5F, -7F);

		bodyModel[121].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F); // Box 11
		bodyModel[121].setRotationPoint(-12.5F, 2.5F, -9.5F);

		bodyModel[122].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 11
		bodyModel[122].setRotationPoint(-12.5F, 2.5F, -9.5F);

		bodyModel[123].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[123].setRotationPoint(-12.5F, 2.5F, -5.5F);

		bodyModel[124].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 11
		bodyModel[124].setRotationPoint(-12.5F, 2.5F, -5.5F);

		bodyModel[125].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 11
		bodyModel[125].setRotationPoint(-12.5F, 2.5F, 5F);

		bodyModel[126].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[126].setRotationPoint(-12.5F, 2.5F, 7F);

		bodyModel[127].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 11
		bodyModel[127].setRotationPoint(-12.5F, 1.5F, 5F);

		bodyModel[128].addShapeBox(0F, 0F, 0F, 32, 2, 2, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 14
		bodyModel[128].setRotationPoint(-12.5F, 1.5F, 7F);

		bodyModel[129].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F); // Box 11
		bodyModel[129].setRotationPoint(-12.5F, 2.5F, 4.5F);

		bodyModel[130].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 11
		bodyModel[130].setRotationPoint(-12.5F, 2.5F, 4.5F);

		bodyModel[131].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[131].setRotationPoint(-12.5F, 2.5F, 8.5F);

		bodyModel[132].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 11
		bodyModel[132].setRotationPoint(-12.5F, 2.5F, 8.5F);

		bodyModel[133].addBox(0F, 0F, 0F, 2, 1, 2, 0F); // Box 0
		bodyModel[133].setRotationPoint(-34F, 1F, -1F);

		bodyModel[134].addBox(0F, 0F, 0F, 2, 1, 2, 0F); // Box 0
		bodyModel[134].setRotationPoint(38F, 1F, -1F);

		bodyModel[135].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F); // Box 11
		bodyModel[135].setRotationPoint(-12.5F, 0.5F, -5F);

		bodyModel[136].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[136].setRotationPoint(-12.5F, 0.5F, -10F);

		bodyModel[137].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F); // Box 11
		bodyModel[137].setRotationPoint(-12.5F, 0.5F, 9F);

		bodyModel[138].addShapeBox(0F, 0F, 0F, 32, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[138].setRotationPoint(-12.5F, 0.5F, 4F);

		bodyModel[139].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 122
		bodyModel[139].setRotationPoint(19.75F, -10F, 11.5F);

		bodyModel[140].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 122
		bodyModel[140].setRotationPoint(19.75F, -10F, -12F);

		bodyModel[141].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 11F, 0F, 0F, -11F, 0F, 0F, -11F, 0F, -0.5F, 11F, 0F, -0.5F); // Box 23
		bodyModel[141].setRotationPoint(18.5F, -18F, 11F);

		bodyModel[142].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 24
		bodyModel[142].setRotationPoint(6.5F, -18F, 11F);

		bodyModel[143].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -11F, 0F, 0F, 11F, 0F, 0F, 11F, 0F, -0.5F, -11F, 0F, -0.5F); // Box 25
		bodyModel[143].setRotationPoint(-5.5F, -18F, 11F);

		bodyModel[144].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 11F, 0F, 0F, -11F, 0F, 0F, -11F, 0F, -0.5F, 11F, 0F, -0.5F); // Box 23
		bodyModel[144].setRotationPoint(18.5F, -18F, -11.5F);

		bodyModel[145].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 24
		bodyModel[145].setRotationPoint(6.5F, -18F, -11.5F);

		bodyModel[146].addShapeBox(0F, 0F, 0F, 1, 18, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -11F, 0F, 0F, 11F, 0F, 0F, 11F, 0F, -0.5F, -11F, 0F, -0.5F); // Box 25
		bodyModel[146].setRotationPoint(-5.5F, -18F, -11.5F);
	}

	public float[] getTrans() {
		return new float[]{ -0.125f, 0.1f, 0.0f };
	}
}