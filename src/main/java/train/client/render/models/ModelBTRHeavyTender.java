//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2023 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: BTR Heavy Tender
// Model Creator: BlueTheWolf1204
// Created on: 31.12.2022 - 14:57:20
// Last changed on: 31.12.2022 - 14:57:20

package train.client.render.models; //Path where the model is located

import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.library.Info;

public class ModelBTRHeavyTender extends ModelConverter //Same as Filename
{
	int textureX = 1024;
	int textureY = 1024;
	public ModelFreightTruckM bogie = new ModelFreightTruckM();

	public ModelBTRHeavyTender() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[64];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}
	@Override
	public void render(Entity entity, float f0, float f1, float f2, float f3, float f4, float scale){
		super.render(entity, f0, f1, f2, f3, f4, scale);
		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, Info.trainsPrefix + "freighttruckm.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(0.35f,-0.1f,-0.18f);
		bogie.render(entity, f0, f1, f2, f3, f4, scale);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glTranslatef(-2.85f,-0.1f,-0.18f);
		bogie.render(entity, f0, f1, f2, f3, f4, scale);
		GL11.glPopMatrix();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 193, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 3
		bodyModel[4] = new ModelRendererTurbo(this, 9, 1, textureX, textureY); // Box 5
		bodyModel[5] = new ModelRendererTurbo(this, 169, 1, textureX, textureY); // Box 6
		bodyModel[6] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Box 8
		bodyModel[7] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 10
		bodyModel[8] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 12
		bodyModel[9] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 13
		bodyModel[10] = new ModelRendererTurbo(this, 561, 1, textureX, textureY); // Box 14
		bodyModel[11] = new ModelRendererTurbo(this, 609, 1, textureX, textureY); // Box 15
		bodyModel[12] = new ModelRendererTurbo(this, 657, 1, textureX, textureY); // Box 16
		bodyModel[13] = new ModelRendererTurbo(this, 681, 1, textureX, textureY); // Box 17
		bodyModel[14] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Box 18
		bodyModel[15] = new ModelRendererTurbo(this, 193, 1, textureX, textureY); // Box 19
		bodyModel[16] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 20
		bodyModel[17] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Box 21
		bodyModel[18] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 22
		bodyModel[19] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Box 23
		bodyModel[20] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 24
		bodyModel[21] = new ModelRendererTurbo(this, 313, 1, textureX, textureY); // Box 25
		bodyModel[22] = new ModelRendererTurbo(this, 761, 1, textureX, textureY); // Box 11
		bodyModel[23] = new ModelRendererTurbo(this, 825, 1, textureX, textureY); // Box 14
		bodyModel[24] = new ModelRendererTurbo(this, 889, 1, textureX, textureY); // Box 11
		bodyModel[25] = new ModelRendererTurbo(this, 953, 1, textureX, textureY); // Box 14
		bodyModel[26] = new ModelRendererTurbo(this, 497, 9, textureX, textureY); // Box 11
		bodyModel[27] = new ModelRendererTurbo(this, 561, 9, textureX, textureY); // Box 11
		bodyModel[28] = new ModelRendererTurbo(this, 761, 9, textureX, textureY); // Box 11
		bodyModel[29] = new ModelRendererTurbo(this, 825, 9, textureX, textureY); // Box 11
		bodyModel[30] = new ModelRendererTurbo(this, 889, 9, textureX, textureY); // Box 11
		bodyModel[31] = new ModelRendererTurbo(this, 953, 9, textureX, textureY); // Box 11
		bodyModel[32] = new ModelRendererTurbo(this, 505, 17, textureX, textureY); // Box 36
		bodyModel[33] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 37
		bodyModel[34] = new ModelRendererTurbo(this, 569, 17, textureX, textureY); // Box 38
		bodyModel[35] = new ModelRendererTurbo(this, 801, 1, textureX, textureY); // Box 39
		bodyModel[36] = new ModelRendererTurbo(this, 633, 1, textureX, textureY); // Box 40
		bodyModel[37] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 41
		bodyModel[38] = new ModelRendererTurbo(this, 649, 1, textureX, textureY); // Box 42
		bodyModel[39] = new ModelRendererTurbo(this, 657, 1, textureX, textureY); // Box 43
		bodyModel[40] = new ModelRendererTurbo(this, 665, 1, textureX, textureY); // Box 44
		bodyModel[41] = new ModelRendererTurbo(this, 681, 1, textureX, textureY); // Box 45
		bodyModel[42] = new ModelRendererTurbo(this, 689, 1, textureX, textureY); // Box 46
		bodyModel[43] = new ModelRendererTurbo(this, 1009, 1, textureX, textureY); // Box 47
		bodyModel[44] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 396
		bodyModel[45] = new ModelRendererTurbo(this, 169, 9, textureX, textureY); // Box 595
		bodyModel[46] = new ModelRendererTurbo(this, 185, 9, textureX, textureY); // Box 596
		bodyModel[47] = new ModelRendererTurbo(this, 193, 9, textureX, textureY); // Box 597
		bodyModel[48] = new ModelRendererTurbo(this, 193, 1, textureX, textureY); // Box 404
		bodyModel[49] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 404
		bodyModel[50] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Box 534
		bodyModel[51] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 534
		bodyModel[52] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Box 534
		bodyModel[53] = new ModelRendererTurbo(this, 201, 9, textureX, textureY); // Box 534
		bodyModel[54] = new ModelRendererTurbo(this, 289, 9, textureX, textureY); // Box 396
		bodyModel[55] = new ModelRendererTurbo(this, 305, 9, textureX, textureY); // Box 396
		bodyModel[56] = new ModelRendererTurbo(this, 409, 9, textureX, textureY); // Box 70
		bodyModel[57] = new ModelRendererTurbo(this, 417, 9, textureX, textureY); // Box 71
		bodyModel[58] = new ModelRendererTurbo(this, 425, 9, textureX, textureY); // Box 72
		bodyModel[59] = new ModelRendererTurbo(this, 177, 9, textureX, textureY); // Box 73
		bodyModel[60] = new ModelRendererTurbo(this, 433, 9, textureX, textureY); // Box 74
		bodyModel[61] = new ModelRendererTurbo(this, 449, 9, textureX, textureY); // Box 75
		bodyModel[62] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 76
		bodyModel[63] = new ModelRendererTurbo(this, 473, 9, textureX, textureY); // Box 77

		bodyModel[0].addBox(0F, 0F, 0F, 72, 1, 20, 0F); // Box 0
		bodyModel[0].setRotationPoint(-45F, 0F, -10F);

		bodyModel[1].addBox(0F, 0F, 0F, 34, 15, 20, 0F); // Box 1
		bodyModel[1].setRotationPoint(-46F, -15F, -10F);

		bodyModel[2].addBox(0F, 0F, 0F, 39, 12, 20, 0F); // Box 2
		bodyModel[2].setRotationPoint(-12F, -12F, -10F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		bodyModel[3].setRotationPoint(-12F, -15F, -10F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		bodyModel[4].setRotationPoint(-10F, -13F, -10F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[5].setRotationPoint(-12F, -15F, 9F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[6].setRotationPoint(-10F, -13F, 9F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		bodyModel[7].setRotationPoint(-14F, -17F, -10F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 20, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		bodyModel[8].setRotationPoint(-45F, -17F, -10F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 30, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		bodyModel[9].setRotationPoint(-44F, -17F, 9F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 30, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[10].setRotationPoint(-44F, -17F, -10F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 1, 1, 20, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		bodyModel[11].setRotationPoint(-13F, -16F, -10F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 1, 1, 20, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		bodyModel[12].setRotationPoint(-46F, -16F, -10F);

		bodyModel[13].addBox(0F, 0F, 0F, 30, 1, 18, 0F); // Box 17
		bodyModel[13].setRotationPoint(-44F, -16.5F, -9F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F); // Box 18
		bodyModel[14].setRotationPoint(-7F, -12.5F, -3F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F); // Box 19
		bodyModel[15].setRotationPoint(-4F, -12.5F, -3F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 20
		bodyModel[16].setRotationPoint(4F, -12.5F, -3F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F); // Box 21
		bodyModel[17].setRotationPoint(6F, -12.5F, -3F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F); // Box 22
		bodyModel[18].setRotationPoint(3F, -12.5F, -3F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 23
		bodyModel[19].setRotationPoint(-6F, -12.5F, -3F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 24
		bodyModel[20].setRotationPoint(-6.75F, -13F, -1F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 25
		bodyModel[21].setRotationPoint(3.25F, -13F, -1F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 27, 2, 2, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 11
		bodyModel[22].setRotationPoint(-21.5F, 2.5F, -9F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 27, 2, 2, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[23].setRotationPoint(-21.5F, 2.5F, 7F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 27, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 11
		bodyModel[24].setRotationPoint(-21.5F, 1.5F, -9F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 27, 2, 2, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 14
		bodyModel[25].setRotationPoint(-21.5F, 1.5F, 7F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 27, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F); // Box 11
		bodyModel[26].setRotationPoint(-21.5F, 2.5F, -9.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 27, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 11
		bodyModel[27].setRotationPoint(-21.5F, 2.5F, -9.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 27, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[28].setRotationPoint(-21.5F, 2.5F, 8.5F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 27, 1, 1, 0F,0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 11
		bodyModel[29].setRotationPoint(-21.5F, 2.5F, 8.5F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 27, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F); // Box 11
		bodyModel[30].setRotationPoint(-21.5F, 0.5F, 9F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 27, 1, 1, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 1F, -0.5F, 0F, 1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[31].setRotationPoint(-21.5F, 0.5F, -10F);

		bodyModel[32].addBox(0F, 0F, 0F, 27, 4, 14, 0F); // Box 36
		bodyModel[32].setRotationPoint(-21.5F, 1F, -7F);

		bodyModel[33].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 37
		bodyModel[33].setRotationPoint(28F, 0F, -1F);

		bodyModel[34].addBox(0F, 0F, 0F, 3, 2, 20, 0F); // Box 38
		bodyModel[34].setRotationPoint(-48F, 0F, -10F);

		bodyModel[35].addBox(0F, 0F, 0F, 1, 2, 20, 0F); // Box 39
		bodyModel[35].setRotationPoint(27F, 0F, -10F);

		bodyModel[36].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 40
		bodyModel[36].setRotationPoint(-52F, 0F, -1F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 1, 15, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		bodyModel[37].setRotationPoint(-47F, -15F, 9F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		bodyModel[38].setRotationPoint(-48F, -14F, 9F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 1, 15, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		bodyModel[39].setRotationPoint(-47F, -15F, -10F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 1, 14, 1, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		bodyModel[40].setRotationPoint(-48F, -14F, -10F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		bodyModel[41].setRotationPoint(-47F, -7F, -4F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		bodyModel[42].setRotationPoint(-47F, -7F, 3F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		bodyModel[43].setRotationPoint(-47F, -7F, -3F);

		bodyModel[44].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 396
		bodyModel[44].setRotationPoint(24.99F, -13F, -1.85F);

		bodyModel[45].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 595
		bodyModel[45].setRotationPoint(26F, -14.3F, -1.6F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 1F, 0F, -4F, 1F, 0F, -4F, 1F, 0F, -4F, 1F); // Box 596
		bodyModel[46].setRotationPoint(26F, -15.3F, -0.6F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F,0F, -4F, 1F, 0F, -4F, 1F, 0F, -4F, 1F, 0F, -4F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 597
		bodyModel[47].setRotationPoint(26F, -17.3F, -0.6F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 404
		bodyModel[48].setRotationPoint(27F, -12.5F, -9.15F);

		bodyModel[49].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 404
		bodyModel[49].setRotationPoint(28F, -12.25F, -9.4F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 534
		bodyModel[50].setRotationPoint(27.75F, -13F, -10F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 534
		bodyModel[51].setRotationPoint(28.75F, -13F, -10F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 534
		bodyModel[52].setRotationPoint(27.75F, -13F, -9F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 534
		bodyModel[53].setRotationPoint(28.75F, -13F, -9F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 396
		bodyModel[54].setRotationPoint(27.5F, -13.5F, -10F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 396
		bodyModel[55].setRotationPoint(27.5F, -11.5F, -10F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 70
		bodyModel[56].setRotationPoint(27.75F, -13F, 8F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 71
		bodyModel[57].setRotationPoint(28.75F, -13F, 8F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 72
		bodyModel[58].setRotationPoint(28.75F, -13F, 7F);

		bodyModel[59].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 73
		bodyModel[59].setRotationPoint(28F, -12.25F, 7.6F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74
		bodyModel[60].setRotationPoint(27.5F, -11.5F, 7F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 75
		bodyModel[61].setRotationPoint(27.75F, -13F, 7F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 76
		bodyModel[62].setRotationPoint(27.5F, -13.5F, 7F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 77
		bodyModel[63].setRotationPoint(27F, -12.5F, 7.85F);
	}

	public float[] getTrans() {
		return new float[]{ 0.6f, 0.08f, 0.0f };
	}
}