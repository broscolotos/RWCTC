//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 13.11.2021 - 14:46:13
// Last changed on: 13.11.2021 - 14:46:13

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.render.RenderRollingStock;
import train.common.api.AbstractTrains;
import train.common.library.Info;

public class ModelAntiAircraftPlatform extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 128;

	public ModelAntiAircraftPlatform() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[190];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private ModelPL42Bogie trucks = new ModelPL42Bogie();

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		Tessellator.bindTexture(RenderRollingStock.getTexture(entity));
		super.render(entity, f, f1, f2, f3, f4, f5);

		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/PL-42 Bogie Texture.png"));
		GL11.glPushMatrix();
		GL11.glTranslated(-2,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glTranslated(4,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glPopMatrix();
	}
	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 3
		bodyModel[4] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Box 4
		bodyModel[5] = new ModelRendererTurbo(this, 369, 1, textureX, textureY); // Box 5
		bodyModel[6] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 6
		bodyModel[7] = new ModelRendererTurbo(this, 457, 17, textureX, textureY); // Box 8
		bodyModel[8] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Box 10
		bodyModel[9] = new ModelRendererTurbo(this, 177, 25, textureX, textureY); // Box 11
		bodyModel[10] = new ModelRendererTurbo(this, 49, 25, textureX, textureY); // Box 12
		bodyModel[11] = new ModelRendererTurbo(this, 153, 25, textureX, textureY); // Box 13
		bodyModel[12] = new ModelRendererTurbo(this, 225, 25, textureX, textureY); // Box 40
		bodyModel[13] = new ModelRendererTurbo(this, 321, 25, textureX, textureY); // Box 40
		bodyModel[14] = new ModelRendererTurbo(this, 457, 1, textureX, textureY); // Box 41
		bodyModel[15] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 42
		bodyModel[16] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 44
		bodyModel[17] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 45
		bodyModel[18] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 48
		bodyModel[19] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 49
		bodyModel[20] = new ModelRendererTurbo(this, 481, 9, textureX, textureY); // Box 50
		bodyModel[21] = new ModelRendererTurbo(this, 489, 17, textureX, textureY); // Box 51
		bodyModel[22] = new ModelRendererTurbo(this, 305, 25, textureX, textureY); // Box 52
		bodyModel[23] = new ModelRendererTurbo(this, 321, 25, textureX, textureY); // Box 53
		bodyModel[24] = new ModelRendererTurbo(this, 361, 25, textureX, textureY); // Box 54
		bodyModel[25] = new ModelRendererTurbo(this, 385, 25, textureX, textureY); // Box 55
		bodyModel[26] = new ModelRendererTurbo(this, 401, 25, textureX, textureY); // Box 56
		bodyModel[27] = new ModelRendererTurbo(this, 489, 25, textureX, textureY); // Box 57
		bodyModel[28] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 58
		bodyModel[29] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 59
		bodyModel[30] = new ModelRendererTurbo(this, 41, 33, textureX, textureY); // Box 60
		bodyModel[31] = new ModelRendererTurbo(this, 177, 33, textureX, textureY); // Box 61
		bodyModel[32] = new ModelRendererTurbo(this, 193, 33, textureX, textureY); // Box 62
		bodyModel[33] = new ModelRendererTurbo(this, 209, 33, textureX, textureY); // Box 63
		bodyModel[34] = new ModelRendererTurbo(this, 305, 33, textureX, textureY); // Box 64
		bodyModel[35] = new ModelRendererTurbo(this, 81, 1, textureX, textureY); // Box 65
		bodyModel[36] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 66
		bodyModel[37] = new ModelRendererTurbo(this, 81, 9, textureX, textureY); // Box 67
		bodyModel[38] = new ModelRendererTurbo(this, 57, 33, textureX, textureY); // Box 68
		bodyModel[39] = new ModelRendererTurbo(this, 361, 33, textureX, textureY); // Box 79
		bodyModel[40] = new ModelRendererTurbo(this, 505, 9, textureX, textureY); // Box 80
		bodyModel[41] = new ModelRendererTurbo(this, 505, 25, textureX, textureY); // Box 81
		bodyModel[42] = new ModelRendererTurbo(this, 233, 33, textureX, textureY); // Box 82
		bodyModel[43] = new ModelRendererTurbo(this, 457, 1, textureX, textureY); // Box 83
		bodyModel[44] = new ModelRendererTurbo(this, 489, 25, textureX, textureY); // Box 84
		bodyModel[45] = new ModelRendererTurbo(this, 473, 9, textureX, textureY); // Box 85
		bodyModel[46] = new ModelRendererTurbo(this, 497, 9, textureX, textureY); // Box 86
		bodyModel[47] = new ModelRendererTurbo(this, 505, 17, textureX, textureY); // Box 87
		bodyModel[48] = new ModelRendererTurbo(this, 321, 33, textureX, textureY); // Box 88
		bodyModel[49] = new ModelRendererTurbo(this, 385, 33, textureX, textureY); // Box 89
		bodyModel[50] = new ModelRendererTurbo(this, 401, 33, textureX, textureY); // Box 90
		bodyModel[51] = new ModelRendererTurbo(this, 377, 25, textureX, textureY); // Box 91
		bodyModel[52] = new ModelRendererTurbo(this, 417, 25, textureX, textureY); // Box 92
		bodyModel[53] = new ModelRendererTurbo(this, 225, 33, textureX, textureY); // Box 93
		bodyModel[54] = new ModelRendererTurbo(this, 433, 33, textureX, textureY); // Box 94
		bodyModel[55] = new ModelRendererTurbo(this, 449, 33, textureX, textureY); // Box 95
		bodyModel[56] = new ModelRendererTurbo(this, 505, 33, textureX, textureY); // Box 96
		bodyModel[57] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 97
		bodyModel[58] = new ModelRendererTurbo(this, 193, 33, textureX, textureY); // Box 98
		bodyModel[59] = new ModelRendererTurbo(this, 9, 41, textureX, textureY); // Box 99
		bodyModel[60] = new ModelRendererTurbo(this, 41, 41, textureX, textureY); // Box 100
		bodyModel[61] = new ModelRendererTurbo(this, 89, 41, textureX, textureY); // Box 101
		bodyModel[62] = new ModelRendererTurbo(this, 97, 41, textureX, textureY); // Box 102
		bodyModel[63] = new ModelRendererTurbo(this, 105, 41, textureX, textureY); // Box 103
		bodyModel[64] = new ModelRendererTurbo(this, 113, 41, textureX, textureY); // Box 104
		bodyModel[65] = new ModelRendererTurbo(this, 121, 41, textureX, textureY); // Box 105
		bodyModel[66] = new ModelRendererTurbo(this, 145, 41, textureX, textureY); // Box 106
		bodyModel[67] = new ModelRendererTurbo(this, 193, 41, textureX, textureY); // Box 107
		bodyModel[68] = new ModelRendererTurbo(this, 209, 41, textureX, textureY); // Box 108
		bodyModel[69] = new ModelRendererTurbo(this, 377, 41, textureX, textureY); // Box 109
		bodyModel[70] = new ModelRendererTurbo(this, 385, 41, textureX, textureY); // Box 110
		bodyModel[71] = new ModelRendererTurbo(this, 417, 41, textureX, textureY); // Box 111
		bodyModel[72] = new ModelRendererTurbo(this, 425, 41, textureX, textureY); // Box 112
		bodyModel[73] = new ModelRendererTurbo(this, 433, 41, textureX, textureY); // Box 113
		bodyModel[74] = new ModelRendererTurbo(this, 441, 41, textureX, textureY); // Box 114
		bodyModel[75] = new ModelRendererTurbo(this, 449, 41, textureX, textureY); // Box 115
		bodyModel[76] = new ModelRendererTurbo(this, 505, 41, textureX, textureY); // Box 116
		bodyModel[77] = new ModelRendererTurbo(this, 57, 49, textureX, textureY); // Box 117
		bodyModel[78] = new ModelRendererTurbo(this, 65, 49, textureX, textureY); // Box 118
		bodyModel[79] = new ModelRendererTurbo(this, 81, 49, textureX, textureY); // Box 119
		bodyModel[80] = new ModelRendererTurbo(this, 89, 49, textureX, textureY); // Box 120
		bodyModel[81] = new ModelRendererTurbo(this, 97, 49, textureX, textureY); // Box 121
		bodyModel[82] = new ModelRendererTurbo(this, 105, 49, textureX, textureY); // Box 122
		bodyModel[83] = new ModelRendererTurbo(this, 113, 49, textureX, textureY); // Box 123
		bodyModel[84] = new ModelRendererTurbo(this, 121, 49, textureX, textureY); // Box 124
		bodyModel[85] = new ModelRendererTurbo(this, 129, 49, textureX, textureY); // Box 125
		bodyModel[86] = new ModelRendererTurbo(this, 137, 49, textureX, textureY); // Box 126
		bodyModel[87] = new ModelRendererTurbo(this, 153, 49, textureX, textureY); // Box 127
		bodyModel[88] = new ModelRendererTurbo(this, 161, 49, textureX, textureY); // Box 128
		bodyModel[89] = new ModelRendererTurbo(this, 169, 49, textureX, textureY); // Box 129
		bodyModel[90] = new ModelRendererTurbo(this, 177, 49, textureX, textureY); // Box 130
		bodyModel[91] = new ModelRendererTurbo(this, 185, 49, textureX, textureY); // Box 131
		bodyModel[92] = new ModelRendererTurbo(this, 225, 49, textureX, textureY); // Box 132
		bodyModel[93] = new ModelRendererTurbo(this, 265, 49, textureX, textureY); // Box 133
		bodyModel[94] = new ModelRendererTurbo(this, 305, 49, textureX, textureY); // Box 134
		bodyModel[95] = new ModelRendererTurbo(this, 345, 49, textureX, textureY); // Box 135
		bodyModel[96] = new ModelRendererTurbo(this, 385, 49, textureX, textureY); // Box 136
		bodyModel[97] = new ModelRendererTurbo(this, 417, 49, textureX, textureY); // Box 137
		bodyModel[98] = new ModelRendererTurbo(this, 441, 49, textureX, textureY); // Box 138
		bodyModel[99] = new ModelRendererTurbo(this, 457, 49, textureX, textureY); // Box 139
		bodyModel[100] = new ModelRendererTurbo(this, 473, 49, textureX, textureY); // Box 140
		bodyModel[101] = new ModelRendererTurbo(this, 489, 49, textureX, textureY); // Box 142
		bodyModel[102] = new ModelRendererTurbo(this, 209, 9, textureX, textureY); // Box 143
		bodyModel[103] = new ModelRendererTurbo(this, 505, 49, textureX, textureY); // Box 144
		bodyModel[104] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 145
		bodyModel[105] = new ModelRendererTurbo(this, 9, 57, textureX, textureY); // Box 146
		bodyModel[106] = new ModelRendererTurbo(this, 433, 49, textureX, textureY); // Box 146
		bodyModel[107] = new ModelRendererTurbo(this, 17, 57, textureX, textureY); // Box 147
		bodyModel[108] = new ModelRendererTurbo(this, 25, 57, textureX, textureY); // Box 148
		bodyModel[109] = new ModelRendererTurbo(this, 33, 57, textureX, textureY); // Box 149
		bodyModel[110] = new ModelRendererTurbo(this, 49, 57, textureX, textureY); // Box 150
		bodyModel[111] = new ModelRendererTurbo(this, 57, 57, textureX, textureY); // Box 151
		bodyModel[112] = new ModelRendererTurbo(this, 73, 57, textureX, textureY); // Box 152
		bodyModel[113] = new ModelRendererTurbo(this, 89, 57, textureX, textureY); // Box 153
		bodyModel[114] = new ModelRendererTurbo(this, 97, 57, textureX, textureY); // Box 155
		bodyModel[115] = new ModelRendererTurbo(this, 113, 57, textureX, textureY); // Box 157
		bodyModel[116] = new ModelRendererTurbo(this, 129, 57, textureX, textureY); // Box 158
		bodyModel[117] = new ModelRendererTurbo(this, 145, 57, textureX, textureY); // Box 159
		bodyModel[118] = new ModelRendererTurbo(this, 161, 57, textureX, textureY); // Box 165
		bodyModel[119] = new ModelRendererTurbo(this, 177, 57, textureX, textureY); // Box 172
		bodyModel[120] = new ModelRendererTurbo(this, 193, 57, textureX, textureY); // Box 173
		bodyModel[121] = new ModelRendererTurbo(this, 209, 57, textureX, textureY); // Box 186
		bodyModel[122] = new ModelRendererTurbo(this, 225, 57, textureX, textureY); // Box 187
		bodyModel[123] = new ModelRendererTurbo(this, 241, 57, textureX, textureY); // Box 188
		bodyModel[124] = new ModelRendererTurbo(this, 257, 57, textureX, textureY); // Box 189
		bodyModel[125] = new ModelRendererTurbo(this, 273, 57, textureX, textureY); // Box 190
		bodyModel[126] = new ModelRendererTurbo(this, 289, 57, textureX, textureY); // Box 396
		bodyModel[127] = new ModelRendererTurbo(this, 297, 57, textureX, textureY); // Box 397
		bodyModel[128] = new ModelRendererTurbo(this, 305, 57, textureX, textureY); // Box 398
		bodyModel[129] = new ModelRendererTurbo(this, 313, 57, textureX, textureY); // Box 171
		bodyModel[130] = new ModelRendererTurbo(this, 321, 57, textureX, textureY); // Box 174
		bodyModel[131] = new ModelRendererTurbo(this, 337, 57, textureX, textureY); // Box 175
		bodyModel[132] = new ModelRendererTurbo(this, 313, 41, textureX, textureY); // Box 178
		bodyModel[133] = new ModelRendererTurbo(this, 369, 41, textureX, textureY); // Box 179
		bodyModel[134] = new ModelRendererTurbo(this, 353, 57, textureX, textureY); // Box 180
		bodyModel[135] = new ModelRendererTurbo(this, 369, 57, textureX, textureY); // Box 181
		bodyModel[136] = new ModelRendererTurbo(this, 385, 57, textureX, textureY); // Box 182
		bodyModel[137] = new ModelRendererTurbo(this, 393, 57, textureX, textureY); // Box 183
		bodyModel[138] = new ModelRendererTurbo(this, 217, 49, textureX, textureY); // Box 184
		bodyModel[139] = new ModelRendererTurbo(this, 257, 49, textureX, textureY); // Box 185
		bodyModel[140] = new ModelRendererTurbo(this, 401, 57, textureX, textureY); // Box 186
		bodyModel[141] = new ModelRendererTurbo(this, 417, 57, textureX, textureY); // Box 187
		bodyModel[142] = new ModelRendererTurbo(this, 433, 57, textureX, textureY); // Box 188
		bodyModel[143] = new ModelRendererTurbo(this, 449, 57, textureX, textureY); // Box 189
		bodyModel[144] = new ModelRendererTurbo(this, 465, 57, textureX, textureY); // Box 101
		bodyModel[145] = new ModelRendererTurbo(this, 481, 57, textureX, textureY); // Box 150
		bodyModel[146] = new ModelRendererTurbo(this, 9, 65, textureX, textureY); // Box 151
		bodyModel[147] = new ModelRendererTurbo(this, 25, 65, textureX, textureY); // Box 152
		bodyModel[148] = new ModelRendererTurbo(this, 41, 65, textureX, textureY); // Box 153
		bodyModel[149] = new ModelRendererTurbo(this, 57, 65, textureX, textureY); // Box 154
		bodyModel[150] = new ModelRendererTurbo(this, 73, 65, textureX, textureY); // Box 155
		bodyModel[151] = new ModelRendererTurbo(this, 89, 65, textureX, textureY); // Box 156
		bodyModel[152] = new ModelRendererTurbo(this, 105, 65, textureX, textureY); // Box 157
		bodyModel[153] = new ModelRendererTurbo(this, 121, 65, textureX, textureY); // Box 158
		bodyModel[154] = new ModelRendererTurbo(this, 137, 65, textureX, textureY); // Box 159
		bodyModel[155] = new ModelRendererTurbo(this, 153, 65, textureX, textureY); // Box 160
		bodyModel[156] = new ModelRendererTurbo(this, 169, 65, textureX, textureY); // Box 161
		bodyModel[157] = new ModelRendererTurbo(this, 185, 65, textureX, textureY); // Box 162
		bodyModel[158] = new ModelRendererTurbo(this, 201, 65, textureX, textureY); // Box 163
		bodyModel[159] = new ModelRendererTurbo(this, 217, 65, textureX, textureY); // Box 164
		bodyModel[160] = new ModelRendererTurbo(this, 233, 65, textureX, textureY); // Box 165
		bodyModel[161] = new ModelRendererTurbo(this, 249, 65, textureX, textureY); // Box 166
		bodyModel[162] = new ModelRendererTurbo(this, 265, 65, textureX, textureY); // Box 167
		bodyModel[163] = new ModelRendererTurbo(this, 281, 65, textureX, textureY); // Box 168
		bodyModel[164] = new ModelRendererTurbo(this, 297, 65, textureX, textureY); // Box 169
		bodyModel[165] = new ModelRendererTurbo(this, 313, 65, textureX, textureY); // Box 170
		bodyModel[166] = new ModelRendererTurbo(this, 329, 65, textureX, textureY); // Box 171
		bodyModel[167] = new ModelRendererTurbo(this, 345, 65, textureX, textureY); // Box 172
		bodyModel[168] = new ModelRendererTurbo(this, 361, 65, textureX, textureY); // Box 173
		bodyModel[169] = new ModelRendererTurbo(this, 377, 65, textureX, textureY); // Box 174
		bodyModel[170] = new ModelRendererTurbo(this, 393, 65, textureX, textureY); // Box 175
		bodyModel[171] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 176
		bodyModel[172] = new ModelRendererTurbo(this, 425, 65, textureX, textureY); // Box 177
		bodyModel[173] = new ModelRendererTurbo(this, 441, 65, textureX, textureY); // Box 178
		bodyModel[174] = new ModelRendererTurbo(this, 457, 65, textureX, textureY); // Box 179
		bodyModel[175] = new ModelRendererTurbo(this, 473, 65, textureX, textureY); // Box 180
		bodyModel[176] = new ModelRendererTurbo(this, 489, 65, textureX, textureY); // Box 181
		bodyModel[177] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Box 182
		bodyModel[178] = new ModelRendererTurbo(this, 17, 73, textureX, textureY); // Box 183
		bodyModel[179] = new ModelRendererTurbo(this, 33, 73, textureX, textureY); // Box 184
		bodyModel[180] = new ModelRendererTurbo(this, 49, 73, textureX, textureY); // Box 185
		bodyModel[181] = new ModelRendererTurbo(this, 65, 73, textureX, textureY); // Box 186
		bodyModel[182] = new ModelRendererTurbo(this, 81, 73, textureX, textureY); // Box 187
		bodyModel[183] = new ModelRendererTurbo(this, 97, 73, textureX, textureY); // Box 188
		bodyModel[184] = new ModelRendererTurbo(this, 473, 57, textureX, textureY); // Box 21
		bodyModel[185] = new ModelRendererTurbo(this, 489, 57, textureX, textureY); // Box 21
		bodyModel[186] = new ModelRendererTurbo(this, 17, 65, textureX, textureY); // Box 21
		bodyModel[187] = new ModelRendererTurbo(this, 33, 65, textureX, textureY); // Box 21
		bodyModel[188] = new ModelRendererTurbo(this, 97, 73, textureX, textureY); // Box 6
		bodyModel[189] = new ModelRendererTurbo(this, 137, 73, textureX, textureY); // Box 6

		bodyModel[0].addShapeBox(0F, 0F, 0F, 28, 1, 17, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 0
		bodyModel[0].setRotationPoint(-42.5F, 3F, -8.5F);

		bodyModel[1].addBox(0F, 0F, 0F, 29, 15, 19, 0F); // Box 1
		bodyModel[1].setRotationPoint(-14.5F, -8F, -9.5F);

		bodyModel[2].addBox(0F, 0F, 0F, 33, 15, 1, 0F); // Box 2
		bodyModel[2].setRotationPoint(-47.5F, -8F, -9.5F);

		bodyModel[3].addBox(0F, 0F, 0F, 33, 15, 1, 0F); // Box 3
		bodyModel[3].setRotationPoint(-47.5F, -8F, 8.5F);

		bodyModel[4].addBox(0F, 0F, 0F, 33, 15, 1, 0F); // Box 4
		bodyModel[4].setRotationPoint(14.5F, -8F, -9.5F);

		bodyModel[5].addBox(0F, 0F, 0F, 33, 15, 1, 0F); // Box 5
		bodyModel[5].setRotationPoint(14.5F, -8F, 8.5F);

		bodyModel[6].addBox(0F, 0F, 0F, 5, 12, 17, 0F); // Box 6
		bodyModel[6].setRotationPoint(-47.5F, -8F, -8.5F);

		bodyModel[7].addBox(0F, 0F, 0F, 5, 12, 17, 0F); // Box 8
		bodyModel[7].setRotationPoint(42.5F, -8F, -8.5F);

		bodyModel[8].addBox(0F, 0F, 0F, 31, 6, 1, 0F); // Box 10
		bodyModel[8].setRotationPoint(-15.5F, -14F, 8.5F);

		bodyModel[9].addBox(0F, 0F, 0F, 31, 6, 1, 0F); // Box 11
		bodyModel[9].setRotationPoint(-15.5F, -14F, -9.5F);

		bodyModel[10].addBox(0F, 0F, 0F, 1, 6, 17, 0F); // Box 12
		bodyModel[10].setRotationPoint(-15.5F, -14F, -8.5F);

		bodyModel[11].addBox(0F, 0F, 0F, 1, 6, 17, 0F); // Box 13
		bodyModel[11].setRotationPoint(14.5F, -14F, -8.5F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 28, 1, 17, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 40
		bodyModel[12].setRotationPoint(14.5F, 3F, -8.5F);

		bodyModel[13].addBox(0F, 0F, 0F, 8, 1, 17, 0F); // Box 40
		bodyModel[13].setRotationPoint(27.5F, 2F, -8.5F);

		bodyModel[14].addBox(0F, 0F, 0F, 5, 1, 5, 0F); // Box 41
		bodyModel[14].setRotationPoint(35.5F, 2F, -2.5F);

		bodyModel[15].addBox(0F, 0F, 0F, 5, 1, 5, 0F); // Box 42
		bodyModel[15].setRotationPoint(22.5F, 2F, -2.5F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,-4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		bodyModel[16].setRotationPoint(23.5F, 2F, -8.5F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		bodyModel[17].setRotationPoint(22.5F, 2F, -5.5F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 48
		bodyModel[18].setRotationPoint(22.5F, 2F, 2.5F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // Box 49
		bodyModel[19].setRotationPoint(23.5F, 2F, 5.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 50
		bodyModel[20].setRotationPoint(35.5F, 2F, 2.5F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		bodyModel[21].setRotationPoint(35.5F, 2F, -5.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		bodyModel[22].setRotationPoint(35.5F, 2F, -8.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 53
		bodyModel[23].setRotationPoint(35.5F, 2F, 5.5F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		bodyModel[24].setRotationPoint(-27.5F, 2F, -5.5F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 55
		bodyModel[25].setRotationPoint(-27.5F, 2F, 5.5F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 56
		bodyModel[26].setRotationPoint(-27.5F, 2F, 2.5F);

		bodyModel[27].addBox(0F, 0F, 0F, 5, 1, 5, 0F); // Box 57
		bodyModel[27].setRotationPoint(-27.5F, 2F, -2.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		bodyModel[28].setRotationPoint(-27.5F, 2F, -8.5F);

		bodyModel[29].addBox(0F, 0F, 0F, 8, 1, 17, 0F); // Box 59
		bodyModel[29].setRotationPoint(-35.5F, 2F, -8.5F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,-4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		bodyModel[30].setRotationPoint(-39.5F, 2F, -8.5F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 61
		bodyModel[31].setRotationPoint(-40.5F, 2F, -5.5F);

		bodyModel[32].addBox(0F, 0F, 0F, 5, 1, 5, 0F); // Box 62
		bodyModel[32].setRotationPoint(-40.5F, 2F, -2.5F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 63
		bodyModel[33].setRotationPoint(-40.5F, 2F, 2.5F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // Box 64
		bodyModel[34].setRotationPoint(-39.5F, 2F, 5.5F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		bodyModel[35].setRotationPoint(25.5F, 1F, -5.5F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		bodyModel[36].setRotationPoint(25.5F, 1F, 3.5F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		bodyModel[37].setRotationPoint(-37.5F, 1F, 3.5F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		bodyModel[38].setRotationPoint(-37.5F, 1F, -5.5F);

		bodyModel[39].addBox(0F, 0F, 0F, 10, 3, 3, 0F); // Box 79
		bodyModel[39].setRotationPoint(30.78F, -8.24F, -1.5F);
		bodyModel[39].rotateAngleZ = -0.83775804F;

		bodyModel[40].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, 0F, 0F, 0F, -2.75F, 0F, 0F, -2.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 80
		bodyModel[40].setRotationPoint(31.5F, -1F, 1.5F);

		bodyModel[41].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 81
		bodyModel[41].setRotationPoint(29.5F, 0F, 1.5F);

		bodyModel[42].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 82
		bodyModel[42].setRotationPoint(28.5F, -2F, 1.5F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 83
		bodyModel[43].setRotationPoint(28.5F, -3F, 3.5F);

		bodyModel[44].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 84
		bodyModel[44].setRotationPoint(30.5F, -5F, 1.5F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -0.25F, 0F, 0F, 0F); // Box 85
		bodyModel[45].setRotationPoint(31.5F, -5F, 1.5F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		bodyModel[46].setRotationPoint(29.5F, -3F, 1.5F);

		bodyModel[47].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 87
		bodyModel[47].setRotationPoint(28.71F, -9.06F, 0.5F);
		bodyModel[47].rotateAngleZ = -0.83775804F;

		bodyModel[48].addBox(0F, 0F, 0F, 4, 2, 1, 0F); // Box 88
		bodyModel[48].setRotationPoint(28.12F, -11.2F, -0.5F);
		bodyModel[48].rotateAngleZ = -0.83775804F;

		bodyModel[49].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 89
		bodyModel[49].setRotationPoint(25.95F, -10.6F, -0.5F);
		bodyModel[49].rotateAngleZ = -0.83775804F;

		bodyModel[50].addShapeBox(0F, 0F, 0F, 13, 1, 1, 0F,0F, -0.1F, -0.1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.1F, -0.1F, 0F, -0.1F, -0.1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.1F, -0.1F); // Box 90
		bodyModel[50].setRotationPoint(18.5F, -20F, -0.5F);
		bodyModel[50].rotateAngleZ = -0.83775804F;

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.25F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.25F); // Box 91
		bodyModel[51].setRotationPoint(25.27F, -11.33F, -0.5F);
		bodyModel[51].rotateAngleZ = -0.83775804F;

		bodyModel[52].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0.25F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0.25F); // Box 92
		bodyModel[52].setRotationPoint(17.15F, -21.5F, -0.5F);
		bodyModel[52].rotateAngleZ = -0.83775804F;

		bodyModel[53].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 93
		bodyModel[53].setRotationPoint(28.71F, -9.06F, -1.5F);
		bodyModel[53].rotateAngleZ = -0.83775804F;

		bodyModel[54].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 94
		bodyModel[54].setRotationPoint(28.5F, -2F, -2.5F);

		bodyModel[55].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 95
		bodyModel[55].setRotationPoint(29.5F, 0F, -2.5F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, 0F, 0F, 0F, -2.75F, 0F, 0F, -2.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		bodyModel[56].setRotationPoint(31.5F, -1F, -2.5F);

		bodyModel[57].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 97
		bodyModel[57].setRotationPoint(30.5F, -5F, -2.5F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		bodyModel[58].setRotationPoint(29.5F, -3F, -2.5F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, 0F, 0F, 0F, 0F); // Box 99
		bodyModel[59].setRotationPoint(31.5F, -5F, -2.5F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		bodyModel[60].setRotationPoint(35.61F, -7.34F, -0.5F);
		bodyModel[60].rotateAngleZ = -0.83775804F;

		bodyModel[61].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 101
		bodyModel[61].setRotationPoint(36.21F, -5.18F, -0.5F);
		bodyModel[61].rotateAngleZ = -0.83775804F;

		bodyModel[62].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		bodyModel[62].setRotationPoint(36.95F, -5.85F, -0.5F);
		bodyModel[62].rotateAngleZ = -0.83775804F;

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 103
		bodyModel[63].setRotationPoint(28.5F, -2F, 2.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 104
		bodyModel[64].setRotationPoint(28.5F, -2F, -3.5F);

		bodyModel[65].addBox(0F, 0F, 0F, 10, 3, 3, 0F); // Box 105
		bodyModel[65].setRotationPoint(-32.22F, -8.24F, -1.5F);
		bodyModel[65].rotateAngleZ = -0.83775804F;

		bodyModel[66].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 106
		bodyModel[66].setRotationPoint(-34.29F, -9.06F, 0.5F);
		bodyModel[66].rotateAngleZ = -0.83775804F;

		bodyModel[67].addBox(0F, 0F, 0F, 4, 2, 1, 0F); // Box 107
		bodyModel[67].setRotationPoint(-34.88F, -11.2F, -0.5F);
		bodyModel[67].rotateAngleZ = -0.83775804F;

		bodyModel[68].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 108
		bodyModel[68].setRotationPoint(-37.05F, -10.6F, -0.5F);
		bodyModel[68].rotateAngleZ = -0.83775804F;

		bodyModel[69].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.25F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.25F); // Box 109
		bodyModel[69].setRotationPoint(-37.73F, -11.33F, -0.5F);
		bodyModel[69].rotateAngleZ = -0.83775804F;

		bodyModel[70].addShapeBox(0F, 0F, 0F, 13, 1, 1, 0F,0F, -0.1F, -0.1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.1F, -0.1F, 0F, -0.1F, -0.1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.1F, -0.1F); // Box 110
		bodyModel[70].setRotationPoint(-44.5F, -20F, -0.5F);
		bodyModel[70].rotateAngleZ = -0.83775804F;

		bodyModel[71].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0.25F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0.25F); // Box 111
		bodyModel[71].setRotationPoint(-45.85F, -21.5F, -0.5F);
		bodyModel[71].rotateAngleZ = -0.83775804F;

		bodyModel[72].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 112
		bodyModel[72].setRotationPoint(-34.29F, -9.06F, -1.5F);
		bodyModel[72].rotateAngleZ = -0.83775804F;

		bodyModel[73].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		bodyModel[73].setRotationPoint(-26.05F, -5.85F, -0.5F);
		bodyModel[73].rotateAngleZ = -0.83775804F;

		bodyModel[74].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 114
		bodyModel[74].setRotationPoint(-27.39F, -7.34F, -0.5F);
		bodyModel[74].rotateAngleZ = -0.83775804F;

		bodyModel[75].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 115
		bodyModel[75].setRotationPoint(-26.79F, -5.18F, -0.5F);
		bodyModel[75].rotateAngleZ = -0.83775804F;

		bodyModel[76].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 116
		bodyModel[76].setRotationPoint(-33.5F, 0F, 1.5F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, 0F, 0F, 0F, -2.75F, 0F, 0F, -2.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 117
		bodyModel[77].setRotationPoint(-31.5F, -1F, 1.5F);

		bodyModel[78].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 118
		bodyModel[78].setRotationPoint(-34.5F, -2F, 1.5F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 119
		bodyModel[79].setRotationPoint(-33.5F, -3F, 1.5F);

		bodyModel[80].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 120
		bodyModel[80].setRotationPoint(-32.5F, -5F, 1.5F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -0.25F, 0F, 0F, 0F); // Box 121
		bodyModel[81].setRotationPoint(-31.5F, -5F, 1.5F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 122
		bodyModel[82].setRotationPoint(-34.5F, -2F, 2.5F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 123
		bodyModel[83].setRotationPoint(-34.5F, -3F, 3.5F);

		bodyModel[84].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 124
		bodyModel[84].setRotationPoint(-33.5F, 0F, -2.5F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,0F, 0F, 0F, 0F, -2.75F, 0F, 0F, -2.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 125
		bodyModel[85].setRotationPoint(-31.5F, -1F, -2.5F);

		bodyModel[86].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 126
		bodyModel[86].setRotationPoint(-34.5F, -2F, -2.5F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 127
		bodyModel[87].setRotationPoint(-34.5F, -2F, -3.5F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 128
		bodyModel[88].setRotationPoint(-33.5F, -3F, -2.5F);

		bodyModel[89].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 129
		bodyModel[89].setRotationPoint(-32.5F, -5F, -2.5F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, 0F, 0F, 0F, 0F); // Box 130
		bodyModel[90].setRotationPoint(-31.5F, -5F, -2.5F);

		bodyModel[91].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 131
		bodyModel[91].setRotationPoint(-4.5F, -11F, -5.5F);
		bodyModel[91].rotateAngleZ = 0.6981317F;

		bodyModel[92].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 132
		bodyModel[92].setRotationPoint(-4.5F, -11F, -3.5F);
		bodyModel[92].rotateAngleZ = 0.6981317F;

		bodyModel[93].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 133
		bodyModel[93].setRotationPoint(-4.5F, -11F, 4.5F);
		bodyModel[93].rotateAngleZ = 0.6981317F;

		bodyModel[94].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 134
		bodyModel[94].setRotationPoint(-4.5F, -11F, 2.5F);
		bodyModel[94].rotateAngleZ = 0.6981317F;

		bodyModel[95].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 135
		bodyModel[95].setRotationPoint(-4.5F, -11F, 0.5F);
		bodyModel[95].rotateAngleZ = 0.6981317F;

		bodyModel[96].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 136
		bodyModel[96].setRotationPoint(-4.5F, -11F, -1.5F);
		bodyModel[96].rotateAngleZ = 0.6981317F;

		bodyModel[97].addBox(0F, 0F, 0F, 4, 1, 5, 0F); // Box 137
		bodyModel[97].setRotationPoint(10.5F, -9F, -7.5F);

		bodyModel[98].addBox(0F, 0F, 0F, 4, 1, 5, 0F); // Box 138
		bodyModel[98].setRotationPoint(10.5F, -9F, 2.5F);

		bodyModel[99].addBox(0F, -3F, 0F, 6, 1, 1, 0F); // Box 139
		bodyModel[99].setRotationPoint(0.5F, -10F, -6.5F);
		bodyModel[99].rotateAngleZ = 0.6981317F;

		bodyModel[100].addBox(0F, -3F, 0F, 6, 1, 1, 0F); // Box 140
		bodyModel[100].setRotationPoint(0.5F, -10F, 5.5F);
		bodyModel[100].rotateAngleZ = 0.6981317F;

		bodyModel[101].addBox(0F, -3F, 0F, 6, 1, 1, 0F); // Box 142
		bodyModel[101].setRotationPoint(0.5F, -10F, -0.5F);
		bodyModel[101].rotateAngleZ = 0.6981317F;

		bodyModel[102].addBox(0F, 0F, 0F, 1, 1, 13, 0F); // Box 143
		bodyModel[102].setRotationPoint(3.2F, -16.15F, -6.5F);
		bodyModel[102].rotateAngleZ = 0.68067841F;

		bodyModel[103].addShapeBox(0F, 0F, 0F, 1, 8, 1, 0F,-0.375F, -0.625F, 0F, 0.125F, 0F, 0F, 0.125F, 0F, 0F, -0.375F, -0.625F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 144
		bodyModel[103].setRotationPoint(3.5F, -16F, -4.5F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 8, 1, 0F,-0.375F, -0.625F, 0F, 0.125F, 0F, 0F, 0.125F, 0F, 0F, -0.375F, -0.625F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 145
		bodyModel[104].setRotationPoint(3.5F, -16F, 3.5F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, -0.25F, 0F, 0F, 0.625F, 0F, 0F, 0.625F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[105].setRotationPoint(-0.5F, -12F, -0.5F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[106].setRotationPoint(30.5F, 1F, -5.5F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[107].setRotationPoint(32.5F, -3F, -5.5F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F); // Box 148
		bodyModel[108].setRotationPoint(30.5F, -3F, 3.5F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 149
		bodyModel[109].setRotationPoint(28.5F, 1F, 3.5F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F); // Box 150
		bodyModel[110].setRotationPoint(-31.5F, -3F, 3.5F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 151
		bodyModel[111].setRotationPoint(-33.5F, 1F, 3.5F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 152
		bodyModel[112].setRotationPoint(-31.5F, 1F, -5.5F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F); // Box 153
		bodyModel[113].setRotationPoint(-29.5F, -3F, -5.5F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 155
		bodyModel[114].setRotationPoint(47.5F, 1F, 7.5F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 157
		bodyModel[115].setRotationPoint(47.5F, 1F, -8.5F);

		bodyModel[116].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 158
		bodyModel[116].setRotationPoint(-50.5F, 1F, -8.5F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 159
		bodyModel[117].setRotationPoint(-50.5F, 1F, 7.5F);

		bodyModel[118].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 165
		bodyModel[118].setRotationPoint(-28.5F, 4F, -10.5F);

		bodyModel[119].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 172
		bodyModel[119].setRotationPoint(39.5F, 4F, -10.5F);

		bodyModel[120].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 173
		bodyModel[120].setRotationPoint(25.5F, 4F, -10.5F);

		bodyModel[121].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 186
		bodyModel[121].setRotationPoint(-42.5F, 4F, -10.5F);

		bodyModel[122].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 187
		bodyModel[122].setRotationPoint(-42.5F, 4F, 9.5F);

		bodyModel[123].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 188
		bodyModel[123].setRotationPoint(-28.5F, 4F, 9.5F);

		bodyModel[124].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 189
		bodyModel[124].setRotationPoint(25.5F, 4F, 9.5F);

		bodyModel[125].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 190
		bodyModel[125].setRotationPoint(39.5F, 4F, 9.5F);

		bodyModel[126].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F); // Box 396
		bodyModel[126].setRotationPoint(-48.53F, 2F, -0.5F);

		bodyModel[127].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 397
		bodyModel[127].setRotationPoint(-49.78F, 3.01F, -0.5F);

		bodyModel[128].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 398
		bodyModel[128].setRotationPoint(-50.78F, 2.01F, -0.5F);

		bodyModel[129].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[129].setRotationPoint(47.5F, 2F, -0.5F);

		bodyModel[130].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 174
		bodyModel[130].setRotationPoint(-50.5F, 3F, -4.5F);

		bodyModel[131].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 175
		bodyModel[131].setRotationPoint(-50.5F, 3F, 3.5F);

		bodyModel[132].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 178
		bodyModel[132].setRotationPoint(-51.5F, 2F, 2.5F);

		bodyModel[133].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 179
		bodyModel[133].setRotationPoint(-51.5F, 2F, -5.5F);

		bodyModel[134].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 180
		bodyModel[134].setRotationPoint(47.5F, 3F, 3.5F);

		bodyModel[135].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 181
		bodyModel[135].setRotationPoint(47.5F, 3F, -4.5F);

		bodyModel[136].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 1F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 182
		bodyModel[136].setRotationPoint(48.78F, 3.01F, -0.5F);

		bodyModel[137].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 183
		bodyModel[137].setRotationPoint(49.78F, 2.01F, -0.5F);

		bodyModel[138].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 184
		bodyModel[138].setRotationPoint(50.5F, 2F, 2.5F);

		bodyModel[139].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 185
		bodyModel[139].setRotationPoint(50.5F, 2F, -5.5F);

		bodyModel[140].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 186
		bodyModel[140].setRotationPoint(6.5F, -9F, 9.5F);

		bodyModel[141].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 187
		bodyModel[141].setRotationPoint(-9.5F, -9F, 9.5F);

		bodyModel[142].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 188
		bodyModel[142].setRotationPoint(6.5F, -9F, -10.5F);

		bodyModel[143].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 189
		bodyModel[143].setRotationPoint(-9.5F, -9F, -10.5F);

		bodyModel[144].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 101
		bodyModel[144].setRotationPoint(15.5F, -7F, -11.2F);

		bodyModel[145].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 150
		bodyModel[145].setRotationPoint(15.5F, -1F, -11.2F);

		bodyModel[146].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 151
		bodyModel[146].setRotationPoint(15.5F, -4F, -11.2F);

		bodyModel[147].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 152
		bodyModel[147].setRotationPoint(15.5F, 2F, -11.2F);

		bodyModel[148].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 153
		bodyModel[148].setRotationPoint(15.5F, 5F, -11.2F);

		bodyModel[149].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 154
		bodyModel[149].setRotationPoint(-19.5F, -7F, -11.2F);

		bodyModel[150].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 155
		bodyModel[150].setRotationPoint(-19.5F, -4F, -11.2F);

		bodyModel[151].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 156
		bodyModel[151].setRotationPoint(-19.5F, -1F, -11.2F);

		bodyModel[152].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 157
		bodyModel[152].setRotationPoint(-19.5F, 2F, -11.2F);

		bodyModel[153].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 158
		bodyModel[153].setRotationPoint(-19.5F, 5F, -11.2F);

		bodyModel[154].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 159
		bodyModel[154].setRotationPoint(-46.5F, -7F, -11.2F);

		bodyModel[155].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 160
		bodyModel[155].setRotationPoint(-46.5F, -4F, -11.2F);

		bodyModel[156].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 161
		bodyModel[156].setRotationPoint(-46.5F, -1F, -11.2F);

		bodyModel[157].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 162
		bodyModel[157].setRotationPoint(-46.5F, 2F, -11.2F);

		bodyModel[158].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 163
		bodyModel[158].setRotationPoint(-46.5F, 5F, -11.2F);

		bodyModel[159].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 164
		bodyModel[159].setRotationPoint(42.5F, -7F, -11.2F);

		bodyModel[160].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 165
		bodyModel[160].setRotationPoint(42.5F, -4F, -11.2F);

		bodyModel[161].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 166
		bodyModel[161].setRotationPoint(42.5F, -1F, -11.2F);

		bodyModel[162].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 167
		bodyModel[162].setRotationPoint(42.5F, 2F, -11.2F);

		bodyModel[163].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 168
		bodyModel[163].setRotationPoint(42.5F, 5F, -11.2F);

		bodyModel[164].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 169
		bodyModel[164].setRotationPoint(42.5F, -7F, 8.8F);

		bodyModel[165].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 170
		bodyModel[165].setRotationPoint(42.5F, -4F, 8.8F);

		bodyModel[166].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 171
		bodyModel[166].setRotationPoint(42.5F, -1F, 8.8F);

		bodyModel[167].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 172
		bodyModel[167].setRotationPoint(42.5F, 2F, 8.8F);

		bodyModel[168].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 173
		bodyModel[168].setRotationPoint(42.5F, 5F, 8.8F);

		bodyModel[169].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 174
		bodyModel[169].setRotationPoint(15.5F, -7F, 8.8F);

		bodyModel[170].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 175
		bodyModel[170].setRotationPoint(15.5F, -4F, 8.8F);

		bodyModel[171].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 176
		bodyModel[171].setRotationPoint(15.5F, -1F, 8.8F);

		bodyModel[172].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 177
		bodyModel[172].setRotationPoint(15.5F, 2F, 8.8F);

		bodyModel[173].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 178
		bodyModel[173].setRotationPoint(15.5F, 5F, 8.8F);

		bodyModel[174].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 179
		bodyModel[174].setRotationPoint(-19.5F, -7F, 8.8F);

		bodyModel[175].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 180
		bodyModel[175].setRotationPoint(-19.5F, -4F, 8.8F);

		bodyModel[176].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 181
		bodyModel[176].setRotationPoint(-19.5F, -1F, 8.8F);

		bodyModel[177].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 182
		bodyModel[177].setRotationPoint(-19.5F, 2F, 8.8F);

		bodyModel[178].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 183
		bodyModel[178].setRotationPoint(-19.5F, 5F, 8.8F);

		bodyModel[179].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 184
		bodyModel[179].setRotationPoint(-46.5F, -7F, 8.8F);

		bodyModel[180].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 185
		bodyModel[180].setRotationPoint(-46.5F, -4F, 8.8F);

		bodyModel[181].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 186
		bodyModel[181].setRotationPoint(-46.5F, -1F, 8.8F);

		bodyModel[182].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 187
		bodyModel[182].setRotationPoint(-46.5F, 2F, 8.8F);

		bodyModel[183].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 188
		bodyModel[183].setRotationPoint(-46.5F, 5F, 8.8F);

		bodyModel[184].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 21
		bodyModel[184].setRotationPoint(33.5F, 3F, -1F);

		bodyModel[185].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 21
		bodyModel[185].setRotationPoint(31.5F, 3F, -1F);

		bodyModel[186].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 21
		bodyModel[186].setRotationPoint(-33.5F, 3F, -1F);

		bodyModel[187].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 21
		bodyModel[187].setRotationPoint(-35.5F, 3F, -1F);

		bodyModel[188].addBox(0F, 0F, 0F, 1, 3, 17, 0F); // Box 6
		bodyModel[188].setRotationPoint(-47.5F, 4F, -8.5F);

		bodyModel[189].addBox(0F, 0F, 0F, 1, 3, 17, 0F); // Box 6
		bodyModel[189].setRotationPoint(46.5F, 4F, -8.5F);
	}
}