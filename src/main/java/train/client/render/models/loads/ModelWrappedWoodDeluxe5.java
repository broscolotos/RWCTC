//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: WrappedWood3
// Model Creator: bida
// Created on: 01.11.2020 - 16:21:37
// Last changed on: 01.11.2020 - 16:21:37

package train.client.render.models.loads; //Path where the model is located

import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

public class ModelWrappedWoodDeluxe5 extends ModelConverter //Same as Filename
{
	int textureX = 128;
	int textureY = 64;

	public ModelWrappedWoodDeluxe5() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[75];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 98, 47, textureX, textureY); // Box 4 support
		bodyModel[1] = new ModelRendererTurbo(this, 63, 47, textureX, textureY); // Box 3 support
		bodyModel[2] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[3] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[4] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 14 strap
		bodyModel[5] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[6] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[7] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 14 strap
		bodyModel[8] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[9] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 14 strap
		bodyModel[10] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[11] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 14 strap
		bodyModel[12] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[13] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 7 strap
		bodyModel[14] = new ModelRendererTurbo(this, 2, 5, textureX, textureY); // Box 1 wood hyper
		bodyModel[15] = new ModelRendererTurbo(this, 98, 47, textureX, textureY); // Box 18
		bodyModel[16] = new ModelRendererTurbo(this, 63, 47, textureX, textureY); // Box 19
		bodyModel[17] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 20
		bodyModel[18] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 21
		bodyModel[19] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 22
		bodyModel[20] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 23
		bodyModel[21] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 24
		bodyModel[22] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 25
		bodyModel[23] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 26
		bodyModel[24] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 27
		bodyModel[25] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 28
		bodyModel[26] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 29
		bodyModel[27] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 30
		bodyModel[28] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 31
		bodyModel[29] = new ModelRendererTurbo(this, 2, 5, textureX, textureY); // Box 32
		bodyModel[30] = new ModelRendererTurbo(this, 2, 5, textureX, textureY); // Box 33
		bodyModel[31] = new ModelRendererTurbo(this, 63, 47, textureX, textureY); // Box 34
		bodyModel[32] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 35
		bodyModel[33] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 36
		bodyModel[34] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 37
		bodyModel[35] = new ModelRendererTurbo(this, 98, 47, textureX, textureY); // Box 38
		bodyModel[36] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 39
		bodyModel[37] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 40
		bodyModel[38] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 41
		bodyModel[39] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 42
		bodyModel[40] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 43
		bodyModel[41] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 44
		bodyModel[42] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 45
		bodyModel[43] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 46
		bodyModel[44] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 47
		bodyModel[45] = new ModelRendererTurbo(this, 2, 5, textureX, textureY); // Box 45
		bodyModel[46] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 46
		bodyModel[47] = new ModelRendererTurbo(this, 63, 47, textureX, textureY); // Box 47
		bodyModel[48] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 48
		bodyModel[49] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 49
		bodyModel[50] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 50
		bodyModel[51] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 51
		bodyModel[52] = new ModelRendererTurbo(this, 98, 47, textureX, textureY); // Box 52
		bodyModel[53] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 53
		bodyModel[54] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 54
		bodyModel[55] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 55
		bodyModel[56] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 56
		bodyModel[57] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 57
		bodyModel[58] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 58
		bodyModel[59] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 59
		bodyModel[60] = new ModelRendererTurbo(this, 2, 5, textureX, textureY); // Box 60
		bodyModel[61] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 61
		bodyModel[62] = new ModelRendererTurbo(this, 63, 47, textureX, textureY); // Box 62
		bodyModel[63] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 63
		bodyModel[64] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 64
		bodyModel[65] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 65
		bodyModel[66] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 66
		bodyModel[67] = new ModelRendererTurbo(this, 98, 47, textureX, textureY); // Box 67
		bodyModel[68] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 68
		bodyModel[69] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 69
		bodyModel[70] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 70
		bodyModel[71] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 71
		bodyModel[72] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 72
		bodyModel[73] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 73
		bodyModel[74] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // Box 74

		bodyModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4 support
		bodyModel[0].setRotationPoint(3.5F, 9F, -4F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3 support
		bodyModel[1].setRotationPoint(-5.5F, 9F, -4F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 7 strap
		bodyModel[2].setRotationPoint(4F, 5F, 4.51F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 7 strap
		bodyModel[3].setRotationPoint(4F, 10.01F, -4F);
		bodyModel[3].rotateAngleX = 1.57079633F;

		bodyModel[4].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 14 strap
		bodyModel[4].setRotationPoint(4F, 9F, 4.5F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 7 strap
		bodyModel[5].setRotationPoint(-5F, 10.01F, -4F);
		bodyModel[5].rotateAngleX = 1.57079633F;

		bodyModel[6].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 7 strap
		bodyModel[6].setRotationPoint(-5F, 5F, 4.51F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 14 strap
		bodyModel[7].setRotationPoint(-5F, 9F, 4.5F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 7 strap
		bodyModel[8].setRotationPoint(4F, 5F, -4.51F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 14 strap
		bodyModel[9].setRotationPoint(4F, 9F, -4.5F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 7 strap
		bodyModel[10].setRotationPoint(-5F, 5F, -4.51F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 14 strap
		bodyModel[11].setRotationPoint(-5F, 9F, -4.5F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 7 strap
		bodyModel[12].setRotationPoint(4F, 4.99F, -4.5F);
		bodyModel[12].rotateAngleX = 1.57079633F;

		bodyModel[13].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 7 strap
		bodyModel[13].setRotationPoint(-5F, 4.99F, -4.5F);
		bodyModel[13].rotateAngleX = 1.57079633F;

		bodyModel[14].addShapeBox(0F, 0F, 0F, 32, 15, 16, 0F,-7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F); // Box 1 wood hyper
		bodyModel[14].setRotationPoint(-16F, -5F, -8F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		bodyModel[15].setRotationPoint(20.5F, 9F, -4F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		bodyModel[16].setRotationPoint(11.5F, 9F, -4F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 20
		bodyModel[17].setRotationPoint(21F, 5F, 4.51F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 21
		bodyModel[18].setRotationPoint(21F, 10.01F, -4F);
		bodyModel[18].rotateAngleX = 1.57079633F;

		bodyModel[19].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 22
		bodyModel[19].setRotationPoint(21F, 9F, 4.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 23
		bodyModel[20].setRotationPoint(12F, 10.01F, -4F);
		bodyModel[20].rotateAngleX = 1.57079633F;

		bodyModel[21].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 24
		bodyModel[21].setRotationPoint(12F, 5F, 4.51F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 25
		bodyModel[22].setRotationPoint(12F, 9F, 4.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 26
		bodyModel[23].setRotationPoint(21F, 5F, -4.51F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 27
		bodyModel[24].setRotationPoint(21F, 9F, -4.5F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 28
		bodyModel[25].setRotationPoint(12F, 5F, -4.51F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 29
		bodyModel[26].setRotationPoint(12F, 9F, -4.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 30
		bodyModel[27].setRotationPoint(21F, 4.99F, -4.5F);
		bodyModel[27].rotateAngleX = 1.57079633F;

		bodyModel[28].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 31
		bodyModel[28].setRotationPoint(12F, 4.99F, -4.5F);
		bodyModel[28].rotateAngleX = 1.57079633F;

		bodyModel[29].addShapeBox(0F, 0F, 0F, 32, 15, 16, 0F,-7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F); // Box 32
		bodyModel[29].setRotationPoint(1F, -5F, -8F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 32, 15, 16, 0F,-7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F); // Box 33
		bodyModel[30].setRotationPoint(-33F, -5F, -8F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		bodyModel[31].setRotationPoint(-22.5F, 9F, -4F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 35
		bodyModel[32].setRotationPoint(-22F, 10.01F, -4F);
		bodyModel[32].rotateAngleX = 1.57079633F;

		bodyModel[33].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 36
		bodyModel[33].setRotationPoint(-22F, 9F, 4.5F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 37
		bodyModel[34].setRotationPoint(-22F, 5F, 4.51F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		bodyModel[35].setRotationPoint(-13.5F, 9F, -4F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 39
		bodyModel[36].setRotationPoint(-13F, 9F, 4.5F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 40
		bodyModel[37].setRotationPoint(-13F, 10.01F, -4F);
		bodyModel[37].rotateAngleX = 1.57079633F;

		bodyModel[38].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 41
		bodyModel[38].setRotationPoint(-13F, 5F, 4.51F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 42
		bodyModel[39].setRotationPoint(-13F, 9F, -4.5F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 43
		bodyModel[40].setRotationPoint(-13F, 5F, -4.51F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 44
		bodyModel[41].setRotationPoint(-22F, 9F, -4.5F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 45
		bodyModel[42].setRotationPoint(-22F, 5F, -4.51F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 46
		bodyModel[43].setRotationPoint(-13F, 4.99F, -4.5F);
		bodyModel[43].rotateAngleX = 1.57079633F;

		bodyModel[44].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 47
		bodyModel[44].setRotationPoint(-22F, 4.99F, -4.5F);
		bodyModel[44].rotateAngleX = 1.57079633F;

		bodyModel[45].addShapeBox(0F, 0F, 0F, 32, 15, 16, 0F,-7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F); // Box 45
		bodyModel[45].setRotationPoint(-50F, -5F, -8F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 46
		bodyModel[46].setRotationPoint(-39F, 10.01F, -4F);
		bodyModel[46].rotateAngleX = 1.57079633F;

		bodyModel[47].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		bodyModel[47].setRotationPoint(-39.5F, 9F, -4F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 48
		bodyModel[48].setRotationPoint(-39F, 9F, 4.5F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 49
		bodyModel[49].setRotationPoint(-39F, 5F, 4.51F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 50
		bodyModel[50].setRotationPoint(-30F, 9F, 4.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 51
		bodyModel[51].setRotationPoint(-30F, 10.01F, -4F);
		bodyModel[51].rotateAngleX = 1.57079633F;

		bodyModel[52].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		bodyModel[52].setRotationPoint(-30.5F, 9F, -4F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 53
		bodyModel[53].setRotationPoint(-30F, 5F, 4.51F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 54
		bodyModel[54].setRotationPoint(-30F, 9F, -4.5F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 55
		bodyModel[55].setRotationPoint(-30F, 5F, -4.51F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 56
		bodyModel[56].setRotationPoint(-39F, 9F, -4.5F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 57
		bodyModel[57].setRotationPoint(-39F, 5F, -4.51F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 58
		bodyModel[58].setRotationPoint(-39F, 4.99F, -4.5F);
		bodyModel[58].rotateAngleX = 1.57079633F;

		bodyModel[59].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 59
		bodyModel[59].setRotationPoint(-30F, 4.99F, -4.5F);
		bodyModel[59].rotateAngleX = 1.57079633F;

		bodyModel[60].addShapeBox(0F, 0F, 0F, 32, 15, 16, 0F,-7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -10F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F, -7.5F, -0.5F, -3.5F); // Box 60
		bodyModel[60].setRotationPoint(18F, -5F, -8F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 61
		bodyModel[61].setRotationPoint(29F, 10.01F, -4F);
		bodyModel[61].rotateAngleX = 1.57079633F;

		bodyModel[62].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		bodyModel[62].setRotationPoint(28.5F, 9F, -4F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 63
		bodyModel[63].setRotationPoint(29F, 9F, 4.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 64
		bodyModel[64].setRotationPoint(29F, 5F, 4.51F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F); // Box 65
		bodyModel[65].setRotationPoint(38F, 9F, 4.5F);

		bodyModel[66].addShapeBox(0F, 0F, 0F, 1, 8, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 66
		bodyModel[66].setRotationPoint(38F, 10.01F, -4F);
		bodyModel[66].rotateAngleX = 1.57079633F;

		bodyModel[67].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		bodyModel[67].setRotationPoint(37.5F, 9F, -4F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 68
		bodyModel[68].setRotationPoint(38F, 5F, 4.51F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 69
		bodyModel[69].setRotationPoint(38F, 9F, -4.5F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 70
		bodyModel[70].setRotationPoint(38F, 5F, -4.51F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 1, 1, 0, 0F,-0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, 0F, -0.5F, -0.125F, 0F, -0.5F, -0.125F, 0F, 0.5F, -0.125F, 0F, 0.5F); // Box 71
		bodyModel[71].setRotationPoint(29F, 9F, -4.5F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 1, 5, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F, -0.125F, -0.5F, 0F); // Box 72
		bodyModel[72].setRotationPoint(29F, 5F, -4.51F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 73
		bodyModel[73].setRotationPoint(29F, 4.99F, -4.5F);
		bodyModel[73].rotateAngleX = 1.57079633F;

		bodyModel[74].addShapeBox(0F, 0F, 0F, 1, 9, 0, 0F,-0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F, -0.125F, 0F, 0F); // Box 74
		bodyModel[74].setRotationPoint(38F, 4.99F, -4.5F);
		bodyModel[74].rotateAngleX = 1.57079633F;
	}
}