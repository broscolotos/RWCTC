//FMT-Marker DFM-1.0
//Creator: Test Account

//Using PER-GROUP-INIT mode with limit '500'!
package train.client.render.models;

import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

/** This file was exported via the (Default) FlansMod Exporter of<br>
 *  FMT (Fex's Modelling Toolbox) v.2.7.3 &copy; 2022 - Fexcraft.net<br>
 *  All rights reserved. For this Model's License contact the Author/Creator.
 */
public class ShinkansenEngineModel extends ModelConverter {

	private int textureX = 512;
	private int textureY = 256;

	public ShinkansenEngineModel(){
		bodyModel = new ModelRendererTurbo[794];
		//
		initGroup_bodyModel0();
		initGroup_bodyModel1();
	}

	private final void initGroup_bodyModel0(){
		bodyModel[0] = new ModelRendererTurbo(this, 461, 134, textureX, textureY);
		bodyModel[0].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[0].setRotationPoint(-43.25f, -7.0f, -11.0f);

		bodyModel[1] = new ModelRendererTurbo(this, 217, 134, textureX, textureY);
		bodyModel[1].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[1].setRotationPoint(-43.25f, -9.0f, -10.625f);

		bodyModel[2] = new ModelRendererTurbo(this, 0, 157, textureX, textureY);
		bodyModel[2].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[2].setRotationPoint(-22.75f, -5.0f, -11.0f);

		bodyModel[3] = new ModelRendererTurbo(this, 339, 156, textureX, textureY);
		bodyModel[3].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[3].setRotationPoint(-22.75f, -7.0f, -11.0f);

		bodyModel[4] = new ModelRendererTurbo(this, 100, 156, textureX, textureY);
		bodyModel[4].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[4].setRotationPoint(-22.75f, -9.0f, -10.625f);

		bodyModel[5] = new ModelRendererTurbo(this, 292, 174, textureX, textureY);
		bodyModel[5].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[5].setRotationPoint(-9.75f, -5.0f, -11.0f);

		bodyModel[6] = new ModelRendererTurbo(this, 241, 174, textureX, textureY);
		bodyModel[6].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[6].setRotationPoint(-9.75f, -7.0f, -11.0f);

		bodyModel[7] = new ModelRendererTurbo(this, 234, 174, textureX, textureY);
		bodyModel[7].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[7].setRotationPoint(-9.75f, -9.0f, -10.625f);

		bodyModel[8] = new ModelRendererTurbo(this, 227, 174, textureX, textureY);
		bodyModel[8].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[8].setRotationPoint(-3.25f, -5.0f, -11.0f);

		bodyModel[9] = new ModelRendererTurbo(this, 210, 174, textureX, textureY);
		bodyModel[9].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[9].setRotationPoint(-3.25f, -7.0f, -11.0f);

		bodyModel[10] = new ModelRendererTurbo(this, 171, 174, textureX, textureY);
		bodyModel[10].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[10].setRotationPoint(-3.25f, -9.0f, -10.625f);

		bodyModel[11] = new ModelRendererTurbo(this, 292, 171, textureX, textureY);
		bodyModel[11].addShapeBox(0, 0, 0, 5, 1, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[11].setRotationPoint(-14.75f, -9.0f, -10.625f);

		bodyModel[12] = new ModelRendererTurbo(this, 209, 171, textureX, textureY);
		bodyModel[12].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[12].setRotationPoint(-7.75f, -9.0f, -10.625f);

		bodyModel[13] = new ModelRendererTurbo(this, 169, 171, textureX, textureY);
		bodyModel[13].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[13].setRotationPoint(-1.25f, -9.0f, -10.625f);

		bodyModel[14] = new ModelRendererTurbo(this, 0, 132, textureX, textureY);
		bodyModel[14].addShapeBox(0, 0, 0, 16, 3, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[14].setRotationPoint(-43.25f, -3.0f, 10.0f);

		bodyModel[15] = new ModelRendererTurbo(this, 462, 130, textureX, textureY);
		bodyModel[15].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f);
		bodyModel[15].setRotationPoint(-43.25f, 0.0f, 9.75f);

		bodyModel[16] = new ModelRendererTurbo(this, 299, 15, textureX, textureY);
		bodyModel[16].addShapeBox(0, 0, 0, 82, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.75f, 0, 0, -0.75f, 0, 0, -0.75f, 0, 0, -0.75f, 0);
		bodyModel[16].setRotationPoint(-43.25f, 1.75f, 9.875f);

		bodyModel[17] = new ModelRendererTurbo(this, 299, 12, textureX, textureY);
		bodyModel[17].addShapeBox(0, 0, 0, 82, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.5f, 0, -0.875f, -0.5f);
		bodyModel[17].setRotationPoint(-43.25f, 2.0f, 9.875f);

		bodyModel[18] = new ModelRendererTurbo(this, 0, 24, textureX, textureY);
		bodyModel[18].addShapeBox(0, 0, 0, 90, 3, 21, 0, 0, 0, 0.625f, -0.375f, 0, 0.625f, -0.375f, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.875f, -0.375f, -0.25f, -0.875f, -0.375f, -0.25f, -1.375f, 0, -0.25f, -1.375f);
		bodyModel[18].setRotationPoint(-43.25f, 2.75f, -10.25f);

		bodyModel[19] = new ModelRendererTurbo(this, 277, 93, textureX, textureY);
		bodyModel[19].addShapeBox(0, 0, 0, 9, 2, 18, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.875f, 0, -0.4375f, -0.875f);
		bodyModel[19].setRotationPoint(-43.25f, 5.5f, -8.75f);

		bodyModel[20] = new ModelRendererTurbo(this, 419, 114, textureX, textureY);
		bodyModel[20].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, 0.125f, -0.0625f, 0, 0.125f, -0.0625f, 0, -0.375f, 0, 0, -0.375f, 0, -0.25f, -0.35500002f, -0.0625f, -1, 0.125f, -0.0625f, -1, -0.375f, 0, -0.25f, -0.855f);
		bodyModel[20].setRotationPoint(-32.625f, 5.5f, -9.25f);

		bodyModel[21] = new ModelRendererTurbo(this, 202, 71, textureX, textureY);
		bodyModel[21].addShapeBox(0, 0, 0, 52, 1, 1, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[21].setRotationPoint(-22.75f, -4.0f, 10.0f);

		bodyModel[22] = new ModelRendererTurbo(this, 218, 130, textureX, textureY);
		bodyModel[22].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[22].setRotationPoint(-43.25f, -5.0f, -11.0f);

		bodyModel[23] = new ModelRendererTurbo(this, 183, 130, textureX, textureY);
		bodyModel[23].addShapeBox(0, 0, 0, 16, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[23].setRotationPoint(-43.25f, -3.0f, -11.0f);

		bodyModel[24] = new ModelRendererTurbo(this, 67, 127, textureX, textureY);
		bodyModel[24].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[24].setRotationPoint(-43.25f, 0.0f, -10.75f);

		bodyModel[25] = new ModelRendererTurbo(this, 191, 66, textureX, textureY);
		bodyModel[25].addShapeBox(0, 0, 0, 62, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[25].setRotationPoint(-22.75f, -3.0f, -11.0f);

		bodyModel[26] = new ModelRendererTurbo(this, 0, 65, textureX, textureY);
		bodyModel[26].addShapeBox(0, 0, 0, 62, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[26].setRotationPoint(-22.75f, 0.0f, -10.75f);

		bodyModel[27] = new ModelRendererTurbo(this, 0, 69, textureX, textureY);
		bodyModel[27].addShapeBox(0, 0, 0, 52, 1, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[27].setRotationPoint(-22.75f, -4.0f, -11.0f);

		bodyModel[28] = new ModelRendererTurbo(this, 314, 93, textureX, textureY);
		bodyModel[28].addShapeBox(0, 0, 0, 9, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[28].setRotationPoint(-43.25f, 7.0625f, -7.75f);

		bodyModel[29] = new ModelRendererTurbo(this, 398, 93, textureX, textureY);
		bodyModel[29].addShapeBox(0, 0, 0, 9, 1, 14, 0, 0, 0, 0.8125f, 0, 0, 0.8125f, 0, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[29].setRotationPoint(-43.25f, 7.8125f, -6.75f);

		bodyModel[30] = new ModelRendererTurbo(this, 271, 114, textureX, textureY);
		bodyModel[30].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.375f, 0, 0, -0.375f, 0, -0.25f, -0.35500002f, -0.375f, -0.25f, -0.35500002f, -0.375f, -0.25f, -0.855f, 0, -0.25f, -0.855f);
		bodyModel[30].setRotationPoint(-34.25f, 5.5f, -9.25f);

		bodyModel[31] = new ModelRendererTurbo(this, 127, 114, textureX, textureY);
		bodyModel[31].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, -0.35500002f, -0.375f, 0, -0.35500002f, -0.375f, 0, -0.855f, 0, 0, -0.855f, 0, -0.1875f, -0.875f, -1, -0.1875f, -0.875f, -1, -0.1875f, -1.375f, 0, -0.1875f, -1.375f);
		bodyModel[31].setRotationPoint(-34.25f, 6.25f, -9.25f);

		bodyModel[32] = new ModelRendererTurbo(this, 48, 127, textureX, textureY);
		bodyModel[32].addShapeBox(0, 0, 0, 1, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, -0.3125f, -0.25f, -0.1875f, -0.3125f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[32].setRotationPoint(-34.25f, 7.0625f, -7.75f);

		bodyModel[33] = new ModelRendererTurbo(this, 0, 137, textureX, textureY);
		bodyModel[33].addShapeBox(0, 0, 0, 1, 1, 14, 0, 0, 0, 0.8125f, -0.3125f, 0, 0.8125f, -0.3125f, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, -0.625f, -0.25f, -0.0625f, -0.625f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[33].setRotationPoint(-34.25f, 7.8125f, -6.75f);

		bodyModel[34] = new ModelRendererTurbo(this, 84, 114, textureX, textureY);
		bodyModel[34].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.0625f, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.375f, -0.0625f, 0, -0.375f, -0.0625f, -1, 0.125f, 0, -0.25f, -0.35500002f, 0, -0.25f, -0.855f, -0.0625f, -1, -0.375f);
		bodyModel[34].setRotationPoint(-9.75f, 5.5f, -9.25f);

		bodyModel[35] = new ModelRendererTurbo(this, 348, 111, textureX, textureY);
		bodyModel[35].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.375f, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.375f, -0.375f, 0, -0.375f, -0.375f, -0.25f, -0.35500002f, 0, -0.25f, -0.35500002f, 0, -0.25f, -0.855f, -0.375f, -0.25f, -0.855f);
		bodyModel[35].setRotationPoint(-8.125f, 5.5f, -9.25f);

		bodyModel[36] = new ModelRendererTurbo(this, 395, 109, textureX, textureY);
		bodyModel[36].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.375f, 0, -0.35500002f, 0, 0, -0.35500002f, 0, 0, -0.855f, -0.375f, 0, -0.855f, -1, -0.1875f, -0.875f, 0, -0.1875f, -0.875f, 0, -0.1875f, -1.375f, -1, -0.1875f, -1.375f);
		bodyModel[36].setRotationPoint(-8.125f, 6.25f, -9.25f);

		bodyModel[37] = new ModelRendererTurbo(this, 375, 125, textureX, textureY);
		bodyModel[37].addShapeBox(0, 0, 0, 1, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, -0.3125f, -0.25f, -0.1875f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.6875f, -0.3125f, -0.25f, -0.6875f);
		bodyModel[37].setRotationPoint(-7.125f, 7.0625f, -7.75f);

		bodyModel[38] = new ModelRendererTurbo(this, 135, 135, textureX, textureY);
		bodyModel[38].addShapeBox(0, 0, 0, 1, 1, 14, 0, -0.3125f, 0, 0.8125f, 0, 0, 0.8125f, 0, 0, 0.3125f, -0.3125f, 0, 0.3125f, -0.625f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.5625f, -0.625f, -0.25f, -0.5625f);
		bodyModel[38].setRotationPoint(-7.125f, 7.8125f, -6.75f);

		bodyModel[39] = new ModelRendererTurbo(this, 271, 18, textureX, textureY);
		bodyModel[39].addShapeBox(0, 0, 0, 52, 2, 18, 0, 0, 0, 0.625f, 0.5f, 0, 0.625f, 0.5f, 0, 0.125f, 0, 0, 0.125f, 0, -0.4375f, -0.375f, 0.5f, -0.4375f, -0.375f, 0.5f, -0.4375f, -0.875f, 0, -0.4375f, -0.875f);
		bodyModel[39].setRotationPoint(-6.125f, 5.5f, -8.75f);

		bodyModel[40] = new ModelRendererTurbo(this, 271, 39, textureX, textureY);
		bodyModel[40].addShapeBox(0, 0, 0, 52, 1, 16, 0, 0, 0, 0.625f, 0.5f, 0, 0.625f, 0.5f, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, 0.5f, -0.25f, -0.1875f, 0.5f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[40].setRotationPoint(-6.125f, 7.0625f, -7.75f);

		bodyModel[41] = new ModelRendererTurbo(this, 0, 49, textureX, textureY);
		bodyModel[41].addShapeBox(0, 0, 0, 52, 1, 14, 0, 0, 0, 0.8125f, 0.5f, 0, 0.8125f, 0.5f, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, 0.5f, -0.25f, -0.0625f, 0.5f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[41].setRotationPoint(-6.125f, 7.8125f, -6.75f);

		bodyModel[42] = new ModelRendererTurbo(this, 267, 9, textureX, textureY);
		bodyModel[42].addShapeBox(0, 0, 0, 82, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.75f, 0, 0, -0.75f, 0, 0, -0.75f, 0, 0, -0.75f, 0);
		bodyModel[42].setRotationPoint(-43.25f, 1.75f, -10.875f);

		bodyModel[43] = new ModelRendererTurbo(this, 267, 6, textureX, textureY);
		bodyModel[43].addShapeBox(0, 0, 0, 82, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.875f, -0.5f, 0, -0.875f, -0.5f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[43].setRotationPoint(-43.25f, 2.0f, -10.875f);

		bodyModel[44] = new ModelRendererTurbo(this, 267, 3, textureX, textureY);
		bodyModel[44].addShapeBox(0, 0, 0, 82, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[44].setRotationPoint(-43.25f, 2.125f, -10.875f);

		bodyModel[45] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[45].addShapeBox(0, 0, 0, 90, 1, 22, 0, 0, -0.75f, -0.125f, -0.375f, -0.75f, -0.125f, -0.375f, -0.75f, -0.125f, 0, -0.75f, -0.125f, 0, 0, -0.125f, -0.375f, 0, -0.125f, -0.375f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[45].setRotationPoint(-43.25f, 1.75f, -11.0f);

		bodyModel[46] = new ModelRendererTurbo(this, 253, 126, textureX, textureY);
		bodyModel[46].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0.5625f, 0, 0, 0.5625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[46].setRotationPoint(-43.25f, -10.75f, -9.9375f);

		bodyModel[47] = new ModelRendererTurbo(this, 435, 61, textureX, textureY);
		bodyModel[47].addShapeBox(0, 0, 0, 25, 2, 1, 0, 0, 0, -1.0625f, 0.5f, 0, -1.0625f, 0.5f, 0, 0.5625f, 0, 0, 0.5625f, 0, -0.25f, 0, 0.5f, -0.25f, 0, 0.5f, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[47].setRotationPoint(-22.75f, -10.75f, -9.9375f);

		bodyModel[48] = new ModelRendererTurbo(this, 161, 80, textureX, textureY);
		bodyModel[48].addShapeBox(0, 0, 0, 46, 1, 2, 0, 0, -0.375f, 0.25f, 0, -0.375f, 0.25f, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.125f, 0.25f, 0, -0.125f, 0.25f, 0, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[48].setRotationPoint(-43.25f, -14.25f, -2.0f);

		bodyModel[49] = new ModelRendererTurbo(this, 218, 126, textureX, textureY);
		bodyModel[49].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[49].setRotationPoint(-43.25f, -7.0f, 10.0f);

		bodyModel[50] = new ModelRendererTurbo(this, 183, 126, textureX, textureY);
		bodyModel[50].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[50].setRotationPoint(-43.25f, -9.0f, 9.625f);

		bodyModel[51] = new ModelRendererTurbo(this, 473, 155, textureX, textureY);
		bodyModel[51].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[51].setRotationPoint(-22.75f, -5.0f, 10.0f);

		bodyModel[52] = new ModelRendererTurbo(this, 397, 155, textureX, textureY);
		bodyModel[52].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[52].setRotationPoint(-22.75f, -7.0f, 10.0f);

		bodyModel[53] = new ModelRendererTurbo(this, 378, 155, textureX, textureY);
		bodyModel[53].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[53].setRotationPoint(-22.75f, -9.0f, 9.625f);

		bodyModel[54] = new ModelRendererTurbo(this, 108, 168, textureX, textureY);
		bodyModel[54].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[54].setRotationPoint(-9.75f, -5.0f, 10.0f);

		bodyModel[55] = new ModelRendererTurbo(this, 157, 173, textureX, textureY);
		bodyModel[55].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[55].setRotationPoint(-9.75f, -7.0f, 10.0f);

		bodyModel[56] = new ModelRendererTurbo(this, 140, 173, textureX, textureY);
		bodyModel[56].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[56].setRotationPoint(-9.75f, -9.0f, 9.625f);

		bodyModel[57] = new ModelRendererTurbo(this, 55, 165, textureX, textureY);
		bodyModel[57].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[57].setRotationPoint(-3.25f, -5.0f, 10.0f);

		bodyModel[58] = new ModelRendererTurbo(this, 72, 173, textureX, textureY);
		bodyModel[58].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[58].setRotationPoint(-3.25f, -7.0f, 10.0f);

		bodyModel[59] = new ModelRendererTurbo(this, 65, 173, textureX, textureY);
		bodyModel[59].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[59].setRotationPoint(-3.25f, -9.0f, 9.625f);

		bodyModel[60] = new ModelRendererTurbo(this, 33, 171, textureX, textureY);
		bodyModel[60].addShapeBox(0, 0, 0, 5, 1, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[60].setRotationPoint(-14.75f, -9.0f, 9.625f);

		bodyModel[61] = new ModelRendererTurbo(this, 351, 170, textureX, textureY);
		bodyModel[61].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[61].setRotationPoint(-7.75f, -9.0f, 9.625f);

		bodyModel[62] = new ModelRendererTurbo(this, 328, 170, textureX, textureY);
		bodyModel[62].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[62].setRotationPoint(-1.25f, -9.0f, 9.625f);

		bodyModel[63] = new ModelRendererTurbo(this, 29, 126, textureX, textureY);
		bodyModel[63].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[63].setRotationPoint(-43.25f, -5.0f, 10.0f);

		bodyModel[64] = new ModelRendererTurbo(this, 475, 84, textureX, textureY);
		bodyModel[64].addShapeBox(0, 0, 0, 16, 2, 1, 0, 0, 0, 0.5625f, 0, 0, 0.5625f, 0, 0, -1.0625f, 0, 0, -1.0625f, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[64].setRotationPoint(-43.25f, -10.75f, 8.9375f);

		bodyModel[65] = new ModelRendererTurbo(this, 406, 57, textureX, textureY);
		bodyModel[65].addShapeBox(0, 0, 0, 25, 2, 1, 0, 0, 0, 0.5625f, 0.5f, 0, 0.5625f, 0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, -0.25f, -0.5f, 0.5f, -0.25f, -0.5f, 0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[65].setRotationPoint(-22.75f, -10.75f, 8.9375f);

		bodyModel[66] = new ModelRendererTurbo(this, 0, 79, textureX, textureY);
		bodyModel[66].addShapeBox(0, 0, 0, 46, 1, 2, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0.25f, 0, -0.375f, 0.25f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.125f, 0.25f, 0, -0.125f, 0.25f);
		bodyModel[66].setRotationPoint(-43.25f, -14.25f, 0.0f);

		bodyModel[67] = new ModelRendererTurbo(this, 345, 160, textureX, textureY);
		bodyModel[67].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[67].setRotationPoint(2.75f, -5.0f, 10.0f);

		bodyModel[68] = new ModelRendererTurbo(this, 206, 158, textureX, textureY);
		bodyModel[68].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[68].setRotationPoint(8.75f, -5.0f, 10.0f);

		bodyModel[69] = new ModelRendererTurbo(this, 256, 154, textureX, textureY);
		bodyModel[69].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[69].setRotationPoint(14.75f, -5.0f, 10.0f);

		bodyModel[70] = new ModelRendererTurbo(this, 320, 153, textureX, textureY);
		bodyModel[70].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[70].setRotationPoint(20.75f, -5.0f, 10.0f);

		bodyModel[71] = new ModelRendererTurbo(this, 356, 150, textureX, textureY);
		bodyModel[71].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[71].setRotationPoint(26.75f, -5.0f, 10.0f);

		bodyModel[72] = new ModelRendererTurbo(this, 0, 72, textureX, textureY);
		bodyModel[72].addShapeBox(0, 0, 0, 46, 1, 5, 0, 0, -1.4375f, -0.25f, 0, -1.4375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0.9375f, -0.25f, 0, 0.9375f, -0.25f, 0, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[72].setRotationPoint(-43.25f, -13.875f, -7.25f);

		bodyModel[73] = new ModelRendererTurbo(this, 159, 84, textureX, textureY);
		bodyModel[73].addShapeBox(0, 0, 0, 46, 1, 1, 0, 0, 0.8125f, -2.0625f, 0, 0.8125f, -2.0625f, 0, 0.3125f, 1.0625f, 0, 0.3125f, 1.0625f, 0, -0.125f, -0.1875f, 0, -0.125f, -0.1875f, 0, -0.125f, -0.3125f, 0, -0.125f, -0.3125f);
		bodyModel[73].setRotationPoint(-43.25f, -11.625f, -9.0625f);

		bodyModel[74] = new ModelRendererTurbo(this, 58, 173, textureX, textureY);
		bodyModel[74].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[74].setRotationPoint(2.75f, -7.0f, 10.0f);

		bodyModel[75] = new ModelRendererTurbo(this, 11, 173, textureX, textureY);
		bodyModel[75].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[75].setRotationPoint(2.75f, -9.0f, 9.625f);

		bodyModel[76] = new ModelRendererTurbo(this, 317, 170, textureX, textureY);
		bodyModel[76].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[76].setRotationPoint(4.75f, -9.0f, 9.625f);

		bodyModel[77] = new ModelRendererTurbo(this, 501, 172, textureX, textureY);
		bodyModel[77].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[77].setRotationPoint(8.75f, -7.0f, 10.0f);

		bodyModel[78] = new ModelRendererTurbo(this, 395, 172, textureX, textureY);
		bodyModel[78].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[78].setRotationPoint(8.75f, -9.0f, 9.625f);

		bodyModel[79] = new ModelRendererTurbo(this, 281, 170, textureX, textureY);
		bodyModel[79].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[79].setRotationPoint(10.75f, -9.0f, 9.625f);

		bodyModel[80] = new ModelRendererTurbo(this, 373, 172, textureX, textureY);
		bodyModel[80].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[80].setRotationPoint(14.75f, -7.0f, 10.0f);

		bodyModel[81] = new ModelRendererTurbo(this, 250, 172, textureX, textureY);
		bodyModel[81].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[81].setRotationPoint(14.75f, -9.0f, 9.625f);

		bodyModel[82] = new ModelRendererTurbo(this, 270, 170, textureX, textureY);
		bodyModel[82].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0, 0, 0.25f, 0, 0, -0.75f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.3125f, 0, 0.5f, -0.1875f, 0, 0.5f, -0.1717f);
		bodyModel[82].setRotationPoint(16.75f, -9.0f, 9.625f);

		bodyModel[83] = new ModelRendererTurbo(this, 220, 172, textureX, textureY);
		bodyModel[83].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[83].setRotationPoint(20.75f, -7.0f, 10.0f);

		bodyModel[84] = new ModelRendererTurbo(this, 234, 160, textureX, textureY);
		bodyModel[84].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.25f, 0, 0, 0.28125f, 0, 0, -0.78125f, 0, 0, -0.75f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[84].setRotationPoint(20.75f, -9.0f, 9.625f);

		bodyModel[85] = new ModelRendererTurbo(this, 259, 170, textureX, textureY);
		bodyModel[85].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.28125f, 0, 0, 0.34375f, 0, 0, -0.84375f, 0, 0, -0.78125f, 0, 0.5f, -0.3046875f, 0, 0.5f, -0.265625f, 0, 0.5f, -0.2344f, 0, 0.5f, -0.19525f);
		bodyModel[85].setRotationPoint(22.75f, -9.0f, 9.625f);

		bodyModel[86] = new ModelRendererTurbo(this, 146, 154, textureX, textureY);
		bodyModel[86].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.09375f, 0, 0, -0.0625f, 0, 0, -0.4375f, 0, 0, -0.40625f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[86].setRotationPoint(26.75f, -7.0f, 10.0f);

		bodyModel[87] = new ModelRendererTurbo(this, 62, 148, textureX, textureY);
		bodyModel[87].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.34375f, 0, 0, 0.4375f, 0, 0, -0.9375f, 0, 0, -0.84375f, 0, 0, -0.46875f, 0, 0, -0.4375f, 0, 0, -0.0625f, 0, 0, -0.03125f);
		bodyModel[87].setRotationPoint(26.75f, -9.0f, 9.625f);

		bodyModel[88] = new ModelRendererTurbo(this, 147, 170, textureX, textureY);
		bodyModel[88].addShapeBox(0, 0, 0, 4, 2, 1, 0, 0, -0.375f, 0.5f, 0, -0.75f, 0.1875f, 0, -0.75f, -0.6875f, 0, -0.375f, -1, 0, -0.25f, -0.5f, 0, -0.25f, -0.4375f, 0, -0.25f, -0.0625f, 0, -0.25f, 0);
		bodyModel[88].setRotationPoint(16.75f, -10.75f, 8.9375f);

		bodyModel[89] = new ModelRendererTurbo(this, 336, 71, textureX, textureY);
		bodyModel[89].addShapeBox(0, 0, 0, 46, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, -1.4375f, -0.25f, 0, -1.4375f, -0.25f, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.9375f, -0.25f, 0, 0.9375f, -0.25f);
		bodyModel[89].setRotationPoint(-43.25f, -13.875f, 2.25f);

		bodyModel[90] = new ModelRendererTurbo(this, 60, 83, textureX, textureY);
		bodyModel[90].addShapeBox(0, 0, 0, 46, 1, 1, 0, 0, 0.3125f, 1.0625f, 0, 0.3125f, 1.0625f, 0, 0.8125f, -2.0625f, 0, 0.8125f, -2.0625f, 0, -0.125f, -0.3125f, 0, -0.125f, -0.3125f, 0, -0.125f, -0.1875f, 0, -0.125f, -0.1875f);
		bodyModel[90].setRotationPoint(-43.25f, -11.625f, 8.0625f);

		bodyModel[91] = new ModelRendererTurbo(this, 283, 130, textureX, textureY);
		bodyModel[91].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0.25f, 0.1875f, 0, 0.0625f, 0.0625f, 0, 0.0625f, -0.5625f, 0, 0.25f, -0.6875f, 0, -0.25f, -0.4375f, 0, -0.25f, -0.40625f, 0, -0.25f, -0.09375f, 0, -0.25f, -0.0625f);
		bodyModel[91].setRotationPoint(20.75f, -9.75f, 8.9375f);

		bodyModel[92] = new ModelRendererTurbo(this, 58, 170, textureX, textureY);
		bodyModel[92].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.0625f, -0.03125f, 0, -0.28125f, -0.21875f, 0, -0.28125f, -0.28125f, 0, 0.0625f, -0.46875f, 0, -0.25f, -0.5f, 0, -0.25f, -0.4375f, 0, -0.25f, -0.0625f, 0, -0.25f, 0);
		bodyModel[92].setRotationPoint(22.75f, -9.75f, 8.84375f);

		bodyModel[93] = new ModelRendererTurbo(this, 92, 106, textureX, textureY);
		bodyModel[93].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -0.28125f, -0.28125f, 0, -0.46875f, -0.28125f, 0, -0.46875f, -0.21875f, 0, -0.28125f, -0.21875f, 0, -0.25f, -0.5f, 0, -0.25f, -0.40625f, 0, -0.25f, -0.09375f, 0, -0.25f, 0);
		bodyModel[93].setRotationPoint(26.75f, -9.75f, 8.78125f);

		bodyModel[94] = new ModelRendererTurbo(this, 282, 153, textureX, textureY);
		bodyModel[94].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.5f, -0.5f, 0, -0.125f, -0.5f, 0, -0.375f, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[94].setRotationPoint(31.75f, -5.0f, 10.0f);

		bodyModel[95] = new ModelRendererTurbo(this, 0, 153, textureX, textureY);
		bodyModel[95].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, 0.0625f, -0.5f, -0.375f, 0.21875f, -0.5f, -0.375f, -0.71875f, 0, 0, -0.5625f, 0, 0, -0.5f, -0.5f, 0, -0.125f, -0.5f, 0, -0.375f, 0, 0, 0);
		bodyModel[95].setRotationPoint(31.75f, -7.0f, 10.0f);

		bodyModel[96] = new ModelRendererTurbo(this, 299, 0, textureX, textureY);
		bodyModel[96].addShapeBox(0, 0, 0, 82, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[96].setRotationPoint(-43.25f, 2.125f, 9.375f);

		bodyModel[97] = new ModelRendererTurbo(this, 180, 158, textureX, textureY);
		bodyModel[97].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -0.25f, -0.5f, 0, -0.25f, -1, 0, -0.25f, 0, 0, 0, 0, 0, -0.75f, -0.5f, 0, -0.75f, -1, 0, -0.75f, 0, 0, -0.625f, 0);
		bodyModel[97].setRotationPoint(38.75f, 1.75f, -11.375f);

		bodyModel[98] = new ModelRendererTurbo(this, 373, 153, textureX, textureY);
		bodyModel[98].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, -1, 0, -0.25f, -0.5f, 0, -0.625f, 0, 0, -0.75f, 0, 0, -0.75f, -1, 0, -0.75f, -0.5f);
		bodyModel[98].setRotationPoint(38.75f, 1.75f, 10.375f);

		bodyModel[99] = new ModelRendererTurbo(this, 0, 91, textureX, textureY);
		bodyModel[99].addShapeBox(0, 0, 0, 42, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[99].setRotationPoint(-14.75f, -5.0f, -10.75f);

		bodyModel[100] = new ModelRendererTurbo(this, 298, 90, textureX, textureY);
		bodyModel[100].addShapeBox(0, 0, 0, 42, 2, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[100].setRotationPoint(-14.75f, -7.0f, -10.75f);

		bodyModel[101] = new ModelRendererTurbo(this, 149, 90, textureX, textureY);
		bodyModel[101].addShapeBox(0, 0, 0, 42, 2, 0, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.6875f, 0, 0, 0.6875f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[101].setRotationPoint(-14.75f, -9.0f, -10.375f);

		bodyModel[102] = new ModelRendererTurbo(this, 0, 89, textureX, textureY);
		bodyModel[102].addShapeBox(0, 0, 0, 42, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f);
		bodyModel[102].setRotationPoint(-14.75f, -5.0f, 10.25f);

		bodyModel[103] = new ModelRendererTurbo(this, 306, 87, textureX, textureY);
		bodyModel[103].addShapeBox(0, 0, 0, 42, 2, 0, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f);
		bodyModel[103].setRotationPoint(-14.75f, -7.0f, 10.25f);

		bodyModel[104] = new ModelRendererTurbo(this, 157, 87, textureX, textureY);
		bodyModel[104].addShapeBox(0, 0, 0, 42, 2, 0, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f);
		bodyModel[104].setRotationPoint(-14.75f, -9.0f, 9.875f);

		bodyModel[105] = new ModelRendererTurbo(this, 320, 148, textureX, textureY);
		bodyModel[105].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[105].setRotationPoint(-43.25f, 0.0f, -10.5f);

		bodyModel[106] = new ModelRendererTurbo(this, 158, 148, textureX, textureY);
		bodyModel[106].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[106].setRotationPoint(-43.25f, -7.0f, -10.5f);

		bodyModel[107] = new ModelRendererTurbo(this, 62, 148, textureX, textureY);
		bodyModel[107].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -1.0625f, -0.5f, 0, -1.0625f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[107].setRotationPoint(-43.25f, -9.0f, -10.5f);

		bodyModel[108] = new ModelRendererTurbo(this, 237, 147, textureX, textureY);
		bodyModel[108].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[108].setRotationPoint(-43.25f, -5.0f, -10.5f);

		bodyModel[109] = new ModelRendererTurbo(this, 184, 147, textureX, textureY);
		bodyModel[109].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[109].setRotationPoint(-43.25f, -3.0f, -10.5f);

		bodyModel[110] = new ModelRendererTurbo(this, 22, 158, textureX, textureY);
		bodyModel[110].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, -1.125f, -0.5f, 0, -1.125f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.0625f, -0.5f, -0.25f, -0.0625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[110].setRotationPoint(-43.25f, -10.75f, -9.5f);

		bodyModel[111] = new ModelRendererTurbo(this, 124, 147, textureX, textureY);
		bodyModel[111].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[111].setRotationPoint(-43.25f, 0.0f, 2.5f);

		bodyModel[112] = new ModelRendererTurbo(this, 23, 147, textureX, textureY);
		bodyModel[112].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[112].setRotationPoint(-43.25f, -7.0f, 2.5f);

		bodyModel[113] = new ModelRendererTurbo(this, 263, 146, textureX, textureY);
		bodyModel[113].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[113].setRotationPoint(-43.25f, -9.0f, 2.5f);

		bodyModel[114] = new ModelRendererTurbo(this, 345, 145, textureX, textureY);
		bodyModel[114].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[114].setRotationPoint(-43.25f, -5.0f, 2.5f);

		bodyModel[115] = new ModelRendererTurbo(this, 301, 145, textureX, textureY);
		bodyModel[115].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[115].setRotationPoint(-43.25f, -3.0f, 2.5f);

		bodyModel[116] = new ModelRendererTurbo(this, 422, 157, textureX, textureY);
		bodyModel[116].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.125f, 0, 0, -1.125f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f);
		bodyModel[116].setRotationPoint(-43.25f, -10.75f, 2.5f);

		bodyModel[117] = new ModelRendererTurbo(this, 238, 130, textureX, textureY);
		bodyModel[117].addShapeBox(0, 0, 0, 1, 1, 15, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0.1875f, 0, -0.5f, 0.1875f, 0, -0.5f, 0.1875f, 1.75f, 0, 0.1875f, 1.75f);
		bodyModel[117].setRotationPoint(-43.25f, -11.9375f, -8.375f);

		bodyModel[118] = new ModelRendererTurbo(this, 497, 114, textureX, textureY);
		bodyModel[118].addShapeBox(0, 0, 0, 1, 1, 6, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0.4375f, 3.375f, -0.5f, 0.4375f, 3.375f, -0.5f, 0.4375f, 4.625f, 0, 0.4375f, 4.625f);
		bodyModel[118].setRotationPoint(-43.25f, -13.375f, -3.625f);

		bodyModel[119] = new ModelRendererTurbo(this, 226, 162, textureX, textureY);
		bodyModel[119].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, -2.5f, -0.5f, 0, -2.5f, -0.5f, 0, -2.5f, 0, 0, -2.5f, 0, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, 0, -0.625f, -0.25f);
		bodyModel[119].setRotationPoint(-43.25f, -13.75f, -2.5f);

		bodyModel[120] = new ModelRendererTurbo(this, 298, 157, textureX, textureY);
		bodyModel[120].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[120].setRotationPoint(-43.25f, 1.75f, -9.5f);

		bodyModel[121] = new ModelRendererTurbo(this, 281, 157, textureX, textureY);
		bodyModel[121].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[121].setRotationPoint(-43.25f, 1.75f, 2.875f);

		bodyModel[122] = new ModelRendererTurbo(this, 136, 176, textureX, textureY);
		bodyModel[122].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.5f, 0.0625f, 0, -0.5f, 0.0625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[122].setRotationPoint(-27.25f, -7.0f, -11.0f);

		bodyModel[123] = new ModelRendererTurbo(this, 7, 176, textureX, textureY);
		bodyModel[123].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.8125f, 0.0625f, 0, -0.8125f, 0.0625f, 0, 0.0625f, 0, 0, 0.0625f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[123].setRotationPoint(-27.25f, -9.0f, -10.625f);

		bodyModel[124] = new ModelRendererTurbo(this, 408, 164, textureX, textureY);
		bodyModel[124].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[124].setRotationPoint(-27.25f, -5.0f, -11.0f);

		bodyModel[125] = new ModelRendererTurbo(this, 395, 164, textureX, textureY);
		bodyModel[125].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[125].setRotationPoint(-27.25f, -3.0f, -11.0f);

		bodyModel[126] = new ModelRendererTurbo(this, 382, 164, textureX, textureY);
		bodyModel[126].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, -0.5f, -0.5f, -0.25f, -0.5f, -0.5f, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[126].setRotationPoint(-27.25f, 0.0f, -10.75f);

		bodyModel[127] = new ModelRendererTurbo(this, 315, 164, textureX, textureY);
		bodyModel[127].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -1.1875f, -0.5f, 0, -1.1875f, -0.5f, 0, 0.4375f, 0, 0, 0.4375f, 0, -0.25f, -0.125f, -0.5f, -0.25f, -0.125f, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[127].setRotationPoint(-27.25f, -10.75f, -9.9375f);

		bodyModel[128] = new ModelRendererTurbo(this, 507, 175, textureX, textureY);
		bodyModel[128].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.5f, 0.0625f, 0, -0.5f, 0.0625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[128].setRotationPoint(-23.8125f, -7.0f, -11.0f);

		bodyModel[129] = new ModelRendererTurbo(this, 200, 163, textureX, textureY);
		bodyModel[129].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.8125f, 0.0625f, 0, -0.8125f, 0.0625f, 0, 0.0625f, 0, 0, 0.0625f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[129].setRotationPoint(-23.8125f, -9.0f, -10.625f);

		bodyModel[130] = new ModelRendererTurbo(this, 450, 160, textureX, textureY);
		bodyModel[130].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f);
		bodyModel[130].setRotationPoint(-26.5625f, -7.0f, -11.0f);

		bodyModel[131] = new ModelRendererTurbo(this, 0, 160, textureX, textureY);
		bodyModel[131].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f);
		bodyModel[131].setRotationPoint(-26.5625f, -9.0f, -10.625f);

		bodyModel[132] = new ModelRendererTurbo(this, 506, 159, textureX, textureY);
		bodyModel[132].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f);
		bodyModel[132].setRotationPoint(-24.4375f, -7.0f, -11.0f);

		bodyModel[133] = new ModelRendererTurbo(this, 244, 158, textureX, textureY);
		bodyModel[133].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f);
		bodyModel[133].setRotationPoint(-24.4375f, -9.0f, -10.625f);

		bodyModel[134] = new ModelRendererTurbo(this, 503, 139, textureX, textureY);
		bodyModel[134].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[134].setRotationPoint(-26.5f, -5.5f, -11.25f);

		bodyModel[135] = new ModelRendererTurbo(this, 152, 137, textureX, textureY);
		bodyModel[135].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[135].setRotationPoint(-26.5f, -9.625f, -10.1875f);

		bodyModel[136] = new ModelRendererTurbo(this, 299, 157, textureX, textureY);
		bodyModel[136].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[136].setRotationPoint(-27.25f, -7.0f, 10.0f);

		bodyModel[137] = new ModelRendererTurbo(this, 291, 157, textureX, textureY);
		bodyModel[137].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0.0625f, 0.0625f, 0, 0.0625f, 0.0625f, 0, -0.8125f, 0, 0, -0.8125f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[137].setRotationPoint(-27.25f, -9.0f, 9.625f);

		bodyModel[138] = new ModelRendererTurbo(this, 108, 164, textureX, textureY);
		bodyModel[138].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[138].setRotationPoint(-27.25f, -5.0f, 10.0f);

		bodyModel[139] = new ModelRendererTurbo(this, 350, 163, textureX, textureY);
		bodyModel[139].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[139].setRotationPoint(-27.25f, -3.0f, 10.0f);

		bodyModel[140] = new ModelRendererTurbo(this, 331, 151, textureX, textureY);
		bodyModel[140].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, -0.25f, -0.25f, -0.5f, -0.25f, -0.25f, -0.5f, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[140].setRotationPoint(-27.25f, 0.0f, 9.75f);

		bodyModel[141] = new ModelRendererTurbo(this, 0, 147, textureX, textureY);
		bodyModel[141].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, 0.4375f, -0.5f, 0, 0.4375f, -0.5f, 0, -1.1875f, 0, 0, -1.1875f, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[141].setRotationPoint(-27.25f, -10.75f, 8.9375f);

		bodyModel[142] = new ModelRendererTurbo(this, 282, 157, textureX, textureY);
		bodyModel[142].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[142].setRotationPoint(-23.8125f, -7.0f, 10.0f);

		bodyModel[143] = new ModelRendererTurbo(this, 274, 157, textureX, textureY);
		bodyModel[143].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0.0625f, 0.0625f, 0, 0.0625f, 0.0625f, 0, -0.8125f, 0, 0, -0.8125f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[143].setRotationPoint(-23.8125f, -9.0f, 9.625f);

		bodyModel[144] = new ModelRendererTurbo(this, 53, 147, textureX, textureY);
		bodyModel[144].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f);
		bodyModel[144].setRotationPoint(-26.5625f, -7.0f, 10.0f);

		bodyModel[145] = new ModelRendererTurbo(this, 423, 146, textureX, textureY);
		bodyModel[145].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f);
		bodyModel[145].setRotationPoint(-26.5625f, -9.0f, 9.625f);

		bodyModel[146] = new ModelRendererTurbo(this, 293, 146, textureX, textureY);
		bodyModel[146].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f);
		bodyModel[146].setRotationPoint(-24.4375f, -7.0f, 10.0f);

		bodyModel[147] = new ModelRendererTurbo(this, 17, 144, textureX, textureY);
		bodyModel[147].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f);
		bodyModel[147].setRotationPoint(-24.4375f, -9.0f, 9.625f);

		bodyModel[148] = new ModelRendererTurbo(this, 503, 136, textureX, textureY);
		bodyModel[148].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[148].setRotationPoint(-26.5f, -5.5f, 10.25f);

		bodyModel[149] = new ModelRendererTurbo(this, 284, 135, textureX, textureY);
		bodyModel[149].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[149].setRotationPoint(-26.5f, -9.625f, 9.1875f);

		bodyModel[150] = new ModelRendererTurbo(this, 151, 125, textureX, textureY);
		bodyModel[150].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.75f, 0, 0, -0.75f);
		bodyModel[150].setRotationPoint(-26.125f, -7.0f, -11.0f);

		bodyModel[151] = new ModelRendererTurbo(this, 162, 124, textureX, textureY);
		bodyModel[151].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.9375f, 0.0625f, 0, -0.9375f, 0.0625f, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.75f, 0, 0, -0.75f);
		bodyModel[151].setRotationPoint(-26.125f, -9.0f, -10.625f);

		bodyModel[152] = new ModelRendererTurbo(this, 151, 121, textureX, textureY);
		bodyModel[152].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0.0625f, 0, -0.375f, 0.0625f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.75f, 0.0625f, 0, -0.75f, 0.0625f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[152].setRotationPoint(-26.125f, -7.0f, 10.0f);

		bodyModel[153] = new ModelRendererTurbo(this, 430, 118, textureX, textureY);
		bodyModel[153].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.0625f, 0.0625f, 0, -0.0625f, 0.0625f, 0, -0.9375f, 0, 0, -0.9375f, 0, 0, -0.75f, 0.0625f, 0, -0.75f, 0.0625f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[153].setRotationPoint(-26.125f, -9.0f, 9.625f);

		bodyModel[154] = new ModelRendererTurbo(this, 0, 40, textureX, textureY);
		bodyModel[154].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, 0.4375f, -0.5f, -0.8125f, 0.5625f, -0.5f, -0.8125f, -1.0625f, 0, 0, -1, 0, 0, -0.5f, -0.5f, 0.375f, -0.34375f, -0.5f, 0.375f, -0.15625f, 0, 0, 0);
		bodyModel[154].setRotationPoint(31.75f, -9.0f, 9.4375f);

		bodyModel[155] = new ModelRendererTurbo(this, 55, 130, textureX, textureY);
		bodyModel[155].addShapeBox(0, 0, 0, 3, 2, 1, 0, 0, 0, 0.34375f, 0, 0, 0.53125f, 0, 0, -1.09375f, 0, 0, -0.84375f, 0, 0, -0.53125f, 0, 0, -0.40625f, 0, 0, -0.09375f, 0, 0, 0.03125f);
		bodyModel[155].setRotationPoint(28.75f, -9.0f, 9.53125f);

		bodyModel[156] = new ModelRendererTurbo(this, 503, 23, textureX, textureY);
		bodyModel[156].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.46875f, -0.28125f, 0, -0.75f, -0.21875f, 0, -0.75f, -0.34375f, 0, -0.46875f, -0.21875f, 0, -0.25f, -0.40625f, 0, -0.25f, -0.21875f, 0, -0.25f, -0.34375f, 0, -0.25f, -0.09375f);
		bodyModel[156].setRotationPoint(28.75f, -9.75f, 8.78125f);

		bodyModel[157] = new ModelRendererTurbo(this, 503, 74, textureX, textureY);
		bodyModel[157].addShapeBox(0, 0, 0, 3, 2, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[157].setRotationPoint(28.75f, -5.0f, 10.0f);

		bodyModel[158] = new ModelRendererTurbo(this, 326, 74, textureX, textureY);
		bodyModel[158].addShapeBox(0, 0, 0, 3, 2, 1, 0, 0, 0, -0.0625f, 0, 0, 0.0625f, 0, 0, -0.5625f, 0, 0, -0.4375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[158].setRotationPoint(28.75f, -7.0f, 10.0f);

		bodyModel[159] = new ModelRendererTurbo(this, 229, 61, textureX, textureY);
		bodyModel[159].addShapeBox(0, 0, 0, 62, 3, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[159].setRotationPoint(-22.75f, -3.0f, 10.0f);

		bodyModel[160] = new ModelRendererTurbo(this, 215, 57, textureX, textureY);
		bodyModel[160].addShapeBox(0, 0, 0, 62, 2, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f);
		bodyModel[160].setRotationPoint(-22.75f, 0.0f, 9.75f);

		bodyModel[161] = new ModelRendererTurbo(this, 51, 172, textureX, textureY);
		bodyModel[161].addShapeBox(0, 0, 0, 2, 12, 1, 0, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[161].setRotationPoint(-45.125f, -10.0f, -3.375f);

		bodyModel[162] = new ModelRendererTurbo(this, 44, 172, textureX, textureY);
		bodyModel[162].addShapeBox(0, 0, 0, 2, 12, 1, 0, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[162].setRotationPoint(-45.125f, -10.0f, 2.375f);

		bodyModel[163] = new ModelRendererTurbo(this, 255, 162, textureX, textureY);
		bodyModel[163].addShapeBox(0, 0, 0, 2, 1, 4, 0, 0, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f);
		bodyModel[163].setRotationPoint(-45.125f, -11.6875f, -2.0f);

		bodyModel[164] = new ModelRendererTurbo(this, 419, 118, textureX, textureY);
		bodyModel[164].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f);
		bodyModel[164].setRotationPoint(-44.875f, -7.0f, -11.0f);

		bodyModel[165] = new ModelRendererTurbo(this, 359, 115, textureX, textureY);
		bodyModel[165].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.875f, -0.375f, 0, -0.875f, -0.375f, 0, 0, 0, 0, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f);
		bodyModel[165].setRotationPoint(-44.875f, -9.0f, -10.625f);

		bodyModel[166] = new ModelRendererTurbo(this, 430, 114, textureX, textureY);
		bodyModel[166].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f);
		bodyModel[166].setRotationPoint(-44.875f, -5.0f, -11.0f);

		bodyModel[167] = new ModelRendererTurbo(this, 419, 113, textureX, textureY);
		bodyModel[167].addShapeBox(0, 0, 0, 2, 3, 1, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, 0, 0, -0.4375f);
		bodyModel[167].setRotationPoint(-44.875f, -3.0f, -11.0f);

		bodyModel[168] = new ModelRendererTurbo(this, 359, 111, textureX, textureY);
		bodyModel[168].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, -0.25f, -0.5625f, -0.375f, -0.25f, -0.5625f, -0.375f, -0.25f, -0.3125f, 0, -0.25f, -0.3125f);
		bodyModel[168].setRotationPoint(-44.875f, 0.0f, -10.75f);

		bodyModel[169] = new ModelRendererTurbo(this, 92, 102, textureX, textureY);
		bodyModel[169].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -1.25f, -0.375f, 0, -1.25f, -0.375f, 0, 0.375f, 0, 0, 0.375f, 0, -0.25f, -0.1875f, -0.375f, -0.25f, -0.1875f, -0.375f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[169].setRotationPoint(-44.875f, -10.75f, -9.9375f);

		bodyModel[170] = new ModelRendererTurbo(this, 82, 102, textureX, textureY);
		bodyModel[170].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.3125f, -0.375f, 0, -0.3125f, -0.375f, 0, -0.5625f, 0, 0, -0.5625f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f);
		bodyModel[170].setRotationPoint(-44.875f, -7.0f, 10.0f);

		bodyModel[171] = new ModelRendererTurbo(this, 233, 101, textureX, textureY);
		bodyModel[171].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f);
		bodyModel[171].setRotationPoint(-44.875f, -9.0f, 9.625f);

		bodyModel[172] = new ModelRendererTurbo(this, 0, 97, textureX, textureY);
		bodyModel[172].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f);
		bodyModel[172].setRotationPoint(-44.875f, -5.0f, 10.0f);

		bodyModel[173] = new ModelRendererTurbo(this, 323, 98, textureX, textureY);
		bodyModel[173].addShapeBox(0, 0, 0, 2, 3, 1, 0, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, 0, 0, -0.4375f);
		bodyModel[173].setRotationPoint(-44.875f, -3.0f, 10.0f);

		bodyModel[174] = new ModelRendererTurbo(this, 92, 96, textureX, textureY);
		bodyModel[174].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, -0.25f, -0.3125f, -0.375f, -0.25f, -0.3125f, -0.375f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[174].setRotationPoint(-44.875f, 0.0f, 9.75f);

		bodyModel[175] = new ModelRendererTurbo(this, 233, 93, textureX, textureY);
		bodyModel[175].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.375f, -0.375f, 0, 0.375f, -0.375f, 0, -1.25f, 0, 0, -1.25f, 0, -0.25f, -0.6875f, -0.375f, -0.25f, -0.6875f, -0.375f, -0.25f, -0.1875f, 0, -0.25f, -0.1875f);
		bodyModel[175].setRotationPoint(-44.875f, -10.75f, 8.9375f);

		bodyModel[176] = new ModelRendererTurbo(this, 149, 162, textureX, textureY);
		bodyModel[176].addShapeBox(0, 0, 0, 2, 3, 4, 0, 0, -0.25f, 0.125f, -0.5f, -0.25f, 0.125f, -0.5f, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0.125f, -0.5f, -0.25f, 0.125f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[176].setRotationPoint(-44.75f, 2.5f, -1.875f);

		bodyModel[177] = new ModelRendererTurbo(this, 248, 166, textureX, textureY);
		bodyModel[177].addShapeBox(0, 0, 0, 1, 1, 4, 0, 0, -0.125f, -0.1875f, -0.75f, -0.125f, -0.1875f, -0.75f, -0.125f, 0, 0, -0.125f, 0, 0, 0, -0.1875f, -0.75f, 0, -0.1875f, -0.75f, 0, 0, 0, 0, 0);
		bodyModel[177].setRotationPoint(-45.0f, 2.4375f, -2.0625f);

		bodyModel[178] = new ModelRendererTurbo(this, 219, 165, textureX, textureY);
		bodyModel[178].addShapeBox(0, 0, 0, 1, 2, 4, 0, 0, -0.125f, 0.125f, -0.75f, -0.125f, 0.125f, -0.75f, -0.125f, 0, 0, -0.125f, 0, 0, -0.0625f, 0.125f, -0.75f, -0.0625f, 0.125f, -0.75f, -0.0625f, 0, 0, -0.0625f, 0);
		bodyModel[178].setRotationPoint(-45.0f, 3.3125f, -1.875f);

		bodyModel[179] = new ModelRendererTurbo(this, 124, 162, textureX, textureY);
		bodyModel[179].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, -0.625f, 0, -0.5f, -0.625f, 0, -0.5f, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[179].setRotationPoint(-43.25f, -10.75f, -2.5f);

		bodyModel[180] = new ModelRendererTurbo(this, 442, 160, textureX, textureY);
		bodyModel[180].addShapeBox(0, 0, 0, 1, 13, 5, 0, -0.125f, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, -0.125f, 0, -0.625f, -0.125f, 0, -0.625f, -0.125f, 0, -0.125f, -0.125f, 0);
		bodyModel[180].setRotationPoint(-43.25f, -10.375f, -2.5f);

		bodyModel[181] = new ModelRendererTurbo(this, 51, 145, textureX, textureY);
		bodyModel[181].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[181].setRotationPoint(33.75f, 0.0f, -10.5f);

		bodyModel[182] = new ModelRendererTurbo(this, 490, 144, textureX, textureY);
		bodyModel[182].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[182].setRotationPoint(33.75f, -7.0f, -10.5f);

		bodyModel[183] = new ModelRendererTurbo(this, 113, 144, textureX, textureY);
		bodyModel[183].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[183].setRotationPoint(33.75f, -5.0f, -10.5f);

		bodyModel[184] = new ModelRendererTurbo(this, 94, 144, textureX, textureY);
		bodyModel[184].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[184].setRotationPoint(33.75f, -3.0f, -10.5f);

		bodyModel[185] = new ModelRendererTurbo(this, 75, 144, textureX, textureY);
		bodyModel[185].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[185].setRotationPoint(33.75f, 0.0f, 2.5f);

		bodyModel[186] = new ModelRendererTurbo(this, 471, 143, textureX, textureY);
		bodyModel[186].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[186].setRotationPoint(33.75f, -7.0f, 2.5f);

		bodyModel[187] = new ModelRendererTurbo(this, 440, 143, textureX, textureY);
		bodyModel[187].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.4375f, 0, 0, -1.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[187].setRotationPoint(33.75f, -9.0f, 2.5f);

		bodyModel[188] = new ModelRendererTurbo(this, 412, 143, textureX, textureY);
		bodyModel[188].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[188].setRotationPoint(33.75f, -5.0f, 2.5f);

		bodyModel[189] = new ModelRendererTurbo(this, 393, 143, textureX, textureY);
		bodyModel[189].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[189].setRotationPoint(33.75f, -3.0f, 2.5f);

		bodyModel[190] = new ModelRendererTurbo(this, 264, 157, textureX, textureY);
		bodyModel[190].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.75f, 0, 0, -1.625f, 0, -0.125f, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, -0.4375f, 0, -0.125f, -0.375f);
		bodyModel[190].setRotationPoint(33.75f, -9.875f, 2.5f);

		bodyModel[191] = new ModelRendererTurbo(this, 374, 143, textureX, textureY);
		bodyModel[191].addShapeBox(0, 0, 0, 1, 1, 8, 0, 0, -0.3125f, -1.6875f, -0.5f, -0.375f, -1.5625f, -0.5f, -0.375f, 0, 0, -0.3125f, 0, 0, 0.1875f, 0, -0.5f, 0.1875f, 0, -0.5f, 0.1875f, 1.75f, 0, 0.1875f, 1.75f);
		bodyModel[191].setRotationPoint(33.75f, -11.9375f, -4.875f);

		bodyModel[192] = new ModelRendererTurbo(this, 136, 155, textureX, textureY);
		bodyModel[192].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[192].setRotationPoint(33.75f, 1.75f, -9.5f);

		bodyModel[193] = new ModelRendererTurbo(this, 112, 155, textureX, textureY);
		bodyModel[193].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[193].setRotationPoint(33.75f, 1.75f, 2.875f);

		bodyModel[194] = new ModelRendererTurbo(this, 337, 160, textureX, textureY);
		bodyModel[194].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, -0.625f, 0, -0.5f, -0.625f, 0, -0.5f, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[194].setRotationPoint(33.75f, -10.75f, -2.5f);

		bodyModel[195] = new ModelRendererTurbo(this, 95, 160, textureX, textureY);
		bodyModel[195].addShapeBox(0, 0, 0, 1, 13, 5, 0, -0.125f, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, -0.125f, 0, -0.625f, -0.125f, 0, -0.625f, -0.125f, 0, -0.125f, -0.125f, 0);
		bodyModel[195].setRotationPoint(33.75f, -10.375f, -2.5f);

		bodyModel[196] = new ModelRendererTurbo(this, 0, 160, textureX, textureY);
		bodyModel[196].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.75f, 0, 0, -1.625f, 0, -0.125f, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, -0.125f, 0.0625f);
		bodyModel[196].setRotationPoint(33.75f, -10.75f, 2.5f);

		bodyModel[197] = new ModelRendererTurbo(this, 226, 143, textureX, textureY);
		bodyModel[197].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -1.375f, -0.5f, 0, -1.4375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[197].setRotationPoint(33.75f, -9.0f, -10.5f);

		bodyModel[198] = new ModelRendererTurbo(this, 196, 154, textureX, textureY);
		bodyModel[198].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, -1.625f, -0.5f, 0, -1.75f, -0.5f, 0, 0, 0, 0, 0, 0, -0.125f, -0.375f, -0.5f, -0.125f, -0.4375f, -0.5f, -0.125f, 0, 0, -0.125f, 0);
		bodyModel[198].setRotationPoint(33.75f, -9.875f, -9.5f);

		bodyModel[199] = new ModelRendererTurbo(this, 291, 157, textureX, textureY);
		bodyModel[199].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, -1.625f, -0.5f, 0, -1.75f, -0.5f, 0, 0, 0, 0, 0, 0, -0.125f, 0.0625f, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, -0.125f, 0);
		bodyModel[199].setRotationPoint(33.75f, -10.75f, -7.5f);

		bodyModel[200] = new ModelRendererTurbo(this, 170, 154, textureX, textureY);
		bodyModel[200].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, -0.125f, 0, -0.875f, -0.125f, 0, -0.875f, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, -0.875f, 0, 0, -0.875f, 0, 0, 0, 0, 0);
		bodyModel[200].setRotationPoint(33.625f, -11.375f, -3.5f);

		bodyModel[201] = new ModelRendererTurbo(this, 0, 86, textureX, textureY);
		bodyModel[201].addShapeBox(0, 0, 0, 45, 1, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0.5f, -0.5f, 0, 0.5f, -0.5f, -0.5f, 0, -0.5f, -0.5f);
		bodyModel[201].setRotationPoint(-42.75f, -13.0f, -4.5f);

		bodyModel[202] = new ModelRendererTurbo(this, 318, 84, textureX, textureY);
		bodyModel[202].addShapeBox(0, 0, 0, 45, 1, 1, 0, 0, 0, -0.5f, 0.5f, 0, -0.5f, 0.5f, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[202].setRotationPoint(-42.75f, -13.0f, 3.5f);

		bodyModel[203] = new ModelRendererTurbo(this, 358, 78, textureX, textureY);
		bodyModel[203].addShapeBox(0, 0, 0, 45, 1, 4, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0.75f, -1, 0, 0.75f, -1, 0, -0.5f, 0, 0.5f, -0.5f, 0, 0.5f, -1.5f, -0.5f, 0, -1.5f, -0.5f);
		bodyModel[203].setRotationPoint(-18.75f, -9.375f, -9.5f);

		bodyModel[204] = new ModelRendererTurbo(this, 199, 74, textureX, textureY);
		bodyModel[204].addShapeBox(0, 0, 0, 45, 1, 4, 0, 0, 0.75f, -1, 0.5f, 0.75f, -1, 0.5f, 0, 0, 0, 0, 0, 0, -1.5f, -0.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[204].setRotationPoint(-18.75f, -9.375f, 5.5f);

		bodyModel[205] = new ModelRendererTurbo(this, 290, 142, textureX, textureY);
		bodyModel[205].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[205].setRotationPoint(-19.25f, 0.0f, -10.5f);

		bodyModel[206] = new ModelRendererTurbo(this, 334, 140, textureX, textureY);
		bodyModel[206].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[206].setRotationPoint(-19.25f, -7.0f, -10.5f);

		bodyModel[207] = new ModelRendererTurbo(this, 173, 140, textureX, textureY);
		bodyModel[207].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -1.0625f, -0.5f, 0, -1.0625f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[207].setRotationPoint(-19.25f, -9.0f, -10.5f);

		bodyModel[208] = new ModelRendererTurbo(this, 429, 139, textureX, textureY);
		bodyModel[208].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[208].setRotationPoint(-19.25f, -5.0f, -10.5f);

		bodyModel[209] = new ModelRendererTurbo(this, 44, 130, textureX, textureY);
		bodyModel[209].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[209].setRotationPoint(-19.25f, -3.0f, -10.5f);

		bodyModel[210] = new ModelRendererTurbo(this, 12, 153, textureX, textureY);
		bodyModel[210].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, -1.125f, -0.5f, 0, -1.125f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.0625f, -0.5f, -0.25f, -0.0625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[210].setRotationPoint(-19.25f, -10.75f, -9.5f);

		bodyModel[211] = new ModelRendererTurbo(this, 151, 121, textureX, textureY);
		bodyModel[211].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[211].setRotationPoint(-19.25f, 0.0f, 2.5f);

		bodyModel[212] = new ModelRendererTurbo(this, 419, 114, textureX, textureY);
		bodyModel[212].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[212].setRotationPoint(-19.25f, -7.0f, 2.5f);

		bodyModel[213] = new ModelRendererTurbo(this, 0, 93, textureX, textureY);
		bodyModel[213].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[213].setRotationPoint(-19.25f, -9.0f, 2.5f);

		bodyModel[214] = new ModelRendererTurbo(this, 456, 39, textureX, textureY);
		bodyModel[214].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[214].setRotationPoint(-19.25f, -5.0f, 2.5f);

		bodyModel[215] = new ModelRendererTurbo(this, 493, 32, textureX, textureY);
		bodyModel[215].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[215].setRotationPoint(-19.25f, -3.0f, 2.5f);

		bodyModel[216] = new ModelRendererTurbo(this, 147, 152, textureX, textureY);
		bodyModel[216].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.125f, 0, 0, -1.125f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f);
		bodyModel[216].setRotationPoint(-19.25f, -10.75f, 2.5f);

		bodyModel[217] = new ModelRendererTurbo(this, 26, 130, textureX, textureY);
		bodyModel[217].addShapeBox(0, 0, 0, 1, 1, 15, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0.1875f, 0, -0.5f, 0.1875f, 0, -0.5f, 0.1875f, 1.75f, 0, 0.1875f, 1.75f);
		bodyModel[217].setRotationPoint(-19.25f, -11.9375f, -8.375f);

		bodyModel[218] = new ModelRendererTurbo(this, 314, 98, textureX, textureY);
		bodyModel[218].addShapeBox(0, 0, 0, 1, 1, 6, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0.4375f, 3.375f, -0.5f, 0.4375f, 3.375f, -0.5f, 0.4375f, 4.625f, 0, 0.4375f, 4.625f);
		bodyModel[218].setRotationPoint(-19.25f, -13.375f, -3.625f);

		bodyModel[219] = new ModelRendererTurbo(this, 274, 157, textureX, textureY);
		bodyModel[219].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, -2.5f, -0.5f, 0, -2.5f, -0.5f, 0, -2.5f, 0, 0, -2.5f, 0, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, 0, -0.625f, -0.25f);
		bodyModel[219].setRotationPoint(-19.25f, -13.75f, -2.5f);

		bodyModel[220] = new ModelRendererTurbo(this, 82, 102, textureX, textureY);
		bodyModel[220].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[220].setRotationPoint(-19.25f, 1.75f, -9.5f);

		bodyModel[221] = new ModelRendererTurbo(this, 82, 93, textureX, textureY);
		bodyModel[221].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[221].setRotationPoint(-19.25f, 1.75f, 2.875f);

		bodyModel[222] = new ModelRendererTurbo(this, 17, 144, textureX, textureY);
		bodyModel[222].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, -0.625f, 0, -0.5f, -0.625f, 0, -0.5f, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[222].setRotationPoint(-19.25f, -10.75f, -2.5f);

		bodyModel[223] = new ModelRendererTurbo(this, 467, 158, textureX, textureY);
		bodyModel[223].addShapeBox(0, 0, 0, 1, 13, 5, 0, -0.125f, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, -0.125f, 0, -0.625f, -0.125f, 0, -0.625f, -0.125f, 0, -0.125f, -0.125f, 0);
		bodyModel[223].setRotationPoint(-19.25f, -10.375f, -2.5f);

		bodyModel[224] = new ModelRendererTurbo(this, 161, 96, textureX, textureY);
		bodyModel[224].addShapeBox(0, 0, 0, 1, 2, 4, 0, 0, 0.75f, -1, -0.5f, 0.75f, -1, -0.5f, 0, -1.125f, 0, 0, -1.125f, 0, -1.5f, -0.5f, -0.5f, -1.5f, -0.5f, -0.5f, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[224].setRotationPoint(26.75f, -10.375f, 5.5f);

		bodyModel[225] = new ModelRendererTurbo(this, 283, 93, textureX, textureY);
		bodyModel[225].addShapeBox(0, 0, 0, 1, 2, 4, 0, 0, 0, -1.125f, -0.5f, 0, -1.125f, -0.5f, 0.75f, -1, 0, 0.75f, -1, 0, -0.5f, 0, -0.5f, -0.5f, 0, -0.5f, -1.5f, -0.5f, 0, -1.5f, -0.5f);
		bodyModel[225].setRotationPoint(26.75f, -10.375f, -9.5f);

		bodyModel[226] = new ModelRendererTurbo(this, 398, 100, textureX, textureY);
		bodyModel[226].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[226].setRotationPoint(-31.5f, 4.0f, -6.01f);

		bodyModel[227] = new ModelRendererTurbo(this, 398, 93, textureX, textureY);
		bodyModel[227].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[227].setRotationPoint(-31.5f, 4.0f, 6.01f);

		bodyModel[228] = new ModelRendererTurbo(this, 161, 93, textureX, textureY);
		bodyModel[228].addShapeBox(0, 0, 0, 21, 2, 12, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0);
		bodyModel[228].setRotationPoint(-30.5f, 5.5625f, -6.0f);

		bodyModel[229] = new ModelRendererTurbo(this, 0, 56, textureX, textureY);
		bodyModel[229].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[229].setRotationPoint(-14.875f, 4.0f, -6.01f);

		bodyModel[230] = new ModelRendererTurbo(this, 0, 49, textureX, textureY);
		bodyModel[230].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[230].setRotationPoint(-14.875f, 4.0f, 6.01f);

		bodyModel[231] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[231].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[231].setRotationPoint(23.5f, 0.0f, 5.875f);

		bodyModel[232] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[232].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[232].setRotationPoint(22.5f, -1.0f, 4.875f);

		bodyModel[233] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[233].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[233].setRotationPoint(22.5f, -1.5f, 4.875f);

		bodyModel[234] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[234].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[234].setRotationPoint(22.5f, -1.5f, 7.875f);

		bodyModel[235] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[235].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[235].setRotationPoint(22.5f, -4.5f, 4.875f);

		bodyModel[236] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[236].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[236].setRotationPoint(23.0f, -4.5f, 4.875f);

		bodyModel[237] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[237].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[237].setRotationPoint(23.0f, -4.5f, 7.875f);

		bodyModel[238] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[238].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[238].setRotationPoint(22.25f, -2.25f, 3.875f);

		bodyModel[239] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[239].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[239].setRotationPoint(22.25f, -2.375f, 3.875f);

		bodyModel[240] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[240].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[240].setRotationPoint(22.25f, -2.375f, 3.875f);

		bodyModel[241] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[241].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[241].setRotationPoint(22.25f, -2.25f, 9.25f);

		bodyModel[242] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[242].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[242].setRotationPoint(22.25f, -2.375f, 9.25f);

		bodyModel[243] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[243].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[243].setRotationPoint(22.25f, -2.375f, 9.25f);

		bodyModel[244] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[244].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[244].setRotationPoint(17.5f, 0.0f, 5.875f);

		bodyModel[245] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[245].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[245].setRotationPoint(16.5f, -1.0f, 4.875f);

		bodyModel[246] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[246].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[246].setRotationPoint(16.5f, -1.5f, 4.875f);

		bodyModel[247] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[247].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[247].setRotationPoint(16.5f, -1.5f, 7.875f);

		bodyModel[248] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[248].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[248].setRotationPoint(16.5f, -4.5f, 4.875f);

		bodyModel[249] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[249].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[249].setRotationPoint(17.0f, -4.5f, 4.875f);

		bodyModel[250] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[250].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[250].setRotationPoint(17.0f, -4.5f, 7.875f);

		bodyModel[251] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[251].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[251].setRotationPoint(16.25f, -2.25f, 3.875f);

		bodyModel[252] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[252].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[252].setRotationPoint(16.25f, -2.375f, 3.875f);

		bodyModel[253] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[253].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[253].setRotationPoint(16.25f, -2.375f, 3.875f);

		bodyModel[254] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[254].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[254].setRotationPoint(16.25f, -2.25f, 9.25f);

		bodyModel[255] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[255].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[255].setRotationPoint(16.25f, -2.375f, 9.25f);

		bodyModel[256] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[256].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[256].setRotationPoint(16.25f, -2.375f, 9.25f);

		bodyModel[257] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[257].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[257].setRotationPoint(11.5f, 0.0f, 5.875f);

		bodyModel[258] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[258].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[258].setRotationPoint(10.5f, -1.0f, 4.875f);

		bodyModel[259] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[259].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[259].setRotationPoint(10.5f, -1.5f, 4.875f);

		bodyModel[260] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[260].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[260].setRotationPoint(10.5f, -1.5f, 7.875f);

		bodyModel[261] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[261].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[261].setRotationPoint(10.5f, -4.5f, 4.875f);

		bodyModel[262] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[262].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[262].setRotationPoint(11.0f, -4.5f, 4.875f);

		bodyModel[263] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[263].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[263].setRotationPoint(11.0f, -4.5f, 7.875f);

		bodyModel[264] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[264].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[264].setRotationPoint(10.25f, -2.25f, 3.875f);

		bodyModel[265] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[265].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[265].setRotationPoint(10.25f, -2.375f, 3.875f);

		bodyModel[266] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[266].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[266].setRotationPoint(10.25f, -2.375f, 3.875f);

		bodyModel[267] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[267].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[267].setRotationPoint(10.25f, -2.25f, 9.25f);

		bodyModel[268] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[268].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[268].setRotationPoint(10.25f, -2.375f, 9.25f);

		bodyModel[269] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[269].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[269].setRotationPoint(10.25f, -2.375f, 9.25f);

		bodyModel[270] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[270].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[270].setRotationPoint(5.5f, 0.0f, 5.875f);

		bodyModel[271] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[271].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[271].setRotationPoint(4.5f, -1.0f, 4.875f);

		bodyModel[272] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[272].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[272].setRotationPoint(4.5f, -1.5f, 4.875f);

		bodyModel[273] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[273].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[273].setRotationPoint(4.5f, -1.5f, 7.875f);

		bodyModel[274] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[274].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[274].setRotationPoint(4.5f, -4.5f, 4.875f);

		bodyModel[275] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[275].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[275].setRotationPoint(5.0f, -4.5f, 4.875f);

		bodyModel[276] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[276].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[276].setRotationPoint(5.0f, -4.5f, 7.875f);

		bodyModel[277] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[277].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[277].setRotationPoint(4.25f, -2.25f, 3.875f);

		bodyModel[278] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[278].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[278].setRotationPoint(4.25f, -2.375f, 3.875f);

		bodyModel[279] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[279].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[279].setRotationPoint(4.25f, -2.375f, 3.875f);

		bodyModel[280] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[280].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[280].setRotationPoint(4.25f, -2.25f, 9.25f);

		bodyModel[281] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[281].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[281].setRotationPoint(4.25f, -2.375f, 9.25f);

		bodyModel[282] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[282].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[282].setRotationPoint(4.25f, -2.375f, 9.25f);

		bodyModel[283] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[283].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[283].setRotationPoint(-0.5f, 0.0f, 5.875f);

		bodyModel[284] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[284].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[284].setRotationPoint(-1.5f, -1.0f, 4.875f);

		bodyModel[285] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[285].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[285].setRotationPoint(-1.5f, -1.5f, 4.875f);

		bodyModel[286] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[286].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[286].setRotationPoint(-1.5f, -1.5f, 7.875f);

		bodyModel[287] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[287].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[287].setRotationPoint(-1.5f, -4.5f, 4.875f);

		bodyModel[288] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[288].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[288].setRotationPoint(-1.0f, -4.5f, 4.875f);

		bodyModel[289] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[289].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[289].setRotationPoint(-1.0f, -4.5f, 7.875f);

		bodyModel[290] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[290].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[290].setRotationPoint(-1.75f, -2.25f, 3.875f);

		bodyModel[291] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[291].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[291].setRotationPoint(-1.75f, -2.375f, 3.875f);

		bodyModel[292] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[292].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[292].setRotationPoint(-1.75f, -2.375f, 3.875f);

		bodyModel[293] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[293].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[293].setRotationPoint(-1.75f, -2.25f, 9.25f);

		bodyModel[294] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[294].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[294].setRotationPoint(-1.75f, -2.375f, 9.25f);

		bodyModel[295] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[295].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[295].setRotationPoint(-1.75f, -2.375f, 9.25f);

		bodyModel[296] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[296].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[296].setRotationPoint(-7.0f, 0.0f, 5.875f);

		bodyModel[297] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[297].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[297].setRotationPoint(-8.0f, -1.0f, 4.875f);

		bodyModel[298] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[298].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[298].setRotationPoint(-8.0f, -1.5f, 4.875f);

		bodyModel[299] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[299].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[299].setRotationPoint(-8.0f, -1.5f, 7.875f);

		bodyModel[300] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[300].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[300].setRotationPoint(-8.0f, -4.5f, 4.875f);

		bodyModel[301] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[301].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[301].setRotationPoint(-7.5f, -4.5f, 4.875f);

		bodyModel[302] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[302].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[302].setRotationPoint(-7.5f, -4.5f, 7.875f);

		bodyModel[303] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[303].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[303].setRotationPoint(-8.25f, -2.25f, 3.875f);

		bodyModel[304] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[304].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[304].setRotationPoint(-8.25f, -2.375f, 3.875f);

		bodyModel[305] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[305].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[305].setRotationPoint(-8.25f, -2.375f, 3.875f);

		bodyModel[306] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[306].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[306].setRotationPoint(-8.25f, -2.25f, 9.25f);

		bodyModel[307] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[307].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[307].setRotationPoint(-8.25f, -2.375f, 9.25f);

		bodyModel[308] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[308].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[308].setRotationPoint(-8.25f, -2.375f, 9.25f);

		bodyModel[309] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[309].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[309].setRotationPoint(-13.5f, 0.0f, 5.875f);

		bodyModel[310] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[310].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[310].setRotationPoint(-14.5f, -1.0f, 4.875f);

		bodyModel[311] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[311].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[311].setRotationPoint(-14.5f, -1.5f, 4.875f);

		bodyModel[312] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[312].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[312].setRotationPoint(-14.5f, -1.5f, 7.875f);

		bodyModel[313] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[313].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[313].setRotationPoint(-14.5f, -4.5f, 4.875f);

		bodyModel[314] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[314].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[314].setRotationPoint(-14.0f, -4.5f, 4.875f);

		bodyModel[315] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[315].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[315].setRotationPoint(-14.0f, -4.5f, 7.875f);

		bodyModel[316] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[316].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[316].setRotationPoint(-14.75f, -2.25f, 3.875f);

		bodyModel[317] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[317].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[317].setRotationPoint(-14.75f, -2.375f, 3.875f);

		bodyModel[318] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[318].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[318].setRotationPoint(-14.75f, -2.375f, 3.875f);

		bodyModel[319] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[319].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, 0.25f, -0.25f, -0.625f, 0.25f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[319].setRotationPoint(-14.75f, -2.25f, 9.25f);

		bodyModel[320] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[320].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, -0.625f, -0.625f, 0, -0.625f, -0.625f, 0, 0, -2, 0, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[320].setRotationPoint(-14.75f, -2.375f, 9.25f);

		bodyModel[321] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[321].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, -0.625f, 0.25f, -0.875f, -0.625f, 0.25f, -0.875f, 0, 0, -0.875f, 0, -0.625f, 0, -0.625f, -1.875f, 0, -0.625f, -1.875f, 0, 0, -0.625f, 0, 0);
		bodyModel[321].setRotationPoint(-14.75f, -2.375f, 9.25f);

		bodyModel[322] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[322].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[322].setRotationPoint(23.5f, 0.0f, -8.875f);

		bodyModel[323] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[323].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[323].setRotationPoint(22.5f, -1.0f, -9.875f);

		bodyModel[324] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[324].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[324].setRotationPoint(22.5f, -1.5f, -6.875f);

		bodyModel[325] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[325].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[325].setRotationPoint(22.5f, -1.5f, -9.875f);

		bodyModel[326] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[326].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[326].setRotationPoint(22.5f, -4.5f, -9.875f);

		bodyModel[327] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[327].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[327].setRotationPoint(23.0f, -4.5f, -6.875f);

		bodyModel[328] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[328].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[328].setRotationPoint(23.0f, -4.5f, -9.875f);

		bodyModel[329] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[329].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[329].setRotationPoint(22.25f, -2.25f, -4.875f);

		bodyModel[330] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[330].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[330].setRotationPoint(22.25f, -2.375f, -4.875f);

		bodyModel[331] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[331].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[331].setRotationPoint(22.25f, -2.375f, -4.875f);

		bodyModel[332] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[332].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[332].setRotationPoint(22.25f, -2.25f, -10.25f);

		bodyModel[333] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[333].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[333].setRotationPoint(22.25f, -2.375f, -10.25f);

		bodyModel[334] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[334].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[334].setRotationPoint(22.25f, -2.375f, -10.25f);

		bodyModel[335] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[335].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[335].setRotationPoint(17.5f, 0.0f, -8.875f);

		bodyModel[336] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[336].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[336].setRotationPoint(16.5f, -1.0f, -9.875f);

		bodyModel[337] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[337].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[337].setRotationPoint(16.5f, -1.5f, -6.875f);

		bodyModel[338] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[338].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[338].setRotationPoint(16.5f, -1.5f, -9.875f);

		bodyModel[339] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[339].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[339].setRotationPoint(16.5f, -4.5f, -9.875f);

		bodyModel[340] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[340].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[340].setRotationPoint(17.0f, -4.5f, -6.875f);

		bodyModel[341] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[341].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[341].setRotationPoint(17.0f, -4.5f, -9.875f);

		bodyModel[342] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[342].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[342].setRotationPoint(16.25f, -2.25f, -4.875f);

		bodyModel[343] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[343].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[343].setRotationPoint(16.25f, -2.375f, -4.875f);

		bodyModel[344] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[344].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[344].setRotationPoint(16.25f, -2.375f, -4.875f);

		bodyModel[345] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[345].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[345].setRotationPoint(16.25f, -2.25f, -10.25f);

		bodyModel[346] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[346].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[346].setRotationPoint(16.25f, -2.375f, -10.25f);

		bodyModel[347] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[347].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[347].setRotationPoint(16.25f, -2.375f, -10.25f);

		bodyModel[348] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[348].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[348].setRotationPoint(11.5f, 0.0f, -8.875f);

		bodyModel[349] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[349].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[349].setRotationPoint(10.5f, -1.0f, -9.875f);

		bodyModel[350] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[350].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[350].setRotationPoint(10.5f, -1.5f, -6.875f);

		bodyModel[351] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[351].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[351].setRotationPoint(10.5f, -1.5f, -9.875f);

		bodyModel[352] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[352].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[352].setRotationPoint(10.5f, -4.5f, -9.875f);

		bodyModel[353] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[353].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[353].setRotationPoint(11.0f, -4.5f, -6.875f);

		bodyModel[354] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[354].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[354].setRotationPoint(11.0f, -4.5f, -9.875f);

		bodyModel[355] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[355].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[355].setRotationPoint(10.25f, -2.25f, -4.875f);

		bodyModel[356] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[356].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[356].setRotationPoint(10.25f, -2.375f, -4.875f);

		bodyModel[357] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[357].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[357].setRotationPoint(10.25f, -2.375f, -4.875f);

		bodyModel[358] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[358].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[358].setRotationPoint(10.25f, -2.25f, -10.25f);

		bodyModel[359] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[359].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[359].setRotationPoint(10.25f, -2.375f, -10.25f);

		bodyModel[360] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[360].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[360].setRotationPoint(10.25f, -2.375f, -10.25f);

		bodyModel[361] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[361].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[361].setRotationPoint(5.5f, 0.0f, -8.875f);

		bodyModel[362] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[362].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[362].setRotationPoint(4.5f, -1.0f, -9.875f);

		bodyModel[363] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[363].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[363].setRotationPoint(4.5f, -1.5f, -6.875f);

		bodyModel[364] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[364].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[364].setRotationPoint(4.5f, -1.5f, -9.875f);

		bodyModel[365] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[365].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[365].setRotationPoint(4.5f, -4.5f, -9.875f);

		bodyModel[366] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[366].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[366].setRotationPoint(5.0f, -4.5f, -6.875f);

		bodyModel[367] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[367].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[367].setRotationPoint(5.0f, -4.5f, -9.875f);

		bodyModel[368] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[368].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[368].setRotationPoint(4.25f, -2.25f, -4.875f);

		bodyModel[369] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[369].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[369].setRotationPoint(4.25f, -2.375f, -4.875f);

		bodyModel[370] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[370].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[370].setRotationPoint(4.25f, -2.375f, -4.875f);

		bodyModel[371] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[371].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[371].setRotationPoint(4.25f, -2.25f, -10.25f);

		bodyModel[372] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[372].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[372].setRotationPoint(4.25f, -2.375f, -10.25f);

		bodyModel[373] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[373].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[373].setRotationPoint(4.25f, -2.375f, -10.25f);

		bodyModel[374] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[374].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[374].setRotationPoint(-0.5f, 0.0f, -8.875f);

		bodyModel[375] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[375].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[375].setRotationPoint(-1.5f, -1.0f, -9.875f);

		bodyModel[376] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[376].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[376].setRotationPoint(-1.5f, -1.5f, -6.875f);

		bodyModel[377] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[377].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[377].setRotationPoint(-1.5f, -1.5f, -9.875f);

		bodyModel[378] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[378].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[378].setRotationPoint(-1.5f, -4.5f, -9.875f);

		bodyModel[379] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[379].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[379].setRotationPoint(-1.0f, -4.5f, -6.875f);

		bodyModel[380] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[380].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[380].setRotationPoint(-1.0f, -4.5f, -9.875f);

		bodyModel[381] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[381].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[381].setRotationPoint(-1.75f, -2.25f, -4.875f);

		bodyModel[382] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[382].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[382].setRotationPoint(-1.75f, -2.375f, -4.875f);

		bodyModel[383] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[383].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[383].setRotationPoint(-1.75f, -2.375f, -4.875f);

		bodyModel[384] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[384].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[384].setRotationPoint(-1.75f, -2.25f, -10.25f);

		bodyModel[385] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[385].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[385].setRotationPoint(-1.75f, -2.375f, -10.25f);

		bodyModel[386] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[386].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[386].setRotationPoint(-1.75f, -2.375f, -10.25f);

		bodyModel[387] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[387].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[387].setRotationPoint(-7.0f, 0.0f, -8.875f);

		bodyModel[388] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[388].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[388].setRotationPoint(-8.0f, -1.0f, -9.875f);

		bodyModel[389] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[389].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[389].setRotationPoint(-8.0f, -1.5f, -6.875f);

		bodyModel[390] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[390].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[390].setRotationPoint(-8.0f, -1.5f, -9.875f);

		bodyModel[391] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[391].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[391].setRotationPoint(-8.0f, -4.5f, -9.875f);

		bodyModel[392] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[392].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[392].setRotationPoint(-7.5f, -4.5f, -6.875f);

		bodyModel[393] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[393].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[393].setRotationPoint(-7.5f, -4.5f, -9.875f);

		bodyModel[394] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[394].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[394].setRotationPoint(-8.25f, -2.25f, -4.875f);

		bodyModel[395] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[395].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[395].setRotationPoint(-8.25f, -2.375f, -4.875f);

		bodyModel[396] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[396].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[396].setRotationPoint(-8.25f, -2.375f, -4.875f);

		bodyModel[397] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[397].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[397].setRotationPoint(-8.25f, -2.25f, -10.25f);

		bodyModel[398] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[398].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[398].setRotationPoint(-8.25f, -2.375f, -10.25f);

		bodyModel[399] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[399].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[399].setRotationPoint(-8.25f, -2.375f, -10.25f);

		bodyModel[400] = new ModelRendererTurbo(this, 0, 180, textureX, textureY);
		bodyModel[400].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[400].setRotationPoint(-13.5f, 0.0f, -8.875f);

		bodyModel[401] = new ModelRendererTurbo(this, 11, 178, textureX, textureY);
		bodyModel[401].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[401].setRotationPoint(-14.5f, -1.0f, -9.875f);

		bodyModel[402] = new ModelRendererTurbo(this, 58, 177, textureX, textureY);
		bodyModel[402].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[402].setRotationPoint(-14.5f, -1.5f, -6.875f);

		bodyModel[403] = new ModelRendererTurbo(this, 58, 181, textureX, textureY);
		bodyModel[403].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[403].setRotationPoint(-14.5f, -1.5f, -9.875f);

		bodyModel[404] = new ModelRendererTurbo(this, 25, 180, textureX, textureY);
		bodyModel[404].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[404].setRotationPoint(-14.5f, -4.5f, -9.875f);

		bodyModel[405] = new ModelRendererTurbo(this, 71, 177, textureX, textureY);
		bodyModel[405].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[405].setRotationPoint(-14.0f, -4.5f, -6.875f);

		bodyModel[406] = new ModelRendererTurbo(this, 78, 178, textureX, textureY);
		bodyModel[406].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[406].setRotationPoint(-14.0f, -4.5f, -9.875f);

		bodyModel[407] = new ModelRendererTurbo(this, 85, 180, textureX, textureY);
		bodyModel[407].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[407].setRotationPoint(-14.75f, -2.25f, -4.875f);

		bodyModel[408] = new ModelRendererTurbo(this, 94, 179, textureX, textureY);
		bodyModel[408].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[408].setRotationPoint(-14.75f, -2.375f, -4.875f);

		bodyModel[409] = new ModelRendererTurbo(this, 85, 177, textureX, textureY);
		bodyModel[409].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[409].setRotationPoint(-14.75f, -2.375f, -4.875f);

		bodyModel[410] = new ModelRendererTurbo(this, 103, 179, textureX, textureY);
		bodyModel[410].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[410].setRotationPoint(-14.75f, -2.25f, -10.25f);

		bodyModel[411] = new ModelRendererTurbo(this, 103, 182, textureX, textureY);
		bodyModel[411].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[411].setRotationPoint(-14.75f, -2.375f, -10.25f);

		bodyModel[412] = new ModelRendererTurbo(this, 94, 182, textureX, textureY);
		bodyModel[412].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[412].setRotationPoint(-14.75f, -2.375f, -10.25f);

		bodyModel[413] = new ModelRendererTurbo(this, 68, 159, textureX, textureY);
		bodyModel[413].addShapeBox(0, 0, 0, 4, 1, 4, 0, 0, -0.375f, 0.28125f, -0.34375f, -2.03125f, -0.359375f, -0.34375f, -2.03125f, 0.328125f, 0, -0.375f, 0.96875f, 0, -0.125f, 0.28125f, -0.34375f, 1.53125f, -0.359375f, -0.34375f, 1.53125f, 0.328125f, 0, -0.125f, 0.96875f);
		bodyModel[413].setRotationPoint(91.125f, 1.28125f, -2.34375f);

		bodyModel[414] = new ModelRendererTurbo(this, 396, 169, textureX, textureY);
		bodyModel[414].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -0.25f, 0.28125f, -0.34375f, -1.828125f, -0.734375f, -0.34375f, -1.65625f, 1.015625f, 0, 0, 0.375f, 0, -0.25f, 0.28125f, -0.34375f, 1.328125f, -0.734375f, -0.34375f, 1.15625f, 1.015625f, 0, -0.5f, 0.375f);
		bodyModel[414].setRotationPoint(91.125f, 1.65625f, -4.0f);

		bodyModel[415] = new ModelRendererTurbo(this, 47, 169, textureX, textureY);
		bodyModel[415].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.375f, -0.34375f, -1.65625f, 1.015625f, -0.34375f, -1.828125f, -0.734375f, 0, -0.25f, 0.28125f, 0, -0.5f, 0.375f, -0.34375f, 1.15625f, 1.015625f, -0.34375f, 1.328125f, -0.734375f, 0, -0.25f, 0.28125f);
		bodyModel[415].setRotationPoint(91.125f, 1.65625f, 3.0f);

		bodyModel[416] = new ModelRendererTurbo(this, 407, 168, textureX, textureY);
		bodyModel[416].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, -0.34375f, -1.578125f, -1.015625f, -0.34375f, -2.078125f, 0.015625f, 0, -0.5f, -1, 0, -0.28125f, 0.96875f, -0.34375f, 1.34375f, 0.125f, -0.34375f, 1.34375f, -0.625f, 0, -0.28125f, -1.46875f);
		bodyModel[416].setRotationPoint(91.125f, 1.90625f, -4.28125f);

		bodyModel[417] = new ModelRendererTurbo(this, 385, 168, textureX, textureY);
		bodyModel[417].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -0.5f, -1, -0.34375f, -2.078125f, 0.015625f, -0.34375f, -1.578125f, -1.015625f, 0, 0, 0, 0, -0.28125f, -1.46875f, -0.34375f, 1.34375f, -0.625f, -0.34375f, 1.34375f, 0.125f, 0, -0.28125f, 0.96875f);
		bodyModel[417].setRotationPoint(91.125f, 1.90625f, 3.28125f);

		bodyModel[418] = new ModelRendererTurbo(this, 83, 168, textureX, textureY);
		bodyModel[418].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.15625f, -1.125f, -0.34375f, -1.46875f, -1.96875f, -0.34375f, -1.46875f, 1.46875f, 0, 0.15625f, 0.625f, 0, -0.71875f, -0.53125f, -0.34375f, 0.46875f, -1.96875f, -0.34375f, 0.46875f, 1.46875f, 0, -0.71875f, 0.03125f);
		bodyModel[418].setRotationPoint(91.125f, 2.78125f, -6.375f);

		bodyModel[419] = new ModelRendererTurbo(this, 21, 168, textureX, textureY);
		bodyModel[419].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.15625f, 0.625f, -0.34375f, -1.46875f, 1.46875f, -0.34375f, -1.46875f, -1.96875f, 0, 0.15625f, -1.125f, 0, -0.71875f, 0.03125f, -0.34375f, 0.46875f, 1.46875f, -0.34375f, 0.46875f, -1.96875f, 0, -0.71875f, -0.53125f);
		bodyModel[419].setRotationPoint(91.125f, 2.78125f, 5.375f);

		bodyModel[420] = new ModelRendererTurbo(this, 480, 167, textureX, textureY);
		bodyModel[420].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.375f, -0.5625f, -0.34375f, -0.8125f, -2, -0.34375f, -0.8125f, 1.5f, 0, 0.375f, 0.0625f, 0, 0.34375f, -0.625f, -0.34375f, 1.015625f, -2, -0.34375f, 1.015625f, 1.5f, 0, 0.34375f, 0.125f);
		bodyModel[420].setRotationPoint(91.125f, 3.4375f, -6.40625f);

		bodyModel[421] = new ModelRendererTurbo(this, 430, 167, textureX, textureY);
		bodyModel[421].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.375f, 0.0625f, -0.34375f, -0.8125f, 1.5f, -0.34375f, -0.8125f, -2, 0, 0.375f, -0.5625f, 0, 0.34375f, 0.125f, -0.34375f, 1.015625f, 1.5f, -0.34375f, 1.015625f, -2, 0, 0.34375f, -0.625f);
		bodyModel[421].setRotationPoint(91.125f, 3.4375f, 5.40625f);

		bodyModel[422] = new ModelRendererTurbo(this, 471, 115, textureX, textureY);
		bodyModel[422].addShapeBox(0, 0, 0, 6, 1, 13, 0, 0, 0.25f, -0.1875f, 0.5f, -0.9375f, -3.046875f, 0.5f, -0.9375f, -3.296875f, 0, 0.25f, -0.4375f, 0, 0.25f, -0.9375f, 0.5f, 0.3125f, -3.2265625f, 0.5f, 0.3125f, -3.4765625f, 0, 0.25f, -1.0625f);
		bodyModel[422].setRotationPoint(91.125f, 5.03125f, -6.375f);

		bodyModel[423] = new ModelRendererTurbo(this, 159, 124, textureX, textureY);
		bodyModel[423].addShapeBox(0, 0, 0, 6, 1, 11, 0, 0, 0.25f, 0.0625f, 0.5f, 0.1875f, -2.2265625f, 0.5f, 0.1875f, -2.4765625f, 0, 0.25f, -0.0625f, 0, -0.53125f, -0.25f, 0.5f, -0.53125f, -2.5234375f, 0.5f, -0.53125f, -2.7734375f, 0, -0.53125f, -0.40625f);
		bodyModel[423].setRotationPoint(91.125f, 6.53125f, -5.375f);

		bodyModel[424] = new ModelRendererTurbo(this, 184, 135, textureX, textureY);
		bodyModel[424].addShapeBox(0, 0, 0, 6, 1, 10, 0, 0, 0.25f, 0.25f, 0.5f, 0.25f, -2.0234375f, 0.5f, 0.25f, -2.2734375f, 0, 0.25f, 0.09375f, 0, -0.59375f, -0.125f, 0.5f, -0.796875f, -2.7734375f, 0.5f, -0.796875f, -3.0234375f, 0, -0.59375f, -0.28125f);
		bodyModel[424].setRotationPoint(91.125f, 7.25f, -4.875f);

		bodyModel[425] = new ModelRendererTurbo(this, 349, 133, textureX, textureY);
		bodyModel[425].addShapeBox(0, 0, 0, 6, 1, 10, 0, 0, 0.25f, -0.125f, 0.5f, 0.453125f, -2.7734375f, 0.5f, 0.453125f, -3.0234375f, 0, 0.25f, -0.28125f, 0, 0.21875f, -1.25f, 0.5f, -1.453125f, -2.7734375f, 0.5f, -1.453125f, -3.0234375f, 0, 0.21875f, -1.5f);
		bodyModel[425].setRotationPoint(91.125f, 7.90625f, -4.875f);

		bodyModel[426] = new ModelRendererTurbo(this, 365, 158, textureX, textureY);
		bodyModel[426].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, -0.9375f, -0.046875f, 0.3125f, -0.9375f, -0.8046875f, 0.3125f, -0.9375f, -1.0546875f, 0, -0.9375f, -0.296875f, 0, 0.3125f, -0.2265625f, 0.3125f, 0.234375f, -0.8046875f, 0.3125f, 0.234375f, -1.0546875f, 0, 0.3125f, -0.4765625f);
		bodyModel[426].setRotationPoint(97.625f, 5.03125f, -3.375f);

		bodyModel[427] = new ModelRendererTurbo(this, 116, 164, textureX, textureY);
		bodyModel[427].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, -0.9375f, 0.1953125f, -0.375f, -0.9375f, -1.2734375f, -0.375f, -0.9375f, -1.5234375f, 0, -0.9375f, -0.0546875f, 0, 0.234375f, 0.1953125f, -0.375f, 0.234375f, -1.2734375f, -0.375f, 0.234375f, -1.5234375f, 0, 0.234375f, -0.0546875f);
		bodyModel[427].setRotationPoint(98.9375f, 5.03125f, -2.375f);

		bodyModel[428] = new ModelRendererTurbo(this, 234, 158, textureX, textureY);
		bodyModel[428].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, -0.65625f, -0.2265625f, 0.3125f, -0.578125f, -0.8046875f, 0.3125f, -0.578125f, -1.0546875f, 0, -0.65625f, -0.4765625f, 0, 0.3125f, -0.5234375f, 0.3125f, -0.015625f, -1.7109375f, 0.3125f, -0.015625f, -1.9609375f, 0, 0.3125f, -0.7734375f);
		bodyModel[428].setRotationPoint(97.625f, 5.6875f, -3.375f);

		bodyModel[429] = new ModelRendererTurbo(this, 34, 164, textureX, textureY);
		bodyModel[429].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, -0.828125f, 0.1953125f, -0.375f, -0.828125f, -1.2734375f, -0.375f, -0.828125f, -1.5234375f, 0, -0.828125f, -0.0546875f, 0, 0.234375f, -0.7109375f, -0.375f, -0.171875f, -1.2734375f, -0.375f, -0.171875f, -1.5234375f, 0, 0.234375f, -0.9609375f);
		bodyModel[429].setRotationPoint(98.9375f, 5.4375f, -2.375f);

		bodyModel[430] = new ModelRendererTurbo(this, 206, 158, textureX, textureY);
		bodyModel[430].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, -0.65625f, -0.5234375f, 0.3125f, -0.328125f, -1.7109375f, 0.3125f, -0.328125f, -1.9609375f, 0, -0.65625f, -0.7734375f, 0, 0.109375f, -1.2734375f, 0.3125f, -0.671875f, -1.7109375f, 0.3125f, -0.671875f, -1.9609375f, 0, 0.109375f, -1.5234375f);
		bodyModel[430].setRotationPoint(97.625f, 6.34375f, -3.375f);

		bodyModel[431] = new ModelRendererTurbo(this, 192, 163, textureX, textureY);
		bodyModel[431].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0.3125f, 0.5625f, 0.1953125f, -0.375f, -0.171875f, -1.2734375f, -0.375f, -0.171875f, -1.5234375f, 0.3125f, 0.5625f, -0.0546875f, 0, -0.828125f, 0.1953125f, -0.375f, -0.828125f, -1.2734375f, -0.375f, -0.828125f, -1.5234375f, 0, -0.828125f, -0.0546875f);
		bodyModel[431].setRotationPoint(98.9375f, 5.796875f, -2.375f);

		bodyModel[432] = new ModelRendererTurbo(this, 73, 148, textureX, textureY);
		bodyModel[432].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0.375f, -0.5625f, -0.15625f, -0.578125f, -1.640625f, -0.15625f, -0.578125f, 1.140625f, 0, 0.375f, 0.0625f, 0, -0.171875f, -0.5625f, -0.15625f, 0.34375f, -1.640625f, -0.15625f, 0.34375f, 1.140625f, 0, -0.171875f, 0.0625f);
		bodyModel[432].setRotationPoint(94.78125f, 4.625f, -4.96875f);

		bodyModel[433] = new ModelRendererTurbo(this, 256, 137, textureX, textureY);
		bodyModel[433].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0.375f, -0.5625f, 0, 0.34375f, -1.3203125f, 0, 0.34375f, 1.140625f, 0, 0.375f, 0.0625f, 0, -0.609375f, -0.5625f, 0.3125f, -0.609375f, -1.3203125f, 0.3125f, -0.609375f, 1.140625f, 0, -0.609375f, 0.0625f);
		bodyModel[433].setRotationPoint(97.625f, 5.578125f, -3.890625f);

		bodyModel[434] = new ModelRendererTurbo(this, 451, 143, textureX, textureY);
		bodyModel[434].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0.375f, 0.0625f, -0.15625f, -0.578125f, 1.140625f, -0.15625f, -0.578125f, -1.640625f, 0, 0.375f, -0.5625f, 0, -0.171875f, 0.0625f, -0.15625f, 0.34375f, 1.140625f, -0.15625f, 0.34375f, -1.640625f, 0, -0.171875f, -0.5625f);
		bodyModel[434].setRotationPoint(94.78125f, 4.625f, 3.96875f);

		bodyModel[435] = new ModelRendererTurbo(this, 113, 135, textureX, textureY);
		bodyModel[435].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0.375f, 0.0625f, 0, 0.34375f, 1.140625f, 0, 0.34375f, -1.3203125f, 0, 0.375f, -0.5625f, 0, -0.609375f, 0.0625f, 0.3125f, -0.609375f, 1.140625f, 0.3125f, -0.609375f, -1.3203125f, 0, -0.609375f, -0.5625f);
		bodyModel[435].setRotationPoint(97.625f, 5.578125f, 2.890625f);

		bodyModel[436] = new ModelRendererTurbo(this, 57, 163, textureX, textureY);
		bodyModel[436].addShapeBox(0, 0, 0, 1, 1, 5, 0, -0.140625f, 0.5f, -1.2578125f, -0.375f, -0.171875f, -1.2734375f, -0.375f, -0.171875f, -1.5234375f, -0.140625f, 0.5f, -1.5078125f, 0.3125f, -1.5625f, 0.1953125f, -0.375f, -0.828125f, -1.2734375f, -0.375f, -0.828125f, -1.5234375f, 0.3125f, -1.5625f, -0.0546875f);
		bodyModel[436].setRotationPoint(98.9375f, 5.796875f, -2.375f);

		bodyModel[437] = new ModelRendererTurbo(this, 333, 143, textureX, textureY);
		bodyModel[437].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, -0.15625f, -1.21875f, -0.796875f, -0.15625f, -1.71875f, -0.203125f, 0, -0.5f, -1, 0, -0.234375f, 1.140625f, -0.15625f, 0.71875f, 0.0625f, -0.15625f, 0.71875f, -0.5625f, 0, -0.234375f, -1.640625f);
		bodyModel[437].setRotationPoint(94.78125f, 3.484375f, -3.265625f);

		bodyModel[438] = new ModelRendererTurbo(this, 207, 142, textureX, textureY);
		bodyModel[438].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.5f, -1, -0.15625f, -1.71875f, -0.203125f, -0.15625f, -1.21875f, -0.796875f, 0, 0, 0, 0, -0.234375f, -1.640625f, -0.15625f, 0.71875f, -0.5625f, -0.15625f, 0.71875f, 0.0625f, 0, -0.234375f, 1.140625f);
		bodyModel[438].setRotationPoint(94.78125f, 3.484375f, 2.265625f);

		bodyModel[439] = new ModelRendererTurbo(this, 333, 140, textureX, textureY);
		bodyModel[439].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.171875f, -0.09375f, -0.15625f, -1.390625f, -0.890625f, -0.15625f, -1.2890625f, 0.859375f, 0, 0, 0.375f, 0, -0.328125f, -0.09375f, -0.15625f, 0.890625f, -0.890625f, -0.15625f, 0.7890625f, 0.859375f, 0, -0.5f, 0.375f);
		bodyModel[439].setRotationPoint(94.78125f, 3.3125f, -3.359375f);

		bodyModel[440] = new ModelRendererTurbo(this, 184, 140, textureX, textureY);
		bodyModel[440].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0.375f, -0.15625f, -1.2890625f, 0.859375f, -0.15625f, -1.390625f, -0.890625f, 0, -0.171875f, -0.09375f, 0, -0.5f, 0.375f, -0.15625f, 0.7890625f, 0.859375f, -0.15625f, 0.890625f, -0.890625f, 0, -0.328125f, -0.09375f);
		bodyModel[440].setRotationPoint(94.78125f, 3.3125f, 2.359375f);

		bodyModel[441] = new ModelRendererTurbo(this, 295, 118, textureX, textureY);
		bodyModel[441].addShapeBox(0, 0, 0, 3, 1, 4, 0, 0, -0.375f, -0.359375f, -0.15625f, -1.6640625f, -0.84375f, -0.15625f, -1.6640625f, -0.15625f, 0, -0.375f, 0.328125f, 0, -0.125f, -0.359375f, -0.15625f, 1.1640625f, -0.84375f, -0.15625f, 1.1640625f, -0.15625f, 0, -0.125f, 0.328125f);
		bodyModel[441].setRotationPoint(94.78125f, 2.9375f, -2.34375f);

		bodyModel[442] = new ModelRendererTurbo(this, 9, 163, textureX, textureY);
		bodyModel[442].addShapeBox(0, 0, 0, 2, 1, 4, 0, 0, -0.375f, -0.84375f, -0.546875f, -1.0703125f, -1.2265625f, -0.546875f, -1.0703125f, -0.5390625f, 0, -0.375f, -0.15625f, 0, -0.125f, -0.84375f, -0.546875f, 0.0703125f, -1.2265625f, -0.546875f, 0.0703125f, -0.5390625f, 0, -0.125f, -0.15625f);
		bodyModel[442].setRotationPoint(97.625f, 4.2265625f, -2.34375f);

		bodyModel[443] = new ModelRendererTurbo(this, 374, 167, textureX, textureY);
		bodyModel[443].addShapeBox(0, 0, 0, 2, 1, 3, 0, 0, -0.4765625f, -1.875f, -0.9921875f, -1.0078125f, -1.7734375f, -0.546875f, -1.0703125f, 0.2265625f, 0, -0.375f, -0.15625f, 0, -0.0234375f, -1.015625f, -1, 0.0078125f, -1.7734375f, -0.546875f, 0.0703125f, 0.2265625f, 0, -0.0234375f, -0.15625f);
		bodyModel[443].setRotationPoint(97.625f, 4.2265625f, -4.34375f);

		bodyModel[444] = new ModelRendererTurbo(this, 307, 166, textureX, textureY);
		bodyModel[444].addShapeBox(0, 0, 0, 2, 1, 3, 0, 0, -0.375f, -0.15625f, -0.546875f, -1.0703125f, 0.2265625f, -0.9921875f, -1.0078125f, -1.7734375f, 0, -0.4765625f, -1.875f, 0, -0.0234375f, -0.15625f, -0.546875f, 0.0703125f, 0.2265625f, -1, 0.0078125f, -1.7734375f, 0, -0.0234375f, -1.015625f);
		bodyModel[444].setRotationPoint(97.625f, 4.2265625f, 1.34375f);

		bodyModel[445] = new ModelRendererTurbo(this, 212, 107, textureX, textureY);
		bodyModel[445].addShapeBox(0, 0, 0, 8, 2, 16, 0, 0, 0.25f, 0, -0.5f, -1.09375f, -1.6875f, -0.5f, -1.09375f, -1.9375f, 0, 0.25f, -0.25f, 0, -0.125f, -1.0625f, -0.5f, 0.59375f, -2.4375f, -0.5f, 0.59375f, -2.5625f, 0, -0.125f, -1.3125f);
		bodyModel[445].setRotationPoint(83.625f, 3.6875f, -7.875f);

		bodyModel[446] = new ModelRendererTurbo(this, 245, 107, textureX, textureY);
		bodyModel[446].addShapeBox(0, 0, 0, 8, 1, 14, 0, 0, 0.25f, -0.0625f, -0.5f, -0.46875f, -1.4375f, -0.5f, -0.46875f, -1.5625f, 0, 0.25f, -0.3125f, 0, -0.09375f, -0.8125f, -0.5f, 0.1875f, -1.75f, -0.5f, 0.1875f, -1.90625f, 0, -0.09375f, -1.0625f);
		bodyModel[446].setRotationPoint(83.625f, 5.8125f, -6.875f);

		bodyModel[447] = new ModelRendererTurbo(this, 443, 114, textureX, textureY);
		bodyModel[447].addShapeBox(0, 0, 0, 8, 1, 12, 0, 0, 0.25f, 0.1875f, -0.5f, -0.03125f, -0.75f, -0.5f, -0.03125f, -0.90625f, 0, 0.25f, -0.0625f, 0, -0.15625f, -1.0625f, -0.5f, -0.3125f, -1.125f, -0.5f, -0.3125f, -1.28125f, 0, -0.15625f, -1.3125f);
		bodyModel[447].setRotationPoint(83.625f, 6.96875f, -5.875f);

		bodyModel[448] = new ModelRendererTurbo(this, 372, 111, textureX, textureY);
		bodyModel[448].addShapeBox(0, 0, 0, 8, 1, 12, 0, 0, 0.25f, -1.1875f, -0.5f, 0.40625f, -1.25f, -0.5f, 0.40625f, -1.15625f, 0, 0.25f, -1.1875f, 0, 0.0625f, -0.875f, -0.5f, 0.0625f, -2.375f, -0.5f, 0.0625f, -2.375f, 0, 0.0625f, -0.875f);
		bodyModel[448].setRotationPoint(83.625f, 8.0625f, -6.0f);

		bodyModel[449] = new ModelRendererTurbo(this, 372, 132, textureX, textureY);
		bodyModel[449].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0.375f, -0.5625f, -0.5f, -1.0625f, -2.28125f, -0.5f, -1.0625f, 1.78125f, 0, 0.375f, 0.0625f, 0, -0.5625f, -0.5f, -0.5f, 0.78125f, -2.34375f, -0.5f, 0.78125f, 1.84375f, 0, -0.5625f, 0);
		bodyModel[449].setRotationPoint(83.625f, 2.0f, -8.125f);

		bodyModel[450] = new ModelRendererTurbo(this, 394, 125, textureX, textureY);
		bodyModel[450].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, -0.5625f, -0.5f, -1.71875f, -2.25f, -0.5f, -1.71875f, 1.75f, 0, 0.375f, 0.0625f, 0, -0.71875f, -0.53125f, -0.5f, 0.71875f, -2.25f, -0.5f, 0.71875f, 1.75f, 0, -0.71875f, 0.03125f);
		bodyModel[450].setRotationPoint(83.625f, 1.34375f, -8.09375f);

		bodyModel[451] = new ModelRendererTurbo(this, 443, 128, textureX, textureY);
		bodyModel[451].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0.375f, 0.0625f, -0.5f, -1.0625f, 1.78125f, -0.5f, -1.0625f, -2.28125f, 0, 0.375f, -0.5625f, 0, -0.5625f, 0, -0.5f, 0.78125f, 1.84375f, -0.5f, 0.78125f, -2.34375f, 0, -0.5625f, -0.5f);
		bodyModel[451].setRotationPoint(83.625f, 2.0f, 7.125f);

		bodyModel[452] = new ModelRendererTurbo(this, 372, 125, textureX, textureY);
		bodyModel[452].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, 0.0625f, -0.5f, -1.71875f, 1.75f, -0.5f, -1.71875f, -2.25f, 0, 0.375f, -0.5625f, 0, -0.71875f, 0.03125f, -0.5f, 0.71875f, 1.75f, -0.5f, 0.71875f, -2.25f, 0, -0.71875f, -0.53125f);
		bodyModel[452].setRotationPoint(83.625f, 1.34375f, 7.09375f);

		bodyModel[453] = new ModelRendererTurbo(this, 345, 124, textureX, textureY);
		bodyModel[453].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, -0.5625f, -0.5f, -2, -1.96875f, -0.5f, -2, 1.46875f, 0, 0.375f, 0.0625f, 0, -0.65625f, 0.3125f, -0.5f, 1.4375f, -1.375f, -0.5f, 1.4375f, 0.875f, 0, -0.65625f, -0.8125f);
		bodyModel[453].setRotationPoint(83.625f, 0.625f, -7.21875f);

		bodyModel[454] = new ModelRendererTurbo(this, 345, 121, textureX, textureY);
		bodyModel[454].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, 0.0625f, -0.5f, -2, 1.46875f, -0.5f, -2, -1.96875f, 0, 0.375f, -0.5625f, 0, -0.65625f, -0.8125f, -0.5f, 1.4375f, 0.875f, -0.5f, 1.4375f, -1.375f, 0, -0.65625f, 0.3125f);
		bodyModel[454].setRotationPoint(83.625f, 0.625f, 6.21875f);

		bodyModel[455] = new ModelRendererTurbo(this, 0, 111, textureX, textureY);
		bodyModel[455].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0, 0, -0.5f, -2.53125f, -1.25f, -0.5f, -3.03125f, 0.25f, 0, -0.5f, -1, 0, -0.125f, 1.125f, -0.5f, 2.25f, -0.28125f, -0.5f, 2.25f, -0.21875f, 0, -0.125f, -1.625f);
		bodyModel[455].setRotationPoint(83.625f, -0.625f, -5.53125f);

		bodyModel[456] = new ModelRendererTurbo(this, 0, 24, textureX, textureY);
		bodyModel[456].addShapeBox(0, 0, 0, 8, 1, 2, 0, 0, -0.375f, -0.25f, -0.5f, -2.90625f, -1.5f, -0.5f, -2.65625f, 1.15625f, 0, -0.0625f, 0.375f, 0, -0.125f, -0.25f, -0.5f, 2.40625f, -1.5f, -0.5f, 2.15625f, 1.15625f, 0, -0.4375f, 0.375f);
		bodyModel[456].setRotationPoint(83.625f, -1.0f, -5.78125f);

		bodyModel[457] = new ModelRendererTurbo(this, 267, 18, textureX, textureY);
		bodyModel[457].addShapeBox(0, 0, 0, 8, 1, 2, 0, 0, -0.4375f, -0.25f, -0.5f, -3.03125f, -1.03125f, -0.5f, -3.03125f, 0.5625f, 0, -0.4375f, 0.5625f, 0, -0.0625f, -0.25f, -0.5f, 2.53125f, -1.03125f, -0.5f, 2.53125f, 0.5625f, 0, -0.0625f, 0.5625f);
		bodyModel[457].setRotationPoint(83.625f, -1.375f, -3.65625f);

		bodyModel[458] = new ModelRendererTurbo(this, 0, 16, textureX, textureY);
		bodyModel[458].addShapeBox(0, 0, 0, 8, 1, 2, 0, 0, -0.375f, -0.25f, -0.5f, -2.96875f, -0.25f, -0.5f, -2.96875f, 0.4375f, 0, -0.375f, 0.4375f, 0, -0.125f, -0.25f, -0.5f, 2.46875f, -0.25f, -0.5f, 2.46875f, 0.4375f, 0, -0.125f, 0.4375f);
		bodyModel[458].setRotationPoint(83.625f, -1.3125f, -1.34375f);

		bodyModel[459] = new ModelRendererTurbo(this, 276, 107, textureX, textureY);
		bodyModel[459].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.5f, -1, -0.5f, -3.03125f, 0.25f, -0.5f, -2.53125f, -1.25f, 0, 0, 0, 0, -0.125f, -1.625f, -0.5f, 2.25f, -0.21875f, -0.5f, 2.25f, -0.28125f, 0, -0.125f, 1.125f);
		bodyModel[459].setRotationPoint(83.625f, -0.625f, 4.53125f);

		bodyModel[460] = new ModelRendererTurbo(this, 0, 4, textureX, textureY);
		bodyModel[460].addShapeBox(0, 0, 0, 8, 1, 2, 0, 0, -0.0625f, 0.375f, -0.5f, -2.65625f, 1.15625f, -0.5f, -2.90625f, -1.5f, 0, -0.375f, -0.25f, 0, -0.4375f, 0.375f, -0.5f, 2.15625f, 1.15625f, -0.5f, 2.40625f, -1.5f, 0, -0.125f, -0.25f);
		bodyModel[460].setRotationPoint(83.625f, -1.0f, 3.78125f);

		bodyModel[461] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[461].addShapeBox(0, 0, 0, 8, 1, 2, 0, 0, -0.4375f, 0.5625f, -0.5f, -3.03125f, 0.5625f, -0.5f, -3.03125f, -1.03125f, 0, -0.4375f, -0.25f, 0, -0.0625f, 0.5625f, -0.5f, 2.53125f, 0.5625f, -0.5f, 2.53125f, -1.03125f, 0, -0.0625f, -0.25f);
		bodyModel[461].setRotationPoint(83.625f, -1.375f, 1.65625f);

		bodyModel[462] = new ModelRendererTurbo(this, 458, 32, textureX, textureY);
		bodyModel[462].addShapeBox(0, 0, 0, 8, 2, 18, 0, 0, 0.25f, 0.125f, -0.375f, -0.6875f, -1.125f, -0.375f, -0.6875f, -1.125f, 0, 0.25f, 0.125f, 0, 0.4375f, -1.25f, -0.375f, 0.8125f, -2.1875f, -0.375f, 0.8125f, -2.1875f, 0, 0.4375f, -1.25f);
		bodyModel[462].setRotationPoint(76.0f, 2.75f, -9.0f);

		bodyModel[463] = new ModelRendererTurbo(this, 365, 93, textureX, textureY);
		bodyModel[463].addShapeBox(0, 0, 0, 8, 1, 16, 0, 0, 0.25f, -0.25f, -0.375f, -0.125f, -1.1875f, -0.375f, -0.125f, -1.1875f, 0, 0.25f, -0.25f, 0, 0.0625f, -1, -0.375f, 0.28125f, -1.9375f, -0.375f, 0.28125f, -1.9375f, 0, 0.0625f, -1);
		bodyModel[463].setRotationPoint(76.0f, 5.4375f, -8.0f);

		bodyModel[464] = new ModelRendererTurbo(this, 165, 108, textureX, textureY);
		bodyModel[464].addShapeBox(0, 0, 0, 8, 1, 14, 0, 0, 0.25f, 0, -0.375f, 0.03125f, -0.9375f, -0.375f, 0.03125f, -0.9375f, 0, 0.25f, 0, 0, 0.0625f, -1.4375f, -0.375f, 0.0625f, -2.1875f, -0.375f, 0.0625f, -2.1875f, 0, 0.0625f, -1.4375f);
		bodyModel[464].setRotationPoint(76.0f, 6.75f, -7.0f);

		bodyModel[465] = new ModelRendererTurbo(this, 0, 118, textureX, textureY);
		bodyModel[465].addShapeBox(0, 0, 0, 8, 1, 12, 0, 0, 0.25f, -0.4375f, -0.375f, 0.25f, -1.1875f, -0.375f, 0.25f, -1.1875f, 0, 0.25f, -0.4375f, 0, 0.0625f, -0.375f, -0.375f, 0.0625f, -0.875f, -0.375f, 0.0625f, -0.875f, 0, 0.0625f, -0.375f);
		bodyModel[465].setRotationPoint(76.0f, 8.0625f, -6.0f);

		bodyModel[466] = new ModelRendererTurbo(this, 234, 138, textureX, textureY);
		bodyModel[466].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0.375f, -0.5625f, -0.375f, -0.6875f, -1.6875f, -0.375f, -0.6875f, 1.1875f, 0, 0.375f, 0.0625f, 0, -0.5625f, -0.5f, -0.375f, 0.5f, -1.625f, -0.375f, 0.5f, 1.125f, 0, -0.5625f, 0);
		bodyModel[466].setRotationPoint(76.0f, 0.9375f, -9.25f);

		bodyModel[467] = new ModelRendererTurbo(this, 17, 137, textureX, textureY);
		bodyModel[467].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, -0.5625f, -0.375f, -1.375f, -1.59375f, -0.375f, -1.375f, 1.09375f, 0, 0.375f, 0.0625f, 0, -0.03125f, -0.4375f, -0.375f, 1.03125f, -1.5625f, -0.375f, 1.03125f, 1.0625f, 0, -0.03125f, -0.0625f);
		bodyModel[467].setRotationPoint(76.0f, -0.40625f, -9.125f);

		bodyModel[468] = new ModelRendererTurbo(this, 67, 135, textureX, textureY);
		bodyModel[468].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, -0.5625f, -0.375f, -1.375f, -1.71875f, -0.375f, -1.375f, 1.21875f, 0, 0.375f, 0.0625f, 0, -0.65625f, 0.1875f, -0.375f, 1.09375f, -0.84375f, -0.375f, 1.09375f, 0.34375f, 0, -0.65625f, -0.6875f);
		bodyModel[468].setRotationPoint(76.0f, -1.125f, -8.375f);

		bodyModel[469] = new ModelRendererTurbo(this, 314, 132, textureX, textureY);
		bodyModel[469].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.3828125f, -0.5625f, -0.375f, -1.75f, -1.15625f, -0.375f, -2.25f, 0.15625f, 0, -0.1171875f, -0.4375f, 0, -0.125f, 1.125f, -0.375f, 1.625f, -0.03125f, -0.375f, 1.625f, -0.46875f, 0, -0.125f, -1.625f);
		bodyModel[469].setRotationPoint(76.0f, -2.375f, -6.6875f);

		bodyModel[470] = new ModelRendererTurbo(this, 372, 136, textureX, textureY);
		bodyModel[470].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0.375f, 0.0625f, -0.375f, -0.6875f, 1.1875f, -0.375f, -0.6875f, -1.6875f, 0, 0.375f, -0.5625f, 0, -0.5625f, 0, -0.375f, 0.5f, 1.125f, -0.375f, 0.5f, -1.625f, 0, -0.5625f, -0.5f);
		bodyModel[470].setRotationPoint(76.0f, 0.9375f, 8.25f);

		bodyModel[471] = new ModelRendererTurbo(this, 414, 130, textureX, textureY);
		bodyModel[471].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, 0.0625f, -0.375f, -1.375f, 1.09375f, -0.375f, -1.375f, -1.59375f, 0, 0.375f, -0.5625f, 0, -0.03125f, -0.0625f, -0.375f, 1.03125f, 1.0625f, -0.375f, 1.03125f, -1.5625f, 0, -0.03125f, -0.4375f);
		bodyModel[471].setRotationPoint(76.0f, -0.40625f, 8.125f);

		bodyModel[472] = new ModelRendererTurbo(this, 345, 127, textureX, textureY);
		bodyModel[472].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.375f, 0.0625f, -0.375f, -1.375f, 1.21875f, -0.375f, -1.375f, -1.71875f, 0, 0.375f, -0.5625f, 0, -0.65625f, -0.6875f, -0.375f, 1.09375f, 0.34375f, -0.375f, 1.09375f, -0.84375f, 0, -0.65625f, 0.1875f);
		bodyModel[472].setRotationPoint(76.0f, -1.125f, 7.375f);

		bodyModel[473] = new ModelRendererTurbo(this, 419, 125, textureX, textureY);
		bodyModel[473].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.1171875f, -0.4375f, -0.375f, -2.25f, 0.15625f, -0.375f, -1.75f, -1.15625f, 0, 0.3828125f, -0.5625f, 0, -0.125f, -1.625f, -0.375f, 1.625f, -0.46875f, -0.375f, 1.625f, -0.03125f, 0, -0.125f, 1.125f);
		bodyModel[473].setRotationPoint(76.0f, -2.375f, 5.6875f);

		bodyModel[474] = new ModelRendererTurbo(this, 455, 158, textureX, textureY);
		bodyModel[474].addShapeBox(0, 0, 0, 5, 1, 3, 0, 0.375f, -0.21875f, -0.375f, -0.1875f, -2.28125f, 0.40625f, -0.1875f, -2.28125f, -1.09375f, 0, -0.375f, 0, 0.375f, -0.28125f, -0.375f, -0.1875f, 1.78125f, 0.40625f, -0.1875f, 1.78125f, -1.09375f, 0, -0.125f, 0);
		bodyModel[474].setRotationPoint(78.8125f, -3.21875f, -3.0f);

		bodyModel[475] = new ModelRendererTurbo(this, 355, 158, textureX, textureY);
		bodyModel[475].addShapeBox(0, 0, 0, 5, 1, 3, 0, 0, -0.375f, 0, -0.1875f, -2.28125f, -1.09375f, -0.1875f, -2.28125f, 0.40625f, 0.375f, -0.21875f, -0.375f, 0, -0.125f, 0, -0.1875f, 1.78125f, -1.09375f, -0.1875f, 1.78125f, 0.40625f, 0.375f, -0.28125f, -0.375f);
		bodyModel[475].setRotationPoint(78.8125f, -3.21875f, 0.0f);

		bodyModel[476] = new ModelRendererTurbo(this, 131, 118, textureX, textureY);
		bodyModel[476].addShapeBox(0, 0, 0, 5, 1, 2, 0, 0, -0.375f, -2, -0.1875f, -2.28125f, -0.90625f, -0.1875f, -2.28125f, 1.09375f, 0, -0.375f, 0, 0, -0.125f, -2, -0.1875f, 1.78125f, -0.90625f, -0.1875f, 1.78125f, 1.09375f, 0, -0.125f, 0);
		bodyModel[476].setRotationPoint(78.8125f, -3.21875f, -2.0f);

		bodyModel[477] = new ModelRendererTurbo(this, 122, 158, textureX, textureY);
		bodyModel[477].addShapeBox(0, 0, 0, 6, 1, 2, 0, 1.4375f, -0.5859375f, 0.375f, 0.1875f, -2.71875f, -0.21875f, 0.1875f, -2.40625f, 0.34375f, 0, -0.0625f, -0.125f, 1.4375f, 0.0859375f, 0.375f, 0.1875f, 2.21875f, -0.21875f, 0.1875f, 1.90625f, 0.34375f, 0, -0.4375f, -0.125f);
		bodyModel[477].setRotationPoint(77.4375f, -3.34375f, -5.75f);

		bodyModel[478] = new ModelRendererTurbo(this, 67, 131, textureX, textureY);
		bodyModel[478].addShapeBox(0, 0, 0, 6, 1, 2, 0, 0, -0.0625f, -0.125f, 0.1875f, -2.40625f, 0.34375f, 0.1875f, -2.71875f, -0.21875f, 1.4375f, -0.5859375f, 0.375f, 0, -0.4375f, -0.125f, 0.1875f, 1.90625f, 0.34375f, 0.1875f, 2.21875f, -0.21875f, 1.4375f, 0.0859375f, 0.375f);
		bodyModel[478].setRotationPoint(77.4375f, -3.34375f, 3.75f);

		bodyModel[479] = new ModelRendererTurbo(this, 169, 151, textureX, textureY);
		bodyModel[479].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.625f, -0.5f, -0.375f, -1.6875f, -1.625f, -0.375f, -1.6875f, 1.125f, 0, -0.625f, 0, 0, -0.25f, -0.5f, -0.375f, 0.6875f, -1.625f, -0.375f, 0.6875f, 1.125f, 0, -0.25f, 0);
		bodyModel[479].setRotationPoint(76.0f, 1.75f, -9.25f);

		bodyModel[480] = new ModelRendererTurbo(this, 62, 145, textureX, textureY);
		bodyModel[480].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.625f, 0, -0.375f, -1.6875f, 1.125f, -0.375f, -1.6875f, -1.625f, 0, -0.625f, -0.5f, 0, -0.25f, 0, -0.375f, 0.6875f, 1.125f, -0.375f, 0.6875f, -1.625f, 0, -0.25f, -0.5f);
		bodyModel[480].setRotationPoint(76.0f, 1.75f, 8.25f);

		bodyModel[481] = new ModelRendererTurbo(this, 480, 164, textureX, textureY);
		bodyModel[481].addShapeBox(0, 0, 0, 5, 1, 1, 0, 1, -0.09375f, 0.25f, 0.1875f, -2.4375f, -0.21875f, 0.1875f, -2.4375f, -0.78125f, 0, -0.375f, 0, 1, -0.40625f, 0.25f, 0.1875f, 1.9375f, -0.21875f, 0.1875f, 1.9375f, -0.78125f, 0, -0.125f, 0);
		bodyModel[481].setRotationPoint(78.4375f, -3.375f, -3.625f);

		bodyModel[482] = new ModelRendererTurbo(this, 132, 164, textureX, textureY);
		bodyModel[482].addShapeBox(0, 0, 0, 5, 1, 1, 0, 0, -0.375f, 0, 0.1875f, -2.4375f, -0.78125f, 0.1875f, -2.4375f, -0.21875f, 1, -0.09375f, 0.25f, 0, -0.125f, 0, 0.1875f, 1.9375f, -0.78125f, 0.1875f, 1.9375f, -0.21875f, 1, -0.40625f, 0.25f);
		bodyModel[482].setRotationPoint(78.4375f, -3.375f, 2.625f);

		bodyModel[483] = new ModelRendererTurbo(this, 82, 164, textureX, textureY);
		bodyModel[483].addShapeBox(0, 0, 0, 3, 0, 3, 0, 0, 0.75f, -0.375f, 0.15625f, -0.4375f, -0.375f, 0.53125f, -0.59375f, 0, 0, 0.75f, 0, 0, -0.75f, -0.375f, 0.15625f, 0.4375f, -0.375f, 0.53125f, 0.59375f, 0, 0, -0.75f, 0);
		bodyModel[483].setRotationPoint(75.28125f, -3.1875f, -3.0f);

		bodyModel[484] = new ModelRendererTurbo(this, 171, 163, textureX, textureY);
		bodyModel[484].addShapeBox(0, 0, 0, 3, 0, 3, 0, 0, 0.75f, 0, 0.53125f, -0.59375f, 0, 0.15625f, -0.4375f, -0.375f, 0, 0.75f, -0.375f, 0, -0.75f, 0, 0.53125f, 0.59375f, 0, 0.15625f, 0.4375f, -0.375f, 0, -0.75f, -0.375f);
		bodyModel[484].setRotationPoint(75.28125f, -3.1875f, 0.0f);

		bodyModel[485] = new ModelRendererTurbo(this, 274, 34, textureX, textureY);
		bodyModel[485].addShapeBox(0, 0, 0, 3, 0, 1, 0, 0, 0.53125f, 0.25f, -0.84375f, -0.15625f, 0.25f, 0.15625f, -0.4375f, 0, 0, 0.75f, 0, 0, -0.53125f, 0.25f, -0.84375f, 0.15625f, 0.25f, 0.15625f, 0.4375f, 0, 0, -0.75f, 0);
		bodyModel[485].setRotationPoint(75.28125f, -3.1875f, -3.625f);

		bodyModel[486] = new ModelRendererTurbo(this, 266, 34, textureX, textureY);
		bodyModel[486].addShapeBox(0, 0, 0, 3, 0, 1, 0, 0, 0.125f, 0.359375f, -3, 0.125f, 0.359375f, -0.84375f, 0.0625f, 0, 0, 0.75f, 0, 0, -0.125f, 0.359375f, -3, -0.125f, 0.359375f, -0.84375f, -0.0625f, 0, 0, -0.75f, 0);
		bodyModel[486].setRotationPoint(75.28125f, -2.96875f, -4.875f);

		bodyModel[487] = new ModelRendererTurbo(this, 8, 20, textureX, textureY);
		bodyModel[487].addShapeBox(0, 0, 0, 3, 0, 1, 0, 0, 0.75f, 0, 0.15625f, -0.4375f, 0, -0.84375f, -0.15625f, 0.25f, 0, 0.53125f, 0.25f, 0, -0.75f, 0, 0.15625f, 0.4375f, 0, -0.84375f, 0.15625f, 0.25f, 0, -0.53125f, 0.25f);
		bodyModel[487].setRotationPoint(75.28125f, -3.1875f, 2.625f);

		bodyModel[488] = new ModelRendererTurbo(this, 152, 140, textureX, textureY);
		bodyModel[488].addShapeBox(0, 0, 0, 2, 1, 2, 0, -0.03125f, 0, -1.484375f, 0.1875f, -0.0625f, -0.125f, -1.25f, -0.5859375f, 0.375f, -0.75f, -0.5859375f, 0.375f, -0.03125f, -0.5f, -1.484375f, 0.1875f, -0.4375f, -0.125f, -1.25f, 0.0859375f, 0.375f, -0.75f, 0.0859375f, 0.375f);
		bodyModel[488].setRotationPoint(75.25f, -3.34375f, 3.75f);

		bodyModel[489] = new ModelRendererTurbo(this, 458, 18, textureX, textureY);
		bodyModel[489].addShapeBox(0, 0, 0, 18, 5, 8, 0, 0, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0, 0, 0, 0, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0, 0, 0);
		bodyModel[489].setRotationPoint(60.875f, -2.375f, -4.0f);

		bodyModel[490] = new ModelRendererTurbo(this, 116, 135, textureX, textureY);
		bodyModel[490].addShapeBox(0, 0, 0, 13, 5, 3, 0, 0, 0, 0, 0.4375f, 0, 0, 5.4375f, 0, 0, 0, 0, 0, 0, 0, 0, 0.4375f, 0, 0, 5.4375f, 0, 0, 0, 0, 0);
		bodyModel[490].setRotationPoint(60.875f, -2.375f, -7.0f);

		bodyModel[491] = new ModelRendererTurbo(this, 83, 135, textureX, textureY);
		bodyModel[491].addShapeBox(0, 0, 0, 13, 5, 3, 0, 0, 0, 0, 5.4375f, 0, 0, 0.4375f, 0, 0, 0, 0, 0, 0, 0, 0, 5.4375f, 0, 0, 0.4375f, 0, 0, 0, 0, 0);
		bodyModel[491].setRotationPoint(60.875f, -2.375f, 4.0f);

		bodyModel[492] = new ModelRendererTurbo(this, 376, 185, textureX, textureY);
		bodyModel[492].addShapeBox(0, 0, 0, 1, 1, 3, 0, 0, 0.00390625f, 0, 0, -0.39453125f, -0.21875f, -0.375f, -0.2421875f, -0.21875f, 0.375f, 0.16015625f, -0.375f, -0.0625f, -0.44140625f, 0, 0, 0.17578125f, -0.21875f, -0.375f, 0.4765625f, -0.21875f, 0.3125f, -0.59765625f, -0.375f);
		bodyModel[492].setRotationPoint(78.8125f, -2.9335938f, 0.0f);

		bodyModel[493] = new ModelRendererTurbo(this, 324, 186, textureX, textureY);
		bodyModel[493].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0.00390625f, -0.5f, 0, -0.39453125f, -0.28125f, 0, -0.39453125f, -0.28125f, 0, 0.00390625f, -0.5f, 0, -0.00390625f, -0.5f, 0, 0.17578125f, -0.28125f, 0, 0.62890625f, -0.28125f, 0, -0.00390625f, -0.5f);
		bodyModel[493].setRotationPoint(78.8125f, -2.9335938f, -0.5f);

		bodyModel[494] = new ModelRendererTurbo(this, 385, 185, textureX, textureY);
		bodyModel[494].addShapeBox(0, 0, 0, 1, 1, 3, 0, 0.375f, 0.16015625f, -0.375f, -0.375f, -0.2421875f, -0.21875f, 0, -0.39453125f, -0.21875f, 0, 0.00390625f, 0, 0.3125f, -0.59765625f, -0.375f, -0.375f, 0.0234375f, -0.21875f, 0, 0.62890625f, -0.21875f, -0.0625f, -0.44140625f, 0);
		bodyModel[494].setRotationPoint(78.8125f, -2.9335938f, -3.0f);

		bodyModel[495] = new ModelRendererTurbo(this, 329, 186, textureX, textureY);
		bodyModel[495].addShapeBox(0, 0, 0, 1, 1, 1, 0, 1.375f, 0.4375f, 0.25f, -1.25f, -0.01953125f, 0.171875f, -0.375f, -0.2421875f, -0.15625f, 0.375f, 0.16015625f, 0, 1.3125f, -0.9375f, 0.25f, -1.25f, -0.77734375f, 0.171875f, -0.375f, -0.2109375f, -0.15625f, 0.3125f, -0.66015625f, 0);
		bodyModel[495].setRotationPoint(78.8125f, -2.9335938f, -3.625f);

		bodyModel[496] = new ModelRendererTurbo(this, 344, 186, textureX, textureY);
		bodyModel[496].addShapeBox(0, 0, 0, 1, 1, 1, 0, 2.5371094f, 0.2265625f, 0.35546875f, -3.1074219f, -0.3046875f, 1.2558594f, -0.2578125f, -0.29296875f, 0.078125f, 0.3828125f, 0.1640625f, 0, 2.5371094f, -0.6640625f, 0.41796875f, -3.1074219f, -0.1953125f, 1.2558594f, -0.2578125f, -0.34765625f, 0.078125f, 0.3203125f, -0.6015625f, 0);
		bodyModel[496].setRotationPoint(77.82031f, -3.2070312f, -4.875f);

		bodyModel[497] = new ModelRendererTurbo(this, 334, 186, textureX, textureY);
		bodyModel[497].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.375f, 0.16015625f, 0, -0.375f, -0.2421875f, -0.15625f, -1.25f, 0.02734375f, 0.171875f, 1.375f, 0.4375f, 0.25f, 0.3125f, -0.66015625f, 0, -0.375f, -0.2109375f, -0.15625f, -1.25f, -0.77734375f, 0.171875f, 1.3125f, -0.9375f, 0.25f);
		bodyModel[497].setRotationPoint(78.8125f, -2.9335938f, 2.625f);

		bodyModel[498] = new ModelRendererTurbo(this, 339, 186, textureX, textureY);
		bodyModel[498].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.3828125f, 0.1640625f, 0, -0.2578125f, -0.24609375f, 0.078125f, -3.1074219f, -0.3046875f, 1.2558594f, 2.5371094f, 0.2265625f, 0.35546875f, 0.3203125f, -0.6015625f, 0, -0.2578125f, -0.34765625f, 0.078125f, -3.1074219f, -0.1953125f, 1.2558594f, 2.5371094f, -0.6640625f, 0.41796875f);
		bodyModel[498].setRotationPoint(77.82031f, -3.2070312f, 3.875f);

		bodyModel[499] = new ModelRendererTurbo(this, 425, 90, textureX, textureY);
		bodyModel[499].addShapeBox(0, 0, 0, 30, 1, 22, 0, 0, -0.75f, 0, -0.375f, -0.75f, -1.75f, -0.375f, -0.75f, -2, 0, -0.75f, -0.25f, 0, 0, 0, -0.375f, 0, -1.8779f, -0.375f, 0, -2.1279f, 0, 0, -0.25f);
		bodyModel[499].setRotationPoint(46.375f, 1.75f, -10.875f);

	}

	private final void initGroup_bodyModel1(){
		bodyModel[500] = new ModelRendererTurbo(this, 195, 151, textureX, textureY);
		bodyModel[500].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.4375f, -0.5f, -0.25f, -0.5f, -0.9375f, -0.25f, -0.5f, 0.4375f, 0, -0.4375f, 0, 0, -0.25f, -0.5f, -0.25f, -0.25f, -0.9375f, -0.25f, -0.25f, 0.4375f, 0, -0.25f, 0);
		bodyModel[500].setRotationPoint(50.3125f, 1.75f, -10.625f);

		bodyModel[501] = new ModelRendererTurbo(this, 256, 142, textureX, textureY);
		bodyModel[501].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, -0.5f, -0.5f, 0.3125f, -0.5625f, -0.8125f, 0.3125f, -0.5625f, 0.3125f, 0, -0.5f, 0, 0, -0.25f, -0.5f, 0.3125f, -0.25f, -0.8125f, 0.3125f, -0.25f, 0.3125f, 0, -0.25f, 0);
		bodyModel[501].setRotationPoint(58.0625f, 1.75f, -10.1875f);

		bodyModel[502] = new ModelRendererTurbo(this, 256, 130, textureX, textureY);
		bodyModel[502].addShapeBox(0, 0, 0, 12, 1, 1, 0, 0, -0.5625f, -0.5f, -0.375f, -0.625f, -1.125f, -0.375f, -0.625f, 0.625f, 0, -0.5625f, 0, 0, -0.25f, -0.5f, -0.375f, -0.25f, -1.125f, -0.375f, -0.25f, 0.625f, 0, -0.25f, 0);
		bodyModel[502].setRotationPoint(64.375f, 1.75f, -9.875f);

		bodyModel[503] = new ModelRendererTurbo(this, 248, 147, textureX, textureY);
		bodyModel[503].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, -0.1875f, -0.625f, -0.25f, -0.25f, -1.125f, -0.25f, -0.25f, 0.625f, 0, -0.1875f, 0.125f, 0, 0, -1, -0.25f, 0.0625f, -1.4375f, -0.25f, 0.0625f, 0.9375f, 0, 0, 0.5f);
		bodyModel[503].setRotationPoint(50.3125f, 0.1875f, -11.125f);

		bodyModel[504] = new ModelRendererTurbo(this, 86, 144, textureX, textureY);
		bodyModel[504].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.1875f, -0.625f, 0.3125f, -0.25f, -1.125f, 0.3125f, -0.25f, 0.625f, 0, -0.1875f, 0.125f, 0, 0, -0.9375f, 0.3125f, 0.0625f, -1.25f, 0.3125f, 0.0625f, 0.75f, 0, 0, 0.4375f);
		bodyModel[504].setRotationPoint(58.0625f, 0.25f, -10.625f);

		bodyModel[505] = new ModelRendererTurbo(this, 471, 138, textureX, textureY);
		bodyModel[505].addShapeBox(0, 0, 0, 12, 2, 1, 0, 0, -0.1875f, -0.625f, -0.375f, -0.25f, -1.4375f, -0.375f, -0.25f, 0.9375f, 0, -0.1875f, 0.125f, 0, 0, -0.75f, -0.375f, 0.0625f, -1.375f, -0.375f, 0.0625f, 0.875f, 0, 0, 0.25f);
		bodyModel[505].setRotationPoint(64.375f, 0.3125f, -10.125f);

		bodyModel[506] = new ModelRendererTurbo(this, 34, 147, textureX, textureY);
		bodyModel[506].addShapeBox(0, 0, 0, 8, 3, 1, 0, 0, 0, -0.5f, -0.25f, -0.8125f, -0.96875f, -0.25f, -0.8125f, 0.46875f, 0, 0, 0, 0, -0.03125f, -0.46875f, -0.25f, 0.03125f, -0.96875f, -0.25f, 0.03125f, 0.46875f, 0, -0.03125f, -0.03125f);
		bodyModel[506].setRotationPoint(50.3125f, -2.59375f, -10.96875f);

		bodyModel[507] = new ModelRendererTurbo(this, 404, 143, textureX, textureY);
		bodyModel[507].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 0, -0.5f, 0.3125f, -0.625f, -0.875f, 0.3125f, -0.625f, 0.375f, 0, 0, 0, 0, 0.21875f, -0.5f, 0.3125f, 0.28125f, -1, 0.3125f, 0.28125f, 0.5f, 0, 0.21875f, 0);
		bodyModel[507].setRotationPoint(58.0625f, -1.78125f, -10.5f);

		bodyModel[508] = new ModelRendererTurbo(this, 261, 123, textureX, textureY);
		bodyModel[508].addShapeBox(0, 0, 0, 12, 1, 1, 0, 0, 0.375f, -0.5f, -0.375f, 0, -1.5625f, -0.375f, 0, 1.0625f, 0, 0.375f, 0, 0, 0.28125f, -0.625f, -0.375f, 0.34375f, -1.4375f, -0.375f, 0.34375f, 0.9375f, 0, 0.28125f, 0.125f);
		bodyModel[508].setRotationPoint(64.375f, -0.78125f, -10.125f);

		bodyModel[509] = new ModelRendererTurbo(this, 124, 144, textureX, textureY);
		bodyModel[509].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.53125f, -0.875f, -0.25f, -1.15625f, -0.9375f, -0.25f, -1.15625f, 0.4375f, 0, 0.53125f, 0.3125f, 0, 0, -0.21875f, -0.25f, 0.8125f, -0.6875f, -0.25f, 0.8125f, 0.1875f, 0, 0, -0.28125f);
		bodyModel[509].setRotationPoint(50.3125f, -3.59375f, -10.6875f);

		bodyModel[510] = new ModelRendererTurbo(this, 497, 130, textureX, textureY);
		bodyModel[510].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, -0.15625f, -0.8125f, 0.3125f, -1.4375f, -0.9375f, 0.3125f, -1.4375f, 0.4375f, 0, -0.15625f, 0.3125f, 0, -0.1875f, -0.5625f, 0.3125f, 0.4375f, -0.9375f, 0.3125f, 0.4375f, 0.4375f, 0, -0.1875f, 0.0625f);
		bodyModel[510].setRotationPoint(58.0625f, -2.59375f, -10.5625f);

		bodyModel[511] = new ModelRendererTurbo(this, 320, 124, textureX, textureY);
		bodyModel[511].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 1.125f, -2, 0.3125f, 0, -2.25f, 0.3125f, 0, 1.75f, 0, 1.125f, 1.5f, 0, -0.84375f, -0.8125f, 0.3125f, 0.4375f, -0.9375f, 0.3125f, 0.4375f, 0.4375f, 0, -0.84375f, 0.3125f);
		bodyModel[511].setRotationPoint(58.0625f, -3.59375f, -10.5625f);

		bodyModel[512] = new ModelRendererTurbo(this, 17, 140, textureX, textureY);
		bodyModel[512].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0.125f, -2, -0.25f, -1.3828125f, -2.1875f, -0.25f, -1.3828125f, 1.6875f, 0, 0.125f, 1.5f, 0, -0.0234375f, -0.9375f, -0.25f, 1.6640625f, -1, -0.25f, 1.6640625f, 0.5f, 0, -0.0234375f, 0.375f);
		bodyModel[512].setRotationPoint(50.3125f, -6.1015625f, -10.75f);

		bodyModel[513] = new ModelRendererTurbo(this, 207, 138, textureX, textureY);
		bodyModel[513].addShapeBox(0, 0, 0, 12, 2, 1, 0, 0, 0.15625f, -1.9375f, -0.375f, -1.9375f, -2.4375f, -0.375f, -1.9375f, 1.9375f, 0, 0.15625f, 1.4375f, 0, 0.28125f, -0.625f, -0.375f, 0.65625f, -1.6875f, -0.375f, 0.65625f, 1.1875f, 0, 0.28125f, 0.125f);
		bodyModel[513].setRotationPoint(64.375f, -3.4375f, -10.25f);

		bodyModel[514] = new ModelRendererTurbo(this, 333, 178, textureX, textureY);
		bodyModel[514].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, -0.0625f, -1.5625f, -0.25f, -1.6953125f, -1.6875f, -0.25f, -2.1953125f, 0.6875f, 0, -0.5625f, 0.5625f, 0, -0.3828125f, 0.6875f, -0.25f, 1.125f, 0.5f, -0.25f, 1.125f, -1, 0, -0.3828125f, -1.1875f);
		bodyModel[514].setRotationPoint(50.3125f, -7.84375f, -8.0625f);

		bodyModel[515] = new ModelRendererTurbo(this, 131, 180, textureX, textureY);
		bodyModel[515].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.0625f, -1.5625f, 0.3125f, -1.3203125f, -1.6875f, 0.3125f, -1.8203125f, 0.6875f, 0, -0.5625f, 0.5625f, 0, -0.5078125f, 0.625f, 0.3125f, 0.6171875f, 0.375f, 0.3125f, 0.6171875f, -0.875f, 0, -0.5078125f, -1.125f);
		bodyModel[515].setRotationPoint(58.0625f, -6.2109375f, -7.9375f);

		bodyModel[516] = new ModelRendererTurbo(this, 151, 184, textureX, textureY);
		bodyModel[516].addShapeBox(0, 0, 0, 12, 2, 1, 0, 0, -0.0625f, -1.5625f, -0.375f, -2.1953125f, -1.6875f, -0.375f, -2.6953125f, 0.6875f, 0, -0.5625f, 0.5625f, 0, -0.640625f, 0.5f, -0.375f, 1.453125f, 0, -0.375f, 1.453125f, -0.5f, 0, -0.640625f, -1);
		bodyModel[516].setRotationPoint(64.375f, -4.953125f, -7.8125f);

		bodyModel[517] = new ModelRendererTurbo(this, 394, 130, textureX, textureY);
		bodyModel[517].addShapeBox(0, 0, 0, 8, 1, 3, 0, 0, -0.15625f, -0.375f, 0.3125f, -0.96875f, -0.375f, 0.3125f, -0.8125f, 0, 0, -0.0625f, 0, 0, -0.34375f, -0.375f, 0.3125f, 0.46875f, -0.375f, 0.3125f, 0.3125f, 0, 0, -0.4375f, 0);
		bodyModel[517].setRotationPoint(49.75f, -10.71875f, -3.0f);

		bodyModel[518] = new ModelRendererTurbo(this, 140, 144, textureX, textureY);
		bodyModel[518].addShapeBox(0, 0, 0, 1, 1, 3, 0, 0, -0.15625f, -0.375f, -0.25f, -0.28125f, -0.375f, -0.25f, -0.1875f, 0, 0, 0, 0, 0, -0.34375f, -0.375f, -0.25f, -0.21875f, -0.375f, -0.25f, -0.3125f, 0, 0, -0.5f, 0);
		bodyModel[518].setRotationPoint(58.0625f, -9.90625f, -3.0f);

		bodyModel[519] = new ModelRendererTurbo(this, 471, 142, textureX, textureY);
		bodyModel[519].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, -1.125f, 0.0625f, -0.25f, -1.25f, 0.0625f, -0.25f, -0.125f, 0, 0, 0, 0, 0, 0.625f, 0.0625f, -0.25f, 0.6875f, 0.0625f, -0.25f, -0.375f, 0, 0, -0.5f, 0);
		bodyModel[519].setRotationPoint(58.0625f, -9.75f, -4.625f);

		bodyModel[520] = new ModelRendererTurbo(this, 44, 134, textureX, textureY);
		bodyModel[520].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, -2.4765625f, -0.3125f, -0.25f, -2.6265f, -0.328125f, -0.25f, -0.125f, 0, 0, 0, 0, 0, 1.9765625f, -0.3125f, -0.25f, 2.127f, -0.328125f, -0.25f, -0.4375f, 0, 0, -0.5f, 0);
		bodyModel[520].setRotationPoint(58.0625f, -8.625f, -6.6875f);

		bodyModel[521] = new ModelRendererTurbo(this, 0, 37, textureX, textureY);
		bodyModel[521].addShapeBox(0, 0, 0, 8, 0, 2, 0, -0.5625f, -0.1796875f, 0, 0.3125f, -1.25f, 0.0625f, 0.3125f, -0.125f, 0, 0, 0.6875f, 0, -0.5625f, 0.1796875f, 0, 0.3125f, 1.25f, 0.0625f, 0.3125f, 0.125f, 0, 0, -0.6875f, 0);
		bodyModel[521].setRotationPoint(49.75f, -9.625f, -4.625f);

		bodyModel[522] = new ModelRendererTurbo(this, 0, 34, textureX, textureY);
		bodyModel[522].addShapeBox(0, 0, 0, 8, 0, 2, 0, -0.5625f, -1.9140625f, -0.125f, 0.3125f, -3.546875f, -0.25f, 0.3125f, -1.0703125f, -0.0625f, -0.5625f, 0, 0, -0.5625f, 1.9140625f, -0.125f, 0.3125f, 3.546875f, -0.25f, 0.3125f, 1.0703125f, -0.0625f, -0.5625f, 0, 0);
		bodyModel[522].setRotationPoint(49.75f, -9.4453125f, -6.625f);

		bodyModel[523] = new ModelRendererTurbo(this, 58, 159, textureX, textureY);
		bodyModel[523].addShapeBox(0, 0, 0, 5, 0, 3, 0, 0, 0.65625f, -0.375f, 0, -0.40625f, -0.375f, 0, -0.375f, 0, 0, 0.75f, 0, 0, -0.65625f, -0.375f, 0, 0.40625f, -0.375f, 0, 0.375f, 0, 0, -0.75f, 0);
		bodyModel[523].setRotationPoint(58.8125f, -8.71875f, -3.0f);

		bodyModel[524] = new ModelRendererTurbo(this, 314, 106, textureX, textureY);
		bodyModel[524].addShapeBox(0, 0, 0, 5, 0, 2, 0, 0, -0.34375f, 0.0625f, 0, -1.28125f, 0, 0, -0.3125f, 0, 0, 0.75f, 0, 0, 0.34375f, 0.0625f, 0, 1.28125f, 0, 0, 0.3125f, 0, 0, -0.75f, 0);
		bodyModel[524].setRotationPoint(58.8125f, -8.625f, -4.625f);

		bodyModel[525] = new ModelRendererTurbo(this, 495, 68, textureX, textureY);
		bodyModel[525].addShapeBox(0, 0, 0, 5, 0, 2, 0, 0, -2.7578125f, -0.3203125f, 0, -3.72025f, -0.421875f, 0, -1.125f, 0.0625f, 0, -0.1875f, 0, 0, 2.7578125f, -0.3203125f, 0, 3.72075f, -0.421875f, 0, 1.125f, 0.0625f, 0, 0.1875f, 0);
		bodyModel[525].setRotationPoint(58.8125f, -8.46875f, -6.6875f);

		bodyModel[526] = new ModelRendererTurbo(this, 0, 12, textureX, textureY);
		bodyModel[526].addShapeBox(0, 0, 0, 7, 0, 3, 0, 0, 0.71875f, -0.375f, 0.375f, -2.0625f, -0.375f, 0.375f, -2.03125f, 0, 0, 0.75f, 0, 0, -0.71875f, -0.375f, 0.375f, 2.0625f, -0.375f, 0.375f, 2.03125f, 0, 0, -0.75f, 0);
		bodyModel[526].setRotationPoint(63.8125f, -7.59375f, -3.0f);

		bodyModel[527] = new ModelRendererTurbo(this, 216, 101, textureX, textureY);
		bodyModel[527].addShapeBox(0, 0, 0, 7, 0, 2, 0, 0, -0.21875f, 0, 0.375f, -2.4375f, -0.625f, 0.375f, -2.03125f, 0, 0, 0.75f, 0, 0, 0.21875f, 0, 0.375f, 2.4375f, -0.625f, 0.375f, 2.03125f, 0, 0, -0.75f, 0);
		bodyModel[527].setRotationPoint(63.8125f, -7.5625f, -4.625f);

		bodyModel[528] = new ModelRendererTurbo(this, 491, 56, textureX, textureY);
		bodyModel[528].addShapeBox(0, 0, 0, 7, 0, 2, 0, 0, -2.796875f, -0.3515625f, 0.375f, -4.15775f, -0.421875f, 0.375f, -2.40625f, 0.625f, 0, -0.1875f, 0, 0, 2.796875f, -0.3515625f, 0.375f, 4.15825f, -0.421875f, 0.375f, 2.40625f, 0.625f, 0, 0.1875f, 0);
		bodyModel[528].setRotationPoint(63.8125f, -7.53125f, -6.625f);

		bodyModel[529] = new ModelRendererTurbo(this, 497, 6, textureX, textureY);
		bodyModel[529].addShapeBox(0, 0, 0, 4, 0, 3, 0, 0, 0.71875f, -0.375f, 0.09375f, -0.875f, -0.375f, 0.09375f, -0.875f, 0, 0, 0.75f, 0, 0, -0.71875f, -0.375f, 0.09375f, 0.875f, -0.375f, 0.09375f, 0.875f, 0, 0, -0.75f, 0);
		bodyModel[529].setRotationPoint(71.1875f, -4.8125f, -3.0f);

		bodyModel[530] = new ModelRendererTurbo(this, 498, 16, textureX, textureY);
		bodyModel[530].addShapeBox(0, 0, 0, 4, 0, 1, 0, 0, 0.34375f, 0.375f, 0.09375f, -1.0625f, 0.25f, 0.09375f, -0.84375f, 0, 0, 0.75f, 0, 0, -0.34375f, 0.375f, 0.09375f, 1.0625f, 0.25f, 0.09375f, 0.84375f, 0, 0, -0.75f, 0);
		bodyModel[530].setRotationPoint(71.1875f, -4.78125f, -3.625f);

		bodyModel[531] = new ModelRendererTurbo(this, 419, 167, textureX, textureY);
		bodyModel[531].addShapeBox(0, 0, 0, 4, 0, 2, 0, 0.03125f, -1, 0.203125f, 0.09375f, -1.28125f, -0.765625f, 0.09375f, -0.65625f, 0.125f, 0, 0.75f, 0, 0.03125f, 1, 0.203125f, 0.09375f, 1.28125f, -0.765625f, 0.09375f, 0.65625f, 0.125f, 0, -0.75f, 0);
		bodyModel[531].setRotationPoint(71.1875f, -4.375f, -6.0f);

		bodyModel[532] = new ModelRendererTurbo(this, 0, 20, textureX, textureY);
		bodyModel[532].addShapeBox(0, 0, 0, 3, 0, 1, 0, 0, 0.75f, 0, -0.84375f, 0.0625f, 0, -3, 0.125f, 0.359375f, 0, 0.125f, 0.359375f, 0, -0.75f, 0, -0.84375f, -0.0625f, 0, -3, -0.125f, 0.359375f, 0, -0.125f, 0.359375f);
		bodyModel[532].setRotationPoint(75.28125f, -2.96875f, 3.875f);

		bodyModel[533] = new ModelRendererTurbo(this, 172, 140, textureX, textureY);
		bodyModel[533].addShapeBox(0, 0, 0, 2, 1, 2, 0, -0.75f, -0.5859375f, 0.375f, -1.25f, -0.5859375f, 0.375f, 0.1875f, -0.0625f, -0.125f, -0.03125f, 0, -1.484375f, -0.75f, 0.0859375f, 0.375f, -1.25f, 0.0859375f, 0.375f, 0.1875f, -0.4375f, -0.125f, -0.03125f, -0.5f, -1.484375f);
		bodyModel[533].setRotationPoint(75.25f, -3.34375f, -5.75f);

		bodyModel[534] = new ModelRendererTurbo(this, 0, 126, textureX, textureY);
		bodyModel[534].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0.25f, -0.45225f, 0.42578125f, 0.5625f, -1.3359375f, 0.375f, -0.15625f, -0.75f, -0.484375f, 0.25f, -0.45225f, -1.4257812f, 0.25f, -0.04882812f, 0.42578125f, 0.5625f, 0.84375f, 0.375f, -0.15625f, 0.25f, -0.484375f, 0.25f, -0.04882812f, -1.4257812f);
		bodyModel[534].setRotationPoint(71.4375f, -4.09375f, -5.75f);

		bodyModel[535] = new ModelRendererTurbo(this, 324, 111, textureX, textureY);
		bodyModel[535].addShapeBox(0, 0, 0, 13, 1, 8, 0, 0, 1, 0, 0.4375f, 0, 0, 0.4375f, 0, 0, 0, 1, 0, 0, 0, 0, 0.4375f, 0, 0, 0.4375f, 0, 0, 0, 0, 0);
		bodyModel[535].setRotationPoint(65.8125f, -2.375f, -4.0f);

		bodyModel[536] = new ModelRendererTurbo(this, 266, 29, textureX, textureY);
		bodyModel[536].addShapeBox(0, 0, 0, 8, 1, 3, 0, 0, 1, 0, 0.4375f, 0, 0, 5.4375f, 0, 0, 0, 1, 0, 0, 0, 0, 0.4375f, 0, 0, 5.4375f, 0, 0, 0, 0, 0);
		bodyModel[536].setRotationPoint(65.8125f, -2.375f, -7.0f);

		bodyModel[537] = new ModelRendererTurbo(this, 266, 24, textureX, textureY);
		bodyModel[537].addShapeBox(0, 0, 0, 8, 1, 3, 0, 0, 1, 0, 5.4375f, 0, 0, 0.4375f, 0, 0, 0, 1, 0, 0, 0, 0, 5.4375f, 0, 0, 0.4375f, 0, 0, 0, 0, 0);
		bodyModel[537].setRotationPoint(65.8125f, -2.375f, 4.0f);

		bodyModel[538] = new ModelRendererTurbo(this, 0, 93, textureX, textureY);
		bodyModel[538].addShapeBox(0, 0, 0, 30, 3, 21, 0, 0, 0, 0.625f, -0.375f, 0, -1.253f, -0.375f, 0, -1.753f, 0, 0, 0.125f, 0, -0.25f, -0.875f, -0.375f, -0.5625f, -2.5f, -0.375f, -0.5625f, -3, 0, -0.25f, -1.375f);
		bodyModel[538].setRotationPoint(46.375f, 2.75f, -10.25f);

		bodyModel[539] = new ModelRendererTurbo(this, 82, 93, textureX, textureY);
		bodyModel[539].addShapeBox(0, 0, 0, 30, 2, 18, 0, 0, 0, 0.625f, -0.375f, 0.3125f, -1, -0.375f, 0.3125f, -1.5f, 0, 0, 0.125f, 0, -0.4375f, -0.375f, -0.375f, -1, -1.75f, -0.375f, -1, -2.25f, 0, -0.4375f, -0.875f);
		bodyModel[539].setRotationPoint(46.375f, 5.5f, -8.75f);

		bodyModel[540] = new ModelRendererTurbo(this, 108, 114, textureX, textureY);
		bodyModel[540].addShapeBox(0, 0, 0, 4, 1, 14, 0, 0, 0.18117f, 0.16825f, -0.375f, 0.25f, 0, -0.375f, 0.25f, 0, 0, 0.18117f, 0.1685f, 0, 0.0625f, -1.3125f, -0.375f, 0.0625f, -1.4375f, -0.375f, 0.0625f, -1.4375f, 0, 0.0625f, -1.3125f);
		bodyModel[540].setRotationPoint(72.375f, 6.75f, -7.0f);

		bodyModel[541] = new ModelRendererTurbo(this, 183, 54, textureX, textureY);
		bodyModel[541].addShapeBox(0, 0, 0, 17, 1, 1, 0, -0.3125f, -0.171875f, -0.640625f, -0.25f, 0.390625f, -1.140625f, -0.25f, -0.234375f, 0.625f, -0.3125f, -0.171875f, 0.125f, -0.3125f, 0.125f, 0.15625f, -0.25f, 0.125f, 0.15625f, -0.25f, 0.125f, 0.9375f, -0.3125f, 0.125f, 0.1875f);
		bodyModel[541].setRotationPoint(52.5f, 5.25f, -9.4375f);

		bodyModel[542] = new ModelRendererTurbo(this, 161, 93, textureX, textureY);
		bodyModel[542].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.390625f, -1.140625f, -0.5f, 0.390625f, -1.640625f, -0.5f, -0.234375f, 0.71875f, 0, -0.171875f, 0.125f, 0, 0.125f, 0.15625f, -0.5f, -0.3125f, -0.875f, -0.5f, -0.3125f, 0.9375f, 0, 0.125f, 0.1875f);
		bodyModel[542].setRotationPoint(69.25f, 5.25f, -9.4375f);

		bodyModel[543] = new ModelRendererTurbo(this, 504, 32, textureX, textureY);
		bodyModel[543].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0.390625f, -0.640625f, -0.4375f, -0.6875f, -1.0625f, -0.4375f, -0.015625f, 0.34375f, 0, 0.015625f, -0.21875f, 0, -0.3125f, 0.125f, -0.4375f, -0.3125f, -1.0625f, -0.5f, -0.3125f, 0.1875f, 0, -0.3125f, -0.0625f);
		bodyModel[543].setRotationPoint(72.75f, 5.25f, -8.4375f);

		bodyModel[544] = new ModelRendererTurbo(this, 345, 132, textureX, textureY);
		bodyModel[544].addShapeBox(0, 0, 0, 5, 1, 1, 0, -0.3125f, -0.171875f, -1.6875f, -0.25f, -0.171875f, -0.640625f, -0.25f, -0.171875f, 0.125f, -0.3125f, -0.171875f, 1.53125f, -0.3125f, 0.6875f, -1.6875f, -0.25f, 0.125f, 0.15625f, -0.25f, 0.125f, 0.1875f, -0.3125f, 0.6875f, 0.6875f);
		bodyModel[544].setRotationPoint(48.0625f, 5.25f, -9.4375f);

		bodyModel[545] = new ModelRendererTurbo(this, 425, 65, textureX, textureY);
		bodyModel[545].addShapeBox(0, 0, 0, 17, 2, 2, 0, -0.3125f, 0.125f, 0.15625f, -0.25f, 0.125f, 0.15625f, -0.25f, 0.125f, 0.0625f, -0.3125f, 0.125f, 0.0625f, -0.3125f, -0.296875f, -0.78125f, -0.25f, -0.296875f, -0.78125f, -0.25f, -0.265625f, 0.0625f, -0.3125f, -0.265625f, 0.0625f);
		bodyModel[545].setRotationPoint(52.5f, 6.5f, -9.4375f);

		bodyModel[546] = new ModelRendererTurbo(this, 0, 142, textureX, textureY);
		bodyModel[546].addShapeBox(0, 0, 0, 4, 2, 2, 0, -0.3125f, 0.125f, 0.15625f, -0.1875f, 0.5625f, -0.875f, -0.1875f, 0.5625f, 0.0625f, -0.3125f, 0.125f, 0.0625f, -0.3125f, -0.296875f, -0.78125f, -0.1875f, -0.734375f, -1.8125f, -0.1875f, -0.546875f, 0.75f, -0.3125f, -0.265625f, 0.0625f);
		bodyModel[546].setRotationPoint(68.9375f, 6.5f, -9.4375f);

		bodyModel[547] = new ModelRendererTurbo(this, 458, 21, textureX, textureY);
		bodyModel[547].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -0.3125f, 0.125f, -0.4375f, -0.3125f, -1.0625f, -0.4375f, -0.3125f, 0.1875f, 0, -0.3125f, 0.0625f, 0, 1.140625f, -0.8125f, -0.265625f, -0.09375f, -1.375f, -0.09375f, 0.65625f, 1.296875f, 0, 1.328125f, 0.75f);
		bodyModel[547].setRotationPoint(72.75f, 5.625f, -8.4375f);

		bodyModel[548] = new ModelRendererTurbo(this, 257, 54, textureX, textureY);
		bodyModel[548].addShapeBox(0, 0, 0, 5, 1, 1, 0, -0.3125f, -0.4375f, -1.6875f, -0.25f, 0.125f, 0.15625f, -0.25f, 0.125f, 1.0625f, -0.3125f, -0.4375f, 0.6875f, -0.3125f, 0.140625f, -2.09375f, -0.25f, 0.703125f, -0.78125f, -0.25f, 0.734375f, 1.0625f, -0.3125f, 0.140625f, 1.09375f);
		bodyModel[548].setRotationPoint(48.0625f, 6.5f, -9.4375f);

		bodyModel[549] = new ModelRendererTurbo(this, 386, 178, textureX, textureY);
		bodyModel[549].addShapeBox(0, 0, 0, 7, 1, 1, 0, 0, -0.625f, 0, -0.1875f, -1.9121094f, -0.07226562f, -0.1875f, -1.2480469f, 0.07226562f, 0, 0.00390625f, 0, 0, -0.1875f, 0, -0.1875f, 1.1621094f, -0.07226562f, -0.1875f, 0.7480469f, 0.00976562f, 0, -0.50390625f, -0.0625f);
		bodyModel[549].setRotationPoint(64.375f, -4.9761877f, -7.25f);

		bodyModel[550] = new ModelRendererTurbo(this, 373, 182, textureX, textureY);
		bodyModel[550].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -0.66015625f, 0, 0.5234375f, -0.8222656f, -1.0449219f, 0.09375f, -0.29101562f, 0.9472656f, 0, 0.00390625f, 0, 0, -0.08984375f, 0, 0.5234375f, 0.32226562f, -1.0449219f, 0.09375f, -0.14648438f, 0.8847656f, 0, -0.50390625f, -0.0625f);
		bodyModel[550].setRotationPoint(71.1875f, -3.7242346f, -7.1777344f);

		bodyModel[551] = new ModelRendererTurbo(this, 371, 179, textureX, textureY);
		bodyModel[551].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, -0.6484375f, 0, 0.3125f, -1.8886719f, -0.12304688f, 0.3125f, -1.2578125f, 0.125f, 0, 0.00390625f, 0, 0, -0.1875f, 0, 0.3125f, 1.2402344f, -0.125f, 0.3125f, 0.7480469f, 0.12304688f, 0, -0.50390625f, -0.0625f);
		bodyModel[551].setRotationPoint(58.0625f, -6.2379065f, -7.375f);

		bodyModel[552] = new ModelRendererTurbo(this, 352, 178, textureX, textureY);
		bodyModel[552].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.38476562f, 0, -0.25f, -2.0566406f, -0.015625f, -0.25f, -1.4042969f, 0.01562499f, 0, 0.22851562f, -0.109375f, 0, -0.51171875f, 0, -0.25f, 1.2363281f, -0.015625f, -0.25f, 0.7480469f, 0.01562499f, 0, -1.1289062f, -0.109375f);
		bodyModel[552].setRotationPoint(50.3125f, -7.6445312f, -7.390625f);

		bodyModel[553] = new ModelRendererTurbo(this, 295, 124, textureX, textureY);
		bodyModel[553].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 1.125f, 1.5f, 0.3125f, 0, 1.75f, 0.3125f, 0, -2.25f, 0, 1.125f, -2, 0, -0.84375f, 0.3125f, 0.3125f, 0.4375f, 0.4375f, 0.3125f, 0.4375f, -0.9375f, 0, -0.84375f, -0.8125f);
		bodyModel[553].setRotationPoint(58.0625f, -3.59375f, 9.5625f);

		bodyModel[554] = new ModelRendererTurbo(this, 0, 167, textureX, textureY);
		bodyModel[554].addShapeBox(0, 0, 0, 3, 5, 2, 0, 0, 0, 0.375f, 0, 0, 0, 10.5f, 0, 0, 0, 0, 0, 0, 0, 0.375f, 0, 0, 0, 10.5f, 0, 0, 0, 0, 0);
		bodyModel[554].setRotationPoint(60.875f, -2.375f, -9.0f);

		bodyModel[555] = new ModelRendererTurbo(this, 211, 142, textureX, textureY);
		bodyModel[555].addShapeBox(0, 0, 0, 5, 2, 6, 0, 0, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0, 0, 0, 0.25f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, 0, 0.25f, -0.5f, 0);
		bodyModel[555].setRotationPoint(60.875f, -3.875f, 1.1875f);

		bodyModel[556] = new ModelRendererTurbo(this, 313, 137, textureX, textureY);
		bodyModel[556].addShapeBox(0, 0, 0, 5, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.9375f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.9375f);
		bodyModel[556].setRotationPoint(55.875f, -2.375f, 0.0f);

		bodyModel[557] = new ModelRendererTurbo(this, 152, 137, textureX, textureY);
		bodyModel[557].addShapeBox(0, 0, 0, 5, 1, 9, 0, 0, 0, 0.9375f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.9375f, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[557].setRotationPoint(55.875f, -2.375f, -9.0f);

		bodyModel[558] = new ModelRendererTurbo(this, 489, 133, textureX, textureY);
		bodyModel[558].addShapeBox(0, 0, 0, 2, 1, 9, 0, 0, 0, 0, 0, 0, -0.0625f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.0625f, 0, 0, 0, 0, 0, 0);
		bodyModel[558].setRotationPoint(53.875f, -2.375f, -10.0f);

		bodyModel[559] = new ModelRendererTurbo(this, 92, 93, textureX, textureY);
		bodyModel[559].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.25f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.25f, 0, -0.5f);
		bodyModel[559].setRotationPoint(53.875f, -2.375f, -1.0f);

		bodyModel[560] = new ModelRendererTurbo(this, 162, 72, textureX, textureY);
		bodyModel[560].addShapeBox(0, 0, 0, 2, 1, 2, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -1.5f, 0, -0.25f, -0.25f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -1.5f, 0, -0.25f);
		bodyModel[560].setRotationPoint(53.875f, -2.375f, -0.5f);

		bodyModel[561] = new ModelRendererTurbo(this, 433, 130, textureX, textureY);
		bodyModel[561].addShapeBox(0, 0, 0, 1, 1, 1, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -1, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -1, 0, -0.25f);
		bodyModel[561].setRotationPoint(54.875f, -2.375f, 1.25f);

		bodyModel[562] = new ModelRendererTurbo(this, 458, 32, textureX, textureY);
		bodyModel[562].addShapeBox(0, 0, 0, 7, 2, 1, 0, -0.75f, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, 0, 0.25f, -0.5f, 0);
		bodyModel[562].setRotationPoint(58.875f, -3.875f, -3.8125f);

		bodyModel[563] = new ModelRendererTurbo(this, 131, 114, textureX, textureY);
		bodyModel[563].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0.25f, 0, 0, 0.25f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, 0, 0.5f, -0.5f, 0);
		bodyModel[563].setRotationPoint(59.875f, -3.875f, -4.8125f);

		bodyModel[564] = new ModelRendererTurbo(this, 254, 157, textureX, textureY);
		bodyModel[564].addShapeBox(0, 0, 0, 6, 2, 2, 0, 0, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0, 0, 0, 0.25f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, 0, 0.25f, -0.5f, 0);
		bodyModel[564].setRotationPoint(59.875f, -3.875f, -6.8125f);

		bodyModel[565] = new ModelRendererTurbo(this, 472, 120, textureX, textureY);
		bodyModel[565].addShapeBox(0, 0, 0, 4, 2, 1, 0, 0, 0, 0, -0.0625f, 0, 0, 1.9375f, 0, 0, 0, 0, 0, 0.25f, -0.5f, 0, -0.0625f, -0.5f, 0, 1.9375f, -0.5f, 0, 0.25f, -0.5f, 0);
		bodyModel[565].setRotationPoint(59.875f, -3.875f, -7.8125f);

		bodyModel[566] = new ModelRendererTurbo(this, 82, 93, textureX, textureY);
		bodyModel[566].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.0625f, -0.0625f, 0, -0.375f, 1.9375f, 0, 0, 0, 0, 0, 0.25f, -0.5f, 0.375f, -0.0625f, -0.5f, 0, 1.9375f, -0.5f, 0, 0.25f, -0.5f, 0);
		bodyModel[566].setRotationPoint(59.875f, -3.875f, -8.8125f);

		bodyModel[567] = new ModelRendererTurbo(this, 36, 136, textureX, textureY);
		bodyModel[567].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0.25f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, 0, 0.25f, -0.5f, 0.8125f);
		bodyModel[567].setRotationPoint(60.875f, -3.875f, 7.1875f);

		bodyModel[568] = new ModelRendererTurbo(this, 216, 93, textureX, textureY);
		bodyModel[568].addShapeBox(0, 0, 0, 5, 1, 6, 0, 0.25f, 0, 0, -0.0625f, 0, 0, -0.6875f, 0, 0, 0.25f, 0, 0, 0.25f, -0.75f, 0, -0.0625f, -0.75f, 0, -0.6875f, -0.75f, 0, 0.25f, -0.75f, 0);
		bodyModel[568].setRotationPoint(60.875f, -4.125f, 1.1875f);

		bodyModel[569] = new ModelRendererTurbo(this, 266, 39, textureX, textureY);
		bodyModel[569].addShapeBox(0, 0, 0, 5, 1, 4, 0, 2.25f, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0.25f, 0, 0, 2.25f, -0.75f, 0, -0.0625f, -0.75f, 0, -0.0625f, -0.75f, 0, 0.25f, -0.75f, 0);
		bodyModel[569].setRotationPoint(60.875f, -4.125f, -2.8125f);

		bodyModel[570] = new ModelRendererTurbo(this, 320, 114, textureX, textureY);
		bodyModel[570].addShapeBox(0, 0, 0, 4, 2, 1, 0, 0.0625f, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, -0.75f, 0.0625f, 0, -0.75f, 0.3125f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, -0.75f, 0.3125f, -0.5f, -0.75f);
		bodyModel[570].setRotationPoint(60.875f, -3.875f, 1.1875f);

		bodyModel[571] = new ModelRendererTurbo(this, 283, 100, textureX, textureY);
		bodyModel[571].addShapeBox(0, 0, 0, 4, 2, 1, 0, 0.0625f, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, -0.75f, 0.0625f, 0, -0.75f, 0.3125f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, -0.75f, 0.3125f, -0.5f, -0.75f);
		bodyModel[571].setRotationPoint(60.875f, -3.875f, 6.9375f);

		bodyModel[572] = new ModelRendererTurbo(this, 29, 118, textureX, textureY);
		bodyModel[572].addShapeBox(-0.5f, -1, -0.5f, 1, 1, 1, 0, -0.375f, -0.25f, -0.375f, -0.375f, -0.25f, -0.375f, -0.375f, -0.25f, -0.375f, -0.375f, -0.25f, -0.375f, -0.375f, 0, -0.375f, -0.375f, 0, -0.375f, -0.375f, 0, -0.375f, -0.375f, 0, -0.375f);
		bodyModel[572].setRotationPoint(58.125f, -2.25f, 5.3125f);
		bodyModel[572].rotateAngleZ = -1.1519173f;

		bodyModel[573] = new ModelRendererTurbo(this, 7, 118, textureX, textureY);
		bodyModel[573].addShapeBox(-0.5f, -1.75f, -0.5f, 1, 1, 1, 0, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f);
		bodyModel[573].setRotationPoint(58.125f, -2.25f, 5.3125f);
		bodyModel[573].rotateAngleZ = -1.1519173f;

		bodyModel[574] = new ModelRendererTurbo(this, 497, 117, textureX, textureY);
		bodyModel[574].addShapeBox(-0.5f, -2, -0.5f, 1, 1, 1, 0, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f);
		bodyModel[574].setRotationPoint(58.125f, -2.25f, 5.3125f);
		bodyModel[574].rotateAngleZ = -1.1519173f;

		bodyModel[575] = new ModelRendererTurbo(this, 222, 117, textureX, textureY);
		bodyModel[575].addShapeBox(-0.5f, -2.25f, -0.5f, 1, 1, 1, 0, -0.3125f, -0.875f, -0.375f, -0.3125f, -0.875f, -0.375f, -0.3125f, -0.875f, -0.375f, -0.3125f, -0.875f, -0.375f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f);
		bodyModel[575].setRotationPoint(58.125f, -2.25f, 5.3125f);
		bodyModel[575].rotateAngleZ = -1.1519173f;

		bodyModel[576] = new ModelRendererTurbo(this, 506, 116, textureX, textureY);
		bodyModel[576].addShapeBox(-0.5f, -1, -0.5f, 1, 1, 1, 0, -0.375f, -0.25f, -0.375f, -0.375f, -0.25f, -0.375f, -0.375f, -0.25f, -0.375f, -0.375f, -0.25f, -0.375f, -0.375f, 0, -0.375f, -0.375f, 0, -0.375f, -0.375f, 0, -0.375f, -0.375f, 0, -0.375f);
		bodyModel[576].setRotationPoint(58.125f, -2.25f, 1.875f);
		bodyModel[576].rotateAngleZ = -1.1519173f;

		bodyModel[577] = new ModelRendererTurbo(this, 497, 114, textureX, textureY);
		bodyModel[577].addShapeBox(-0.5f, -1.75f, -0.5f, 1, 1, 1, 0, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f);
		bodyModel[577].setRotationPoint(58.125f, -2.25f, 1.875f);
		bodyModel[577].rotateAngleZ = -1.1519173f;

		bodyModel[578] = new ModelRendererTurbo(this, 507, 113, textureX, textureY);
		bodyModel[578].addShapeBox(-0.5f, -2, -0.5f, 1, 1, 1, 0, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, -0.75f, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f);
		bodyModel[578].setRotationPoint(58.125f, -2.25f, 1.875f);
		bodyModel[578].rotateAngleZ = -1.1519173f;

		bodyModel[579] = new ModelRendererTurbo(this, 222, 112, textureX, textureY);
		bodyModel[579].addShapeBox(-0.5f, -2.25f, -0.5f, 1, 1, 1, 0, -0.3125f, -0.875f, -0.375f, -0.3125f, -0.875f, -0.375f, -0.3125f, -0.875f, -0.375f, -0.3125f, -0.875f, -0.375f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f);
		bodyModel[579].setRotationPoint(58.125f, -2.25f, 1.875f);
		bodyModel[579].rotateAngleZ = -1.1519173f;

		bodyModel[580] = new ModelRendererTurbo(this, 452, 138, textureX, textureY);
		bodyModel[580].addShapeBox(0, 0, 0, 5, 4, 8, 0, 0, 0, 0.75f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.75f, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[580].setRotationPoint(55.875f, -1.375f, -9.0f);

		bodyModel[581] = new ModelRendererTurbo(this, 455, 163, textureX, textureY);
		bodyModel[581].addShapeBox(0, 0, 0, 3, 5, 2, 0, 0, 0, 0, 10.5f, 0, 0, 0, 0, 0, 0, 0, 0.375f, 0, 0, 0, 10.5f, 0, 0, 0, 0, 0, 0, 0, 0.375f);
		bodyModel[581].setRotationPoint(60.875f, -2.375f, 7.0f);

		bodyModel[582] = new ModelRendererTurbo(this, 0, 104, textureX, textureY);
		bodyModel[582].addShapeBox(0, 0, 0, 5, 2, 4, 0, 2, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0, 0, 0, 2.25f, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, 0, 0.25f, -0.5f, 0);
		bodyModel[582].setRotationPoint(60.875f, -3.875f, -2.8125f);

		bodyModel[583] = new ModelRendererTurbo(this, 295, 114, textureX, textureY);
		bodyModel[583].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 0, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, 0.25f, 0, 0, 0, -0.5f, 0, -0.0625f, -0.5f, 0, -0.0625f, -0.5f, 0, 0.25f, -0.5f, 0);
		bodyModel[583].setRotationPoint(59.875f, -3.875f, -4.8125f);

		bodyModel[584] = new ModelRendererTurbo(this, 431, 98, textureX, textureY);
		bodyModel[584].addShapeBox(0, 0, 0, 5, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1.25f, 0, 0);
		bodyModel[584].setRotationPoint(55.875f, -1.375f, -1.0f);

		bodyModel[585] = new ModelRendererTurbo(this, 401, 111, textureX, textureY);
		bodyModel[585].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[585].setRotationPoint(67.5f, 4.0f, -6.01f);

		bodyModel[586] = new ModelRendererTurbo(this, 295, 114, textureX, textureY);
		bodyModel[586].addShapeBox(0, 0, 0, 4, 1, 16, 0, 0, 0, 0.625f, 0.1875f, 0.0795f, 0.43064f, 0.1875f, 0.0795f, -0.06937f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, 0.1875f, -0.25f, -0.1875f, 0.1875f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[586].setRotationPoint(46.375f, 7.0625f, -7.75f);

		bodyModel[587] = new ModelRendererTurbo(this, 322, 121, textureX, textureY);
		bodyModel[587].addShapeBox(0, 0, 0, 4, 1, 14, 0, 0, 0, 0.8125f, 0.1875f, 0, 0.8125f, 0.1875f, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, 0.1875f, -0.25f, -0.0625f, 0.1875f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[587].setRotationPoint(46.375f, 7.8125f, -6.75f);

		bodyModel[588] = new ModelRendererTurbo(this, 472, 53, textureX, textureY);
		bodyModel[588].addShapeBox(0, 0, 0, 17, 1, 1, 0, -0.3125f, -0.171875f, 0.125f, -0.25f, -0.234375f, 0.625f, -0.25f, 0.390625f, -1.140625f, -0.3125f, -0.171875f, -0.640625f, -0.3125f, 0.125f, 0.1875f, -0.25f, 0.125f, 0.9375f, -0.25f, 0.125f, 0.15625f, -0.3125f, 0.125f, 0.15625f);
		bodyModel[588].setRotationPoint(52.5f, 5.25f, 8.4375f);

		bodyModel[589] = new ModelRendererTurbo(this, 501, 65, textureX, textureY);
		bodyModel[589].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -0.171875f, 0.125f, -0.5f, -0.234375f, 0.71875f, -0.5f, 0.390625f, -1.640625f, 0, 0.390625f, -1.140625f, 0, 0.125f, 0.1875f, -0.5f, -0.3125f, 0.9375f, -0.5f, -0.3125f, -0.875f, 0, 0.125f, 0.15625f);
		bodyModel[589].setRotationPoint(69.25f, 5.25f, 8.4375f);

		bodyModel[590] = new ModelRendererTurbo(this, 493, 32, textureX, textureY);
		bodyModel[590].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0.015625f, -0.21875f, -0.4375f, -0.015625f, 0.34375f, -0.4375f, -0.6875f, -1.0625f, 0, 0.390625f, -0.640625f, 0, -0.3125f, -0.0625f, -0.5f, -0.3125f, 0.1875f, -0.4375f, -0.3125f, -1.0625f, 0, -0.3125f, 0.125f);
		bodyModel[590].setRotationPoint(72.75f, 5.25f, 7.4375f);

		bodyModel[591] = new ModelRendererTurbo(this, 108, 124, textureX, textureY);
		bodyModel[591].addShapeBox(0, 0, 0, 5, 1, 1, 0, -0.3125f, -0.171875f, 1.53125f, -0.25f, -0.171875f, 0.125f, -0.25f, -0.171875f, -0.640625f, -0.3125f, -0.171875f, -1.6875f, -0.3125f, 0.6875f, 0.6875f, -0.25f, 0.125f, 0.1875f, -0.25f, 0.125f, 0.15625f, -0.3125f, 0.6875f, -1.6875f);
		bodyModel[591].setRotationPoint(48.0625f, 5.25f, 8.4375f);

		bodyModel[592] = new ModelRendererTurbo(this, 386, 65, textureX, textureY);
		bodyModel[592].addShapeBox(0, 0, 0, 17, 2, 2, 0, -0.3125f, 0.125f, 0.0625f, -0.25f, 0.125f, 0.0625f, -0.25f, 0.125f, 0.15625f, -0.3125f, 0.125f, 0.15625f, -0.3125f, -0.265625f, 0.0625f, -0.25f, -0.265625f, 0.0625f, -0.25f, -0.296875f, -0.78125f, -0.3125f, -0.296875f, -0.78125f);
		bodyModel[592].setRotationPoint(52.5f, 6.5f, 7.4375f);

		bodyModel[593] = new ModelRendererTurbo(this, 0, 137, textureX, textureY);
		bodyModel[593].addShapeBox(0, 0, 0, 4, 2, 2, 0, -0.3125f, 0.125f, 0.0625f, -0.1875f, 0.5625f, 0.0625f, -0.1875f, 0.5625f, -0.875f, -0.3125f, 0.125f, 0.15625f, -0.3125f, -0.265625f, 0.0625f, -0.1875f, -0.546875f, 0.75f, -0.1875f, -0.734375f, -1.8125f, -0.3125f, -0.296875f, -0.78125f);
		bodyModel[593].setRotationPoint(68.9375f, 6.5f, 7.4375f);

		bodyModel[594] = new ModelRendererTurbo(this, 458, 18, textureX, textureY);
		bodyModel[594].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -0.3125f, 0.0625f, -0.5f, -0.3125f, 0.1875f, -0.4375f, -0.3125f, -1.0625f, 0, -0.3125f, 0.125f, 0, 1.328125f, 0.75f, -0.09375f, 0.65625f, 1.296875f, -0.265625f, -0.09375f, -1.375f, 0, 1.140625f, -0.8125f);
		bodyModel[594].setRotationPoint(72.75f, 5.625f, 7.4375f);

		bodyModel[595] = new ModelRendererTurbo(this, 108, 121, textureX, textureY);
		bodyModel[595].addShapeBox(0, 0, 0, 5, 1, 1, 0, -0.3125f, -0.4375f, -1.6875f, -0.25f, 0.125f, 0.15625f, -0.25f, 0.125f, 1.0625f, -0.3125f, -0.4375f, 0.6875f, -0.3125f, 0.140625f, -2.09375f, -0.25f, 0.703125f, -0.78125f, -0.25f, 0.734375f, 1.0625f, -0.3125f, 0.140625f, 1.09375f);
		bodyModel[595].setRotationPoint(48.0625f, 6.5f, -9.4375f);

		bodyModel[596] = new ModelRendererTurbo(this, 222, 49, textureX, textureY);
		bodyModel[596].addShapeBox(0, 0, 0, 17, 2, 2, 0, -0.3125f, 0.125f, 0.21875f, -0.25f, 0.125f, 0.21875f, -0.25f, 0.125f, 0.0625f, -0.3125f, 0.125f, 0.0625f, -0.3125f, -0.265625f, 0.21875f, -0.25f, -0.265625f, 0.21875f, -0.25f, -0.640625f, 0.0625f, -0.3125f, -0.640625f, 0.0625f);
		bodyModel[596].setRotationPoint(52.5f, 6.5f, -7.15625f);

		bodyModel[597] = new ModelRendererTurbo(this, 183, 49, textureX, textureY);
		bodyModel[597].addShapeBox(0, 0, 0, 17, 2, 2, 0, -0.3125f, 0.125f, 0.0625f, -0.25f, 0.125f, 0.0625f, -0.25f, 0.125f, 0.21875f, -0.3125f, 0.125f, 0.21875f, -0.3125f, -0.640625f, 0.0625f, -0.25f, -0.640625f, 0.0625f, -0.25f, -0.265625f, 0.21875f, -0.3125f, -0.265625f, 0.21875f);
		bodyModel[597].setRotationPoint(52.5f, 6.5f, 5.15625f);

		bodyModel[598] = new ModelRendererTurbo(this, 401, 118, textureX, textureY);
		bodyModel[598].addShapeBox(0, 0, 0, 4, 2, 2, 0, -0.3125f, 0.125f, 0.21875f, -0.1875f, 0.125f, 0.21875f, -0.1875f, 0.125f, 0.0625f, -0.3125f, 0.125f, 0.0625f, -0.3125f, -0.265625f, 0.21875f, -0.1875f, -0.546875f, -0.46875f, -0.1875f, -0.65625f, 0.0625f, -0.3125f, -0.640625f, 0.0625f);
		bodyModel[598].setRotationPoint(68.9375f, 6.5f, -7.15625f);

		bodyModel[599] = new ModelRendererTurbo(this, 467, 39, textureX, textureY);
		bodyModel[599].addShapeBox(0, 0, 0, 2, 2, 2, 0, 0, 0.125f, 0.21875f, -0.1875f, 0.125f, -0.40625f, -0.1875f, 0.125f, 0.0625f, 0, 0.125f, 0.0625f, 0, -0.546875f, -0.46875f, -0.09375f, -1.21875f, -1.015625f, -0.1875f, -1.21875f, 0.0625f, 0, -0.65625f, 0.0625f);
		bodyModel[599].setRotationPoint(72.75f, 6.5f, -7.15625f);

		bodyModel[600] = new ModelRendererTurbo(this, 276, 114, textureX, textureY);
		bodyModel[600].addShapeBox(0, 0, 0, 4, 2, 2, 0, -0.3125f, 0.125f, 0.0625f, -0.1875f, 0.125f, 0.0625f, -0.1875f, 0.125f, 0.21875f, -0.3125f, 0.125f, 0.21875f, -0.3125f, -0.640625f, 0.0625f, -0.1875f, -0.65625f, 0.0625f, -0.1875f, -0.546875f, -0.46875f, -0.3125f, -0.265625f, 0.21875f);
		bodyModel[600].setRotationPoint(68.9375f, 6.5f, 5.15625f);

		bodyModel[601] = new ModelRendererTurbo(this, 503, 18, textureX, textureY);
		bodyModel[601].addShapeBox(0, 0, 0, 2, 2, 2, 0, 0, 0.125f, 0.0625f, -0.1875f, 0.125f, 0.0625f, -0.1875f, 0.125f, -0.40625f, 0, 0.125f, 0.21875f, 0, -0.65625f, 0.0625f, -0.1875f, -1.21875f, 0.0625f, -0.09375f, -1.21875f, -1.015625f, 0, -0.546875f, -0.46875f);
		bodyModel[601].setRotationPoint(72.75f, 6.5f, 5.15625f);

		bodyModel[602] = new ModelRendererTurbo(this, 431, 93, textureX, textureY);
		bodyModel[602].addShapeBox(0, 0, 0, 5, 2, 2, 0, -0.3125f, -0.4375f, 0.59375f, -0.25f, 0.125f, 0.21875f, -0.25f, 0.125f, 0.0625f, -0.3125f, 0.125f, 0.0625f, -0.3125f, -0.859375f, 0.1875f, -0.25f, -0.265625f, 0.21875f, -0.25f, -0.640625f, 0.0625f, -0.3125f, -0.671875f, 0.0625f);
		bodyModel[602].setRotationPoint(48.0625f, 6.5f, -7.15625f);

		bodyModel[603] = new ModelRendererTurbo(this, 314, 93, textureX, textureY);
		bodyModel[603].addShapeBox(0, 0, 0, 5, 2, 2, 0, -0.3125f, 0.125f, 0.0625f, -0.25f, 0.125f, 0.0625f, -0.25f, 0.125f, 0.21875f, -0.3125f, -0.4375f, 0.59375f, -0.3125f, -0.671875f, 0.0625f, -0.25f, -0.640625f, 0.0625f, -0.25f, -0.265625f, 0.21875f, -0.3125f, -0.859375f, 0.1875f);
		bodyModel[603].setRotationPoint(48.0625f, 6.5f, 5.15625f);

		bodyModel[604] = new ModelRendererTurbo(this, 245, 114, textureX, textureY);
		bodyModel[604].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[604].setRotationPoint(50.875f, 4.0f, -6.01f);

		bodyModel[605] = new ModelRendererTurbo(this, 108, 114, textureX, textureY);
		bodyModel[605].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[605].setRotationPoint(50.875f, 4.0f, 6.01f);

		bodyModel[606] = new ModelRendererTurbo(this, 228, 93, textureX, textureY);
		bodyModel[606].addShapeBox(0, 0, 0, 21, 1, 12, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0, -0.4375f, 0, -0.375f, -0.4375f, 0, -0.375f, -0.4375f, 0, 0, -0.4375f, 0);
		bodyModel[606].setRotationPoint(51.875f, 7.0f, -6.0f);

		bodyModel[607] = new ModelRendererTurbo(this, 245, 107, textureX, textureY);
		bodyModel[607].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[607].setRotationPoint(67.5f, 4.0f, 6.01f);

		bodyModel[608] = new ModelRendererTurbo(this, 196, 117, textureX, textureY);
		bodyModel[608].addShapeBox(0, 0, 0, 11, 1, 3, 0, 0, -0.0625f, 0, -0.5f, -0.8125f, 0, -0.5f, -0.90625f, -0.375f, 0, -0.15625f, -0.375f, 0, -0.4375f, 0, -0.5f, 0.3125f, 0, -0.5f, 0.40625f, -0.375f, 0, -0.34375f, -0.375f);
		bodyModel[608].setRotationPoint(39.25f, -11.46875f, 0.0f);

		bodyModel[609] = new ModelRendererTurbo(this, 216, 160, textureX, textureY);
		bodyModel[609].addShapeBox(0, 0, 0, 5, 1, 2, 0, 0, -0.0625f, 0, 0.1875f, -0.433f, 0, 0.75f, -1.296875f, 0, 0, -0.7143f, 0, 0, -0.4375f, 0, 0.1875f, -0.067f, 0, 0.75f, 0.796875f, 0, 0, 0.214237f, 0);
		bodyModel[609].setRotationPoint(44.5625f, -10.9955f, 2.625f);

		bodyModel[610] = new ModelRendererTurbo(this, 308, 159, textureX, textureY);
		bodyModel[610].addShapeBox(0, 0, 0, 6, 1, 2, 0, 0, -0.06184f, 0, -0.25f, -0.64434f, 0, -0.25f, -2.5625f, -0.125f, 0, -1.555f, -0.125f, 0, -0.4383f, 0, -0.25f, 0.14435f, 0, -0.25f, 2.0625f, -0.125f, 0, 1.05491f, -0.125f);
		bodyModel[610].setRotationPoint(44.5625f, -10.343f, 4.625f);

		bodyModel[611] = new ModelRendererTurbo(this, 404, 147, textureX, textureY);
		bodyModel[611].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, 0.5625f, 0.375f, -0.25f, -0.3984375f, 0.6875f, -0.25f, -0.3984375f, -1.1875f, 0, 0.5625f, -0.875f, 0, 0.25f, -0.65625f, -0.25f, 1.5f, -0.4375f, -0.25f, 1.5f, -0.125f, 0, 0.25f, 0.15625f);
		bodyModel[611].setRotationPoint(44.5625f, -6.625f, 8.9375f);

		bodyModel[612] = new ModelRendererTurbo(this, 385, 147, textureX, textureY);
		bodyModel[612].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.375f, 0.0625f, -0.25f, -1.625f, 0.28125f, -0.25f, -1.625f, -0.84375f, 0, -0.375f, -0.5625f, 0, -0.25f, -0.375f, -0.25f, 1.15625f, -0.3125f, -0.25f, 1.15625f, -0.1875f, 0, -0.25f, -0.125f);
		bodyModel[612].setRotationPoint(44.5625f, -5.75f, 9.65625f);

		bodyModel[613] = new ModelRendererTurbo(this, 195, 147, textureX, textureY);
		bodyModel[613].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, -0.375f, 0.0625f, -0.25f, -1.78125f, 0.125f, -0.25f, -1.78125f, -0.625f, 0, -0.375f, -0.5625f, 0, 0.375f, -0.28125f, -0.25f, 0.78125f, 0.125f, -0.25f, 0.78125f, -0.625f, 0, 0.375f, -0.21875f);
		bodyModel[613].setRotationPoint(44.5625f, -4.375f, 10.09375f);

		bodyModel[614] = new ModelRendererTurbo(this, 312, 148, textureX, textureY);
		bodyModel[614].addShapeBox(0, 0, 0, 6, 3, 1, 0, 0, 0, 0, -0.25f, -0.40625f, 0.40625f, -0.25f, -0.40625f, -0.90625f, 0, 0, -0.5f, 0, 0, 0.125f, -0.25f, 0.375f, 0.375f, -0.25f, 0.375f, -0.875f, 0, 0, -0.625f);
		bodyModel[614].setRotationPoint(44.5625f, -3.0f, 10.375f);

		bodyModel[615] = new ModelRendererTurbo(this, 105, 147, textureX, textureY);
		bodyModel[615].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 0, 0.125f, -0.25f, -0.375f, 0.375f, -0.25f, -0.375f, -0.875f, 0, 0, -0.625f, 0, -0.25f, 0.5f, -0.25f, 0.1875f, 0.75f, -0.25f, 0.1875f, -1.25f, 0, -0.25f, -1);
		bodyModel[615].setRotationPoint(44.5625f, 0.0f, 10.375f);

		bodyModel[616] = new ModelRendererTurbo(this, 482, 144, textureX, textureY);
		bodyModel[616].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, 0, 0, -0.25f, -0.4375f, 0.25f, -0.25f, -0.4375f, -0.75f, 0, 0, -0.5f, 0, -0.25f, 0, -0.25f, -0.25f, 0.25f, -0.25f, -0.25f, -0.75f, 0, -0.25f, -0.5f);
		bodyModel[616].setRotationPoint(44.5625f, 1.75f, 9.875f);

		bodyModel[617] = new ModelRendererTurbo(this, 135, 151, textureX, textureY);
		bodyModel[617].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.4375f, 0, -0.25f, -0.5f, 0.4375f, -0.25f, -0.5f, -0.9375f, 0, -0.4375f, -0.5f, 0, -0.25f, 0, -0.25f, -0.25f, 0.4375f, -0.25f, -0.25f, -0.9375f, 0, -0.25f, -0.5f);
		bodyModel[617].setRotationPoint(50.3125f, 1.75f, 9.625f);

		bodyModel[618] = new ModelRendererTurbo(this, 237, 142, textureX, textureY);
		bodyModel[618].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, -0.5f, 0, 0.3125f, -0.5625f, 0.3125f, 0.3125f, -0.5625f, -0.8125f, 0, -0.5f, -0.5f, 0, -0.25f, 0, 0.3125f, -0.25f, 0.3125f, 0.3125f, -0.25f, -0.8125f, 0, -0.25f, -0.5f);
		bodyModel[618].setRotationPoint(58.0625f, 1.75f, 9.1875f);

		bodyModel[619] = new ModelRendererTurbo(this, 108, 130, textureX, textureY);
		bodyModel[619].addShapeBox(0, 0, 0, 12, 1, 1, 0, 0, -0.5625f, 0, -0.375f, -0.625f, 0.625f, -0.375f, -0.625f, -1.125f, 0, -0.5625f, -0.5f, 0, -0.25f, 0, -0.375f, -0.25f, 0.625f, -0.375f, -0.25f, -1.125f, 0, -0.25f, -0.5f);
		bodyModel[619].setRotationPoint(64.375f, 1.75f, 8.875f);

		bodyModel[620] = new ModelRendererTurbo(this, 274, 146, textureX, textureY);
		bodyModel[620].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, -0.1875f, 0.125f, -0.25f, -0.25f, 0.625f, -0.25f, -0.25f, -1.125f, 0, -0.1875f, -0.625f, 0, 0, 0.5f, -0.25f, 0.0625f, 0.9375f, -0.25f, 0.0625f, -1.4375f, 0, 0, -1);
		bodyModel[620].setRotationPoint(50.3125f, 0.1875f, 10.125f);

		bodyModel[621] = new ModelRendererTurbo(this, 385, 143, textureX, textureY);
		bodyModel[621].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.1875f, 0.125f, 0.3125f, -0.25f, 0.625f, 0.3125f, -0.25f, -1.125f, 0, -0.1875f, -0.625f, 0, 0, 0.4375f, 0.3125f, 0.0625f, 0.75f, 0.3125f, 0.0625f, -1.25f, 0, 0, -0.9375f);
		bodyModel[621].setRotationPoint(58.0625f, 0.25f, 9.625f);

		bodyModel[622] = new ModelRendererTurbo(this, 356, 145, textureX, textureY);
		bodyModel[622].addShapeBox(0, 0, 0, 8, 3, 1, 0, 0, 0, 0, -0.25f, -0.8125f, 0.46875f, -0.25f, -0.8125f, -0.96875f, 0, 0, -0.5f, 0, -0.03125f, -0.03125f, -0.25f, 0.03125f, 0.46875f, -0.25f, 0.03125f, -0.96875f, 0, -0.03125f, -0.46875f);
		bodyModel[622].setRotationPoint(50.3125f, -2.59375f, 9.96875f);

		bodyModel[623] = new ModelRendererTurbo(this, 67, 138, textureX, textureY);
		bodyModel[623].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 0, 0, 0.3125f, -0.625f, 0.375f, 0.3125f, -0.625f, -0.875f, 0, 0, -0.5f, 0, 0.21875f, 0, 0.3125f, 0.28125f, 0.5f, 0.3125f, 0.28125f, -1, 0, 0.21875f, -0.5f);
		bodyModel[623].setRotationPoint(58.0625f, -1.78125f, 9.5f);

		bodyModel[624] = new ModelRendererTurbo(this, 301, 142, textureX, textureY);
		bodyModel[624].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.53125f, 0.3125f, -0.25f, -1.15625f, 0.4375f, -0.25f, -1.15625f, -0.9375f, 0, 0.53125f, -0.875f, 0, 0, -0.28125f, -0.25f, 0.8125f, 0.1875f, -0.25f, 0.8125f, -0.6875f, 0, 0, -0.21875f);
		bodyModel[624].setRotationPoint(50.3125f, -3.59375f, 9.6875f);

		bodyModel[625] = new ModelRendererTurbo(this, 131, 125, textureX, textureY);
		bodyModel[625].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, -0.15625f, 0.3125f, 0.3125f, -1.4375f, 0.4375f, 0.3125f, -1.4375f, -0.9375f, 0, -0.15625f, -0.8125f, 0, -0.1875f, 0.0625f, 0.3125f, 0.4375f, 0.4375f, 0.3125f, 0.4375f, -0.9375f, 0, -0.1875f, -0.5625f);
		bodyModel[625].setRotationPoint(58.0625f, -2.59375f, 9.5625f);

		bodyModel[626] = new ModelRendererTurbo(this, 440, 139, textureX, textureY);
		bodyModel[626].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0.125f, 1.5f, -0.25f, -1.3828125f, 1.6875f, -0.25f, -1.3828125f, -2.1875f, 0, 0.125f, -2, 0, -0.0234375f, 0.375f, -0.25f, 1.6640625f, 0.5f, -0.25f, 1.6640625f, -1, 0, -0.0234375f, -0.9375f);
		bodyModel[626].setRotationPoint(50.3125f, -6.1015625f, 9.75f);

		bodyModel[627] = new ModelRendererTurbo(this, 419, 109, textureX, textureY);
		bodyModel[627].addShapeBox(0, 0, 0, 12, 1, 1, 0, 0, 0.375f, 0, -0.375f, 0, 1.0625f, -0.375f, 0, -1.5625f, 0, 0.375f, -0.5f, 0, 0.28125f, 0.125f, -0.375f, 0.34375f, 0.9375f, -0.375f, 0.34375f, -1.4375f, 0, 0.28125f, -0.625f);
		bodyModel[627].setRotationPoint(64.375f, -0.78125f, 9.125f);

		bodyModel[628] = new ModelRendererTurbo(this, 151, 114, textureX, textureY);
		bodyModel[628].addShapeBox(0, 0, 0, 12, 2, 1, 0, 0, 0.15625f, 1.4375f, -0.375f, -1.9375f, 1.9375f, -0.375f, -1.9375f, -2.4375f, 0, 0.15625f, -1.9375f, 0, 0.28125f, 0.125f, -0.375f, 0.65625f, 1.1875f, -0.375f, 0.65625f, -1.6875f, 0, 0.28125f, -0.625f);
		bodyModel[628].setRotationPoint(64.375f, -3.4375f, 9.25f);

		bodyModel[629] = new ModelRendererTurbo(this, 172, 179, textureX, textureY);
		bodyModel[629].addShapeBox(0, 0, 0, 8, 2, 2, 0, 0, -0.5625f, 0.5625f, -0.25f, -2.1953125f, 0.6875f, -0.25f, -1.6953125f, -2.6875f, 0, -0.0625f, -2.5625f, 0, -0.3828125f, -1.1875f, -0.25f, 1.125f, -1, -0.25f, 1.125f, -0.5f, 0, -0.3828125f, -0.3125f);
		bodyModel[629].setRotationPoint(50.3125f, -7.84375f, 7.0625f);

		bodyModel[630] = new ModelRendererTurbo(this, 320, 182, textureX, textureY);
		bodyModel[630].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.5625f, 0.5625f, 0.3125f, -1.8203125f, 0.6875f, 0.3125f, -1.3203125f, -1.6875f, 0, -0.0625f, -1.5625f, 0, -0.5078125f, -1.125f, 0.3125f, 0.6171875f, -0.875f, 0.3125f, 0.6171875f, 0.375f, 0, -0.5078125f, 0.625f);
		bodyModel[630].setRotationPoint(58.0625f, -6.2109375f, 6.9375f);

		bodyModel[631] = new ModelRendererTurbo(this, 349, 186, textureX, textureY);
		bodyModel[631].addShapeBox(0, 0, 0, 12, 2, 1, 0, 0, -0.5625f, 0.5625f, -0.375f, -2.6953125f, 0.6875f, -0.375f, -2.1953125f, -1.6875f, 0, -0.0625f, -1.5625f, 0, -0.640625f, -1, -0.375f, 1.453125f, -0.5f, -0.375f, 1.453125f, 0, 0, -0.640625f, 0.5f);
		bodyModel[631].setRotationPoint(64.375f, -4.953125f, 6.8125f);

		bodyModel[632] = new ModelRendererTurbo(this, 485, 78, textureX, textureY);
		bodyModel[632].addShapeBox(0, 0, 0, 12, 2, 1, 0, 0, -0.1875f, 0.125f, -0.375f, -0.25f, 0.9375f, -0.375f, -0.25f, -1.4375f, 0, -0.1875f, -0.625f, 0, 0, 0.25f, -0.375f, 0.0625f, 0.875f, -0.375f, 0.0625f, -1.375f, 0, 0, -0.75f);
		bodyModel[632].setRotationPoint(64.375f, 0.3125f, 9.125f);

		bodyModel[633] = new ModelRendererTurbo(this, 301, 145, textureX, textureY);
		bodyModel[633].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, -0.64434f, 0, -0.5f, -1.6875f, -0.875f, -0.5f, -2.667f, -0.1335f, 0, -2.5624f, -0.125f, 0, 0.14434001f, 0, -0.5f, 1.1875f, -0.875f, -0.5f, 2.167f, -0.1335f, 0, 2.0625f, -0.125f);
		bodyModel[633].setRotationPoint(50.3125f, -10.343f, 4.625f);

		bodyModel[634] = new ModelRendererTurbo(this, 432, 157, textureX, textureY);
		bodyModel[634].addShapeBox(0.5f, 2, 0, 3, 1, 1, 0, 0, 0.3125f, 0.125f, -0.5f, -1.1939f, -0.82664f, -0.5f, -1.1939f, -0.17336f, 0, -0.6672f, -0.1335f, 0, -0.8125f, 0.125f, -0.5f, 0.6939f, -0.82664f, -0.5f, 0.6939f, -0.17336f, 0, 0.167f, -0.1335f);
		bodyModel[634].setRotationPoint(50.3125f, -10.343f, 5.625f);

		bodyModel[635] = new ModelRendererTurbo(this, 156, 180, textureX, textureY);
		bodyModel[635].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.5625f, 0.5625f, -0.25f, -1.5703125f, 0.5625f, -0.25f, -1.0703125f, -1.5625f, 0, -0.0605f, -1.5625f, 0, -0.3359375f, -1.5f, -0.25f, 0.625f, -1.1875f, -0.25f, 0.625f, 0.6875f, 0, -0.3359375f, 1);
		bodyModel[635].setRotationPoint(44.5625f, -8.8515625f, 7.0625f);

		bodyModel[636] = new ModelRendererTurbo(this, 498, 71, textureX, textureY);
		bodyModel[636].addShapeBox(0, 0, 0, 5, 1, 1, 0, -0.3125f, -0.4375f, 0.6875f, -0.25f, 0.125f, 1.0625f, -0.25f, 0.125f, 0.15625f, -0.3125f, -0.4375f, -1.6875f, -0.3125f, 0.140625f, 1.09375f, -0.25f, 0.734375f, 1.0625f, -0.25f, 0.703125f, -0.78125f, -0.3125f, 0.140625f, -2.09375f);
		bodyModel[636].setRotationPoint(48.0625f, 6.5f, 8.4375f);

		bodyModel[637] = new ModelRendererTurbo(this, 221, 177, textureX, textureY);
		bodyModel[637].addShapeBox(0, 0, 0, 1, 1, 3, 0, 0, -0.84765625f, 0, 0, -0.92578125f, 0, 0, -1.0117188f, -0.375f, 0, -0.9394531f, -0.375f, 0, -0.05859375f, 0, 0, 0.01171875f, 0, 0, 0.10546875f, -0.375f, 0, 0.03320312f, -0.375f);
		bodyModel[637].setRotationPoint(45.75f, -11.8828125f, 0.0f);

		bodyModel[638] = new ModelRendererTurbo(this, 212, 180, textureX, textureY);
		bodyModel[638].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, -0.84375f, 0, 0, -0.9160156f, 0, 0.5625f, -1.6914062f, 0, -0.5625f, -1.5898438f, 0, 0, -0.0625f, 0, 0, 0.01171875f, 0, 0.5625f, 0.8125f, 0, -0.5625f, 0.7109375f, 0);
		bodyModel[638].setRotationPoint(45.75f, -11.787109f, 2.625f);

		bodyModel[639] = new ModelRendererTurbo(this, 193, 182, textureX, textureY);
		bodyModel[639].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -0.8125f, 0, 0, -0.9140625f, 0, 0.5f, -1.8632812f, -0.125f, -0.5f, -1.7480469f, -0.125f, 0, -0.09375f, 0, 0, 0.0078125f, 0, 0.5f, 0.9570312f, -0.125f, -0.5f, 0.8417969f, -0.125f);
		bodyModel[639].setRotationPoint(46.3125f, -11.009766f, 4.625f);

		bodyModel[640] = new ModelRendererTurbo(this, 403, 178, textureX, textureY);
		bodyModel[640].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.22851562f, -0.109375f, -0.25f, -1.4042969f, 0.01562499f, -0.25f, -2.0566406f, -0.015625f, 0, -0.38476562f, 0, 0, -1.1289062f, -0.109375f, -0.25f, 0.7480469f, 0.01562499f, -0.25f, 1.2363281f, -0.015625f, 0, -0.51171875f, 0);
		bodyModel[640].setRotationPoint(50.3125f, -7.6445312f, 6.390625f);

		bodyModel[641] = new ModelRendererTurbo(this, 336, 182, textureX, textureY);
		bodyModel[641].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.58984375f, -0.0625f, -0.5f, -1.8632812f, -1.0625f, -0.5f, -2.4765625f, 0.953125f, 0, -1.4238281f, 0.0625f, 0, -0.31640625f, -0.0625f, -0.5f, 0.9570312f, -1.0625f, -0.5f, 1.5703125f, 0.953125f, 0, 0.5175781f, 0.0625f);
		bodyModel[641].setRotationPoint(47.8125f, -9.736328f, 5.4375f);

		bodyModel[642] = new ModelRendererTurbo(this, 209, 178, textureX, textureY);
		bodyModel[642].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -0.875f, 0, 0, -0.9902344f, 0, 0, -1.8242188f, 0, -1, -1.8242188f, 0, 0, -0.03125f, 0, 0, 0.08398438f, 0, 0, 0.91796875f, 0, -1, 0.91796875f, 0);
		bodyModel[642].setRotationPoint(46.8125f, -10.136719f, 5.5f);

		bodyModel[643] = new ModelRendererTurbo(this, 395, 181, textureX, textureY);
		bodyModel[643].addShapeBox(0, 0, 0, 7, 1, 1, 0, 0, 0.00390625f, 0, -0.1875f, -1.2480469f, 0.07226562f, -0.1875f, -1.9902344f, -0.07226562f, 0, -0.625f, 0, 0, -0.50390625f, -0.0625f, -0.1875f, 0.7480469f, 0.00976562f, -0.1875f, 1.2402344f, -0.07226562f, 0, -0.1875f, 0);
		bodyModel[643].setRotationPoint(64.375f, -4.9761877f, 6.25f);

		bodyModel[644] = new ModelRendererTurbo(this, 384, 182, textureX, textureY);
		bodyModel[644].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.00390625f, 0, 0.09375f, -0.29101562f, 0.9472656f, 0.5234375f, -0.8222656f, -1.0449219f, 0, -0.73828125f, 0, 0, -0.50390625f, -0.0625f, 0.09375f, -0.14648438f, 0.8847656f, 0.5234375f, 0.32226562f, -1.0449219f, 0, -0.01171875f, 0);
		bodyModel[644].setRotationPoint(71.1875f, -3.7242346f, 6.1777344f);

		bodyModel[645] = new ModelRendererTurbo(this, 358, 182, textureX, textureY);
		bodyModel[645].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, 0.00390625f, 0, 0.3125f, -1.2578125f, 0.125f, 0.3125f, -1.8886719f, -0.12304687f, 0, -0.6484375f, 0, 0, -0.50390625f, -0.0625f, 0.3125f, 0.7480469f, 0.12304688f, 0.3125f, 1.2402344f, -0.125f, 0, -0.1875f, 0);
		bodyModel[645].setRotationPoint(58.0625f, -6.2379065f, 6.375f);

		bodyModel[646] = new ModelRendererTurbo(this, 432, 160, textureX, textureY);
		bodyModel[646].addShapeBox(0, 0, 0, 5, 1, 2, 0, 0, -0.0625f, 0, 0.3125f, -0.442f, 0, 0.3125f, -1.09375f, 0, 0, -0.71184f, 0, 0, -0.4375f, 0, 0.3125f, -0.058f, 0, 0.3125f, 0.59375f, 0, 0, 0.21186f, 0);
		bodyModel[646].setRotationPoint(39.25f, -11.375f, 2.625f);

		bodyModel[647] = new ModelRendererTurbo(this, 325, 159, textureX, textureY);
		bodyModel[647].addShapeBox(0, 0, 0, 5, 1, 3, 0, 0, -0.0625f, 0, 0.3125f, -0.4444f, 0, 0.3125f, -1.9375f, -1.125f, 0, -0.9756f, -0.1875f, 0, -0.4375f, 0, 0.3125f, -0.0556f, 0, 0.3125f, 1.4375f, -1.125f, 0, 0.4756f, -0.1875f);
		bodyModel[647].setRotationPoint(39.25f, -10.7256f, 4.625f);

		bodyModel[648] = new ModelRendererTurbo(this, 239, 179, textureX, textureY);
		bodyModel[648].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, -0.5625f, 0.5625f, 0.3125f, -1.5234375f, 1.5f, 0.3125f, -1.0214844f, -2.5f, 0, -0.0625f, -1.5625f, 0, -0.375f, -0.875f, 0.3125f, 0.625f, -0.5625f, 0.3125f, 0.625f, 0.0625f, 0, -0.375f, 0.375f);
		bodyModel[648].setRotationPoint(39.25f, -9.8125f, 8.0f);

		bodyModel[649] = new ModelRendererTurbo(this, 268, 166, textureX, textureY);
		bodyModel[649].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, -0.375f, 0.0625f, 0.3125f, -1.625f, 0.25f, 0.3125f, -1.625f, -0.75f, 0, -0.375f, -0.5625f, 0, 0, -0.28125f, 0.3125f, 1, -0.1875f, 0.3125f, 1, -0.3125f, 0, 0, -0.21875f);
		bodyModel[649].setRotationPoint(39.25f, -7.0f, 9.84375f);

		bodyModel[650] = new ModelRendererTurbo(this, 162, 166, textureX, textureY);
		bodyModel[650].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, -0.375f, 0.0625f, 0.3125f, -1.375f, 0.15625f, 0.3125f, -1.375f, -0.65625f, 0, -0.375f, -0.5625f, 0, 0.375f, -0.3125f, 0.3125f, 0.375f, -0.1875f, 0.3125f, 0.375f, -0.3125f, 0, 0.375f, -0.1875f);
		bodyModel[650].setRotationPoint(39.25f, -5.375f, 10.1875f);

		bodyModel[651] = new ModelRendererTurbo(this, 70, 165, textureX, textureY);
		bodyModel[651].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, -0.5f, 0.3125f, 0, -0.375f, 0.3125f, 0, -0.125f, 0, 0, 0, 0, 0, -0.25f, 0.3125f, 0, -0.25f, 0.3125f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[651].setRotationPoint(39.25f, -3.0f, 10.0f);

		bodyModel[652] = new ModelRendererTurbo(this, 42, 165, textureX, textureY);
		bodyModel[652].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.5f, 0.3125f, 0, -0.5f, 0.3125f, 0, 0, 0, 0, 0, 0, -0.25f, -0.125f, 0.3125f, -0.25f, -0.125f, 0.3125f, -0.25f, -0.375f, 0, -0.25f, -0.375f);
		bodyModel[652].setRotationPoint(39.25f, 0.0f, 9.75f);

		bodyModel[653] = new ModelRendererTurbo(this, 105, 144, textureX, textureY);
		bodyModel[653].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, 0, 0, -0.1875f, 0, 0, -0.1875f, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, -0.1875f, -0.25f, 0, -0.1875f, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[653].setRotationPoint(38.75f, 1.75f, 9.875f);

		bodyModel[654] = new ModelRendererTurbo(this, 423, 143, textureX, textureY);
		bodyModel[654].addShapeBox(0, 0, 0, 5, 1, 1, 0, 0, 0.5625f, 0.40625f, 0.3125f, -0.4375f, 0.71875f, 0.3125f, -0.4375f, -1.21875f, 0, 0.5625f, -0.90625f, 0, 0, -0.5f, 0.3125f, 1.25f, -0.3125f, 0.3125f, 1.25f, -0.1875f, 0, 0, 0);
		bodyModel[654].setRotationPoint(39.25f, -7.625f, 9.28125f);

		bodyModel[655] = new ModelRendererTurbo(this, 349, 99, textureX, textureY);
		bodyModel[655].addShapeBox(0, 0, 0, 14, 2, 1, 0, 0, 0, 0.5625f, 0, -0.375f, 0.5f, 0, -0.375f, -1, 0, 0, -1.0625f, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[655].setRotationPoint(2.75f, -10.75f, 8.9375f);

		bodyModel[656] = new ModelRendererTurbo(this, 349, 96, textureX, textureY);
		bodyModel[656].addShapeBox(0, 0, 0, 14, 1, 1, 0, 0, 0.3125f, 1.0625f, 0, -0.5f, 0.625f, 0, 0, -1.625f, 0, 0.8125f, -2.0625f, 0, -0.125f, -0.3125f, 0, 0.25f, -0.375f, 0, 0.25f, -0.125f, 0, -0.125f, -0.1875f);
		bodyModel[656].setRotationPoint(2.75f, -11.625f, 8.0625f);

		bodyModel[657] = new ModelRendererTurbo(this, 29, 118, textureX, textureY);
		bodyModel[657].addShapeBox(0, 0, 0, 14, 1, 5, 0, 0, 0, 0, 0, -0.625f, 0, 0, -2.25f, 0.1875f, 0, -1.4375f, -0.25f, 0, -0.5f, 0, 0, 0.125f, 0, 0, 1.75f, 0.1875f, 0, 0.9375f, -0.25f);
		bodyModel[657].setRotationPoint(2.75f, -13.875f, 2.25f);

		bodyModel[658] = new ModelRendererTurbo(this, 394, 135, textureX, textureY);
		bodyModel[658].addShapeBox(0, 0, 0, 14, 1, 2, 0, 0, -0.125f, 0, 0, -0.625f, 0, 0, -1, 0.25f, 0, -0.375f, 0.25f, 0, -0.5f, 0, 0, 0.125f, 0, 0, 0.5f, 0.25f, 0, -0.125f, 0.25f);
		bodyModel[658].setRotationPoint(2.75f, -14.25f, 0.0f);

		bodyModel[659] = new ModelRendererTurbo(this, 261, 49, textureX, textureY);
		bodyModel[659].addShapeBox(0, 0, 0, 8, 1, 3, 0, 0, -0.0625f, 0, 0.3125f, -0.8125f, 0, 0.3125f, -0.96875f, -0.375f, 0, -0.15625f, -0.375f, 0, -0.4375f, 0, 0.3125f, 0.3125f, 0, 0.3125f, 0.46875f, -0.375f, 0, -0.34375f, -0.375f);
		bodyModel[659].setRotationPoint(49.75f, -10.71875f, 0.0f);

		bodyModel[660] = new ModelRendererTurbo(this, 157, 151, textureX, textureY);
		bodyModel[660].addShapeBox(0, 0, 0, 1, 1, 3, 0, 0, 0, 0, -0.25f, -0.1875f, 0, -0.25f, -0.28125f, -0.375f, 0, -0.15625f, -0.375f, 0, -0.5f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.21875f, -0.375f, 0, -0.34375f, -0.375f);
		bodyModel[660].setRotationPoint(58.0625f, -9.90625f, 0.0f);

		bodyModel[661] = new ModelRendererTurbo(this, 440, 143, textureX, textureY);
		bodyModel[661].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, -0.25f, -0.125f, 0, -0.25f, -1.25f, 0.0625f, 0, -1.125f, 0.0625f, 0, -0.5f, 0, -0.25f, -0.375f, 0, -0.25f, 0.6875f, 0.0625f, 0, 0.625f, 0.0625f);
		bodyModel[661].setRotationPoint(58.0625f, -9.75f, 2.625f);

		bodyModel[662] = new ModelRendererTurbo(this, 44, 130, textureX, textureY);
		bodyModel[662].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, -0.25f, -0.125f, 0, -0.25f, -2.6265f, -0.328125f, 0, -2.4765625f, -0.3125f, 0, -0.5f, 0, -0.25f, -0.4375f, 0, -0.25f, 2.127f, -0.328125f, 0, 1.9765625f, -0.3125f);
		bodyModel[662].setRotationPoint(58.0625f, -8.625f, 4.6875f);

		bodyModel[663] = new ModelRendererTurbo(this, 0, 31, textureX, textureY);
		bodyModel[663].addShapeBox(0, 0, 0, 8, 0, 2, 0, 0, 0.6875f, 0, 0.3125f, -0.125f, 0, 0.3125f, -1.25f, 0.0625f, -0.5625f, -0.1796875f, 0, 0, -0.6875f, 0, 0.3125f, 0.125f, 0, 0.3125f, 1.25f, 0.0625f, -0.5625f, 0.1796875f, 0);
		bodyModel[663].setRotationPoint(49.75f, -9.625f, 2.625f);

		bodyModel[664] = new ModelRendererTurbo(this, 0, 28, textureX, textureY);
		bodyModel[664].addShapeBox(0, 0, 0, 8, 0, 2, 0, -0.5625f, 0, 0, 0.3125f, -1.0703125f, -0.0625f, 0.3125f, -3.546875f, -0.25f, -0.5625f, -1.9140625f, -0.125f, -0.5625f, 0, 0, 0.3125f, 1.0703125f, -0.0625f, 0.3125f, 3.546875f, -0.25f, -0.5625f, 1.9140625f, -0.125f);
		bodyModel[664].setRotationPoint(49.75f, -9.4453125f, 4.625f);

		bodyModel[665] = new ModelRendererTurbo(this, 472, 158, textureX, textureY);
		bodyModel[665].addShapeBox(0, 0, 0, 5, 0, 3, 0, 0, 0.75f, 0, 0, -0.375f, 0, 0, -0.40625f, -0.375f, 0, 0.65625f, -0.375f, 0, -0.75f, 0, 0, 0.375f, 0, 0, 0.40625f, -0.375f, 0, -0.65625f, -0.375f);
		bodyModel[665].setRotationPoint(58.8125f, -8.71875f, 0.0f);

		bodyModel[666] = new ModelRendererTurbo(this, 497, 13, textureX, textureY);
		bodyModel[666].addShapeBox(0, 0, 0, 5, 0, 2, 0, 0, 0.75f, 0, 0, -0.3125f, 0, 0, -1.28125f, 0, 0, -0.34375f, 0.0625f, 0, -0.75f, 0, 0, 0.3125f, 0, 0, 1.28125f, 0, 0, 0.34375f, 0.0625f);
		bodyModel[666].setRotationPoint(58.8125f, -8.625f, 2.625f);

		bodyModel[667] = new ModelRendererTurbo(this, 497, 10, textureX, textureY);
		bodyModel[667].addShapeBox(0, 0, 0, 5, 0, 2, 0, 0, -0.1875f, 0, 0, -1.125f, 0.0625f, 0, -3.72025f, -0.421875f, 0, -2.7578125f, -0.3203125f, 0, 0.1875f, 0, 0, 1.125f, 0.0625f, 0, 3.72075f, -0.421875f, 0, 2.7578125f, -0.3203125f);
		bodyModel[667].setRotationPoint(58.8125f, -8.46875f, 4.6875f);

		bodyModel[668] = new ModelRendererTurbo(this, 0, 8, textureX, textureY);
		bodyModel[668].addShapeBox(0, 0, 0, 7, 0, 3, 0, 0, 0.75f, 0, 0.375f, -2.03125f, 0, 0.375f, -2.0625f, -0.375f, 0, 0.71875f, -0.375f, 0, -0.75f, 0, 0.375f, 2.03125f, 0, 0.375f, 2.0625f, -0.375f, 0, -0.71875f, -0.375f);
		bodyModel[668].setRotationPoint(63.8125f, -7.59375f, 0.0f);

		bodyModel[669] = new ModelRendererTurbo(this, 455, 87, textureX, textureY);
		bodyModel[669].addShapeBox(0, 0, 0, 7, 0, 2, 0, 0, 0.75f, 0, 0.375f, -2.03125f, 0, 0.375f, -2.4375f, -0.625f, 0, -0.21875f, 0, 0, -0.75f, 0, 0.375f, 2.03125f, 0, 0.375f, 2.4375f, -0.625f, 0, 0.21875f, 0);
		bodyModel[669].setRotationPoint(63.8125f, -7.5625f, 2.625f);

		bodyModel[670] = new ModelRendererTurbo(this, 493, 47, textureX, textureY);
		bodyModel[670].addShapeBox(0, 0, 0, 7, 0, 2, 0, 0, -0.1875f, 0, 0.375f, -2.40625f, 0.625f, 0.375f, -4.15775f, -0.421875f, 0, -2.796875f, -0.3515625f, 0, 0.1875f, 0, 0.375f, 2.40625f, 0.625f, 0.375f, 4.15825f, -0.421875f, 0, 2.796875f, -0.3515625f);
		bodyModel[670].setRotationPoint(63.8125f, -7.53125f, 4.625f);

		bodyModel[671] = new ModelRendererTurbo(this, 497, 0, textureX, textureY);
		bodyModel[671].addShapeBox(0, 0, 0, 4, 0, 3, 0, 0, 0.75f, 0, 0.09375f, -0.875f, 0, 0.09375f, -0.875f, -0.375f, 0, 0.71875f, -0.375f, 0, -0.75f, 0, 0.09375f, 0.875f, 0, 0.09375f, 0.875f, -0.375f, 0, -0.71875f, -0.375f);
		bodyModel[671].setRotationPoint(71.1875f, -4.8125f, 0.0f);

		bodyModel[672] = new ModelRendererTurbo(this, 498, 4, textureX, textureY);
		bodyModel[672].addShapeBox(0, 0, 0, 4, 0, 1, 0, 0, 0.75f, 0, 0.09375f, -0.84375f, 0, 0.09375f, -1.0625f, 0.25f, 0, 0.34375f, 0.375f, 0, -0.75f, 0, 0.09375f, 0.84375f, 0, 0.09375f, 1.0625f, 0.25f, 0, -0.34375f, 0.375f);
		bodyModel[672].setRotationPoint(71.1875f, -4.78125f, 2.625f);

		bodyModel[673] = new ModelRendererTurbo(this, 135, 130, textureX, textureY);
		bodyModel[673].addShapeBox(0, 0, 0, 4, 0, 2, 0, 0, 0.75f, 0, 0.09375f, -0.65625f, 0.125f, 0.09375f, -1.28125f, -0.765625f, 0, -1, 0.203125f, 0, -0.75f, 0, 0.09375f, 0.65625f, 0.125f, 0.09375f, 1.28125f, -0.765625f, 0, 1, 0.203125f);
		bodyModel[673].setRotationPoint(71.1875f, -4.375f, 4.0f);

		bodyModel[674] = new ModelRendererTurbo(this, 443, 122, textureX, textureY);
		bodyModel[674].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0.25f, -0.45225f, -1.4257812f, -0.15625f, -0.75f, -0.484375f, 0.5625f, -1.3359375f, 0.375f, 0.25f, -0.45225f, 0.42578125f, 0.25f, -0.04882812f, -1.4257812f, -0.15625f, 0.25f, -0.484375f, 0.5625f, 0.84375f, 0.375f, 0.25f, -0.04882812f, 0.42578125f);
		bodyModel[674].setRotationPoint(71.4375f, -4.09375f, 4.75f);

		bodyModel[675] = new ModelRendererTurbo(this, 412, 155, textureX, textureY);
		bodyModel[675].addShapeBox(-2, -1, -2, 4, 1, 4, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[675].setRotationPoint(51.4375f, 0.1875f, 4.1875f);

		bodyModel[676] = new ModelRendererTurbo(this, 501, 144, textureX, textureY);
		bodyModel[676].addShapeBox(-2, -3.75f, -2, 1, 3, 4, 0, 0.25f, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, 0, 0.25f, -0.25f, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0);
		bodyModel[676].setRotationPoint(51.4375f, 0.1875f, 4.1875f);

		bodyModel[677] = new ModelRendererTurbo(this, 372, 117, textureX, textureY);
		bodyModel[677].addShapeBox(-2.25f, -4.5f, -2, 1, 1, 4, 0, 0.0625f, -0.625f, -0.375f, -0.3125f, -0.625f, -0.375f, -0.3125f, -0.625f, -0.375f, 0.0625f, -0.625f, -0.375f, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0);
		bodyModel[677].setRotationPoint(51.4375f, 0.1875f, 4.1875f);

		bodyModel[678] = new ModelRendererTurbo(this, 505, 62, textureX, textureY);
		bodyModel[678].addShapeBox(-1.8125f, -2.375f, -2.75f, 2, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[678].setRotationPoint(51.4375f, 0.1875f, 4.1875f);

		bodyModel[679] = new ModelRendererTurbo(this, 504, 59, textureX, textureY);
		bodyModel[679].addShapeBox(-1.8125f, -2.375f, 1.75f, 2, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[679].setRotationPoint(51.4375f, 0.1875f, 4.1875f);

		bodyModel[680] = new ModelRendererTurbo(this, 441, 154, textureX, textureY);
		bodyModel[680].addShapeBox(-2, -1, -2, 4, 1, 4, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[680].setRotationPoint(39.0f, 0.1875f, -6.25f);
		bodyModel[680].rotateAngleY = -0.7853982f;

		bodyModel[681] = new ModelRendererTurbo(this, 0, 118, textureX, textureY);
		bodyModel[681].addShapeBox(-2, -3.75f, -2, 1, 3, 4, 0, 0.25f, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, 0, 0.25f, -0.25f, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0);
		bodyModel[681].setRotationPoint(39.0f, 0.1875f, -6.25f);
		bodyModel[681].rotateAngleY = -0.7853982f;

		bodyModel[682] = new ModelRendererTurbo(this, 472, 114, textureX, textureY);
		bodyModel[682].addShapeBox(-2.25f, -4.5f, -2, 1, 1, 4, 0, 0.0625f, -0.625f, -0.375f, -0.3125f, -0.625f, -0.375f, -0.3125f, -0.625f, -0.375f, 0.0625f, -0.625f, -0.375f, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0);
		bodyModel[682].setRotationPoint(39.0f, 0.1875f, -6.25f);
		bodyModel[682].rotateAngleY = -0.7853982f;

		bodyModel[683] = new ModelRendererTurbo(this, 456, 42, textureX, textureY);
		bodyModel[683].addShapeBox(-1.8125f, -2.375f, -2.75f, 2, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[683].setRotationPoint(39.0f, 0.1875f, -6.25f);
		bodyModel[683].rotateAngleY = -0.7853982f;

		bodyModel[684] = new ModelRendererTurbo(this, 456, 39, textureX, textureY);
		bodyModel[684].addShapeBox(-1.8125f, -2.375f, 1.75f, 2, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[684].setRotationPoint(39.0f, 0.1875f, -6.25f);
		bodyModel[684].rotateAngleY = -0.7853982f;

		bodyModel[685] = new ModelRendererTurbo(this, 224, 154, textureX, textureY);
		bodyModel[685].addShapeBox(-2, -1, -2, 4, 1, 4, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[685].setRotationPoint(39.0f, 0.1875f, 6.25f);
		bodyModel[685].rotateAngleY = 0.7853982f;

		bodyModel[686] = new ModelRendererTurbo(this, 443, 114, textureX, textureY);
		bodyModel[686].addShapeBox(-2, -3.75f, -2, 1, 3, 4, 0, 0.25f, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, 0, 0.25f, -0.25f, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0);
		bodyModel[686].setRotationPoint(39.0f, 0.1875f, 6.25f);
		bodyModel[686].rotateAngleY = 0.7853982f;

		bodyModel[687] = new ModelRendererTurbo(this, 372, 111, textureX, textureY);
		bodyModel[687].addShapeBox(-2.25f, -4.5f, -2, 1, 1, 4, 0, 0.0625f, -0.625f, -0.375f, -0.3125f, -0.625f, -0.375f, -0.3125f, -0.625f, -0.375f, 0.0625f, -0.625f, -0.375f, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0);
		bodyModel[687].setRotationPoint(39.0f, 0.1875f, 6.25f);
		bodyModel[687].rotateAngleY = 0.7853982f;

		bodyModel[688] = new ModelRendererTurbo(this, 504, 35, textureX, textureY);
		bodyModel[688].addShapeBox(-1.8125f, -2.375f, -2.75f, 2, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[688].setRotationPoint(39.0f, 0.1875f, 6.25f);
		bodyModel[688].rotateAngleY = 0.7853982f;

		bodyModel[689] = new ModelRendererTurbo(this, 493, 35, textureX, textureY);
		bodyModel[689].addShapeBox(-1.8125f, -2.375f, 1.75f, 2, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[689].setRotationPoint(39.0f, 0.1875f, 6.25f);
		bodyModel[689].rotateAngleY = 0.7853982f;

		bodyModel[690] = new ModelRendererTurbo(this, 196, 112, textureX, textureY);
		bodyModel[690].addShapeBox(0, 0, 0, 11, 1, 3, 0, 0, -0.15625f, -0.375f, -0.5f, -0.90625f, -0.375f, -0.5f, -0.8125f, 0, 0, -0.0625f, 0, 0, -0.34375f, -0.375f, -0.5f, 0.40625f, -0.375f, -0.5f, 0.3125f, 0, 0, -0.4375f, 0);
		bodyModel[690].setRotationPoint(39.25f, -11.46875f, -3.0f);

		bodyModel[691] = new ModelRendererTurbo(this, 497, 122, textureX, textureY);
		bodyModel[691].addShapeBox(0, 0, 0, 5, 1, 2, 0, 0, -0.7143f, 0, 0.75f, -1.296875f, 0, 0.1875f, -0.433f, 0, 0, -0.0625f, 0, 0, 0.214237f, 0, 0.75f, 0.796875f, 0, 0.1875f, -0.067f, 0, 0, -0.4375f, 0);
		bodyModel[691].setRotationPoint(44.5625f, -10.9955f, -4.625f);

		bodyModel[692] = new ModelRendererTurbo(this, 375, 145, textureX, textureY);
		bodyModel[692].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, -2.5624f, -0.125f, -0.5f, -2.667f, -0.1335f, -0.5f, -1.6875f, -0.875f, 0, -0.64434f, 0, 0, 2.0625f, -0.125f, -0.5f, 2.167f, -0.1335f, -0.5f, 1.1875f, -0.875f, 0, 0.14434001f, 0);
		bodyModel[692].setRotationPoint(50.3125f, -10.343f, -6.625f);

		bodyModel[693] = new ModelRendererTurbo(this, 103, 160, textureX, textureY);
		bodyModel[693].addShapeBox(0.5f, 2, 0, 3, 1, 1, 0, 0, -0.6672f, -0.1335f, -0.5f, -1.1939f, -0.17336f, -0.5f, -1.1939f, -0.82664f, 0, 0.3125f, 0.125f, 0, 0.167f, -0.1335f, -0.5f, 0.6939f, -0.17336f, -0.5f, 0.6939f, -0.82664f, 0, -0.8125f, 0.125f);
		bodyModel[693].setRotationPoint(50.3125f, -10.343f, -6.625f);

		bodyModel[694] = new ModelRendererTurbo(this, 0, 93, textureX, textureY);
		bodyModel[694].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, -0.3125f, -0.5625f, 0, 0, -0.8125f, 0.375f, 0, -0.8125f, 0.375f, -0.3125f, -0.5625f, 0);
		bodyModel[694].setRotationPoint(57.75f, -9.4375f, -1.0f);

		bodyModel[695] = new ModelRendererTurbo(this, 196, 177, textureX, textureY);
		bodyModel[695].addShapeBox(0, 0, 0, 1, 1, 3, 0, 0, -0.9394531f, -0.375f, 0, -1.0117188f, -0.375f, 0, -0.92578125f, 0, 0, -0.84765625f, 0, 0, 0.03320312f, -0.375f, 0, 0.10546875f, -0.375f, 0, 0.01171875f, 0, 0, -0.05859375f, 0);
		bodyModel[695].setRotationPoint(45.75f, -11.8828125f, -3.0f);

		bodyModel[696] = new ModelRendererTurbo(this, 375, 159, textureX, textureY);
		bodyModel[696].addShapeBox(0, 0, 0, 6, 1, 2, 0, 0, -1.555f, -0.125f, -0.25f, -2.5625f, -0.125f, -0.25f, -0.64434f, 0, 0, -0.06184f, 0, 0, 1.05491f, -0.125f, -0.25f, 2.0625f, -0.125f, -0.25f, 0.14435f, 0, 0, -0.4383f, 0);
		bodyModel[696].setRotationPoint(44.5625f, -10.343f, -6.625f);

		bodyModel[697] = new ModelRendererTurbo(this, 34, 190, textureX, textureY);
		bodyModel[697].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.0605f, -1.5625f, -0.25f, -1.0703125f, -1.5625f, -0.25f, -1.5703125f, 0.5625f, 0, -0.5625f, 0.5625f, 0, -0.3359375f, 1, -0.25f, 0.625f, 0.6875f, -0.25f, 0.625f, -1.1875f, 0, -0.3359375f, -1.5f);
		bodyModel[697].setRotationPoint(44.5625f, -8.8515625f, -8.0625f);

		bodyModel[698] = new ModelRendererTurbo(this, 85, 160, textureX, textureY);
		bodyModel[698].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 0.0625f, -0.875f, -0.25f, -0.8984375f, -1.1875f, -0.25f, -0.8984375f, 0.6875f, 0, 0.0625f, 0.375f, 0, -0.25f, 0.15625f, -0.25f, 1, -0.125f, -0.25f, 1, -0.4375f, 0, -0.25f, -0.65625f);
		bodyModel[698].setRotationPoint(44.5625f, -7.125f, -9.9375f);

		bodyModel[699] = new ModelRendererTurbo(this, 32, 160, textureX, textureY);
		bodyModel[699].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, -0.375f, -0.5625f, -0.25f, -1.625f, -0.84375f, -0.25f, -1.625f, 0.28125f, 0, -0.375f, 0.0625f, 0, -0.25f, -0.125f, -0.25f, 1.15625f, -0.1875f, -0.25f, 1.15625f, -0.3125f, 0, -0.25f, -0.375f);
		bodyModel[699].setRotationPoint(44.5625f, -5.75f, -10.65625f);

		bodyModel[700] = new ModelRendererTurbo(this, 274, 150, textureX, textureY);
		bodyModel[700].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, -0.375f, -0.5625f, -0.25f, -1.78125f, -0.625f, -0.25f, -1.78125f, 0.125f, 0, -0.375f, 0.0625f, 0, 0.375f, -0.21875f, -0.25f, 0.78125f, -0.625f, -0.25f, 0.78125f, 0.125f, 0, 0.375f, -0.28125f);
		bodyModel[700].setRotationPoint(44.5625f, -4.375f, -11.09375f);

		bodyModel[701] = new ModelRendererTurbo(this, 486, 159, textureX, textureY);
		bodyModel[701].addShapeBox(0, 0, 0, 6, 3, 1, 0, 0, 0, -0.5f, -0.25f, -0.40625f, -0.90625f, -0.25f, -0.40625f, 0.40625f, 0, 0, 0, 0, 0, -0.625f, -0.25f, 0.375f, -0.875f, -0.25f, 0.375f, 0.375f, 0, 0, 0.125f);
		bodyModel[701].setRotationPoint(44.5625f, -3.0f, -11.375f);

		bodyModel[702] = new ModelRendererTurbo(this, 86, 148, textureX, textureY);
		bodyModel[702].addShapeBox(0, 0, 0, 6, 2, 1, 0, 0, 0, -0.625f, -0.25f, -0.375f, -0.875f, -0.25f, -0.375f, 0.375f, 0, 0, 0.125f, 0, -0.25f, -1, -0.25f, 0.1875f, -1.25f, -0.25f, 0.1875f, 0.75f, 0, -0.25f, 0.5f);
		bodyModel[702].setRotationPoint(44.5625f, 0.0f, -11.375f);

		bodyModel[703] = new ModelRendererTurbo(this, 482, 147, textureX, textureY);
		bodyModel[703].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, 0, -0.5f, -0.25f, -0.4375f, -0.75f, -0.25f, -0.4375f, 0.25f, 0, 0, 0, 0, -0.25f, -0.5f, -0.25f, -0.25f, -0.75f, -0.25f, -0.25f, 0.25f, 0, -0.25f, 0);
		bodyModel[703].setRotationPoint(44.5625f, 1.75f, -10.875f);

		bodyModel[704] = new ModelRendererTurbo(this, 376, 106, textureX, textureY);
		bodyModel[704].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.046875f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, 0.046875f, -0.0625f, -0.40625f, -0.171875f, -0.625f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.171875f, -0.625f, -0.40625f);
		bodyModel[704].setRotationPoint(57.8125f, -9.4375f, -1.3125f);

		bodyModel[705] = new ModelRendererTurbo(this, 15, 104, textureX, textureY);
		bodyModel[705].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.046875f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, 0.046875f, -0.0625f, -0.40625f, -0.171875f, -0.625f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.171875f, -0.625f, -0.40625f);
		bodyModel[705].setRotationPoint(57.8125f, -9.4375f, -1.03125f);

		bodyModel[706] = new ModelRendererTurbo(this, 168, 96, textureX, textureY);
		bodyModel[706].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.046875f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, 0.046875f, -0.0625f, -0.40625f, -0.171875f, -0.625f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.171875f, -0.625f, -0.40625f);
		bodyModel[706].setRotationPoint(57.8125f, -9.4375f, -0.75f);

		bodyModel[707] = new ModelRendererTurbo(this, 290, 93, textureX, textureY);
		bodyModel[707].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.046875f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, 0.046875f, -0.0625f, -0.40625f, -0.171875f, -0.625f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.171875f, -0.625f, -0.40625f);
		bodyModel[707].setRotationPoint(57.8125f, -9.4375f, -0.46875f);

		bodyModel[708] = new ModelRendererTurbo(this, 281, 49, textureX, textureY);
		bodyModel[708].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.046875f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, 0.046875f, -0.0625f, -0.40625f, -0.171875f, -0.625f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.171875f, -0.625f, -0.40625f);
		bodyModel[708].setRotationPoint(57.8125f, -9.4375f, -0.1875f);

		bodyModel[709] = new ModelRendererTurbo(this, 266, 36, textureX, textureY);
		bodyModel[709].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0.046875f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, -0.8125f, -0.0625f, -0.40625f, 0.046875f, -0.0625f, -0.40625f, -0.171875f, -0.625f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.8125f, -0.875f, -0.40625f, -0.171875f, -0.625f, -0.40625f);
		bodyModel[709].setRotationPoint(57.8125f, -9.4375f, 0.09375f);

		bodyModel[710] = new ModelRendererTurbo(this, 370, 176, textureX, textureY);
		bodyModel[710].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -1.4238281f, 0.0625f, -0.5f, -2.4765625f, 0.953125f, -0.5f, -1.8632812f, -1.0625f, 0, -0.40234375f, -0.0625f, 0, 0.5175781f, 0.0625f, -0.5f, 1.5703125f, 0.953125f, -0.5f, 0.9570312f, -1.0625f, 0, -0.50390625f, -0.0625f);
		bodyModel[710].setRotationPoint(47.8125f, -9.736328f, -6.4375f);

		bodyModel[711] = new ModelRendererTurbo(this, 203, 180, textureX, textureY);
		bodyModel[711].addShapeBox(0, 0, 0, 1, 1, 2, 0, -0.5625f, -1.5898438f, 0, 0.5625f, -1.6914062f, 0, 0, -0.9160156f, 0, 0, -0.84375f, 0, -0.5625f, 0.7109375f, 0, 0.5625f, 0.8125f, 0, 0, 0.01171875f, 0, 0, -0.0625f, 0);
		bodyModel[711].setRotationPoint(45.75f, -11.787109f, -4.625f);

		bodyModel[712] = new ModelRendererTurbo(this, 185, 176, textureX, textureY);
		bodyModel[712].addShapeBox(0, 0, 0, 1, 1, 1, 0, -0.5f, -1.5605469f, -0.125f, 0.5f, -1.6757812f, -0.125f, 0, -0.9140625f, 0, 0, -0.8125f, 0, -0.5f, 0.6542969f, -0.125f, 0.5f, 0.7695312f, -0.125f, 0, 0.0078125f, 0, 0, -0.09375f, 0);
		bodyModel[712].setRotationPoint(46.3125f, -11.009766f, -5.625f);

		bodyModel[713] = new ModelRendererTurbo(this, 194, 177, textureX, textureY);
		bodyModel[713].addShapeBox(0, 0, 0, 1, 1, 1, 0, -1, -1.8242188f, 0, 0, -1.8242188f, 0, 0, -0.8027344f, 0, 0, -0.6875f, 0, -1, 0.91796875f, 0, 0, 0.91796875f, 0, 0, -0.10351562f, 0, 0, -0.21875f, 0);
		bodyModel[713].setRotationPoint(46.8125f, -10.136719f, -6.5f);

		bodyModel[714] = new ModelRendererTurbo(this, 409, 161, textureX, textureY);
		bodyModel[714].addShapeBox(0, 0, 0, 6, 1, 1, 0, 0, 0, -0.5f, -0.1875f, 0, -0.5f, -0.1875f, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, -0.1875f, -0.25f, -0.5f, -0.1875f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[714].setRotationPoint(38.75f, 1.75f, -10.875f);

		bodyModel[715] = new ModelRendererTurbo(this, 47, 161, textureX, textureY);
		bodyModel[715].addShapeBox(0, 0, 0, 5, 1, 2, 0, 0, -0.71184f, 0, 0.3125f, -1.09375f, 0, 0.3125f, -0.442f, 0, 0, -0.0625f, 0, 0, 0.21186f, 0, 0.3125f, 0.59375f, 0, 0.3125f, -0.058f, 0, 0, -0.4375f, 0);
		bodyModel[715].setRotationPoint(39.25f, -11.375f, -4.625f);

		bodyModel[716] = new ModelRendererTurbo(this, 392, 159, textureX, textureY);
		bodyModel[716].addShapeBox(0, 0, 0, 5, 1, 3, 0, 0, -0.9756f, -0.1875f, 0.3125f, -1.9375f, -1.125f, 0.3125f, -0.4444f, 0, 0, -0.0625f, 0, 0, 0.4756f, -0.1875f, 0.3125f, 1.4375f, -1.125f, 0.3125f, -0.0556f, 0, 0, -0.4375f, 0);
		bodyModel[716].setRotationPoint(39.25f, -10.7256f, -7.625f);

		bodyModel[717] = new ModelRendererTurbo(this, 205, 167, textureX, textureY);
		bodyModel[717].addShapeBox(0, 0, 0, 5, 1, 1, 0, 0, 0.5625f, -0.90625f, 0.3125f, -0.4375f, -1.21875f, 0.3125f, -0.4375f, 0.71875f, 0, 0.5625f, 0.40625f, 0, 0, 0, 0.3125f, 1.25f, -0.1875f, 0.3125f, 1.25f, -0.3125f, 0, 0, -0.5f);
		bodyModel[717].setRotationPoint(39.25f, -7.625f, -10.28125f);

		bodyModel[718] = new ModelRendererTurbo(this, 175, 167, textureX, textureY);
		bodyModel[718].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, -0.375f, -0.5625f, 0.3125f, -1.625f, -0.75f, 0.3125f, -1.625f, 0.25f, 0, -0.375f, 0.0625f, 0, 0, -0.21875f, 0.3125f, 1, -0.3125f, 0.3125f, 1, -0.1875f, 0, 0, -0.28125f);
		bodyModel[718].setRotationPoint(39.25f, -7.0f, -10.84375f);

		bodyModel[719] = new ModelRendererTurbo(this, 492, 166, textureX, textureY);
		bodyModel[719].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, -0.375f, -0.5625f, 0.3125f, -1.375f, -0.65625f, 0.3125f, -1.375f, 0.15625f, 0, -0.375f, 0.0625f, 0, 0.375f, -0.1875f, 0.3125f, 0.375f, -0.3125f, 0.3125f, 0.375f, -0.1875f, 0, 0.375f, -0.3125f);
		bodyModel[719].setRotationPoint(39.25f, -5.375f, -11.1875f);

		bodyModel[720] = new ModelRendererTurbo(this, 294, 166, textureX, textureY);
		bodyModel[720].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, 0, 0.3125f, 0, -0.125f, 0.3125f, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.25f, 0.3125f, 0, -0.25f, 0.3125f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[720].setRotationPoint(39.25f, -3.0f, -11.0f);

		bodyModel[721] = new ModelRendererTurbo(this, 281, 166, textureX, textureY);
		bodyModel[721].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, -0.375f, 0.3125f, -0.25f, -0.375f, 0.3125f, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[721].setRotationPoint(39.25f, 0.0f, -10.75f);

		bodyModel[722] = new ModelRendererTurbo(this, 345, 182, textureX, textureY);
		bodyModel[722].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, -0.0625f, -1.5625f, 0.3125f, -1.0214844f, -2.5f, 0.3125f, -1.5234375f, 1.5f, 0, -0.5625f, 0.5625f, 0, -0.375f, 0.375f, 0.3125f, 0.625f, 0.0625f, 0.3125f, 0.625f, -0.5625f, 0, -0.375f, -0.875f);
		bodyModel[722].setRotationPoint(39.25f, -9.8125f, -9.0f);

		bodyModel[723] = new ModelRendererTurbo(this, 164, 174, textureX, textureY);
		bodyModel[723].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[723].setRotationPoint(2.75f, -5.0f, -11.0f);

		bodyModel[724] = new ModelRendererTurbo(this, 147, 174, textureX, textureY);
		bodyModel[724].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[724].setRotationPoint(2.75f, -7.0f, -11.0f);

		bodyModel[725] = new ModelRendererTurbo(this, 129, 174, textureX, textureY);
		bodyModel[725].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[725].setRotationPoint(2.75f, -9.0f, -10.625f);

		bodyModel[726] = new ModelRendererTurbo(this, 130, 171, textureX, textureY);
		bodyModel[726].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[726].setRotationPoint(4.75f, -9.0f, -10.625f);

		bodyModel[727] = new ModelRendererTurbo(this, 122, 174, textureX, textureY);
		bodyModel[727].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[727].setRotationPoint(8.75f, -5.0f, -11.0f);

		bodyModel[728] = new ModelRendererTurbo(this, 115, 174, textureX, textureY);
		bodyModel[728].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[728].setRotationPoint(8.75f, -7.0f, -11.0f);

		bodyModel[729] = new ModelRendererTurbo(this, 108, 174, textureX, textureY);
		bodyModel[729].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[729].setRotationPoint(8.75f, -9.0f, -10.625f);

		bodyModel[730] = new ModelRendererTurbo(this, 119, 171, textureX, textureY);
		bodyModel[730].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[730].setRotationPoint(10.75f, -9.0f, -10.625f);

		bodyModel[731] = new ModelRendererTurbo(this, 86, 174, textureX, textureY);
		bodyModel[731].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[731].setRotationPoint(14.75f, -5.0f, -11.0f);

		bodyModel[732] = new ModelRendererTurbo(this, 79, 174, textureX, textureY);
		bodyModel[732].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[732].setRotationPoint(14.75f, -7.0f, -11.0f);

		bodyModel[733] = new ModelRendererTurbo(this, 32, 174, textureX, textureY);
		bodyModel[733].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[733].setRotationPoint(14.75f, -9.0f, -10.625f);

		bodyModel[734] = new ModelRendererTurbo(this, 108, 171, textureX, textureY);
		bodyModel[734].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0, 0, -0.75f, 0, 0, 0.25f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0, 0.5f, -0.1875f, 0, 0.5f, -0.3125f, 0, 0.5f, -0.328125f);
		bodyModel[734].setRotationPoint(16.75f, -9.0f, -10.625f);

		bodyModel[735] = new ModelRendererTurbo(this, 25, 174, textureX, textureY);
		bodyModel[735].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[735].setRotationPoint(20.75f, -5.0f, -11.0f);

		bodyModel[736] = new ModelRendererTurbo(this, 18, 174, textureX, textureY);
		bodyModel[736].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[736].setRotationPoint(20.75f, -7.0f, -11.0f);

		bodyModel[737] = new ModelRendererTurbo(this, 494, 173, textureX, textureY);
		bodyModel[737].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.75f, 0, 0, -0.78125f, 0, 0, 0.28125f, 0, 0, 0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[737].setRotationPoint(20.75f, -9.0f, -10.625f);

		bodyModel[738] = new ModelRendererTurbo(this, 80, 171, textureX, textureY);
		bodyModel[738].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.78125f, 0, 0, -0.84375f, 0, 0, 0.34375f, 0, 0, 0.28125f, 0, 0.5f, -0.19525f, 0, 0.5f, -0.2344f, 0, 0.5f, -0.265625f, 0, 0.5f, -0.3046875f);
		bodyModel[738].setRotationPoint(22.75f, -9.0f, -10.625f);

		bodyModel[739] = new ModelRendererTurbo(this, 487, 173, textureX, textureY);
		bodyModel[739].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[739].setRotationPoint(26.75f, -5.0f, -11.0f);

		bodyModel[740] = new ModelRendererTurbo(this, 480, 173, textureX, textureY);
		bodyModel[740].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.40625f, 0, 0, -0.4375f, 0, 0, -0.0625f, 0, 0, -0.09375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[740].setRotationPoint(26.75f, -7.0f, -11.0f);

		bodyModel[741] = new ModelRendererTurbo(this, 430, 173, textureX, textureY);
		bodyModel[741].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.84375f, 0, 0, -0.9375f, 0, 0, 0.4375f, 0, 0, 0.34375f, 0, 0, -0.03125f, 0, 0, -0.0625f, 0, 0, -0.4375f, 0, 0, -0.46875f);
		bodyModel[741].setRotationPoint(26.75f, -9.0f, -10.625f);

		bodyModel[742] = new ModelRendererTurbo(this, 81, 156, textureX, textureY);
		bodyModel[742].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, 0, -0.5f, 0, -0.375f, -0.5f, 0, -0.125f, 0, 0, -0.5f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[742].setRotationPoint(31.75f, -5.0f, -11.0f);

		bodyModel[743] = new ModelRendererTurbo(this, 42, 156, textureX, textureY);
		bodyModel[743].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.5625f, -0.5f, -0.375f, -0.71875f, -0.5f, -0.375f, 0.21875f, 0, 0, 0.0625f, 0, 0, 0, -0.5f, 0, -0.375f, -0.5f, 0, -0.125f, 0, 0, -0.5f);
		bodyModel[743].setRotationPoint(31.75f, -7.0f, -11.0f);

		bodyModel[744] = new ModelRendererTurbo(this, 492, 155, textureX, textureY);
		bodyModel[744].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -1, -0.5f, -0.8125f, -1.0625f, -0.5f, -0.8125f, 0.5625f, 0, 0, 0.4375f, 0, 0, 0, -0.5f, 0.375f, -0.15625f, -0.5f, 0.375f, -0.34375f, 0, 0, -0.5f);
		bodyModel[744].setRotationPoint(31.75f, -9.0f, -10.4375f);

		bodyModel[745] = new ModelRendererTurbo(this, 196, 108, textureX, textureY);
		bodyModel[745].addShapeBox(0, 0, 0, 14, 2, 1, 0, 0, 0, -1.0625f, 0, -0.375f, -1, 0, -0.375f, 0.5f, 0, 0, 0.5625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[745].setRotationPoint(2.75f, -10.75f, -9.9375f);

		bodyModel[746] = new ModelRendererTurbo(this, 230, 170, textureX, textureY);
		bodyModel[746].addShapeBox(0, 0, 0, 4, 2, 1, 0, 0, -0.375f, -1, 0, -0.75f, -0.6875f, 0, -0.75f, 0.1875f, 0, -0.375f, 0.5f, 0, -0.25f, 0, 0, -0.25f, -0.0625f, 0, -0.25f, -0.4375f, 0, -0.25f, -0.5f);
		bodyModel[746].setRotationPoint(16.75f, -10.75f, -9.9375f);

		bodyModel[747] = new ModelRendererTurbo(this, 237, 147, textureX, textureY);
		bodyModel[747].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0.25f, -0.6875f, 0, 0.0625f, -0.5625f, 0, 0.0625f, 0.0625f, 0, 0.25f, 0.1875f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.09375f, 0, -0.25f, -0.40625f, 0, -0.25f, -0.4375f);
		bodyModel[747].setRotationPoint(20.75f, -9.75f, -9.9375f);

		bodyModel[748] = new ModelRendererTurbo(this, 198, 170, textureX, textureY);
		bodyModel[748].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.0625f, -0.46875f, 0, -0.28125f, -0.28125f, 0, -0.28125f, -0.21875f, 0, 0.0625f, -0.03125f, 0, -0.25f, 0, 0, -0.25f, -0.0625f, 0, -0.25f, -0.4375f, 0, -0.25f, -0.5f);
		bodyModel[748].setRotationPoint(22.75f, -9.75f, -9.84375f);

		bodyModel[749] = new ModelRendererTurbo(this, 124, 147, textureX, textureY);
		bodyModel[749].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -0.28125f, -0.21875f, 0, -0.46875f, -0.21875f, 0, -0.46875f, -0.28125f, 0, -0.28125f, -0.28125f, 0, -0.25f, 0, 0, -0.25f, -0.09375f, 0, -0.25f, -0.40625f, 0, -0.25f, -0.5f);
		bodyModel[749].setRotationPoint(26.75f, -9.75f, -9.78125f);

		bodyModel[750] = new ModelRendererTurbo(this, 349, 103, textureX, textureY);
		bodyModel[750].addShapeBox(0, 0, 0, 14, 1, 1, 0, 0, 0.8125f, -2.0625f, 0, 0, -1.625f, 0, -0.5f, 0.625f, 0, 0.3125f, 1.0625f, 0, -0.125f, -0.1875f, 0, 0.25f, -0.125f, 0, 0.25f, -0.375f, 0, -0.125f, -0.3125f);
		bodyModel[750].setRotationPoint(2.75f, -11.625f, -9.0625f);

		bodyModel[751] = new ModelRendererTurbo(this, 187, 170, textureX, textureY);
		bodyModel[751].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -1.625f, 0, -0.125f, -1.625f, 0, -0.625f, 0.625f, 0, -0.5f, 0.625f, 0, 0.25f, -0.125f, 0, 0.625f, 0.1875f, 0, 0.625f, -0.6875f, 0, 0.25f, -0.375f);
		bodyModel[751].setRotationPoint(16.75f, -11.625f, -9.0625f);

		bodyModel[752] = new ModelRendererTurbo(this, 345, 145, textureX, textureY);
		bodyModel[752].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -1.625f, 0, -0.0625f, -1.625f, 0, -0.5625f, 0.625f, 0, -0.5f, 0.625f, 0, 0.5f, 0.1875f, 0, 0.6875f, 0.3125f, 0, 0.6875f, -0.8125f, 0, 0.5f, -0.6875f);
		bodyModel[752].setRotationPoint(20.75f, -11.5f, -9.0625f);

		bodyModel[753] = new ModelRendererTurbo(this, 158, 170, textureX, textureY);
		bodyModel[753].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0.125f, -1.75f, 0, -0.125f, -1.75f, 0, -0.625f, 0.75f, 0, -0.375f, 0.75f, 0, 0.5f, 0.1875f, 0, 0.84375f, 0.375f, 0, 0.84375f, -0.875f, 0, 0.5f, -0.6875f);
		bodyModel[753].setRotationPoint(22.75f, -11.3125f, -9.1875f);

		bodyModel[754] = new ModelRendererTurbo(this, 352, 137, textureX, textureY);
		bodyModel[754].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -1.625f, 0, -0.1875f, -1.625f, 0, -0.6875f, 0.625f, 0, -0.5f, 0.625f, 0, 0.71875f, 0.5f, 0, 0.90625f, 0.5f, 0, 0.90625f, -1, 0, 0.71875f, -1);
		bodyModel[754].setRotationPoint(26.75f, -11.1875f, -9.0625f);

		bodyModel[755] = new ModelRendererTurbo(this, 63, 120, textureX, textureY);
		bodyModel[755].addShapeBox(0, 0, 0, 14, 1, 5, 0, 0, -1.4375f, -0.25f, 0, -2.25f, 0.1875f, 0, -0.625f, 0, 0, 0, 0, 0, 0.9375f, -0.25f, 0, 1.75f, 0.1875f, 0, 0.125f, 0, 0, -0.5f, 0);
		bodyModel[755].setRotationPoint(2.75f, -13.875f, -7.25f);

		bodyModel[756] = new ModelRendererTurbo(this, 427, 135, textureX, textureY);
		bodyModel[756].addShapeBox(0, 0, 0, 14, 1, 2, 0, 0, -0.375f, 0.25f, 0, -1, 0.25f, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, 0.25f, 0, 0.5f, 0.25f, 0, 0.125f, 0, 0, -0.5f, 0);
		bodyModel[756].setRotationPoint(2.75f, -14.25f, -2.0f);

		bodyModel[757] = new ModelRendererTurbo(this, 454, 151, textureX, textureY);
		bodyModel[757].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -1.4375f, -0.25f, 0, -1.5625f, -0.25f, 0, 0, 0.4375f, 0, 0.1875f, 0.4375f, 0, 0.9375f, -0.25f, 0, 1.0625f, -0.25f, 0, -0.5f, 0.4375f, 0, -0.6875f, 0.4375f);
		bodyModel[757].setRotationPoint(16.75f, -13.0625f, -7.6875f);

		bodyModel[758] = new ModelRendererTurbo(this, 496, 159, textureX, textureY);
		bodyModel[758].addShapeBox(0, 0, 0, 2, 1, 5, 0, 0, -1.4375f, -0.25f, 0, -1.5f, -0.25f, 0, 0, 0.4375f, 0, 0.125f, 0.4375f, 0, 0.9375f, -0.25f, 0, 1, -0.25f, 0, -0.5f, 0.4375f, 0, -0.625f, 0.4375f);
		bodyModel[758].setRotationPoint(20.75f, -12.9375f, -7.6875f);

		bodyModel[759] = new ModelRendererTurbo(this, 209, 151, textureX, textureY);
		bodyModel[759].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -1.375f, -0.25f, 0, -1.625f, -0.25f, 0, -0.1875f, 0.4375f, 0, 0.125f, 0.4375f, 0, 0.875f, -0.25f, 0, 1.125f, -0.25f, 0, -0.3125f, 0.4375f, 0, -0.625f, 0.4375f);
		bodyModel[759].setRotationPoint(22.75f, -12.8125f, -7.6875f);

		bodyModel[760] = new ModelRendererTurbo(this, 182, 159, textureX, textureY);
		bodyModel[760].addShapeBox(0, 0, 0, 2, 1, 5, 0, 0, -1.4375f, -0.25f, 0, -1.625f, -0.25f, 0, -0.25f, 0.4375f, 0, 0, 0.4375f, 0, 0.9375f, -0.25f, 0, 1.125f, -0.25f, 0, -0.25f, 0.4375f, 0, -0.5f, 0.4375f);
		bodyModel[760].setRotationPoint(26.75f, -12.625f, -7.6875f);

		bodyModel[761] = new ModelRendererTurbo(this, 289, 135, textureX, textureY);
		bodyModel[761].addShapeBox(0, 0, 0, 11, 1, 5, 0, 0, -1.375f, -0.25f, -0.5f, -2.625f, -0.25f, -0.5f, -1.0625f, 0.0625f, 0, 0, 0.4375f, 0, 0.875f, -0.25f, -0.5f, 2.125f, -0.25f, -0.5f, 0.5625f, 0.0625f, 0, -0.5f, 0.4375f);
		bodyModel[761].setRotationPoint(28.75f, -12.375f, -7.6875f);

		bodyModel[762] = new ModelRendererTurbo(this, 493, 44, textureX, textureY);
		bodyModel[762].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0.0187f, -1.625f, -0.5f, -0.875f, -1.625f, -0.5f, -1.375f, 0.625f, 0, -0.482f, 0.625f, 0, 0.625f, 0.375f, -0.5f, 1.4375f, 0.3125f, -0.5f, 1.4375f, -0.8125f, 0, 0.625f, -0.9375f);
		bodyModel[762].setRotationPoint(31.75f, -10.625f, -9.0625f);

		bodyModel[763] = new ModelRendererTurbo(this, 207, 135, textureX, textureY);
		bodyModel[763].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, -1.625f, 0, -0.3563f, -1.625f, 0, -0.857f, 0.625f, 0, -0.5f, 0.625f, 0, 0.71875f, 0.5f, 0, 1, 0.375f, 0, 1, -0.9375f, 0, 0.71875f, -1);
		bodyModel[763].setRotationPoint(28.75f, -11.0f, -9.0625f);

		bodyModel[764] = new ModelRendererTurbo(this, 55, 134, textureX, textureY);
		bodyModel[764].addShapeBox(0, 0, 0, 3, 2, 1, 0, 0, 0, -0.84375f, 0, 0, -1.09375f, 0, 0, 0.53125f, 0, 0, 0.34375f, 0, 0, 0.03125f, 0, 0, -0.09375f, 0, 0, -0.40625f, 0, 0, -0.53125f);
		bodyModel[764].setRotationPoint(28.75f, -9.0f, -10.53125f);

		bodyModel[765] = new ModelRendererTurbo(this, 503, 133, textureX, textureY);
		bodyModel[765].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.46875f, -0.21875f, 0, -0.75f, -0.34375f, 0, -0.75f, -0.21875f, 0, -0.46875f, -0.28125f, 0, -0.25f, -0.09375f, 0, -0.25f, -0.34375f, 0, -0.25f, -0.21875f, 0, -0.25f, -0.40625f);
		bodyModel[765].setRotationPoint(28.75f, -9.75f, -9.78125f);

		bodyModel[766] = new ModelRendererTurbo(this, 11, 97, textureX, textureY);
		bodyModel[766].addShapeBox(0, 0, 0, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[766].setRotationPoint(28.75f, -5.0f, -11.0f);

		bodyModel[767] = new ModelRendererTurbo(this, 11, 93, textureX, textureY);
		bodyModel[767].addShapeBox(0, 0, 0, 3, 2, 1, 0, 0, 0, -0.4375f, 0, 0, -0.5625f, 0, 0, 0.0625f, 0, 0, -0.0625f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[767].setRotationPoint(28.75f, -7.0f, -11.0f);

		bodyModel[768] = new ModelRendererTurbo(this, 361, 167, textureX, textureY);
		bodyModel[768].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.5f, 0.25f, 0, -0.6875f, 0.25f, 0, -0.375f, 0, 0, -0.125f, 0, 0, 0, 0.25f, 0, 0.1875f, 0.25f, 0, -0.125f, 0, 0, -0.375f, 0);
		bodyModel[768].setRotationPoint(16.75f, -13.75f, -2.0f);

		bodyModel[769] = new ModelRendererTurbo(this, 359, 151, textureX, textureY);
		bodyModel[769].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, 0.1875f, 0.4375f, 0, 0, 0.4375f, 0, -1.5625f, -0.25f, 0, -1.4375f, -0.25f, 0, -0.6875f, 0.4375f, 0, -0.5f, 0.4375f, 0, 1.0625f, -0.25f, 0, 0.9375f, -0.25f);
		bodyModel[769].setRotationPoint(16.75f, -13.0625f, 2.6875f);

		bodyModel[770] = new ModelRendererTurbo(this, 339, 167, textureX, textureY);
		bodyModel[770].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.6875f, 0.25f, 0, -0.5f, 0.25f, 0, -0.375f, 0, 0, -0.125f, 0, 0, 0.1875f, 0.25f, 0, 0, 0.25f);
		bodyModel[770].setRotationPoint(16.75f, -13.75f, 0.0f);

		bodyModel[771] = new ModelRendererTurbo(this, 455, 171, textureX, textureY);
		bodyModel[771].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, -0.4375f, 0.25f, 0, -0.5625f, 0.25f, 0, -0.25f, 0, 0, -0.125f, 0, 0, -0.0625f, 0.25f, 0, 0.0625f, 0.25f, 0, -0.25f, 0, 0, -0.375f, 0);
		bodyModel[771].setRotationPoint(20.75f, -13.5f, -2.0f);

		bodyModel[772] = new ModelRendererTurbo(this, 159, 159, textureX, textureY);
		bodyModel[772].addShapeBox(0, 0, 0, 2, 1, 5, 0, 0, 0.125f, 0.4375f, 0, 0, 0.4375f, 0, -1.5f, -0.25f, 0, -1.4375f, -0.25f, 0, -0.625f, 0.4375f, 0, -0.5f, 0.4375f, 0, 1, -0.25f, 0, 0.9375f, -0.25f);
		bodyModel[772].setRotationPoint(20.75f, -12.9375f, 2.6875f);

		bodyModel[773] = new ModelRendererTurbo(this, 426, 150, textureX, textureY);
		bodyModel[773].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, 0.125f, 0.4375f, 0, -0.1875f, 0.4375f, 0, -1.625f, -0.25f, 0, -1.375f, -0.25f, 0, -0.625f, 0.4375f, 0, -0.3125f, 0.4375f, 0, 1.125f, -0.25f, 0, 0.875f, -0.25f);
		bodyModel[773].setRotationPoint(22.75f, -12.8125f, 2.6875f);

		bodyModel[774] = new ModelRendererTurbo(this, 244, 158, textureX, textureY);
		bodyModel[774].addShapeBox(0, 0, 0, 2, 1, 5, 0, 0, 0, 0.4375f, 0, -0.25f, 0.4375f, 0, -1.625f, -0.25f, 0, -1.4375f, -0.25f, 0, -0.5f, 0.4375f, 0, -0.25f, 0.4375f, 0, 1.125f, -0.25f, 0, 0.9375f, -0.25f);
		bodyModel[774].setRotationPoint(26.75f, -12.625f, 2.6875f);

		bodyModel[775] = new ModelRendererTurbo(this, 180, 171, textureX, textureY);
		bodyModel[775].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, -0.125f, 0, 0, -0.25f, 0, 0, -0.5625f, 0.25f, 0, -0.4375f, 0.25f, 0, -0.375f, 0, 0, -0.25f, 0, 0, 0.0625f, 0.25f, 0, -0.0625f, 0.25f);
		bodyModel[775].setRotationPoint(20.75f, -13.5f, 0.0f);

		bodyModel[776] = new ModelRendererTurbo(this, 135, 167, textureX, textureY);
		bodyModel[776].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.5f, 0.25f, 0, -0.8125f, 0.25f, 0, -0.4375f, 0, 0, -0.1875f, 0, 0, 0, 0.25f, 0, 0.3125f, 0.25f, 0, -0.0625f, 0, 0, -0.3125f, 0);
		bodyModel[776].setRotationPoint(22.75f, -13.4375f, -2.0f);

		bodyModel[777] = new ModelRendererTurbo(this, 241, 170, textureX, textureY);
		bodyModel[777].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, -0.4375f, 0.25f, 0, -0.6875f, 0.25f, 0, -0.3125f, 0, 0, -0.0625f, 0, 0, -0.0625f, 0.25f, 0, 0.1875f, 0.25f, 0, -0.1875f, 0, 0, -0.4375f, 0);
		bodyModel[777].setRotationPoint(26.75f, -13.0625f, -2.0f);

		bodyModel[778] = new ModelRendererTurbo(this, 326, 166, textureX, textureY);
		bodyModel[778].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.1875f, 0, 0, -0.4375f, 0, 0, -0.8125f, 0.25f, 0, -0.5f, 0.25f, 0, -0.3125f, 0, 0, -0.0625f, 0, 0, 0.3125f, 0.25f, 0, 0, 0.25f);
		bodyModel[778].setRotationPoint(22.75f, -13.4375f, 0.0f);

		bodyModel[779] = new ModelRendererTurbo(this, 503, 168, textureX, textureY);
		bodyModel[779].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, -0.0625f, 0, 0, -0.3125f, 0, 0, -0.6875f, 0.25f, 0, -0.4375f, 0.25f, 0, -0.4375f, 0, 0, -0.1875f, 0, 0, 0.1875f, 0.25f, 0, -0.0625f, 0.25f);
		bodyModel[779].setRotationPoint(26.75f, -13.0625f, 0.0f);

		bodyModel[780] = new ModelRendererTurbo(this, 271, 142, textureX, textureY);
		bodyModel[780].addShapeBox(0, 0, 0, 11, 1, 2, 0, 0, -0.4375f, 0.25f, -0.5f, -1.5f, 0.625f, -0.5f, -1.40625f, 0, 0, -0.0625f, 0, 0, -0.0625f, 0.25f, -0.5f, 1, 0.625f, -0.5f, 0.90625f, 0, 0, -0.4375f, 0);
		bodyModel[780].setRotationPoint(28.75f, -12.8125f, -2.0f);

		bodyModel[781] = new ModelRendererTurbo(this, 410, 139, textureX, textureY);
		bodyModel[781].addShapeBox(0, 0, 0, 11, 1, 2, 0, 0, -0.0625f, 0, -0.5f, -1.40625f, 0, -0.5f, -1.5f, 0.625f, 0, -0.4375f, 0.25f, 0, -0.4375f, 0, -0.5f, 0.90625f, 0, -0.5f, 1, 0.625f, 0, -0.0625f, 0.25f);
		bodyModel[781].setRotationPoint(28.75f, -12.8125f, 0.0f);

		bodyModel[782] = new ModelRendererTurbo(this, 256, 135, textureX, textureY);
		bodyModel[782].addShapeBox(0, 0, 0, 11, 1, 5, 0, 0, 0, 0.4375f, -0.5f, -1.0625f, 0.0625f, -0.5f, -2.625f, -0.25f, 0, -1.375f, -0.25f, 0, -0.5f, 0.4375f, -0.5f, 0.5625f, 0.0625f, -0.5f, 2.125f, -0.25f, 0, 0.875f, -0.25f);
		bodyModel[782].setRotationPoint(28.75f, -12.375f, 2.6875f);

		bodyModel[783] = new ModelRendererTurbo(this, 349, 93, textureX, textureY);
		bodyModel[783].addShapeBox(0, 0, 0, 14, 1, 1, 0, 0, 0.625f, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0.625f, -0.5f, 0, -1.125f, 0, 0, -0.5f, 0, 0, -0.5f, -0.5f, 0, -1.125f, -0.5f);
		bodyModel[783].setRotationPoint(2.75f, -12.375f, -4.5f);

		bodyModel[784] = new ModelRendererTurbo(this, 464, 68, textureX, textureY);
		bodyModel[784].addShapeBox(0, 0, 0, 14, 1, 1, 0, 0, 0.625f, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0.625f, 0, 0, -1.125f, -0.5f, 0, -0.5f, -0.5f, 0, -0.5f, 0, 0, -1.125f, 0);
		bodyModel[784].setRotationPoint(2.75f, -12.375f, 3.5f);

		bodyModel[785] = new ModelRendererTurbo(this, 464, 65, textureX, textureY);
		bodyModel[785].addShapeBox(0, 0, 0, 17, 1, 1, 0, 0, 1.1875f, 0, 0, 0, 0, 0, 0, -0.5f, 0, 1.1875f, -0.5f, 0, -1.6875f, 0, 0, -0.5f, 0, 0, -0.5f, -0.5f, 0, -1.6875f, -0.5f);
		bodyModel[785].setRotationPoint(16.75f, -11.1875f, -4.5f);

		bodyModel[786] = new ModelRendererTurbo(this, 69, 170, textureX, textureY);
		bodyModel[786].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -0.5f, 0.625f, 0, -0.625f, 0.625f, 0, -0.125f, -1.625f, 0, 0, -1.625f, 0, 0.25f, -0.375f, 0, 0.625f, -0.6875f, 0, 0.625f, 0.1875f, 0, 0.25f, -0.125f);
		bodyModel[786].setRotationPoint(16.75f, -11.625f, 8.0625f);

		bodyModel[787] = new ModelRendererTurbo(this, 82, 106, textureX, textureY);
		bodyModel[787].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -0.5f, 0.625f, 0, -0.5625f, 0.625f, 0, -0.0625f, -1.625f, 0, 0, -1.625f, 0, 0.5f, -0.6875f, 0, 0.6875f, -0.8125f, 0, 0.6875f, 0.3125f, 0, 0.5f, 0.1875f);
		bodyModel[787].setRotationPoint(20.75f, -11.5f, 8.0625f);

		bodyModel[788] = new ModelRendererTurbo(this, 11, 170, textureX, textureY);
		bodyModel[788].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -0.375f, 0.75f, 0, -0.625f, 0.75f, 0, -0.125f, -1.75f, 0, 0.125f, -1.75f, 0, 0.5f, -0.6875f, 0, 0.84375f, -0.875f, 0, 0.84375f, 0.375f, 0, 0.5f, 0.1875f);
		bodyModel[788].setRotationPoint(22.75f, -11.3125f, 8.1875f);

		bodyModel[789] = new ModelRendererTurbo(this, 82, 97, textureX, textureY);
		bodyModel[789].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -0.5f, 0.625f, 0, -0.6875f, 0.625f, 0, -0.1875f, -1.625f, 0, 0, -1.625f, 0, 0.71875f, -1, 0, 0.90625f, -1, 0, 0.90625f, 0.5f, 0, 0.71875f, 0.5f);
		bodyModel[789].setRotationPoint(26.75f, -11.1875f, 8.0625f);

		bodyModel[790] = new ModelRendererTurbo(this, 248, 151, textureX, textureY);
		bodyModel[790].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, -0.482f, 0.625f, -0.5f, -1.375f, 0.625f, -0.5f, -0.875f, -1.625f, 0, 0.0187f, -1.625f, 0, 0.625f, -0.9375f, -0.5f, 1.4375f, -0.8125f, -0.5f, 1.4375f, 0.3125f, 0, 0.625f, 0.375f);
		bodyModel[790].setRotationPoint(31.75f, -10.625f, 8.0625f);

		bodyModel[791] = new ModelRendererTurbo(this, 467, 44, textureX, textureY);
		bodyModel[791].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.5f, 0.625f, 0, -0.857f, 0.625f, 0, -0.3563f, -1.625f, 0, 0, -1.625f, 0, 0.71875f, -1, 0, 1, -0.9375f, 0, 1, 0.375f, 0, 0.71875f, 0.5f);
		bodyModel[791].setRotationPoint(28.75f, -11.0f, 8.0625f);

		bodyModel[792] = new ModelRendererTurbo(this, 44, 172, textureX, textureY);
		bodyModel[792].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0.6875f, -0.125f, 0, 0.6875f, -0.125f, 0.375f, -1.6875f, 0, 0.375f, -1.6875f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[792].setRotationPoint(-45.125f, -11.0f, 2.375f);

		bodyModel[793] = new ModelRendererTurbo(this, 44, 172, textureX, textureY);
		bodyModel[793].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0.375f, -1.6875f, -0.125f, 0.375f, -1.6875f, -0.125f, 0.0f, 0.6875f, 0, 0.0f, 0.6875f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[793].setRotationPoint(-45.125f, -11.0f, -3.375f);
	}

}
