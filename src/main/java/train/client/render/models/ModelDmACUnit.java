//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 02.09.2021 - 12:49:00
// Last changed on: 02.09.2021 - 12:49:00

package train.client.render.models;


import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.core.ClientProxy;
import train.client.render.models.blocks.ModelLights;
import train.common.library.Info;

public class ModelDmACUnit extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelDmACUnit() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[147];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Du loco part01
		bodyModel[1] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Du loco part02
		bodyModel[2] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Du loco part03
		bodyModel[3] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Du loco part05
		bodyModel[4] = new ModelRendererTurbo(this, 225, 25, textureX, textureY); // Du loco part06
		bodyModel[5] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Du loco part07
		bodyModel[6] = new ModelRendererTurbo(this, 137, 33, textureX, textureY); // Du loco part08
		bodyModel[7] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Du loco part09
		bodyModel[8] = new ModelRendererTurbo(this, 209, 1, textureX, textureY); // Du loco part11
		bodyModel[9] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Du loco part12
		bodyModel[10] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Du loco part14
		bodyModel[11] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Du loco part15
		bodyModel[12] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Du loco part16
		bodyModel[13] = new ModelRendererTurbo(this, 313, 33, textureX, textureY); // Du loco part17
		bodyModel[14] = new ModelRendererTurbo(this, 433, 17, textureX, textureY); // Du loco part19
		bodyModel[15] = new ModelRendererTurbo(this, 465, 17, textureX, textureY); // Du loco part20
		bodyModel[16] = new ModelRendererTurbo(this, 137, 41, textureX, textureY); // Du loco part22
		bodyModel[17] = new ModelRendererTurbo(this, 449, 17, textureX, textureY); // Du loco part23
		bodyModel[18] = new ModelRendererTurbo(this, 489, 17, textureX, textureY); // Du loco part24
		bodyModel[19] = new ModelRendererTurbo(this, 185, 41, textureX, textureY); // Du loco part25
		bodyModel[20] = new ModelRendererTurbo(this, 209, 41, textureX, textureY); // Du loco part26
		bodyModel[21] = new ModelRendererTurbo(this, 233, 41, textureX, textureY); // Du loco part27
		bodyModel[22] = new ModelRendererTurbo(this, 353, 41, textureX, textureY); // Du loco part28
		bodyModel[23] = new ModelRendererTurbo(this, 401, 41, textureX, textureY); // Du loco part41
		bodyModel[24] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Du loco part47
		bodyModel[25] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Du loco part56
		bodyModel[26] = new ModelRendererTurbo(this, 209, 9, textureX, textureY); // Du loco part68
		bodyModel[27] = new ModelRendererTurbo(this, 177, 57, textureX, textureY); // Du loco part69
		bodyModel[28] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Du loco part71
		bodyModel[29] = new ModelRendererTurbo(this, 417, 49, textureX, textureY); // Du loco part82
		bodyModel[30] = new ModelRendererTurbo(this, 185, 9, textureX, textureY); // Du loco part83
		bodyModel[31] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Du loco part84
		bodyModel[32] = new ModelRendererTurbo(this, 209, 9, textureX, textureY); // Du loco part85
		bodyModel[33] = new ModelRendererTurbo(this, 497, 9, textureX, textureY); // Du loco part94
		bodyModel[34] = new ModelRendererTurbo(this, 217, 9, textureX, textureY); // Du loco part95
		bodyModel[35] = new ModelRendererTurbo(this, 209, 17, textureX, textureY); // Du loco part97
		bodyModel[36] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Du loco part100
		bodyModel[37] = new ModelRendererTurbo(this, 217, 17, textureX, textureY); // Du loco part101
		bodyModel[38] = new ModelRendererTurbo(this, 225, 17, textureX, textureY); // Du loco part102
		bodyModel[39] = new ModelRendererTurbo(this, 233, 17, textureX, textureY); // Du loco part Ligjhtpart14
		bodyModel[40] = new ModelRendererTurbo(this, 425, 17, textureX, textureY); // Du loco part Ligjhtpart17
		bodyModel[41] = new ModelRendererTurbo(this, 169, 25, textureX, textureY); // Du loco part Ligjhtpart16
		bodyModel[42] = new ModelRendererTurbo(this, 393, 25, textureX, textureY); // Du loco part Ligjhtpart18
		bodyModel[43] = new ModelRendererTurbo(this, 409, 25, textureX, textureY); // Du loco part Ligjhtpart15
		bodyModel[44] = new ModelRendererTurbo(this, 473, 25, textureX, textureY); // Du loco part Ligjhtpart13
		bodyModel[45] = new ModelRendererTurbo(this, 177, 65, textureX, textureY); // Box 146
		bodyModel[46] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Box 147
		bodyModel[47] = new ModelRendererTurbo(this, 177, 73, textureX, textureY); // Box 148
		bodyModel[48] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 149
		bodyModel[49] = new ModelRendererTurbo(this, 433, 17, textureX, textureY); // Box 154
		bodyModel[50] = new ModelRendererTurbo(this, 401, 25, textureX, textureY); // Box 156
		bodyModel[51] = new ModelRendererTurbo(this, 393, 57, textureX, textureY); // Box 167
		bodyModel[52] = new ModelRendererTurbo(this, 473, 57, textureX, textureY); // Box 168
		bodyModel[53] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 173
		bodyModel[54] = new ModelRendererTurbo(this, 241, 17, textureX, textureY); // Box 174
		bodyModel[55] = new ModelRendererTurbo(this, 417, 25, textureX, textureY); // Box 175
		bodyModel[56] = new ModelRendererTurbo(this, 425, 25, textureX, textureY); // Box 176
		bodyModel[57] = new ModelRendererTurbo(this, 401, 41, textureX, textureY); // Box 177
		bodyModel[58] = new ModelRendererTurbo(this, 433, 25, textureX, textureY); // Box 178
		bodyModel[59] = new ModelRendererTurbo(this, 465, 33, textureX, textureY); // Box 179
		bodyModel[60] = new ModelRendererTurbo(this, 473, 33, textureX, textureY); // Box 189
		bodyModel[61] = new ModelRendererTurbo(this, 489, 33, textureX, textureY); // Box 190
		bodyModel[62] = new ModelRendererTurbo(this, 497, 33, textureX, textureY); // Box 191
		bodyModel[63] = new ModelRendererTurbo(this, 505, 33, textureX, textureY); // Box 192
		bodyModel[64] = new ModelRendererTurbo(this, 409, 41, textureX, textureY); // Box 193
		bodyModel[65] = new ModelRendererTurbo(this, 425, 41, textureX, textureY); // Box 194
		bodyModel[66] = new ModelRendererTurbo(this, 233, 49, textureX, textureY); // Box 195
		bodyModel[67] = new ModelRendererTurbo(this, 241, 49, textureX, textureY); // Box 196
		bodyModel[68] = new ModelRendererTurbo(this, 177, 81, textureX, textureY); // Box 135
		bodyModel[69] = new ModelRendererTurbo(this, 329, 65, textureX, textureY); // Box 136
		bodyModel[70] = new ModelRendererTurbo(this, 449, 57, textureX, textureY); // Box 138
		bodyModel[71] = new ModelRendererTurbo(this, 353, 65, textureX, textureY); // Box 139
		bodyModel[72] = new ModelRendererTurbo(this, 409, 73, textureX, textureY); // Box 141
		bodyModel[73] = new ModelRendererTurbo(this, 249, 49, textureX, textureY); // Box 142
		bodyModel[74] = new ModelRendererTurbo(this, 265, 49, textureX, textureY); // Box 143
		bodyModel[75] = new ModelRendererTurbo(this, 441, 73, textureX, textureY); // Box 144
		bodyModel[76] = new ModelRendererTurbo(this, 457, 81, textureX, textureY); // Box 145
		bodyModel[77] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 146
		bodyModel[78] = new ModelRendererTurbo(this, 289, 49, textureX, textureY); // Box 147
		bodyModel[79] = new ModelRendererTurbo(this, 489, 57, textureX, textureY); // Box 148
		bodyModel[80] = new ModelRendererTurbo(this, 265, 49, textureX, textureY); // Box 149
		bodyModel[81] = new ModelRendererTurbo(this, 377, 81, textureX, textureY); // Box 150
		bodyModel[82] = new ModelRendererTurbo(this, 49, 89, textureX, textureY); // Box 151
		bodyModel[83] = new ModelRendererTurbo(this, 497, 57, textureX, textureY); // Box 152
		bodyModel[84] = new ModelRendererTurbo(this, 313, 81, textureX, textureY); // Box 153
		bodyModel[85] = new ModelRendererTurbo(this, 401, 81, textureX, textureY); // Box 154
		bodyModel[86] = new ModelRendererTurbo(this, 497, 81, textureX, textureY); // Box 155
		bodyModel[87] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 156
		bodyModel[88] = new ModelRendererTurbo(this, 73, 89, textureX, textureY); // Box 157
		bodyModel[89] = new ModelRendererTurbo(this, 305, 49, textureX, textureY); // Box 158
		bodyModel[90] = new ModelRendererTurbo(this, 321, 49, textureX, textureY); // Box 159
		bodyModel[91] = new ModelRendererTurbo(this, 329, 49, textureX, textureY); // Box 160
		bodyModel[92] = new ModelRendererTurbo(this, 337, 49, textureX, textureY); // Du loco part Ligjhtpart8
		bodyModel[93] = new ModelRendererTurbo(this, 345, 49, textureX, textureY); // Du loco part37
		bodyModel[94] = new ModelRendererTurbo(this, 401, 49, textureX, textureY); // Du loco part37
		bodyModel[95] = new ModelRendererTurbo(this, 409, 49, textureX, textureY); // Du loco part37
		bodyModel[96] = new ModelRendererTurbo(this, 425, 49, textureX, textureY); // Du loco part37
		bodyModel[97] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Du loco part37
		bodyModel[98] = new ModelRendererTurbo(this, 49, 57, textureX, textureY); // Du loco part37
		bodyModel[99] = new ModelRendererTurbo(this, 89, 89, textureX, textureY); // Du loco part37
		bodyModel[100] = new ModelRendererTurbo(this, 105, 89, textureX, textureY); // Du loco part37
		bodyModel[101] = new ModelRendererTurbo(this, 121, 89, textureX, textureY); // Du loco part37
		bodyModel[102] = new ModelRendererTurbo(this, 145, 89, textureX, textureY); // Du loco part37
		bodyModel[103] = new ModelRendererTurbo(this, 449, 49, textureX, textureY); // Du loco part47
		bodyModel[104] = new ModelRendererTurbo(this, 41, 57, textureX, textureY); // Du loco part47
		bodyModel[105] = new ModelRendererTurbo(this, 89, 57, textureX, textureY); // Du loco part47
		bodyModel[106] = new ModelRendererTurbo(this, 105, 57, textureX, textureY); // Du loco part47
		bodyModel[107] = new ModelRendererTurbo(this, 401, 89, textureX, textureY); // Du loco part42
		bodyModel[108] = new ModelRendererTurbo(this, 361, 97, textureX, textureY); // Du loco part42
		bodyModel[109] = new ModelRendererTurbo(this, 97, 57, textureX, textureY); // Du loco part37
		bodyModel[110] = new ModelRendererTurbo(this, 113, 57, textureX, textureY); // Du loco part37
		bodyModel[111] = new ModelRendererTurbo(this, 425, 73, textureX, textureY); // Du loco part37
		bodyModel[112] = new ModelRendererTurbo(this, 129, 57, textureX, textureY); // Du loco part37
		bodyModel[113] = new ModelRendererTurbo(this, 481, 57, textureX, textureY); // Du loco part37
		bodyModel[114] = new ModelRendererTurbo(this, 465, 57, textureX, textureY); // Du loco part47
		bodyModel[115] = new ModelRendererTurbo(this, 425, 89, textureX, textureY); // Box 146
		bodyModel[116] = new ModelRendererTurbo(this, 377, 73, textureX, textureY); // Box 146
		bodyModel[117] = new ModelRendererTurbo(this, 377, 65, textureX, textureY); // Box 146
		bodyModel[118] = new ModelRendererTurbo(this, 441, 89, textureX, textureY); // Box 146
		bodyModel[119] = new ModelRendererTurbo(this, 161, 97, textureX, textureY); // Box 146
		bodyModel[120] = new ModelRendererTurbo(this, 169, 73, textureX, textureY); // Box 146
		bodyModel[121] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // Du loco part16
		bodyModel[122] = new ModelRendererTurbo(this, 25, 105, textureX, textureY); // Du loco part26
		bodyModel[123] = new ModelRendererTurbo(this, 49, 105, textureX, textureY); // Box 146
		bodyModel[124] = new ModelRendererTurbo(this, 201, 105, textureX, textureY); // Box 146
		bodyModel[125] = new ModelRendererTurbo(this, 401, 105, textureX, textureY); // Box 149
		bodyModel[126] = new ModelRendererTurbo(this, 49, 113, textureX, textureY); // Box 149
		bodyModel[127] = new ModelRendererTurbo(this, 89, 113, textureX, textureY); // Box 149
		bodyModel[128] = new ModelRendererTurbo(this, 473, 57, textureX, textureY); // Du loco part37
		bodyModel[129] = new ModelRendererTurbo(this, 353, 65, textureX, textureY); // Du loco part37
		bodyModel[130] = new ModelRendererTurbo(this, 385, 65, textureX, textureY); // Du loco part37
		bodyModel[131] = new ModelRendererTurbo(this, 121, 113, textureX, textureY); // Du loco part37
		bodyModel[132] = new ModelRendererTurbo(this, 385, 97, textureX, textureY); // Du loco part37
		bodyModel[133] = new ModelRendererTurbo(this, 441, 97, textureX, textureY); // Du loco part37
		bodyModel[134] = new ModelRendererTurbo(this, 257, 113, textureX, textureY); // Du loco part37
		bodyModel[135] = new ModelRendererTurbo(this, 281, 113, textureX, textureY); // Du loco part37
		bodyModel[136] = new ModelRendererTurbo(this, 305, 113, textureX, textureY); // Box 138
		bodyModel[137] = new ModelRendererTurbo(this, 329, 113, textureX, textureY); // Box 139
		bodyModel[138] = new ModelRendererTurbo(this, 361, 113, textureX, textureY); // Box 141
		bodyModel[139] = new ModelRendererTurbo(this, 385, 113, textureX, textureY); // Box 142
		bodyModel[140] = new ModelRendererTurbo(this, 401, 113, textureX, textureY); // Box 143
		bodyModel[141] = new ModelRendererTurbo(this, 433, 113, textureX, textureY); // Box 144
		bodyModel[142] = new ModelRendererTurbo(this, 449, 113, textureX, textureY); // Box 145
		bodyModel[143] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 146
		bodyModel[144] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 147
		bodyModel[145] = new ModelRendererTurbo(this, 505, 73, textureX, textureY); // Box 148
		bodyModel[146] = new ModelRendererTurbo(this, 457, 49, textureX, textureY); // Box 149

		bodyModel[0].addBox(0F, 0F, 0F, 84, 8, 12, 0F); // Du loco part01
		bodyModel[0].setRotationPoint(0F, -11F, -6F);

		bodyModel[1].addBox(0F, 0F, 0F, 1, 3, 22, 0F); // Du loco part02
		bodyModel[1].setRotationPoint(-3F, -11F, -11F);

		bodyModel[2].addBox(0F, 0F, 0F, 87, 1, 22, 0F); // Du loco part03
		bodyModel[2].setRotationPoint(-3F, -12F, -11F);

		bodyModel[3].addBox(0F, 0F, 0F, 83, 1, 1, 0F); // Du loco part05
		bodyModel[3].setRotationPoint(1F, -11F, 7F);

		bodyModel[4].addBox(0F, 0F, 0F, 83, 1, 1, 0F); // Du loco part06
		bodyModel[4].setRotationPoint(1F, -11F, -8F);

		bodyModel[5].addBox(0F, 0F, 0F, 66, 18, 1, 0F); // Du loco part07
		bodyModel[5].setRotationPoint(18F, -30F, -11F);

		bodyModel[6].addBox(0F, 0F, 0F, 85, 3, 4, 0F); // Du loco part08
		bodyModel[6].setRotationPoint(-1F, -33F, -2F);

		bodyModel[7].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Du loco part09
		bodyModel[7].setRotationPoint(4F, -7F, 6.05F);

		bodyModel[8].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Du loco part11
		bodyModel[8].setRotationPoint(4F, -7F, -6.05F);

		bodyModel[9].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part12
		bodyModel[9].setRotationPoint(16F, -11F, -6.05F);

		bodyModel[10].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part14
		bodyModel[10].setRotationPoint(30F, -11F, -6.05F);

		bodyModel[11].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part15
		bodyModel[11].setRotationPoint(43F, -11F, -6.05F);

		bodyModel[12].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part16
		bodyModel[12].setRotationPoint(58F, -11F, -6.05F);

		bodyModel[13].addBox(0F, 0F, 0F, 57, 1, 0, 0F); // Du loco part17
		bodyModel[13].setRotationPoint(20F, -9F, -6.1F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 1, 18, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part19
		bodyModel[14].setRotationPoint(-2F, -30F, -6F);

		bodyModel[15].addBox(0F, 0F, 0F, 1, 18, 20, 0F); // Du loco part20
		bodyModel[15].setRotationPoint(83F, -30F, -10F);

		bodyModel[16].addBox(0F, 0F, 0F, 19, 18, 1, 0F); // Du loco part22
		bodyModel[16].setRotationPoint(-1F, -30F, 10F);

		bodyModel[17].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part23
		bodyModel[17].setRotationPoint(16F, -11F, 6.05F);

		bodyModel[18].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part24
		bodyModel[18].setRotationPoint(30F, -11F, 6.05F);

		bodyModel[19].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part25
		bodyModel[19].setRotationPoint(43F, -11F, 6.05F);

		bodyModel[20].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part26
		bodyModel[20].setRotationPoint(58F, -11F, 6.05F);

		bodyModel[21].addBox(0F, 0F, 0F, 57, 1, 0, 0F); // Du loco part27
		bodyModel[21].setRotationPoint(20F, -9F, 6.1F);

		bodyModel[22].addBox(0F, 0F, 0F, 19, 18, 1, 0F); // Du loco part28
		bodyModel[22].setRotationPoint(-1F, -30F, -11F);

		bodyModel[23].addBox(0F, 0F, 0F, 1, 2, 16, 0F); // Du loco part41
		bodyModel[23].setRotationPoint(-3F, -8F, -8F);

		bodyModel[24].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Du loco part47
		bodyModel[24].setRotationPoint(-7F, -8F, 4F);

		bodyModel[25].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Du loco part56
		bodyModel[25].setRotationPoint(-7F, -8F, -7F);

		bodyModel[26].addBox(0F, 0F, 0F, 0, 0, 4, 0F); // Du loco part68
		bodyModel[26].setRotationPoint(-2F, -33F, -2F);

		bodyModel[27].addBox(0F, 0F, 0F, 84, 2, 4, 0F); // Du loco part69
		bodyModel[27].setRotationPoint(0F, -32F, 2F);

		bodyModel[28].addBox(0F, 0F, 0F, 84, 2, 4, 0F); // Du loco part71
		bodyModel[28].setRotationPoint(0F, -32F, -6F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 5, 1, 20, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part82
		bodyModel[29].setRotationPoint(-1F, -19F, -10F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part83
		bodyModel[30].setRotationPoint(6F, -13F, -1F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part84
		bodyModel[31].setRotationPoint(7F, -13F, -1F);
		bodyModel[31].rotateAngleY = -1.57079633F;

		bodyModel[32].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part85
		bodyModel[32].setRotationPoint(7F, -13F, 2F);
		bodyModel[32].rotateAngleY = -1.57079633F;

		bodyModel[33].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part94
		bodyModel[33].setRotationPoint(6F, -16F, -1F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part95
		bodyModel[34].setRotationPoint(7F, -16F, 2F);
		bodyModel[34].rotateAngleY = -1.57079633F;

		bodyModel[35].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part97
		bodyModel[35].setRotationPoint(7F, -16F, -1F);
		bodyModel[35].rotateAngleY = -1.57079633F;

		bodyModel[36].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part100
		bodyModel[36].setRotationPoint(9F, -20F, -1F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part101
		bodyModel[37].setRotationPoint(9F, -19F, -2F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part102
		bodyModel[38].setRotationPoint(9F, -19F, 1F);

		bodyModel[39].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Du loco part Ligjhtpart14
		bodyModel[39].setRotationPoint(-3F, -17F, 6F);

		bodyModel[40].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Du loco part Ligjhtpart17
		bodyModel[40].setRotationPoint(-3F, -17F, -9F);

		bodyModel[41].addShapeBox(0F, 0F, 1F, 2, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part Ligjhtpart16
		bodyModel[41].setRotationPoint(-3F, -18F, -10F);

		bodyModel[42].addShapeBox(0F, 0F, 1F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Du loco part Ligjhtpart18
		bodyModel[42].setRotationPoint(-3F, -16F, -10F);

		bodyModel[43].addShapeBox(0F, 0F, 1F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Du loco part Ligjhtpart15
		bodyModel[43].setRotationPoint(-3F, -16F, 5F);

		bodyModel[44].addShapeBox(0F, 0F, 1F, 2, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part Ligjhtpart13
		bodyModel[44].setRotationPoint(-3F, -18F, 5F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 84, 1, 1, 0F,0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, -1F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 146
		bodyModel[45].setRotationPoint(0F, -31F, 6F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 84, 1, 1, 0F,0F, 1F, -1F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[46].setRotationPoint(0F, -31F, -7F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 83, 1, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 148
		bodyModel[47].setRotationPoint(1F, -33F, -3F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 83, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F); // Box 149
		bodyModel[48].setRotationPoint(1F, -33F, 2F);

		bodyModel[49].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 154
		bodyModel[49].setRotationPoint(-6F, -8F, -6F);

		bodyModel[50].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 156
		bodyModel[50].setRotationPoint(-6F, -8F, 5F);

		bodyModel[51].addShapeBox(0F, 0F, 1F, 1, 18, 5, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 167
		bodyModel[51].setRotationPoint(-2F, -30F, -12F);

		bodyModel[52].addShapeBox(0F, 0F, 1F, 1, 18, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 168
		bodyModel[52].setRotationPoint(-2F, -30F, 5F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 173
		bodyModel[53].setRotationPoint(-3F, -8F, -9F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 174
		bodyModel[54].setRotationPoint(-3F, -8F, 8F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 175
		bodyModel[55].setRotationPoint(6F, -16F, -1F);
		bodyModel[55].rotateAngleY = -1.57079633F;

		bodyModel[56].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 176
		bodyModel[56].setRotationPoint(6F, -16F, 2F);
		bodyModel[56].rotateAngleY = -1.57079633F;

		bodyModel[57].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 177
		bodyModel[57].setRotationPoint(9F, -16F, -2F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 178
		bodyModel[58].setRotationPoint(9F, -20F, 1F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 179
		bodyModel[59].setRotationPoint(9F, -20F, -2F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 189
		bodyModel[60].setRotationPoint(9F, -13F, 2F);
		bodyModel[60].rotateAngleY = -1.57079633F;

		bodyModel[61].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 190
		bodyModel[61].setRotationPoint(6F, -13F, 2F);
		bodyModel[61].rotateAngleY = -1.57079633F;

		bodyModel[62].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 191
		bodyModel[62].setRotationPoint(6F, -13F, -1F);
		bodyModel[62].rotateAngleY = -1.57079633F;

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 192
		bodyModel[63].setRotationPoint(9F, -13F, -1F);
		bodyModel[63].rotateAngleY = -1.57079633F;

		bodyModel[64].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 193
		bodyModel[64].setRotationPoint(8F, -15F, 1F);
		bodyModel[64].rotateAngleY = -1.57079633F;

		bodyModel[65].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 194
		bodyModel[65].setRotationPoint(8F, -15F, 0F);
		bodyModel[65].rotateAngleY = -1.57079633F;

		bodyModel[66].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 195
		bodyModel[66].setRotationPoint(7F, -15F, 0F);
		bodyModel[66].rotateAngleY = -1.57079633F;

		bodyModel[67].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 196
		bodyModel[67].setRotationPoint(7F, -15F, 1F);
		bodyModel[67].rotateAngleY = -1.57079633F;

		bodyModel[68].addBox(0F, 0F, 0F, 66, 18, 1, 0F); // Box 135
		bodyModel[68].setRotationPoint(18F, -30F, 10F);

		bodyModel[69].addBox(0F, 0F, 0F, 1, 18, 20, 0F); // Box 136
		bodyModel[69].setRotationPoint(18F, -30F, -10F);

		bodyModel[70].addBox(0F, 0F, 0F, 4, 4, 4, 0F); // Box 138
		bodyModel[70].setRotationPoint(20F, -16F, -2F);

		bodyModel[71].addBox(0F, 0F, 0F, 6, 6, 6, 0F); // Box 139
		bodyModel[71].setRotationPoint(19F, -22F, -3F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 141
		bodyModel[72].setRotationPoint(19F, -24F, -3F);

		bodyModel[73].addBox(0F, 0F, 0F, 4, 1, 4, 0F); // Box 142
		bodyModel[73].setRotationPoint(20F, -25F, -2F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 6, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 143
		bodyModel[74].setRotationPoint(19F, -26F, -3F);

		bodyModel[75].addBox(0F, 0F, 0F, 6, 4, 6, 0F); // Box 144
		bodyModel[75].setRotationPoint(19F, -30F, -3F);

		bodyModel[76].addBox(0F, 0F, 0F, 12, 15, 12, 0F); // Box 145
		bodyModel[76].setRotationPoint(28F, -27F, -5F);

		bodyModel[77].addBox(0F, 0F, 0F, 14, 1, 14, 0F); // Box 146
		bodyModel[77].setRotationPoint(27F, -28F, -6F);

		bodyModel[78].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 147
		bodyModel[78].setRotationPoint(22F, -19F, -4F);

		bodyModel[79].addBox(0F, 0F, 0F, 1, 7, 1, 0F); // Box 148
		bodyModel[79].setRotationPoint(26F, -26F, -4F);

		bodyModel[80].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 149
		bodyModel[80].setRotationPoint(27F, -26F, -4F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 150
		bodyModel[81].setRotationPoint(41F, -16F, -2F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 151
		bodyModel[82].setRotationPoint(51F, -16F, -2F);

		bodyModel[83].addBox(0F, 0F, 0F, 2, 14, 2, 0F); // Box 152
		bodyModel[83].setRotationPoint(52F, -30F, -1F);

		bodyModel[84].addBox(0F, 0F, 0F, 2, 14, 2, 0F); // Box 153
		bodyModel[84].setRotationPoint(42F, -30F, -1F);

		bodyModel[85].addBox(0F, 0F, 0F, 2, 9, 2, 0F); // Box 154
		bodyModel[85].setRotationPoint(45F, -27F, -1F);

		bodyModel[86].addBox(0F, 0F, 0F, 2, 9, 2, 0F); // Box 155
		bodyModel[86].setRotationPoint(49F, -27F, -1F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 1F, 0F, 1F); // Box 156
		bodyModel[87].setRotationPoint(49F, -18F, -1F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, 1F, 1F, 0F, 1F, 0F, 0F, 1F); // Box 157
		bodyModel[88].setRotationPoint(45F, -18F, -1F);

		bodyModel[89].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 158
		bodyModel[89].setRotationPoint(47F, -26F, -1F);

		bodyModel[90].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 159
		bodyModel[90].setRotationPoint(51F, -26F, -1F);

		bodyModel[91].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 160
		bodyModel[91].setRotationPoint(44F, -26F, -1F);

		bodyModel[92].addBox(0F, 0F, 0F, 1, 3, 4, 0F); // Du loco part Ligjhtpart8
		bodyModel[92].setRotationPoint(-2F, -33F, -2F);

		bodyModel[93].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Du loco part37
		bodyModel[93].setRotationPoint(0F, -33F, -6F);

		bodyModel[94].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Du loco part37
		bodyModel[94].setRotationPoint(0F, -33F, 5F);

		bodyModel[95].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Du loco part37
		bodyModel[95].setRotationPoint(18F, -33F, -6F);

		bodyModel[96].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Du loco part37
		bodyModel[96].setRotationPoint(18F, -33F, 5F);

		bodyModel[97].addBox(0F, 0F, 0F, 19, 1, 1, 0F); // Du loco part37
		bodyModel[97].setRotationPoint(0F, -34F, -6F);

		bodyModel[98].addBox(0F, 0F, 0F, 19, 1, 1, 0F); // Du loco part37
		bodyModel[98].setRotationPoint(0F, -34F, 5F);

		bodyModel[99].addShapeBox(0F, 0F, 0F, 6, 7, 1, 0F,0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F); // Du loco part37
		bodyModel[99].setRotationPoint(13F, -40F, 4F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 6, 7, 1, 0F,0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F); // Du loco part37
		bodyModel[100].setRotationPoint(0F, -40F, 4F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 9, 7, 1, 0F,-1F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -1F, 0F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F); // Du loco part37
		bodyModel[101].setRotationPoint(10F, -46F, 3F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 9, 7, 1, 0F,0F, -6F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F); // Du loco part37
		bodyModel[102].setRotationPoint(0F, -46F, 3F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Du loco part47
		bodyModel[103].setRotationPoint(-7F, -7F, 4F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part47
		bodyModel[104].setRotationPoint(-7F, -9F, 4F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Du loco part47
		bodyModel[105].setRotationPoint(-7F, -7F, -7F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part47
		bodyModel[106].setRotationPoint(-7F, -9F, -7F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 5, 4, 11, 0F,0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F); // Du loco part42
		bodyModel[107].setRotationPoint(-5F, -6F, 0F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 5, 4, 11, 0F,-5F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Du loco part42
		bodyModel[108].setRotationPoint(-5F, -6F, -11F);

		bodyModel[109].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Du loco part37
		bodyModel[109].setRotationPoint(8F, -47F, -4F);

		bodyModel[110].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Du loco part37
		bodyModel[110].setRotationPoint(8F, -47F, 3F);

		bodyModel[111].addBox(0F, 0F, 0F, 1, 1, 12, 0F); // Du loco part37
		bodyModel[111].setRotationPoint(9F, -48F, -6F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Du loco part37
		bodyModel[112].setRotationPoint(9F, -48F, 6F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Du loco part37
		bodyModel[113].setRotationPoint(9F, -48F, -8F);

		bodyModel[114].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Du loco part47
		bodyModel[114].setRotationPoint(-4F, -8F, -1F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F,-2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[115].setRotationPoint(-2F, -32F, 2F);

		bodyModel[116].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F,-2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, -2F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 146
		bodyModel[116].setRotationPoint(-2F, -32F, 6F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[117].setRotationPoint(0F, -33F, 2F);

		bodyModel[118].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F,-2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[118].setRotationPoint(-2F, -32F, -6F);

		bodyModel[119].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F,-2F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[119].setRotationPoint(-2F, -32F, -11F);

		bodyModel[120].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[120].setRotationPoint(0F, -33F, -6F);

		bodyModel[121].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part16
		bodyModel[121].setRotationPoint(72F, -11F, -6.05F);

		bodyModel[122].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part26
		bodyModel[122].setRotationPoint(72F, -11F, 6.05F);

		bodyModel[123].addShapeBox(0F, 0F, 0F, 71, 2, 2, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[123].setRotationPoint(13F, -32F, -11F);

		bodyModel[124].addShapeBox(0F, 0F, 0F, 71, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[124].setRotationPoint(13F, -32F, 9F);

		bodyModel[125].addBox(0F, 0F, 0F, 14, 1, 3, 0F); // Box 149
		bodyModel[125].setRotationPoint(70F, -34F, -5F);

		bodyModel[126].addShapeBox(0F, 0F, 0F, 14, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 149
		bodyModel[126].setRotationPoint(70F, -35F, -5F);

		bodyModel[127].addShapeBox(0F, 0F, 0F, 14, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 149
		bodyModel[127].setRotationPoint(70F, -33F, -5F);

		bodyModel[128].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Du loco part37
		bodyModel[128].setRotationPoint(37F, -34F, -6F);

		bodyModel[129].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Du loco part37
		bodyModel[129].setRotationPoint(56F, -34F, -6F);

		bodyModel[130].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Du loco part37
		bodyModel[130].setRotationPoint(76F, -34F, -6F);

		bodyModel[131].addBox(0F, 0F, 0F, 65, 1, 0, 0F); // Du loco part37
		bodyModel[131].setRotationPoint(19F, -34F, -6F);

		bodyModel[132].addShapeBox(0F, 0F, 0F, 6, 7, 1, 0F,0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F); // Du loco part37
		bodyModel[132].setRotationPoint(13F, -40F, -5F);

		bodyModel[133].addShapeBox(0F, 0F, 0F, 6, 7, 1, 0F,0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F); // Du loco part37
		bodyModel[133].setRotationPoint(0F, -40F, -5F);

		bodyModel[134].addShapeBox(0F, 0F, 0F, 9, 7, 1, 0F,-1F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -1F, 0F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F); // Du loco part37
		bodyModel[134].setRotationPoint(10F, -46F, -4F);

		bodyModel[135].addShapeBox(0F, 0F, 0F, 9, 7, 1, 0F,0F, -6F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F); // Du loco part37
		bodyModel[135].setRotationPoint(0F, -46F, -4F);

		bodyModel[136].addBox(0F, 0F, 0F, 4, 4, 4, 0F); // Box 138
		bodyModel[136].setRotationPoint(72F, -16F, -2F);

		bodyModel[137].addBox(0F, 0F, 0F, 6, 6, 6, 0F); // Box 139
		bodyModel[137].setRotationPoint(71F, -22F, -3F);

		bodyModel[138].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 141
		bodyModel[138].setRotationPoint(71F, -24F, -3F);

		bodyModel[139].addBox(0F, 0F, 0F, 4, 1, 4, 0F); // Box 142
		bodyModel[139].setRotationPoint(72F, -25F, -2F);

		bodyModel[140].addShapeBox(0F, 0F, 0F, 6, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 143
		bodyModel[140].setRotationPoint(71F, -26F, -3F);

		bodyModel[141].addBox(0F, 0F, 0F, 6, 4, 6, 0F); // Box 144
		bodyModel[141].setRotationPoint(71F, -30F, -3F);

		bodyModel[142].addBox(0F, 0F, 0F, 12, 15, 12, 0F); // Box 145
		bodyModel[142].setRotationPoint(56F, -27F, -5F);

		bodyModel[143].addBox(0F, 0F, 0F, 14, 1, 14, 0F); // Box 146
		bodyModel[143].setRotationPoint(55F, -28F, -6F);

		bodyModel[144].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 147
		bodyModel[144].setRotationPoint(69F, -19F, -4F);

		bodyModel[145].addBox(0F, 0F, 0F, 1, 7, 1, 0F); // Box 148
		bodyModel[145].setRotationPoint(69F, -26F, -4F);

		bodyModel[146].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 149
		bodyModel[146].setRotationPoint(68F, -26F, -4F);
	}
}