//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 09.11.2022 - 09:12:16
// Last changed on: 09.11.2022 - 09:12:16

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.library.Info;

public class ModelBTRLogCar extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelBTRLogCar() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[120];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 249, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Box 3
		bodyModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 5
		bodyModel[4] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 6
		bodyModel[5] = new ModelRendererTurbo(this, 241, 1, textureX, textureY); // Box 11
		bodyModel[6] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 13
		bodyModel[7] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Box 15
		bodyModel[8] = new ModelRendererTurbo(this, 329, 1, textureX, textureY); // Box 17
		bodyModel[9] = new ModelRendererTurbo(this, 369, 1, textureX, textureY); // Box 19
		bodyModel[10] = new ModelRendererTurbo(this, 385, 1, textureX, textureY); // Box 21
		bodyModel[11] = new ModelRendererTurbo(this, 401, 1, textureX, textureY); // Box 23
		bodyModel[12] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 25
		bodyModel[13] = new ModelRendererTurbo(this, 433, 1, textureX, textureY); // Box 27
		bodyModel[14] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Box 29
		bodyModel[15] = new ModelRendererTurbo(this, 345, 9, textureX, textureY); // Box 30
		bodyModel[16] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 31
		bodyModel[17] = new ModelRendererTurbo(this, 193, 33, textureX, textureY); // Box 65
		bodyModel[18] = new ModelRendererTurbo(this, 321, 41, textureX, textureY); // Box 69
		bodyModel[19] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 72
		bodyModel[20] = new ModelRendererTurbo(this, 225, 9, textureX, textureY); // Box 193
		bodyModel[21] = new ModelRendererTurbo(this, 257, 1, textureX, textureY); // Box 194
		bodyModel[22] = new ModelRendererTurbo(this, 281, 9, textureX, textureY); // Box 195
		bodyModel[23] = new ModelRendererTurbo(this, 337, 17, textureX, textureY); // Box 197
		bodyModel[24] = new ModelRendererTurbo(this, 225, 9, textureX, textureY); // Box 198
		bodyModel[25] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Box 199
		bodyModel[26] = new ModelRendererTurbo(this, 241, 9, textureX, textureY); // Box 200
		bodyModel[27] = new ModelRendererTurbo(this, 393, 41, textureX, textureY); // Box 50
		bodyModel[28] = new ModelRendererTurbo(this, 393, 49, textureX, textureY); // Box 51
		bodyModel[29] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 52
		bodyModel[30] = new ModelRendererTurbo(this, 97, 57, textureX, textureY); // Box 56
		bodyModel[31] = new ModelRendererTurbo(this, 393, 57, textureX, textureY); // Box 60
		bodyModel[32] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 64
		bodyModel[33] = new ModelRendererTurbo(this, 97, 65, textureX, textureY); // Box 68
		bodyModel[34] = new ModelRendererTurbo(this, 241, 57, textureX, textureY); // Box 72
		bodyModel[35] = new ModelRendererTurbo(this, 369, 65, textureX, textureY); // Box 73
		bodyModel[36] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Box 75
		bodyModel[37] = new ModelRendererTurbo(this, 97, 73, textureX, textureY); // Box 76
		bodyModel[38] = new ModelRendererTurbo(this, 241, 73, textureX, textureY); // Box 77
		bodyModel[39] = new ModelRendererTurbo(this, 337, 73, textureX, textureY); // Box 78
		bodyModel[40] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 80
		bodyModel[41] = new ModelRendererTurbo(this, 97, 81, textureX, textureY); // Box 81
		bodyModel[42] = new ModelRendererTurbo(this, 193, 81, textureX, textureY); // Box 82
		bodyModel[43] = new ModelRendererTurbo(this, 289, 81, textureX, textureY); // Box 83
		bodyModel[44] = new ModelRendererTurbo(this, 385, 81, textureX, textureY); // Box 84
		bodyModel[45] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 85
		bodyModel[46] = new ModelRendererTurbo(this, 97, 89, textureX, textureY); // Box 86
		bodyModel[47] = new ModelRendererTurbo(this, 193, 89, textureX, textureY); // Box 87
		bodyModel[48] = new ModelRendererTurbo(this, 289, 89, textureX, textureY); // Box 88
		bodyModel[49] = new ModelRendererTurbo(this, 385, 89, textureX, textureY); // Box 89
		bodyModel[50] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Box 90
		bodyModel[51] = new ModelRendererTurbo(this, 97, 97, textureX, textureY); // Box 91
		bodyModel[52] = new ModelRendererTurbo(this, 193, 97, textureX, textureY); // Box 92
		bodyModel[53] = new ModelRendererTurbo(this, 289, 97, textureX, textureY); // Box 96
		bodyModel[54] = new ModelRendererTurbo(this, 385, 97, textureX, textureY); // Box 97
		bodyModel[55] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // Box 99
		bodyModel[56] = new ModelRendererTurbo(this, 97, 105, textureX, textureY); // Box 100
		bodyModel[57] = new ModelRendererTurbo(this, 193, 105, textureX, textureY); // Box 101
		bodyModel[58] = new ModelRendererTurbo(this, 289, 105, textureX, textureY); // Box 102
		bodyModel[59] = new ModelRendererTurbo(this, 385, 105, textureX, textureY); // Box 103
		bodyModel[60] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 104
		bodyModel[61] = new ModelRendererTurbo(this, 97, 113, textureX, textureY); // Box 105
		bodyModel[62] = new ModelRendererTurbo(this, 193, 113, textureX, textureY); // Box 106
		bodyModel[63] = new ModelRendererTurbo(this, 289, 113, textureX, textureY); // Box 107
		bodyModel[64] = new ModelRendererTurbo(this, 385, 113, textureX, textureY); // Box 108
		bodyModel[65] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 109
		bodyModel[66] = new ModelRendererTurbo(this, 97, 121, textureX, textureY); // Box 110
		bodyModel[67] = new ModelRendererTurbo(this, 193, 121, textureX, textureY); // Box 111
		bodyModel[68] = new ModelRendererTurbo(this, 289, 121, textureX, textureY); // Box 112
		bodyModel[69] = new ModelRendererTurbo(this, 385, 121, textureX, textureY); // Box 113
		bodyModel[70] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // Box 114
		bodyModel[71] = new ModelRendererTurbo(this, 97, 129, textureX, textureY); // Box 115
		bodyModel[72] = new ModelRendererTurbo(this, 193, 129, textureX, textureY); // Box 116
		bodyModel[73] = new ModelRendererTurbo(this, 289, 129, textureX, textureY); // Box 117
		bodyModel[74] = new ModelRendererTurbo(this, 385, 129, textureX, textureY); // Box 118
		bodyModel[75] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 119
		bodyModel[76] = new ModelRendererTurbo(this, 97, 137, textureX, textureY); // Box 120
		bodyModel[77] = new ModelRendererTurbo(this, 193, 137, textureX, textureY); // Box 121
		bodyModel[78] = new ModelRendererTurbo(this, 289, 137, textureX, textureY); // Box 122
		bodyModel[79] = new ModelRendererTurbo(this, 385, 137, textureX, textureY); // Box 123
		bodyModel[80] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 124
		bodyModel[81] = new ModelRendererTurbo(this, 97, 145, textureX, textureY); // Box 125
		bodyModel[82] = new ModelRendererTurbo(this, 193, 145, textureX, textureY); // Box 126
		bodyModel[83] = new ModelRendererTurbo(this, 289, 145, textureX, textureY); // Box 127
		bodyModel[84] = new ModelRendererTurbo(this, 385, 145, textureX, textureY); // Box 128
		bodyModel[85] = new ModelRendererTurbo(this, 1, 153, textureX, textureY); // Box 129
		bodyModel[86] = new ModelRendererTurbo(this, 97, 153, textureX, textureY); // Box 130
		bodyModel[87] = new ModelRendererTurbo(this, 193, 153, textureX, textureY); // Box 131
		bodyModel[88] = new ModelRendererTurbo(this, 289, 153, textureX, textureY); // Box 132
		bodyModel[89] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // Box 83
		bodyModel[90] = new ModelRendererTurbo(this, 313, 1, textureX, textureY); // Box 109
		bodyModel[91] = new ModelRendererTurbo(this, 345, 1, textureX, textureY); // Box 110
		bodyModel[92] = new ModelRendererTurbo(this, 281, 9, textureX, textureY); // Box 111
		bodyModel[93] = new ModelRendererTurbo(this, 297, 9, textureX, textureY); // Box 112
		bodyModel[94] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 113
		bodyModel[95] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 140
		bodyModel[96] = new ModelRendererTurbo(this, 265, 9, textureX, textureY); // Box 141
		bodyModel[97] = new ModelRendererTurbo(this, 313, 9, textureX, textureY); // Box 142
		bodyModel[98] = new ModelRendererTurbo(this, 329, 9, textureX, textureY); // Box 143
		bodyModel[99] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 144
		bodyModel[100] = new ModelRendererTurbo(this, 345, 9, textureX, textureY); // Box 145
		bodyModel[101] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 120
		bodyModel[102] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 121
		bodyModel[103] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 122
		bodyModel[104] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 123
		bodyModel[105] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 124
		bodyModel[106] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 125
		bodyModel[107] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 126
		bodyModel[108] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 127
		bodyModel[109] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 128
		bodyModel[110] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 129
		bodyModel[111] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 130
		bodyModel[112] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 131
		bodyModel[113] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 132
		bodyModel[114] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 133
		bodyModel[115] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 134
		bodyModel[116] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 135
		bodyModel[117] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 136
		bodyModel[118] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 137
		bodyModel[119] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 138

		bodyModel[0].addShapeBox(0F, 0F, 0F, 100, 2, 22, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[0].setRotationPoint(-51F, -6F, -11F);

		bodyModel[1].addBox(0F, 0F, 0F, 1, 20, 22, 0F); // Box 1
		bodyModel[1].setRotationPoint(47F, -26F, -11F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 2, 2, 22, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		bodyModel[2].setRotationPoint(47F, -28F, -11F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		bodyModel[3].setRotationPoint(-40F, -9F, -11F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[4].setRotationPoint(-40F, -9F, 8F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[5].setRotationPoint(-24F, -9F, 8F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		bodyModel[6].setRotationPoint(-24F, -9F, -11F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		bodyModel[7].setRotationPoint(-9F, -9F, 8F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		bodyModel[8].setRotationPoint(-9F, -9F, -11F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		bodyModel[9].setRotationPoint(7F, -9F, 8F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		bodyModel[10].setRotationPoint(7F, -9F, -11F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		bodyModel[11].setRotationPoint(24F, -9F, 8F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		bodyModel[12].setRotationPoint(24F, -9F, -11F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		bodyModel[13].setRotationPoint(38F, -9F, 8F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,-1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		bodyModel[14].setRotationPoint(38F, -9F, -11F);

		bodyModel[15].addShapeBox(-25F, 0F, 0F, 50, 3, 22, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -13F, 0F, 0F, -13F, 0F, 0F, -13F, 0F, 0F, -13F, 0F, 0F); // Box 30
		bodyModel[15].setRotationPoint(0F, -4F, -11F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		bodyModel[16].setRotationPoint(-38.5F, -28F, 9F);

		bodyModel[17].addBox(0F, 0F, 0F, 1, 20, 22, 0F); // Box 65
		bodyModel[17].setRotationPoint(-50F, -26F, -11F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 2, 2, 22, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		bodyModel[18].setRotationPoint(-51F, -28F, -11F);

		bodyModel[19].addBox(0F, 0F, 0F, 6, 2, 2, 0F); // Box 72
		bodyModel[19].setRotationPoint(-57F, -4F, -0.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 193
		bodyModel[20].setRotationPoint(-53F, -5F, -10.5F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 194
		bodyModel[21].setRotationPoint(-53F, -5F, -0.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 195
		bodyModel[22].setRotationPoint(-53F, -5F, 1.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 197
		bodyModel[23].setRotationPoint(49F, -5F, -10.5F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 198
		bodyModel[24].setRotationPoint(49F, -5F, -0.5F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 199
		bodyModel[25].setRotationPoint(49F, -5F, 1.5F);

		bodyModel[26].addBox(0F, 0F, 0F, 6, 2, 2, 0F); // Box 200
		bodyModel[26].setRotationPoint(49F, -4F, -0.5F);

		bodyModel[27].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 50
		bodyModel[27].setRotationPoint(3F, -9F, 1F);

		bodyModel[28].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 51
		bodyModel[28].setRotationPoint(3F, -9F, 5F);

		bodyModel[29].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 52
		bodyModel[29].setRotationPoint(1F, -12F, 4.5F);
		bodyModel[29].rotateAngleX = 0.2443461F;

		bodyModel[30].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 56
		bodyModel[30].setRotationPoint(3F, -15F, 6.5F);
		bodyModel[30].rotateAngleX = -0.33161256F;

		bodyModel[31].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 60
		bodyModel[31].setRotationPoint(3F, -17.1F, 3.75F);
		bodyModel[31].rotateAngleX = 0.36651914F;

		bodyModel[32].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 64
		bodyModel[32].setRotationPoint(1F, -21.25F, 6.25F);
		bodyModel[32].rotateAngleX = -0.33161256F;

		bodyModel[33].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 68
		bodyModel[33].setRotationPoint(3F, -20.75F, 2.55F);
		bodyModel[33].rotateAngleX = 1.06465084F;

		bodyModel[34].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 72
		bodyModel[34].setRotationPoint(3F, -23F, 7F);
		bodyModel[34].rotateAngleX = -0.20943951F;

		bodyModel[35].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 73
		bodyModel[35].setRotationPoint(1F, -12F, 0.5F);
		bodyModel[35].rotateAngleX = 0.2443461F;

		bodyModel[36].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 75
		bodyModel[36].setRotationPoint(3F, -15F, 0.25F);
		bodyModel[36].rotateAngleX = 0.87266463F;

		bodyModel[37].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 76
		bodyModel[37].setRotationPoint(2F, -20.75F, 2.65F);
		bodyModel[37].rotateAngleX = -0.62831853F;

		bodyModel[38].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 77
		bodyModel[38].setRotationPoint(3F, -14.75F, 4F);
		bodyModel[38].rotateAngleX = -0.55850536F;

		bodyModel[39].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 78
		bodyModel[39].setRotationPoint(3F, -25F, 4F);
		bodyModel[39].rotateAngleX = 0.54105207F;

		bodyModel[40].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 80
		bodyModel[40].setRotationPoint(3F, -9F, -2.75F);

		bodyModel[41].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 81
		bodyModel[41].setRotationPoint(3F, -9F, -6.75F);

		bodyModel[42].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 82
		bodyModel[42].setRotationPoint(1F, -12F, -7.25F);
		bodyModel[42].rotateAngleX = 0.2443461F;

		bodyModel[43].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 83
		bodyModel[43].setRotationPoint(1F, -12F, -3.25F);
		bodyModel[43].rotateAngleX = 0.2443461F;

		bodyModel[44].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 84
		bodyModel[44].setRotationPoint(3F, -15F, -1.25F);
		bodyModel[44].rotateAngleX = -0.33161256F;

		bodyModel[45].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 85
		bodyModel[45].setRotationPoint(3F, -14.75F, -3.75F);
		bodyModel[45].rotateAngleX = -0.55850536F;

		bodyModel[46].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 86
		bodyModel[46].setRotationPoint(3F, -15F, -7.5F);
		bodyModel[46].rotateAngleX = 0.87266463F;

		bodyModel[47].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 87
		bodyModel[47].setRotationPoint(3F, -17.1F, -4F);
		bodyModel[47].rotateAngleX = 0.36651914F;

		bodyModel[48].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 88
		bodyModel[48].setRotationPoint(2F, -20.75F, -5.1F);
		bodyModel[48].rotateAngleX = -0.62831853F;

		bodyModel[49].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 89
		bodyModel[49].setRotationPoint(1F, -21.25F, -1.5F);
		bodyModel[49].rotateAngleX = -0.33161256F;

		bodyModel[50].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 90
		bodyModel[50].setRotationPoint(3F, -20.75F, -5.2F);
		bodyModel[50].rotateAngleX = 1.06465084F;

		bodyModel[51].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 91
		bodyModel[51].setRotationPoint(3F, -23F, -0.75F);
		bodyModel[51].rotateAngleX = -0.20943951F;

		bodyModel[52].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 92
		bodyModel[52].setRotationPoint(3F, -25F, -3.75F);
		bodyModel[52].rotateAngleX = 0.38397244F;

		bodyModel[53].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 96
		bodyModel[53].setRotationPoint(3F, -14.75F, -7.75F);
		bodyModel[53].rotateAngleX = -0.55850536F;

		bodyModel[54].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 97
		bodyModel[54].setRotationPoint(3F, -17.7F, -7.6F);
		bodyModel[54].rotateAngleX = -0.76794487F;

		bodyModel[55].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 99
		bodyModel[55].setRotationPoint(3F, -20.7F, -7.6F);
		bodyModel[55].rotateAngleX = -0.76794487F;

		bodyModel[56].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 100
		bodyModel[56].setRotationPoint(0F, -22.22F, -8.75F);
		bodyModel[56].rotateAngleX = 0.6981317F;

		bodyModel[57].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 101
		bodyModel[57].setRotationPoint(2F, -26F, 2.25F);
		bodyModel[57].rotateAngleX = -0.71558499F;

		bodyModel[58].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 102
		bodyModel[58].setRotationPoint(-45F, -9F, -6.75F);

		bodyModel[59].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 103
		bodyModel[59].setRotationPoint(-45F, -9F, -2.75F);

		bodyModel[60].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 104
		bodyModel[60].setRotationPoint(-45F, -9F, 1F);

		bodyModel[61].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 105
		bodyModel[61].setRotationPoint(-45F, -9F, 5F);

		bodyModel[62].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 106
		bodyModel[62].setRotationPoint(-47F, -12F, 4.5F);
		bodyModel[62].rotateAngleX = 0.2443461F;

		bodyModel[63].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 107
		bodyModel[63].setRotationPoint(-47F, -12F, 0.5F);
		bodyModel[63].rotateAngleX = 0.2443461F;

		bodyModel[64].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 108
		bodyModel[64].setRotationPoint(-47F, -12F, -3.25F);
		bodyModel[64].rotateAngleX = 0.2443461F;

		bodyModel[65].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 109
		bodyModel[65].setRotationPoint(-47F, -12F, -7.25F);
		bodyModel[65].rotateAngleX = 0.2443461F;

		bodyModel[66].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 110
		bodyModel[66].setRotationPoint(-45F, -14.75F, -7.75F);
		bodyModel[66].rotateAngleX = -0.55850536F;

		bodyModel[67].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 111
		bodyModel[67].setRotationPoint(-45F, -14.75F, -3.75F);
		bodyModel[67].rotateAngleX = -0.55850536F;

		bodyModel[68].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 112
		bodyModel[68].setRotationPoint(-45F, -15F, -7.5F);
		bodyModel[68].rotateAngleX = 0.87266463F;

		bodyModel[69].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 113
		bodyModel[69].setRotationPoint(-45F, -15F, -1.25F);
		bodyModel[69].rotateAngleX = -0.33161256F;

		bodyModel[70].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 114
		bodyModel[70].setRotationPoint(-45F, -17.1F, -4F);
		bodyModel[70].rotateAngleX = 0.36651914F;

		bodyModel[71].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 115
		bodyModel[71].setRotationPoint(-45F, -15F, 0.25F);
		bodyModel[71].rotateAngleX = 0.87266463F;

		bodyModel[72].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 116
		bodyModel[72].setRotationPoint(-45F, -14.75F, 4F);
		bodyModel[72].rotateAngleX = -0.55850536F;

		bodyModel[73].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 117
		bodyModel[73].setRotationPoint(-45F, -15F, 6.5F);
		bodyModel[73].rotateAngleX = -0.33161256F;

		bodyModel[74].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 118
		bodyModel[74].setRotationPoint(-45F, -17.1F, 3.75F);
		bodyModel[74].rotateAngleX = 0.36651914F;

		bodyModel[75].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 119
		bodyModel[75].setRotationPoint(-47F, -21.25F, 6.25F);
		bodyModel[75].rotateAngleX = -0.33161256F;

		bodyModel[76].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 120
		bodyModel[76].setRotationPoint(-46F, -20.75F, 2.65F);
		bodyModel[76].rotateAngleX = -0.62831853F;

		bodyModel[77].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 121
		bodyModel[77].setRotationPoint(-45F, -20.75F, 2.55F);
		bodyModel[77].rotateAngleX = 1.06465084F;

		bodyModel[78].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 122
		bodyModel[78].setRotationPoint(-45F, -23F, 7F);
		bodyModel[78].rotateAngleX = -0.20943951F;

		bodyModel[79].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 123
		bodyModel[79].setRotationPoint(-45F, -25F, 4F);
		bodyModel[79].rotateAngleX = 0.54105207F;

		bodyModel[80].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 124
		bodyModel[80].setRotationPoint(-46F, -26F, 2.25F);
		bodyModel[80].rotateAngleX = -0.71558499F;

		bodyModel[81].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 125
		bodyModel[81].setRotationPoint(-45F, -23F, -0.75F);
		bodyModel[81].rotateAngleX = -0.20943951F;

		bodyModel[82].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 126
		bodyModel[82].setRotationPoint(-45F, -25F, -3.75F);
		bodyModel[82].rotateAngleX = 0.38397244F;

		bodyModel[83].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 127
		bodyModel[83].setRotationPoint(-47F, -21.25F, -1.5F);
		bodyModel[83].rotateAngleX = -0.33161256F;

		bodyModel[84].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 128
		bodyModel[84].setRotationPoint(-45F, -20.75F, -5.2F);
		bodyModel[84].rotateAngleX = 1.06465084F;

		bodyModel[85].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 129
		bodyModel[85].setRotationPoint(-46F, -20.75F, -5.1F);
		bodyModel[85].rotateAngleX = -0.62831853F;

		bodyModel[86].addBox(0F, 0F, 0F, 43, 3, 3, 0F); // Box 130
		bodyModel[86].setRotationPoint(-48F, -22.22F, -8.75F);
		bodyModel[86].rotateAngleX = 0.6981317F;

		bodyModel[87].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 131
		bodyModel[87].setRotationPoint(-45F, -20.7F, -7.6F);
		bodyModel[87].rotateAngleX = -0.76794487F;

		bodyModel[88].addBox(0F, 0F, 0F, 43, 2, 2, 0F); // Box 132
		bodyModel[88].setRotationPoint(-45F, -17.7F, -7.6F);
		bodyModel[88].rotateAngleX = -0.76794487F;

		bodyModel[89].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 83
		bodyModel[89].setRotationPoint(-53F, -8F, 9F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 109
		bodyModel[90].setRotationPoint(-53F, -9F, 8F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 110
		bodyModel[91].setRotationPoint(-53F, -9F, 10.75F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, -0.5F, -0.5F, -0.75F); // Box 111
		bodyModel[92].setRotationPoint(-53.5F, -9F, 8.5F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, -0.5F, -0.5F, -0.75F); // Box 112
		bodyModel[93].setRotationPoint(-53.5F, -6.5F, 8.5F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.5F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -1.75F, 0F, -0.5F, -1.75F, 0F, -0.5F, 1.25F, -0.5F, -0.5F, 1.25F); // Box 113
		bodyModel[94].setRotationPoint(-53.5F, -9F, 8.5F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 140
		bodyModel[95].setRotationPoint(49F, -8F, -10F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.5F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -1.75F, 0F, -0.5F, -1.75F, 0F, -0.5F, 1.25F, -0.5F, -0.5F, 1.25F); // Box 141
		bodyModel[96].setRotationPoint(49.5F, -9F, -10.5F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 142
		bodyModel[97].setRotationPoint(50F, -9F, -8.25F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, -0.5F, -0.5F, -0.75F); // Box 143
		bodyModel[98].setRotationPoint(49.5F, -9F, -10.5F);

		bodyModel[99].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F,-0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, -0.5F, -0.5F, -0.75F); // Box 144
		bodyModel[99].setRotationPoint(49.5F, -6.5F, -10.5F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 145
		bodyModel[100].setRotationPoint(50F, -9F, -11F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 120
		bodyModel[101].setRotationPoint(-22.5F, -28F, 9F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 121
		bodyModel[102].setRotationPoint(8.5F, -28F, 9F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 122
		bodyModel[103].setRotationPoint(-7.5F, -28F, 9F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 123
		bodyModel[104].setRotationPoint(39.5F, -28F, 9F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 124
		bodyModel[105].setRotationPoint(25.5F, -28F, 9F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 125
		bodyModel[106].setRotationPoint(39.5F, -28F, -10F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 126
		bodyModel[107].setRotationPoint(25.5F, -28F, -10F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 127
		bodyModel[108].setRotationPoint(8.5F, -28F, -10F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 128
		bodyModel[109].setRotationPoint(-7.5F, -28F, -10F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 129
		bodyModel[110].setRotationPoint(-22.5F, -28F, -10F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 130
		bodyModel[111].setRotationPoint(-38.5F, -28F, -10F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 131
		bodyModel[112].setRotationPoint(-51F, -26F, 9F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 132
		bodyModel[113].setRotationPoint(-51F, -26F, 4F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 133
		bodyModel[114].setRotationPoint(-51F, -26F, -5F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 134
		bodyModel[115].setRotationPoint(-51F, -26F, -10F);

		bodyModel[116].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 135
		bodyModel[116].setRotationPoint(48F, -26F, -10F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 136
		bodyModel[117].setRotationPoint(48F, -26F, -5F);

		bodyModel[118].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 137
		bodyModel[118].setRotationPoint(48F, -26F, 4F);

		bodyModel[119].addShapeBox(0F, 0F, 0F, 1, 20, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 138
		bodyModel[119].setRotationPoint(48F, -26F, 9F);
	}
	private ModelFreightTruckM trucks = new ModelFreightTruckM();

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(int i = 0; i < 120; i++)
		{
			bodyModel[i].render(f5);
		}
		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/freighttruckm.png"));

		GL11.glPushMatrix();
		GL11.glTranslated(-3,-0.43,-0.2);
		trucks.render(entity,f,f1,f2,f3,f4,f5);

		GL11.glTranslated(4.5,0,0.00);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glPopMatrix();
	}

	public float[] getTrans() { return new float[]{-0F, -0.25F, 0F}; }
}