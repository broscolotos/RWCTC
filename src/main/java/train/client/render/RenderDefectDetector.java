package train.client.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.Tessellator;
import train.client.render.models.blocks.ModelDefectDetector;
import train.common.library.Info;
import train.common.tile.TileDefectDetector;

@SideOnly(Side.CLIENT)
    public class RenderDefectDetector extends TileEntitySpecialRenderer {
        private ResourceLocation texture1 = new ResourceLocation(Info.resourceLocation,Info.modelTexPrefix + "defectdetector.png");
        private ModelDefectDetector model = new ModelDefectDetector();

        @Override
        public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float tick) {
            if(!(tileEntity instanceof TileDefectDetector)){return;}
            GL11.glPushMatrix();
            Tessellator.bindTexture(texture1);
            GL11.glTranslated(x + 0.20, y - 1.0, z + 0.45);
            GL11.glRotatef(180F, 1F, 0F, 0F);
            GL11.glRotatef(90F, 0F, 1F, 0F);
            int dir = ((TileDefectDetector)tileEntity).getFacing();
            switch(dir) {
                case 0:{//west // north
                    GL11.glRotated(180,0,1,0);
                    GL11.glTranslated(-0.05,0,-0.3);
                    break;
                }
                case 3: {//south? //west
                    GL11.glRotated(90,0,1,0);
                    GL11.glTranslated(-0.25,0,0.0);
                    break;
                }
                case 1: {//north //east
                    GL11.glRotated(270,0,1,0);
                    GL11.glTranslated(0.25,0,-0.1);
                    break;
                }
                case 2: {//east //south
                    GL11.glRotated(180,0,1,0);
                    GL11.glRotated(180,0,1,0);
                    GL11.glTranslated(0.05,0,0.2);
                    break;
                }
            }
            model.render(null, 0, 0, 0, 0, 0, 0.0625f);
            GL11.glPopMatrix();
        }
    }